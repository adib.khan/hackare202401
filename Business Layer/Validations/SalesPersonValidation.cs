﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/22
Author : Sahil Gupta
Purpose : This  Class is responsible for Sales Person Master Validation
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using GenService.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;

namespace Cact.BL.Validation
{
    public class SalesPersonValidation : GenValidation
    {
        public SalesPersonValidation(DbData dbData) : base(dbData)
        {

        }
        public override int ValidateChild()
        {
            string id, customerId, itemType, itemCategory, itemId, ls_ownAccountId, ls_category;
            bool isUnique = false;
            int count = 0;
            List<string> llst_customer = new List<string>();

            try
            {
                //for sales person
                id = dbData.requestDataset.Tables[0].Rows[0]["id"].ToString().Trim();
                if (string.IsNullOrEmpty(id)) ValidationLog.Add("Header", 1, "Sales Person #", "Sales Person # can not be empty.", "");

                if (dbData.isRecordNew)
                {
                    isUnique = DML.Select("saoimssalesperson", " where id = '" + id + "'", dbConnection);
                    if (!isUnique) ValidationLog.Add("Header", 1, "Sales Person #", "Sales Person # is not unique.", "");
                }

                //for account id
                ls_ownAccountId = dbData.requestDataset.Tables[0].Rows[0]["own_account_id"].ToString().Trim();
                if (!string.IsNullOrWhiteSpace(ls_ownAccountId))
                {
                    isUnique = DML.Select("faarmscustomer", " where id = '" + ls_ownAccountId + "'", dbConnection);
                    if (isUnique) ValidationLog.Add("Header", 1, "Accopunt Id #", "Accopunt Id # is not unique.", "");
                }

                //for category
                ls_category = dbData.requestDataset.Tables[0].Rows[0]["category"].ToString().Trim();
                if (string.IsNullOrWhiteSpace(ls_category)) ValidationLog.Add("Header", 1, "Category #", "Category can not be empty.", "");
                if (ls_category != "NA")
                {
                    isUnique = DML.Select("sapemsspcategory", " where category = '" + ls_category + "'", dbConnection);
                    if (isUnique) ValidationLog.Add("Header", 1, "Category #", "Category # is not unique.", "");
                }

                //for detail
                if (dbData.requestDataset.Tables.Contains("saoimssalespersondtl"))
                    if (dbData.requestDataset.Tables["saoimssalespersondtl"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["saoimssalespersondtl"].Rows)
                        {
                            customerId = dr["customer_id"].ToString().Trim();
                            if (llst_customer.Contains(customerId))
                                ValidationLog.Add("Customer List", 1, "Customer #", "The selected customer already exists in the salesperson's list.", "");
                            else
                            {
                                if (string.IsNullOrEmpty(customerId)) ValidationLog.Add("Header", 1, "Customer #", "Customer # can not be empty in Customer List.", "");
                                isUnique = DML.Select("faarmscustomer", " where id = '" + customerId + "' and trans_flag='A'", dbConnection);
                                if (isUnique) ValidationLog.Add("Customer List", 1, "Customer #", "Customer # is not unique.", "");
                                dr["id"] = id;
                                llst_customer.Add(customerId);
                            }
                        }


                if (dbData.requestDataset.Tables.Contains("sapemsinventory"))
                    if (dbData.requestDataset.Tables["sapemsinventory"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["sapemsinventory"].Rows)
                        {
                            count++;
                            itemType = dr["item_type"].ToString().Trim();
                            itemCategory = dr["item_category"].ToString().Trim();
                            itemId = dr["item_id"].ToString().Trim();

                            if (string.IsNullOrEmpty(itemType)) ValidationLog.Add("Inventory", 1, "Item Type #", "Item Type Can Not be empty at Row :" + count, "");

                            //for item category
                            if (string.IsNullOrEmpty(itemCategory))
                                ValidationLog.Add("Inventory", 1, "Item Categry #", "Item Categry Can Not be empty at Row :" + count, "");
                            isUnique = DML.Select("saoimsitemcategory", " where id = '" + itemCategory + "' and trans_flag='A'", dbConnection);
                            if (isUnique) ValidationLog.Add("Inventory", 1, "Item Categry #", "Item Categry # is not unique.", "");

                            //for item id
                            if (string.IsNullOrEmpty(itemId)) ValidationLog.Add("Inventory", 1, "Item #", "Item # Can Not be empty at Row :" + count, "");
                            isUnique = DML.Select("saoimsitem", " where id = '" + itemId + "' and trans_flag='A'", dbConnection);
                            if (isUnique) ValidationLog.Add("Inventory", 1, "Item Id #", "Item Id # is not unique.", "");

                            validateItem(itemId, count);

                            dr["salesperson_id"] = id;
                            dr["company_id"] = CompanyDbData.companyId.Trim();
                            dr["update_flag"] = 'Y';
                            dr["update_dt"] = DateTime.Today;
                            dr["user_cd"] = CompanyDbData.userCd.Trim();

                        }
            }

            catch (Exception e)
            {
                throw;
            }
            return 0;
        }

        public void validateItem(string itemId, int rowNumber)
        {
            int count;
            var tableFiltered = dbData.requestDataset.Tables["sapemsinventory"].AsEnumerable()
                                    .Where(r => r.Field<string>("item_id") == itemId)
                                    .CopyToDataTable();
            count = tableFiltered.Rows.Count;

            if (count > 1) ValidationLog.Add("Header", 1, "Item Id #", "Duplicate Item Id # at row : " + rowNumber, "");

        }
    }
}

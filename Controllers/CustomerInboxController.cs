﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : transfer Customer from TOV to CRM.
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json;

namespace cact.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class CustomerInboxController : DataSavingController
    {

        [EnableCors]
        [HttpPost]

        public IActionResult ApproveCustomer()
        {
            try
            {
                SetCompany();
                //ValidateSession();
               
                CustomerInboxService customerInboxService = new CustomerInboxService();
                //var responseLog = customerInboxService.ApproveCustomer(requestJson);
                var responseLog = customerInboxService.ApproveCustomer();

                return Ok(responseLog);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}

﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Cact.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivityController : DataSavingController
    {
        public ActivityController()
        {
            service = new ActivityService();
        }

        [EnableCors]
        [HttpPost]
        public IActionResult Save(JsonElement requestJson)
        {
            return Saving(requestJson);
        }

    }
}
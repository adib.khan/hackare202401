﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;
using System.Data;

namespace Cact.Bl.Validation
{
    public class TaskValidation : GenValidation
    {
        public TaskValidation(DbData dbData) : base(dbData) { }

        public override int ValidateChild()
        {
            string prospectId, transBk, transNo, userCd, companyId, subject, dtlContactId, dtlProspectId;
            int max_serial_no;
            DateTime transDate, dueDate;

            try
            {
                prospectId = dbData.requestDataset.Tables["cacttrtask"].Rows[0]["prospect_id"].ToString();
                transNo = dbData.requestDataset.Tables["cacttrtask"].Rows[0]["trans_no"].ToString();
                transBk = dbData.requestDataset.Tables["cacttrtask"].Rows[0]["trans_bk"].ToString();
                transDate = DateTime.Parse((string)dbData.requestDataset.Tables["cacttrtask"].Rows[0]["trans_dt"]);
                companyId = dbData.requestDataset.Tables["cacttrtask"].Rows[0]["company_id"].ToString();
                userCd = dbData.requestDataset.Tables["cacttrtask"].Rows[0]["user_cd"].ToString();
                subject = dbData.requestDataset.Tables["cacttrtask"].Rows[0]["subject"].ToString();
                dueDate = DateTime.Parse((string)dbData.requestDataset.Tables["cacttrtask"].Rows[0]["start_date"]);

                if (string.IsNullOrEmpty(subject))
                    throw new Exception("Task subject can not be empty.");

                if (dueDate == DateTime.MinValue)
                    throw new Exception("Task due date can not be empty.");

                if (string.IsNullOrEmpty(prospectId))
                    throw new Exception("Company # can not be empty.");

                max_serial_no = 100;
                if (dbData.requestDataset.Tables.Contains("cacttrtaskcontacts"))
                {
                    if (dbData.requestDataset.Tables["cacttrtaskcontacts"].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dbData.requestDataset.Tables["cacttrtaskcontacts"].Rows)
                        {
                            dtlContactId = dr["contact_id"].ToString();
                            dtlProspectId = dr["prospect_id"].ToString();

                            if ((string.IsNullOrEmpty(dtlProspectId)) && (string.IsNullOrEmpty(dtlContactId)))
                                throw new Exception("Detail can not be empty.");

                            string WhereClause = " Where id = '" + dtlProspectId + "' and serial_no = '" + dtlContactId + "'";
                            bool isUnique = DML.Select("cactmscontact", WhereClause, dbConnection);

                            if (isUnique)
                            {
                                throw new Exception("Contact info is incorrect, please choose correct detail.");
                            }

                            max_serial_no++;
                            dr["trans_bk"] = transBk;
                            dr["trans_no"] = transNo;
                            dr["trans_dt"] = transDate;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = max_serial_no;
                        }
                    }
                }
                else
                {
                    throw new Exception("Detail can not be empty.");
                }

                max_serial_no = 101;

                if (dbData.requestDataset.Tables["cacttrtaskactions"] != null)
                    foreach (DataRow dr in dbData.requestDataset.Tables["cacttrtaskactions"].Rows)
                    {
                        max_serial_no++;
                        dr["trans_bk"] = transBk;
                        dr["trans_no"] = transNo;
                        dr["trans_dt"] = transDate;
                        dr["company_id"] = companyId;
                        dr["serial_no"] = max_serial_no;
                    }

                dbData.requestDataset.Tables["cacttrtask"].Rows[0]["assigned_by"] = userCd;


            }
            catch (Exception e)
            {
                throw e;
            }
            return 0;
        }
    }
}
/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : Transfer TOV data for customer to CRM Customer.
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.BL.Posting;
using GenService.Db;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Cact.Bl.Services
{
    public class CustomerInboxService
    {
        string diasCustId, errorLogs = "", lsSql, ls_ytdSales, ls_noOfInvoices;
        DataTable selectResult;
        string is_TovtoDiasparkId = ""; //Jay 20230911
        public string ApproveCustomer(/*JsonElement requestJson*/)
        {
            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();

            try
            {

                dbConnection.conn.Open();
                //var dtl = requestJson.EnumerateArray();

                lsSql = "SELECT	ID, tov_customer_id, dias_cust_id, service_rep_cd, service_rep_name, inv_memo_cons, bo_ynd, " + 
                        "customer_category_cd, customer_category_name, credit_limit, credit_status_cd, "+
                        "credit_status_name, statement_frequency_cd, statement_frequency_name, terms_cd, terms_name, " +
                        "web, ship_via_cd, ship_via_name, salesperson_cd_1, salesperson_name_1, salesperson_cd_2, " +
                        "salesperson_name_2, salesperson_cd_3, salesperson_name_3, last_activity, ytd_sales, last_year_sales, " +
                        "sales_2years_ago, sales_3years_ago, ptd_sales, date_created, no_of_invoices, customer_bal, on_order, " +
                        "last_pay_dt, flag_dt, class, return_per, user_cd, update_dt, update_flag, company_id, trans_flag, " +
                        "trfr_flag, customerExist, address1, address2, city, state, zip, country, phone, fax, email, rjo, sjo, cbg, ijo, resale_certificate_no " +
                        "FROM cacttp_tovToDiaspark " +
                        "where trfr_flag = 'U' ";

                selectResult = DML.Select(lsSql, dbConnection);

                foreach (DataRow row in selectResult.Rows)
                {
                    diasCustId = row["dias_cust_id"].ToString();
                    is_TovtoDiasparkId = row["id"].ToString(); //Jay 20230911
                    //}

                    //foreach (var currentRow in dtl)
                    //{

                    errorLogs = "";

                    //All records are independent to each other.
                    sqlTransaction = dbConnection.conn.BeginTransaction();

                    //tov_customer_id = currentRow.GetProperty("tov_customer_id").ToString().Trim();

                    bool isUnique = DML.Select("cactmsaccount", " where id = '" + diasCustId + "'", dbConnection, sqlTransaction);

                    if (isUnique)
                    {
                        continue;   //skip
                    }

                    isUnique = DML.Select("faarmscustomer", " where id = '" + diasCustId + "'", dbConnection, sqlTransaction);

                    if (!isUnique)  //Already exist
                    {

                        //1. Validate TOV data compatablity for Diaspark Customer.
                        validateTOVData(diasCustId, dbConnection, sqlTransaction);

                        //2. If customer already exist then just update data from TOV.
                        UpdateCustomerFromTOV(diasCustId, dbConnection, sqlTransaction);

                        //3. Mark as posted for TOV record in staging table.
                        DML.Update(MarkAsPosted(diasCustId), dbConnection, sqlTransaction);

                        //4. SyncToProspect: Sync data from Customer to Prospect.
                        new ProspectCustomerSync().SyncToProspect(diasCustId, dbConnection, sqlTransaction);

                    }
                    else    //New Customer Creation
                    {

                        //Jay 20220727 :Start

                        //No need to create Customer if there is no sales against it.
                        ls_ytdSales = row["ytd_sales"].ToString() ?? "0";
                        ls_noOfInvoices = row["no_of_invoices"].ToString() ?? "0";

                        if (ls_noOfInvoices.ToUpper().Trim() == "0" || ls_ytdSales.ToUpper().Trim() == "0")
                        {
                            //Jay 20230911 : comment
                            //if (sqlTransaction != null) //Jay 20220830 
                            //    sqlTransaction.Commit();

                            //continue; 

                            DML.Update(MarkAsPosted(diasCustId), dbConnection, sqlTransaction); //Jay 2020911
                        }

                        //Jay 20220727 : End

                        else //Jay 20230911 :add else condition
                        {
                            //1. Validate Prospect data before to post to customer.
                            new ProspectCustomerSync().ValidateProspect(diasCustId, dbConnection, sqlTransaction);

                            //2. Copy data from prospect to customer.
                            new ProspectCustomerSync().CreateCustomer(diasCustId, dbConnection, sqlTransaction);

                            //3. Validate TOV data compatablity for Diaspark Customer.
                            validateTOVData(diasCustId, dbConnection, sqlTransaction);

                            //4. Copy data from TOV data staging to Diaspark CRM customer master.
                            UpdateCustomerFromTOV(diasCustId, dbConnection, sqlTransaction);

                            //4. Mark as posted for TOV record in staging table.
                            DML.Update(MarkAsPosted(diasCustId), dbConnection, sqlTransaction);

                            //5. SyncToProspect: Sync data from Customer to Prospect.
                            new ProspectCustomerSync().SyncToProspect(diasCustId, dbConnection, sqlTransaction);

                            //Jay 20231017 :Start
                            string sql;
                            sql = "UPDATE B" +
                                " Set current_stage = 'CONVERTED' " +
                                " From cactmsaccount B" +
                                " WHERE b.id = '" + diasCustId + "';";
                            DML.Update(sql, dbConnection, sqlTransaction);
                            //Jay 20231017 : End
                        }
                    }
                    //Each record in independent to be posted to CRM.
                    sqlTransaction.Commit();
                }

               return errorLogs;
            }
            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                if (string.IsNullOrEmpty(errorLogs)) //Jay202208330 : Add condition
                {
                    throw new Exception(e.Message);
                }
                else
                {
                    throw new Exception(errorLogs);
                }
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        public void validateTOVData(string diasCustId, DbConnection dbConnection, SqlTransaction sqlTransaction)
        {
            DataTable selectResult;

            string lsSql = "";

            try
            {
                lsSql = "SELECT inv_memo_cons, bo_ynd, customer_category_cd, credit_status_cd, statement_frequency_cd, " +
                    "terms_cd, ship_via_cd, salesperson_cd_1, salesperson_cd_2, salesperson_cd_3, " +
                    "last_activity, date_created, last_pay_dt, flag_dt " +
                    "from cacttp_tovToDiaspark " +
                    "where trfr_flag = 'U' AND dias_cust_id = '" + diasCustId + "'";

                selectResult = DML.Select(lsSql, dbConnection, sqlTransaction);

                //Jay 20230911
                if (selectResult.Rows.Count ==0 )
                {
                    return;
                }
                string inv_memo_cons = selectResult.Rows[0]["inv_memo_cons"].ToString().Trim();
                string bo_ynd = selectResult.Rows[0]["bo_ynd"].ToString().Trim();
                string customer_category_cd = selectResult.Rows[0]["customer_category_cd"].ToString().Trim();
                string credit_status_cd = selectResult.Rows[0]["credit_status_cd"].ToString().Trim();
                string statement_frequency_cd = selectResult.Rows[0]["statement_frequency_cd"].ToString().Trim();
                string terms_cd = selectResult.Rows[0]["terms_cd"].ToString().Trim();
                string ship_via_cd = selectResult.Rows[0]["ship_via_cd"].ToString().Trim();
                string salesperson_cd_1 = selectResult.Rows[0]["salesperson_cd_1"].ToString().Trim();
                string salesperson_cd_2 = selectResult.Rows[0]["salesperson_cd_2"].ToString().Trim();
                string salesperson_cd_3 = selectResult.Rows[0]["salesperson_cd_3"].ToString().Trim();
                DateTime last_activity = (DateTime)selectResult.Rows[0]["last_activity"];
                //DateTime date_created = (DateTime)selectResult.Rows[0]["date_created"];
                DateTime last_pay_dt = (DateTime)selectResult.Rows[0]["last_pay_dt"];

                if (!(inv_memo_cons == "I" || inv_memo_cons == "M" || inv_memo_cons == "S"))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {invoice/memo/cons} value. : "+ inv_memo_cons;
                }

                if (!(bo_ynd == "Y" || bo_ynd == "N" || bo_ynd == "D"))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {BO Y/N/D} value. : "+ bo_ynd;
                }

                bool isUnique = DML.Select("faarmscustomercategory", " where category = '" + customer_category_cd + "'", dbConnection, sqlTransaction);
                if (isUnique)
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {category} value. : "+ customer_category_cd;
                }

                isUnique = DML.Select("cactmssetupdtl", " WHERE setup_id = 'CREDIT_STATUS' AND trans_flag = 'A' AND RTRIM(id) = '" + credit_status_cd + "'", dbConnection, sqlTransaction);
                if (isUnique && !string.IsNullOrEmpty(credit_status_cd))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Credit Status} value. : "+credit_status_cd;
                }

                isUnique = DML.Select("cactmssetupdtl", " WHERE setup_id = 'STAT_FREQUENCY' AND trans_flag = 'A' AND RTRIM(id) = '" + statement_frequency_cd + "'", dbConnection, sqlTransaction);
                if (isUnique && !string.IsNullOrEmpty(statement_frequency_cd))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Statement Frequency} value. : "+ statement_frequency_cd;
                }

                isUnique = DML.Select("saoimsterms", " WHERE trans_flag = 'A' AND RTRIM(id) = '" + terms_cd + "'", dbConnection, sqlTransaction);
                if (isUnique && !string.IsNullOrEmpty(terms_cd))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Sales Terms} value. : "+ terms_cd;
                }

                isUnique = DML.Select("saoimsshipvia", " WHERE trans_flag = 'A' AND RTRIM(shipvia_id) = '" + ship_via_cd + "'", dbConnection, sqlTransaction);
                if (isUnique && !string.IsNullOrEmpty(ship_via_cd))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Ship Via} value. : "+ ship_via_cd;
                }

                isUnique = DML.Select("saoimssalesperson", " WHERE trans_flag = 'A' AND RTRIM(id) = '" + salesperson_cd_1 + "'", dbConnection, sqlTransaction);
                if (isUnique && !string.IsNullOrEmpty(salesperson_cd_1))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Sales person 1} value. : "+salesperson_cd_1;
                }

                isUnique = DML.Select("saoimssalesperson", " WHERE trans_flag = 'A' AND RTRIM(id) = '" + salesperson_cd_2 + "'", dbConnection, sqlTransaction);
                if (isUnique && !string.IsNullOrEmpty(salesperson_cd_2))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Sales person 2} value. : "+ salesperson_cd_2;
                }

                isUnique = DML.Select("saoimssalesperson", " WHERE trans_flag = 'A' AND RTRIM(id) = '" + salesperson_cd_3 + "'", dbConnection, sqlTransaction);
                if (isUnique && !string.IsNullOrEmpty(salesperson_cd_3))
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Sales person 3} value. "+ salesperson_cd_3;
                }

                if (last_activity > DateTime.Now)
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Last activity date} value. : "+ last_activity.ToString();
                }

                //if (date_created > DateTime.Now)
                //{
                //    errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Date Created} value. :"+ date_created.ToString();
                //}

                if (last_pay_dt > DateTime.Now)
                {
                    //errorLogs += "\n\n {" + tov_customer_id + "} Customer : incorrect {Last pay date} value. "+ last_pay_dt.ToString();
                }

                if (!string.IsNullOrEmpty(errorLogs)) //Test Commetend
                {
                    throw new Exception(errorLogs);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void UpdateCustomerFromTOV(string tov_customer_id, DbConnection dbConnection, SqlTransaction sqlTransaction)
        {
            string sql= "";
            try
            {
                sql = "UPDATE B " +
                    "SET B.service_rep = A.service_rep_name," +
                        "B.inv_memo_cons = A.inv_memo_cons," +
                        "B.back_order = A.bo_ynd," +
                        "B.category = A.customer_category_cd," +
                        "B.credit_limit = (CASE WHEN CHARINDEX('-', A.credit_limit) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(A.credit_limit)), 1, LEN(LTRIM(A.credit_limit))-1) ELSE RTRIM(LTRIM(A.credit_limit)) END)," +
                        "B.credit_status = A.credit_status_cd," +
                        "B.statement_frequency = A.statement_frequency_cd," +
                        "B.terms = A.terms_cd, " +
                        "B.memo_terms = A.terms_cd, " +
                        "B.repair_terms = A.terms_cd," +
                        "B.web = A.web," +
                        "B.ship_via = A.ship_via_cd," +
                        "B.sales_person = A.salesperson_cd_1," +
                        "B.tov_customer_id = A.tov_customer_id," +
                        "B.price_level = A.class, " +
                        "B.address1 = A.address1," +
                        "B.address2 = A.address2," +
                        "B.city = A.city," +
                        "B.state = A.state," +
                        "B.zip = A.zip," +
                        "B.country = A.country," +
                        "B.business_email = A.email," +
                        "B.fax1 = A.fax," +
                        "B.resale_certificate_no = A.resale_certificate_no," +
                        "B.cell_no = A.phone " +
                    "FROM cacttp_tovToDiaspark A " +
                    "JOIN faarmscustomer B ON " +
                    "A.dias_cust_id = B.id " +
                    "WHERE A.trfr_flag = 'U' AND A.dias_cust_id = '" + diasCustId + "';";

                DML.Update(sql, dbConnection, sqlTransaction);
                 
                //Jay 20220817 : Add 'C.' for getting tov_customer_id to remove Ambiguous column name 'tov_customer_id'
                sql = "UPDATE A " +
                    "SET A.last_activity = C.last_activity," +
                    "A.ytd_sales = CASE WHEN CHARINDEX('-', C.ytd_sales) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.ytd_sales)), 1, LEN(LTRIM(C.ytd_sales))-1) ELSE RTRIM(LTRIM(C.ytd_sales)) END," +
                    "A.last_year_sales = CASE WHEN CHARINDEX('-', C.last_year_sales) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.last_year_sales)), 1, LEN(LTRIM(C.last_year_sales))-1) ELSE RTRIM(LTRIM(C.last_year_sales)) END," +
                    "A.sales_2years_ago = CASE WHEN CHARINDEX('-', C.sales_2years_ago) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.sales_2years_ago)), 1, LEN(LTRIM(C.sales_2years_ago))-1) ELSE RTRIM(LTRIM(C.sales_2years_ago)) END," +
                    "A.sales_3years_ago = CASE WHEN CHARINDEX('-', C.sales_3years_ago) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.sales_3years_ago)), 1, LEN(LTRIM(C.sales_3years_ago))-1) ELSE RTRIM(LTRIM(C.sales_3years_ago)) END," +
                    "A.ptd_sales = CASE WHEN CHARINDEX('-', C.ptd_sales) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.ptd_sales)), 1, LEN(LTRIM(C.ptd_sales))-1) ELSE RTRIM(LTRIM(C.ptd_sales)) END," +
                    "A.no_of_invoices = CASE WHEN CHARINDEX('-', C.no_of_invoices) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.no_of_invoices)), 1, LEN(LTRIM(C.no_of_invoices))-1) ELSE RTRIM(LTRIM(C.no_of_invoices)) END," +
                    "A.customer_bal = CASE WHEN CHARINDEX('-', C.customer_bal) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.customer_bal)), 1, LEN(LTRIM(C.customer_bal))-1) ELSE RTRIM(LTRIM(C.customer_bal)) END," +
                    "A.on_order = CASE WHEN CHARINDEX('-', C.on_order) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.on_order)), 1, LEN(LTRIM(C.on_order))-1) ELSE RTRIM(LTRIM(C.on_order)) END," +
                    "A.last_pay_dt = C.last_pay_dt," +
                    "A.return_per = CASE WHEN CHARINDEX('-', C.return_per) > 1 THEN '-'+SUBSTRING(RTRIM(LTRIM(C.return_per)), 1, LEN(LTRIM(C.return_per))-1) ELSE RTRIM(LTRIM(C.return_per)) END, " +
                    "A.bg_rjo = isnull(C.rjo,'N')," +
                    "A.bg_sjo = isnull(C.sjo,'N')," +
                    "A.bg_ijo = isnull(C.ijo,'N')," +
                    "A.bg_cbg = isnull(C.cbg,'N'), " +
                    "A.bill_via_sjo = case when SUBSTRING(C.tov_customer_id,1,3)='SJO' then 'Y' else 'N' end, " +
                    //"A.billing_via_rjo_sjo = case when SUBSTRING(C.tov_customer_id,1,3)='RJO' then 'Y' else 'N' end " +
                    "A.billing_via_rjo_sjo = case when SUBSTRING(C.tov_customer_id,1,3)='RJO' then 'R' when SUBSTRING(C.tov_customer_id,1,3)='SJO' then 'S' else 'N' end " +
                    "FROM cactmsaccountsummary A " +
                    "JOIN faarmscustomer B ON " +
                        "A.account_id = B.ID " +
                    "JOIN cacttp_tovToDiaspark C ON " +
                        "B.ID = C.dias_cust_id " +
                    "WHERE C.trfr_flag = 'U' AND C.dias_cust_id = '" + diasCustId + "';";

                DML.Update(sql, dbConnection, sqlTransaction);
            }

            catch(Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public string MarkAsPosted(string tov_customer_id)
        {

         //Jay 20230911 :post on the basis of id instead of dias id;
        string sql = "UPDATE cacttp_tovToDiaspark " +
            "SET trfr_flag = 'P' " +
            "where trfr_flag = 'U' AND id = '" + is_TovtoDiasparkId
 + "';";
            return sql;
        }        
    }
}

﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Validation;
using GenService.BL.Services;
using GenService.Db;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;


namespace Cact.Bl.Services
{
    public class CampaignService : DataSavingService
    {
        public CampaignService() : base()
        {
            validation = new CampaignValidation(dbData);
            dbData.bookType = "CACTCAM";
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("cacttrcampaignhd", "dwc_master", true, false, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
            dbData.updateProperties[1] = new UpdateProperty("cacttrcampaigndtl", "dwc_detail", true, true, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
            dbData.updateProperties[2] = new UpdateProperty("cactcampaigndocs", "dwc_detaildoc", true, false, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
        }

        public override void PostSaveChild()
        {
            string transNo, ls_sql;
            int result;

            transNo = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["trans_no"].ToString();


            if (int.TryParse(transNo, out result))  //Check is is a pure numeric or not
            {
                //ok, do something
            }
            else
            {
                transNo = transNo.Remove(transNo.Length - 1);

                ls_sql = "UPDATE gnmsbook" +
                        " SET book_lno = '" + transNo + "'" +
                        " WHERE docu_typ = 'CACTCAM'; ";

                DML.Update(ls_sql, dbConnection, trans);
            }
        }


        public void AddRemoveCampaign(JsonElement requestJson)
        {
            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();

            try
            {
                string lsSql, transBk, transNo, selectFlag, prospectId, WhereClause;
                string companyId, gUserCd;
                DateTime transDt;
                bool isUnique;
                var dtl = requestJson.EnumerateArray();

                if (dtl.Count() == 0)
                {
                    throw new Exception("Empty Campaign list ..");
                }

                dbConnection.conn.Open();
                sqlTransaction = dbConnection.conn.BeginTransaction();

                foreach (var currentRow in dtl)
                {
                    transNo = currentRow.GetProperty("trans_no").ToString().Trim();
                    transBk = currentRow.GetProperty("trans_bk").ToString().Trim();
                    transDt = Convert.ToDateTime(currentRow.GetProperty("trans_dt").ToString());
                    selectFlag = currentRow.GetProperty("select_flag").ToString().Trim();
                    prospectId = currentRow.GetProperty("prospect_id").ToString().Trim();
                    companyId = currentRow.GetProperty("company_id").ToString().Trim();
                    gUserCd = currentRow.GetProperty("user_cd").ToString().Trim();

                    if (selectFlag == "Y")
                    {
                        WhereClause = " Where trans_bk = '" + transBk + "' and trans_no = '" + transNo + "' and trans_dt = '" + transDt + "' and account_id = '" + prospectId + "' ";
                        isUnique = DML.Select("cacttrcampaigndtl", WhereClause, dbConnection, sqlTransaction);

                        if (isUnique)
                        {
                            lsSql = GetCampaignDtl(prospectId, transNo, transBk, transDt, companyId, gUserCd, dbConnection, sqlTransaction);
                            DML.Insert(lsSql, dbConnection, sqlTransaction);
                        }
                    }
                    else
                    {
                        lsSql = "Delete from cacttrcampaigndtl " +
                        "where trans_no = '" + transNo + "' and " +
                        "trans_bk = '" + transBk + "'and " +
                        "trans_dt = '" + transDt + "' and " +
                        "account_id = '" + prospectId + "'";
                        DML.Delete(lsSql, dbConnection, sqlTransaction);
                    }
                }

                sqlTransaction.Commit();
            }

            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                throw e;
            }

            finally
            {
                dbConnection.conn.Close();
            }
        }


        public string GetCampaignDtl(string prospectId, string transNo, string transBk, DateTime transDt, string companyId, string gUserCd, DbConnection dbConnection, SqlTransaction sqlTransaction)
        {

            string categoryId, companyName, contact1, contact2, address1, address2, city, state, zip, country, customerId;
            string phone, fax, email, salespersonCd, shipVia;
            int maxSerialNo = 0;
            float creditLimit = 0;
            DataTable selectResult;
            string lsSql;
            bool isUnique;

            lsSql = "Select category_id, company_name, contact1, contact2," +
                            "address1, address2, city, state, " +
                            "zip, country, phone, fax, " +
                            "email, credit_limit, salesperson_cd, ship_via " +
                            "from cactmsaccount where id = '" + prospectId + "'";
            selectResult = DML.Select(lsSql, dbConnection, sqlTransaction);

            categoryId = selectResult.Rows[0]["category_id"].ToString().Trim();
            companyName = selectResult.Rows[0]["company_name"].ToString().Trim();
            contact1 = selectResult.Rows[0]["contact1"].ToString().Trim();
            contact2 = selectResult.Rows[0]["contact2"].ToString().Trim();
            address1 = selectResult.Rows[0]["address1"].ToString().Trim();
            address2 = selectResult.Rows[0]["address2"].ToString().Trim();
            city = selectResult.Rows[0]["city"].ToString().Trim();
            state = selectResult.Rows[0]["state"].ToString().Trim();
            zip = selectResult.Rows[0]["zip"].ToString().Trim();
            country = selectResult.Rows[0]["country"].ToString().Trim();
            phone = selectResult.Rows[0]["phone"].ToString().Trim();
            fax = selectResult.Rows[0]["fax"].ToString().Trim();
            email = selectResult.Rows[0]["email"].ToString().Trim();
            salespersonCd = selectResult.Rows[0]["salesperson_cd"].ToString().Trim();
            shipVia = selectResult.Rows[0]["ship_via"].ToString().Trim();

            if (selectResult.Rows[0]["credit_limit"] != null && selectResult.Rows[0]["credit_limit"] != DBNull.Value)
            {
                //creditLimit = float.Parse((string)selectResult.Rows[0]["credit_limit"]);
                creditLimit = float.Parse(selectResult.Rows[0]["credit_limit"].ToString());
            }

            lsSql = "Select ISNULL(max(serial_no),0)  as max_Serial_no " +
                        "from cacttrcampaigndtl " +
                    "where trans_no = '" + transNo + "' and " +
                    "trans_bk = '" + transBk + "'and " +
                    "trans_dt = '" + transDt + "'";
            selectResult = DML.Select(lsSql, dbConnection, sqlTransaction);


            if (selectResult.Rows.Count > 0)
            {
                maxSerialNo = int.Parse(selectResult.Rows[0]["max_serial_no"].ToString());
            }

            if (maxSerialNo == 0)
            {
                maxSerialNo = 101;
            }
            else
            {
                maxSerialNo++;
            }

            isUnique = DML.Select("faarmscustomer", " where id = '" + prospectId + "'", dbConnection, sqlTransaction);
            if (isUnique)
            {
                customerId = null;
            }
            else
            {
                customerId = prospectId;
            }

            lsSql = "INSERT INTO cacttrcampaigndtl" +
                   "(" +
                       "company_id, trans_bk, trans_no, trans_dt, serial_no, " +
                       "user_cd, update_dt, update_flag, trans_flag, id, " +
                       "category, name, contact1, contact2, address1, " +
                       "address2, city, state, zip, country, phone1, " +
                       "phone2, fax1, email, credit_limit, sales_person, " +
                       "ship_via, account_id, tracking_no, package_wt, ship_id" +
                   ")" +
                   "VALUES" +
                   "(" +
                       "'" + companyId + "','" + transBk + "','" + transNo + "','" + transDt + "','" + Convert.ToString(maxSerialNo) + "','" +
                        gUserCd + "','" + DateTime.Now + "','Y','A','" + customerId + "','" +
                       categoryId + "','" + companyName + "','" + contact1 + "','" + contact2 + "','" + address1 + "','" +
                       address2 + "','" + city + "','" + state + "','" + zip + "','" + country + "','" + phone + "'," +
                       "NULL,'" + fax + "','" + email + "','" + creditLimit + "','" + salespersonCd + "','" +
                       shipVia + "','" + prospectId + "',NULL,NULL,NULL" +
                    "); ";

            return lsSql;

        }

    }
}

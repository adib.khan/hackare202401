﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/08/09
Author : Akshay Malviya
Purpose : Prepare Customer List Service
***********************************************************************************/
using Cact.BL.Posting;
using GenService.BL.Services;
using GenService.Db;
using System;
using System.Text.Json;

namespace Cact.BL.Services
{ 
    public class PrepareCustomerListService : DataSavingService
    {
        public PrepareCustomerListService() : base() { }
        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("cacttrcustomerlist", "dwc_list", true, false, "trans_bk={trans_bk} and trans_no={trans_no} and trans_dt={trans_dt} and serial_no={serial_no}");
        }
        public override void PostSaveChild()
        {
            PrepareCustomerListPosting prepareCustomerListPosting = new PrepareCustomerListPosting();
            prepareCustomerListPosting.postSavePrepareCustomerList(dbConnection, dbData, trans);
        }
        public void deleteCustomerList(JsonElement requestJson)
        {
            DbConnection dbConnection;
            dbConnection = new DbConnection();
            dbConnection.conn.Open();
            string deleteType = requestJson.GetProperty("delete_type").ToString().Trim();
            if (deleteType == "A")
            {
                try
                {
                    string listName = requestJson.GetProperty("list_name").ToString().Trim();
                    string sql = "UPDATE cacttrcustomerlist SET trans_flag = 'D' WHERE list_name = '" + listName + "';";
                    DML.Update(sql, dbConnection);
                    dbConnection.conn.Close();
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else if (deleteType == "B")
            {
                //try
                //{
                //    string listName = requestJson.GetProperty("list_name").ToString().Trim();
                //    string sql = "DELET FROM cacttrcustomerlist SET trans_flag = 'D' WHERE list_name = '" + listName + "';";
                //    DML.Update(sql, dbConnection);
                //    dbConnection.conn.Close();
                //}
                //catch (Exception e)
                //{
                //    throw e;
                //}
            }
        }
    }
}

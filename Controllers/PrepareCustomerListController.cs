﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/08/09
Author : Akshay Malviya
Purpose : Prepare Customer List Controller
***********************************************************************************/
using Gen.Controllers;
using Microsoft.AspNetCore.Mvc;
using Cact.BL.Services;
using System.Text.Json;
using System;
using Microsoft.AspNetCore.Cors;

namespace Cact.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class PrepareCustomerListController : DataSavingController
    {
        public PrepareCustomerListController()
        {
            service = new PrepareCustomerListService();
        }

        [EnableCors]
        [HttpPost]
        public IActionResult Create(JsonElement requestJson) 
        {
            try
            {
                return Saving(requestJson);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [EnableCors]
        [HttpPost]
        public IActionResult Delete(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                ValidateSession();
                PrepareCustomerListService prepareCustomerListService = (PrepareCustomerListService)service;
                prepareCustomerListService.deleteCustomerList(requestJson);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
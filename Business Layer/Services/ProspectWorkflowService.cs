﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/10/05
Author : Sunil Agrawal
Purpose : Prospect Workflow Service
***********************************************************************************/
using GenService.Db;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.Json;

namespace Cact.BL.Services
{
    public class ProspectWorkflowService
    {
        public void updateStage(JsonElement requestJson)
        {
            string accountId = requestJson.GetProperty("account_id").GetString();
            string newStage = requestJson.GetProperty("stage_name").GetString();

            bool changeProspectStage = false, changeCreditApprovalStage = false;
            string sql, transferredToTov, lsSQL, oldStage, prospectStage ="", billByRjoSjo;
            DataTable selectResult;
            decimal oldSeqNo = 0, newSeqNo = 0;

            if (newStage == "CONVERTED")
            {
                throw new Exception("Prospect can not be transferred to CONVERETED stage directly by user action.");
            }

            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();

            dbConnection.conn.Open();
            sqlTransaction = dbConnection.conn.BeginTransaction();

            switch (newStage)
            {
                //Prospect Current Stages
                case "NEW":
                    changeProspectStage = true;
                    break;
                case "CONTACTED":
                    changeProspectStage = true;
                    break;
                case "CREDIT_APP":
                    changeProspectStage = true;
                    break;
                case "OPPORTUNITY":

                    lsSQL = "SELECT billing_via_rjo_sjo = ISNULL(billing_via_rjo_sjo,'') FROM cactmsaccountsummary WHERE account_id = '" + accountId + "'";

                    selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);
                    
                    if (selectResult.Rows.Count > 0)
                    {
                        billByRjoSjo = selectResult.Rows[0]["billing_via_rjo_sjo"].ToString().Trim();
                        if (string.IsNullOrEmpty(billByRjoSjo))
                            throw new Exception("Prospect's profile: Bill by field can not be empty.");
                    }
                    else
                        throw new Exception("Prospect's profile: Bill by field can not be empty.");

                    changeProspectStage = true;
                    changeCreditApprovalStage = true;
                    break;

                //Credit Approval Stages
                case "CARECEIVED":
                    changeCreditApprovalStage = true;
                    break;
                case "INREVIEW":
                    changeCreditApprovalStage = true;
                    break;
                case "APPROVAL":
                    changeCreditApprovalStage = true;
                    break;
                case "APPROVED":

                    lsSQL = "SELECT billing_via_rjo_sjo = ISNULL(billing_via_rjo_sjo,'') FROM cactmsaccountsummary WHERE account_id = '" + accountId + "'";

                    selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);

                    if (selectResult.Rows.Count > 0)
                    {
                        billByRjoSjo = selectResult.Rows[0]["billing_via_rjo_sjo"].ToString().Trim();
                        if (string.IsNullOrEmpty(billByRjoSjo))
                            throw new Exception("Prospect's profile: Bill by field can not be empty.");
                    }
                    else
                        throw new Exception("Prospect's profile: Bill by field can not be empty.");

                    changeCreditApprovalStage = true;
                    changeProspectStage = true;
                    break;
                default:
                    break;
            }

            try
            { 

                sql = "SELECT transferred_to_tov FROM cactmsaccount WHERE id='" + accountId + "'";

                selectResult = DML.Select(sql, dbConnection, sqlTransaction);
                transferredToTov = selectResult.Rows[0]["transferred_to_tov"].ToString().Trim();

                if (transferredToTov == "Y")
                    throw new Exception("Account " + accountId + " is already transferred to TOV so no meaning to change the stage...");

                //Prospect current stage change valiadtion
                if (changeProspectStage == true && changeCreditApprovalStage == false)
                {
                    lsSQL = "SELECT sequence_no FROM cactmsstagedtl WHERE stage_id = 'PROSPECT_STAGE' AND substage_id = '" + newStage + "'";
                    selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);

                    if (selectResult.Rows.Count > 0)
                    {
                        newSeqNo = (decimal)selectResult.Rows[0]["sequence_no"];
                    }

                    lsSQL = "SELECT current_stage FROM cactmsaccount where id = '" + accountId + "'";
                    selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);

                    if (selectResult.Rows.Count > 0)
                    {
                        oldStage = selectResult.Rows[0]["current_stage"].ToString().Trim();

                        if (oldStage == "CONVERTED")
                            throw new Exception("Prospect's current stage is already CONVERTED so no meaning to transfer further.");

                        lsSQL = "SELECT sequence_no FROM cactmsstagedtl WHERE stage_id = 'PROSPECT_STAGE' AND substage_id = '" + oldStage + "'";
                        selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);
                        oldSeqNo = (decimal)selectResult.Rows[0]["sequence_no"];

                        if (oldSeqNo > newSeqNo)
                            throw new Exception("Prospect's current stage can not bet set to older stage");
                        else if (newSeqNo - oldSeqNo != 10)
                            throw new Exception("Prospect's current stage can be changed to just next stage only.");
                    }
                }

                //Credit Approval stage change valiadtion
                else if (changeProspectStage == false && changeCreditApprovalStage == true)
                {
                    lsSQL = "SELECT sequence_no FROM cactmsstagedtl WHERE stage_id = 'CREDIT_APPROVAL' AND substage_id = '" + newStage + "'";
                    selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);

                    if (selectResult.Rows.Count > 0)
                    {
                        newSeqNo = (decimal)selectResult.Rows[0]["sequence_no"];
                    }

                    lsSQL = "SELECT credit_approval_stage FROM cactmsaccount where id = '" + accountId + "'";
                    selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);

                    if (selectResult.Rows.Count > 0)
                    {
                        oldStage = selectResult.Rows[0]["credit_approval_stage"].ToString().Trim();

                        lsSQL = "SELECT sequence_no FROM cactmsstagedtl WHERE stage_id = 'CREDIT_APPROVAL' AND substage_id = '" + oldStage + "'";
                        selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);
                        if (selectResult.Rows.Count > 0)
                            oldSeqNo = (decimal)selectResult.Rows[0]["sequence_no"];

                        if (oldSeqNo > newSeqNo)
                            throw new Exception("Prospect's credit approval stage can not bet set to older stage");
                        else if (newSeqNo - oldSeqNo != 10)
                            throw new Exception("Prospect's credit approval stage can be changed to just next stage only.");
                    }

                    lsSQL = "SELECT current_stage FROM cactmsaccount where id = '" + accountId + "'";
                    selectResult = DML.Select(lsSQL, dbConnection, sqlTransaction);
                    if (selectResult.Rows.Count > 0)
                    {
                        prospectStage = selectResult.Rows[0]["current_stage"].ToString().Trim();
                    }
                     
                    if (newStage == "CARECEIVED" && prospectStage != "CONTACTED")
                        throw new Exception("Invalid operation: firstly change prospect current stage to CONTACTED stage.");
                    else if (prospectStage != "CONTACTED" && prospectStage != "CREDIT_APP")
                        throw new Exception("Invalid operation: firstly change prospect current stage to Credit Approval.");
                }

                if (newStage == "OPPORTUNITY" || newStage == "APPROVED")
                    sql = "UPDATE cactmsaccount SET current_stage ='OPPORTUNITY', credit_approval_stage = 'APPROVED' WHERE id='" + accountId + "'";
                else if (newStage == "NEW" || newStage == "CONTACTED")
                    sql = "UPDATE cactmsaccount SET current_stage ='"+ newStage +"', credit_approval_stage = '' WHERE id='" + accountId + "'";
                else if (newStage == "APPROVAL" || newStage == "APPROVED" || newStage == "CARECEIVED" || newStage == "INREVIEW")
                    sql = "UPDATE cactmsaccount SET current_stage ='CREDIT_APP', credit_approval_stage = '"+ newStage +"' WHERE id='" + accountId + "'";
                else if (changeProspectStage == true)
                    sql = "UPDATE cactmsaccount SET current_stage ='" + newStage + "' WHERE id='" + accountId + "'";
                else if (changeCreditApprovalStage == true)
                    sql = "UPDATE cactmsaccount SET credit_approval_stage ='" + newStage + "' WHERE id='" + accountId + "'";
                else
                    return;

                DML.Update(sql, dbConnection, sqlTransaction);

                sqlTransaction.Commit();

                dbConnection.conn.Close();
            }

            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                throw e;
            }

            finally
            {
                dbConnection.conn.Close();
            }
        }
    }
}

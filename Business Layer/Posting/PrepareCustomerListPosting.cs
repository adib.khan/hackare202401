﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/08/09
Author : Akshay Malviya
Purpose : Prepare Customer List Posting
***********************************************************************************/
using GenService.Db;
using System.Data.SqlClient;

namespace Cact.BL.Posting
{
    public class PrepareCustomerListPosting
    {
        public PrepareCustomerListPosting() { }
        public void postSavePrepareCustomerList(DbConnection dbConnection, DbData dbData, SqlTransaction sqlTransaction)
        {
            string transNo = dbData.requestDataset.Tables["cacttrcustomerlist"].Rows[0]["trans_no"].ToString();
            string sql = "update gnmsbook with (HOLDlock, rowlock) set book_lno = '"+ transNo + "', lno_date = GETDATE() " +
                         " where book_cd = 'LC01' and company_id = '"+CompanyDbData.companyId+"' and docu_typ = 'LC01'; ";
            DML.Update(sql, dbConnection,sqlTransaction);
        }
    }
}

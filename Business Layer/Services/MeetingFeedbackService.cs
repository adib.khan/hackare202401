﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Validation;
using GenService.BL.Services;
using GenService.Db;
using System;
using System.Data.SqlClient;
using System.Text.Json;

namespace Cact.Bl.Services
{
    public class MeetingFeedbackService : DataSavingService
    {
        public MeetingFeedbackService() : base()
        {
            validation = new MeetingFeedbackValidation(dbData);
            dbData.bookType = "CMMET";
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("cacttrmeeting", "dwc_master", true, false, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
            dbData.updateProperties[1] = new UpdateProperty("cacttrmeetingcontacts", "dwc_detail", true, true, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
        }

        public void CreateActivity(JsonElement requestJson)
        {
            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();

            try
            {
                string prospectId, contactId, taskType, salespPersonCd, timeZone, timeFormat;
                string remarks, lsSql, companyId, feedBackNo, newActivityTransNo, taskSubject, gUserCd, feedBackBk;
                int hour, minute;
                DateTime feedBackDt;

                newActivityTransNo = requestJson.GetProperty("activity_trans_no").ToString().Trim();
                prospectId = requestJson.GetProperty("prospect_id").ToString().Trim();
                contactId = requestJson.GetProperty("contact_id").ToString().Trim();
                taskType = requestJson.GetProperty("task_type").ToString().Trim();
                salespPersonCd = requestJson.GetProperty("salesperson_cd").ToString().Trim();
                gUserCd = requestJson.GetProperty("user_cd").ToString().Trim();

                timeZone = requestJson.GetProperty("time_zone").ToString().Trim();
                timeFormat = requestJson.GetProperty("time_format").ToString().Trim();
                hour = int.Parse(requestJson.GetProperty("hour").ToString().Trim());
                minute = int.Parse(requestJson.GetProperty("minute").ToString().Trim());

                feedBackNo = requestJson.GetProperty("trans_no").ToString().Trim();
                feedBackDt = Convert.ToDateTime(requestJson.GetProperty("trans_dt").ToString());
                companyId = requestJson.GetProperty("company_id").ToString().Trim();
                feedBackBk = "CMME";
                taskSubject = "Created from Meeting Feedback";
                remarks = "Created from Meeting Feedback...";

                dbConnection.conn.Open();
                sqlTransaction = dbConnection.conn.BeginTransaction();


                lsSql = "INSERT INTO cacttractivity" +
                   "(" +
                       "company_id, trans_bk, trans_no, trans_dt, prospect_id, " +
                       "contact_id, activity_type, remarks, activity_dt, user_cd," +
                       "update_dt, update_flag, trans_flag, post_flag, subject," +
                       "salesperson_cd, task_id, hour, minute, time_format, time_zone," +
                       "ref_bk, ref_no, ref_dt" +
                   ")" +
                   "VALUES" +
                   "(" +
                       "'" + companyId + "','CMAT','" + newActivityTransNo + "','" + DateTime.Today + "','" + prospectId + "','" +
                       contactId + "','" + taskType + "','" + remarks + "','" + DateTime.Today + "','" + gUserCd + "','" +
                       DateTime.Now + "','V','A','Y','" + taskSubject + "','"
                       + salespPersonCd + "','" + feedBackNo + "'," + hour + "," + minute + ",'" + timeFormat + "','" +
                       timeZone + "','" + feedBackBk + "','" + feedBackNo + "','" + feedBackDt +
                    "'); ";

                DML.Insert(lsSql, dbConnection, sqlTransaction);

                lsSql = "Insert into cacttractivitycontacts" +
                        "(" +
                         "company_id, trans_bk, trans_no, trans_dt, serial_no, " +
                        "prospect_id, contact_id, remarks, user_cd, update_dt," +
                        "update_flag, trans_flag, default_flag, contact_type" +
                        ")" +
                        "Select '" + companyId + "', 'CMAT', '" + newActivityTransNo + "', '" + DateTime.Today + "',serial_no ," +
                        "prospect_id , contact_id, '', '" + gUserCd + "', '" + DateTime.Now + "'," +
                        "'Y', 'A', default_flag, contact_type" +
                        " FROM cacttrmeetingcontacts " +
                        " WHERE  company_id = '" + companyId + "'  AND  " +
                        " trans_bk = '" + feedBackBk + "'  AND  " +
                        " trans_no = '" + feedBackNo + "'  AND  " +
                        " trans_dt = '" + feedBackDt + "';";

                DML.Insert(lsSql, dbConnection, sqlTransaction);

                lsSql = "UPDATE gnmsbook" +
                        " SET book_lno = '" + newActivityTransNo + "'," +
                        "  lno_date = '" + DateTime.Today + "'" +
                        " WHERE docu_typ = 'CMACT'" +
                        " And book_cd = 'CMAT' ";

                DML.Update(lsSql, dbConnection, sqlTransaction);
                sqlTransaction.Commit();

            }

            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                throw e;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

    }
}

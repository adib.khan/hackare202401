﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;
using System.Data;

namespace Cact.Bl.Validation
{
    public class CampaignValidation : GenValidation
    {
        public CampaignValidation(DbData dbData) : base(dbData){}

        public override int ValidateChild()
        {
            string companyId, campaignName, prospectId, transBk, transNo, parentTransNo, userCd, updateFlag, transFlag;
            DateTime transDt;
            int count = 1, result;
            string lsSql;
            DataTable selectResult;

            try
            {
                campaignName = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["campaign_id"].ToString();
                companyId = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["company_id"].ToString();
                transBk = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["trans_bk"].ToString();
                transNo = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["trans_no"].ToString();
                transDt = Convert.ToDateTime(dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["trans_dt"]);
                parentTransNo = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["parent_campaign_no"].ToString();
                userCd = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["user_cd"].ToString();
                updateFlag = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["update_flag"].ToString();
                transFlag = dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["trans_flag"].ToString();

                if (string.IsNullOrEmpty(campaignName))
                    throw new Exception("campaign name can not be empty.");

                if (dbData.requestDataset.Tables["cacttrcampaigndtl"] != null)
                    for (int i = dbData.requestDataset.Tables["cacttrcampaigndtl"].Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dbData.requestDataset.Tables["cacttrcampaigndtl"].Rows[i];
                        prospectId = dr["account_id"].ToString();
                        if (string.IsNullOrEmpty(prospectId))
                        {
                            dr.Delete();
                        }
                    }

                    foreach (DataRow dr in dbData.requestDataset.Tables["cacttrcampaigndtl"].Rows)
                    {
                        prospectId = dr["account_id"].ToString();

                    lsSql = "Select category_id, company_name, contact1, contact2, address1, address2, city, state, " +
                        "zip, country, phone, fax, email, credit_limit, salesperson_cd, " +
                        "ship_via, parent_account_id " +
                        "from cactmsaccount where id = '" + prospectId + "'";

                        selectResult = DML.Select(lsSql, dbConnection);

                        if (selectResult.Rows.Count > 0)
                        {
                            string category, name, contact1, contact2, address1, address2, city, state, zip, country, phone1;//, phone2;
                            string fax1, email, sales_person, ship_via, parent_account_id;
                            decimal credit_limit = 0;

                            category = selectResult.Rows[0]["category_id"].ToString();
                            name = selectResult.Rows[0]["company_name"].ToString();
                            contact1 = selectResult.Rows[0]["contact1"].ToString();
                            contact2 = selectResult.Rows[0]["contact2"].ToString();
                            address1 = selectResult.Rows[0]["address1"].ToString();
                            address2 = selectResult.Rows[0]["address2"].ToString();
                            city = selectResult.Rows[0]["city"].ToString();
                            state = selectResult.Rows[0]["state"].ToString();
                            zip = selectResult.Rows[0]["zip"].ToString();
                            country = selectResult.Rows[0]["country"].ToString();
                            phone1 = selectResult.Rows[0]["phone"].ToString();
                            //phone2 = selectResult.Rows[0]["phone2"].ToString();
                            fax1 = selectResult.Rows[0]["fax"].ToString();
                            email = selectResult.Rows[0]["email"].ToString();

                            if (selectResult.Rows[0]["credit_limit"] != DBNull.Value)
                            {
                                credit_limit = (decimal)selectResult.Rows[0]["credit_limit"];
                            }
                            sales_person = selectResult.Rows[0]["salesperson_cd"].ToString();
                            ship_via = selectResult.Rows[0]["ship_via"].ToString();
                            parent_account_id = selectResult.Rows[0]["parent_account_id"].ToString();

                            dr["category"] = category;
                            dr["name"] = name;
                            dr["contact1"] = contact1;
                            dr["contact2"] = contact2;
                            dr["address1"] = address1;
                            dr["address2"] = address2;
                            dr["city"] = city;
                            dr["state"] = state;
                            dr["zip"] = zip;
                            dr["country"] = country;
                            dr["phone1"] = phone1;
                            //dr["phone2"] = phone2;
                            dr["fax1"] = fax1;
                            dr["email"] = email;
                            dr["credit_limit"] = credit_limit;
                            dr["sales_person"] = sales_person;
                            dr["ship_via"] = ship_via;
                            dr["id"] = parent_account_id;
                    }
                }

                if (string.IsNullOrEmpty(companyId))
                {
                    dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["company_id"] = "DS02";
                }

                if (string.IsNullOrEmpty(transBk))
                {
                    dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["transBk"] = "CACT";
                }

                //Generate child campaign
                if (int.TryParse(transNo, out result))  //Check is is a pure numeric or not
                {
                    dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["parent_campaign_no"] = transNo;
                    dbData.requestDataset.Tables["cacttrcampaignhd"].Rows[0]["parent_campaign_dt"] = transDt;
                }

                if (dbData.requestDataset.Tables.Contains("cacttrcampaigndtl"))
                    if (dbData.requestDataset.Tables["cacttrcampaigndtl"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cacttrcampaigndtl"].Rows)
                        {
                            dr["company_id"] = companyId;
                            dr["trans_bk"] = transBk;
                            dr["trans_no"] = transNo;
                            dr["trans_dt"] = transDt;

                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

                if (dbData.requestDataset.Tables.Contains("cactaccountdocs"))
                    if (dbData.requestDataset.Tables["cactaccountdocs"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactaccountdocs"].Rows)
                        {
                            dr["company_id"] = companyId;
                            dr["trans_bk"] = transBk;
                            dr["trans_no"] = transNo;
                            dr["trans_dt"] = transDt;
                            dr["user_cd"] = userCd;
                            dr["update_flag"] = updateFlag;
                            dr["trans_flag"] = transFlag;
                            dr["serial_no"] = (100 + count).ToString();

                            count++;
                        }
            }

            catch (Exception e)
            {
                throw e;
            }
            return 0;
        }

    }
}
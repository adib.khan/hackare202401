﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/09/06
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Services;
using GenService.Db;
using Newtonsoft.Json;
using System;
using System.Data.SqlClient;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace Cact.Bl.Services
{
    public class SyncCreditInboxService : DataSavingService
    {

        public JsonElement GetAwsData(string as_company_name)
        {
            string ls_aws_url;
            
            DbConnection dbConnection = new DbConnection(as_company_name);

            dbConnection.conn.Open();
            ls_aws_url = Convert.ToString(DML.Select("field_value", "gnini", " where field_id =  'EXTERNAL_RETRIEVE_1' AND MODULE_ID = 'CACT'", dbConnection));

            if (string.IsNullOrEmpty(ls_aws_url)) 
            {
                throw new Exception("Unable to get EXTERNAL_RETRIEVE_1 ");
            }

            string ls_json = System.Text.Json.JsonSerializer.Serialize(
              new
              {
                  SQL = "SELECT * FROM cactaccountinboxhd WHERE trfr_flag <> 'P'"
              }
            );

            string ls_jsona, ls_jsonb, ls_jsonc;
            ls_jsona = '"' + "HEADER" + '"' + ':' + GetDatafromAws(ls_json, ls_aws_url);

           
            ls_json = System.Text.Json.JsonSerializer.Serialize(
              new
              {
                  SQL = "SELECT A.* FROM cactaccountinboxcontacts A INNER JOIN cactaccountinboxhd B ON 	A.ID = B.ID WHERE trfr_flag <> 'P'"
              }
            );

            ls_jsonb = '"' + "CONTACT" + '"' + ':' + GetDatafromAws(ls_json, ls_aws_url);

            ls_json = System.Text.Json.JsonSerializer.Serialize(
             new
             {
                 SQL = "SELECT A.* FROM cactaccountinboxtraderef A INNER JOIN cactaccountinboxhd B ON 	A.ID = B.ID WHERE trfr_flag <> 'P'"
             }
           );

            ls_jsonc = '"' + "TRADEREF" + '"' + ':' + GetDatafromAws(ls_json, ls_aws_url);
            
            ls_json = "{" + ls_jsona + "," + ls_jsonb + "," + ls_jsonc + "}";

            JsonDocument doc = JsonDocument.Parse(ls_json);
            return doc.RootElement;

          //  return JsonDocument
            //return  JsonConvert.DeserializeObject<JsonElement>(ls_json);
            //return ls_json;
        }

        public dynamic GetDatafromAws(string as_json, string as_aws_url)
        {;
            string result;
            var data = new StringContent(as_json, Encoding.UTF8, "application/json");

            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(as_aws_url)
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("companyName", "RCCRM");
            //   client.DefaultRequestHeaders.Add("Session", is_sessionId);
            HttpResponseMessage response = client.PostAsync(as_aws_url, data).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                 result = response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                throw new Exception(response.Content.ReadAsStringAsync().Result);
            }

            return JsonConvert.DeserializeObject(result);
            //return result;
        }
        public string Sync(JsonElement requestJson)
        {
            long id;
            string companyName, tradeName, address1, zip, city, state, business_phone, fax, comp_email_address;
            string website, facebook, instagram, buying_group1, buying_group2, buying_group3, buying_group4;
            string buying_group5, buying_group6, buying_group7, howWeMet, requestCatalog, subscribeToEmail;
            string typeOfBusiness, jbtNumber, jbtPhone, jbtMembershipVerified, bankInstitutionName;
            string bankAccountNumber, bankCity, bankState, bankPhone, bankFax, bankOfficer;
            string resaleCertificateNo, usaPatriotAct, caApplicantName, userCd, companyId;
            string updateFlag, transFlag, howWeMetSpecify, trfrFlag, categoryId, creditType, sql, address2, is_comments;
            decimal jbtCreditAmount = 0;
            DateTime caSubmitDate, updateDt;
            decimal noOfYears = 0;

            string ls_shipToaddress1, ls_shipToaddress2, ls_shipTozip, ls_state, ls_shipTocity;
       
            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();

           
            var inboxHeader = requestJson.GetProperty("HEADER").EnumerateArray();
            var inboxContacts = requestJson.GetProperty("CONTACT").EnumerateArray();
            var inboxTrades = requestJson.GetProperty("TRADEREF").EnumerateArray();

            dbConnection.conn.Open();

            try
            {
                sqlTransaction = dbConnection.conn.BeginTransaction();

                sql = "";
                //    sql = "SET IDENTITY_INSERT dbo.cactaccountinboxhd ON;"; //Jay 20221021 : comment

                //SqlCommand command = new SqlCommand(sql, dbConnection.conn, sqlTransaction);  //Jay 20221021 : comment
                //command.ExecuteNonQuery();  //Jay 20221021 : comment

                foreach (var currentRow in inboxHeader)
                {                   
                    id = long.Parse(currentRow.GetProperty("id").ToString().Trim());
                    companyName = currentRow.GetProperty("company_name").ToString().Trim();
                    tradeName = currentRow.GetProperty("trade_name").ToString().Trim();
                    address1 = currentRow.GetProperty("address").ToString().Trim();
                    address2 = currentRow.GetProperty("address2").ToString().Trim();
                    zip = currentRow.GetProperty("zip").ToString().Trim();
                    city = currentRow.GetProperty("city").ToString().Trim();
                    state = currentRow.GetProperty("state").ToString().Trim();
                    business_phone = currentRow.GetProperty("business_phone").ToString().Trim();
                    fax = currentRow.GetProperty("fax").ToString().Trim();
                    comp_email_address = currentRow.GetProperty("comp_email_address").ToString().Trim();
                    website = currentRow.GetProperty("website").ToString().Trim();
                    facebook = currentRow.GetProperty("facebook").ToString().Trim();
                    instagram = currentRow.GetProperty("instagram").ToString().Trim();
                    buying_group1 = currentRow.GetProperty("buying_group1").ToString().Trim();
                    buying_group2 = currentRow.GetProperty("buying_group2").ToString().Trim();
                    buying_group3 = currentRow.GetProperty("buying_group3").ToString().Trim();
                    buying_group4 = currentRow.GetProperty("buying_group4").ToString().Trim();
                    buying_group5 = currentRow.GetProperty("buying_group5").ToString().Trim();
                    buying_group6 = currentRow.GetProperty("buying_group6").ToString().Trim();
                    buying_group7 = currentRow.GetProperty("buying_group7").ToString().Trim();
                    howWeMet = currentRow.GetProperty("how_we_met").ToString().Trim();
                    howWeMetSpecify = currentRow.GetProperty("how_we_met_specify").ToString().Trim();
                    requestCatalog = currentRow.GetProperty("request_catalog").ToString().Trim();
                    subscribeToEmail = currentRow.GetProperty("subscribe_to_email").ToString().Trim();
                    typeOfBusiness = currentRow.GetProperty("type_of_business").ToString().Trim();
                    is_comments = currentRow.GetProperty("comments").ToString().Trim(); //Jay 20221019

                    //Jay 20221021
                    ls_shipToaddress1 = currentRow.GetProperty("shipToaddress1").ToString().Trim(); 
                    ls_shipToaddress2 = currentRow.GetProperty("shipToaddress2").ToString().Trim(); 
                    ls_shipTozip = currentRow.GetProperty("shipTozip").ToString().Trim(); 
                    ls_shipTocity = currentRow.GetProperty("shipTocity").ToString().Trim(); 
                    ls_state = currentRow.GetProperty("shipTostate").ToString().Trim(); 
                    //Jay 20221021 :End
                    string no_of_years = currentRow.GetProperty("no_of_years").ToString();


                    //Jay /Vijay 20220613
                    companyName = companyName.Replace("'", "''");
                    tradeName = tradeName.Replace("'", "''");

                    if (!string.IsNullOrEmpty(currentRow.GetProperty("no_of_years").ToString()))
                    {
                        noOfYears = Convert.ToDecimal(currentRow.GetProperty("no_of_years").ToString());
                    }

                    jbtNumber = currentRow.GetProperty("jbt_number").ToString().Trim();
                    jbtPhone = currentRow.GetProperty("jbt_phone").ToString().Trim();

                    if (!string.IsNullOrEmpty(currentRow.GetProperty("jbt_credit_amount").ToString()))
                    {
                        jbtCreditAmount = Convert.ToDecimal(currentRow.GetProperty("jbt_credit_amount").ToString());
                    }

                    jbtMembershipVerified = currentRow.GetProperty("jbt_membership_verified").ToString().Trim();
                    bankInstitutionName = currentRow.GetProperty("bank_institution_name").ToString().Trim();
                    bankAccountNumber = currentRow.GetProperty("bank_account_numer").ToString().Trim();
                    bankCity = currentRow.GetProperty("bank_city").ToString().Trim();
                    bankState = currentRow.GetProperty("bank_state").ToString().Trim();
                    bankPhone = currentRow.GetProperty("bank_phone").ToString().Trim();
                    bankFax = currentRow.GetProperty("bank_fax").ToString().Trim();
                    bankOfficer = currentRow.GetProperty("bank_officer").ToString().Trim();
                    resaleCertificateNo = currentRow.GetProperty("resale_certificate_no").ToString().Trim();
                    usaPatriotAct = currentRow.GetProperty("usa_patriot_act").ToString().Trim();
                    caSubmitDate = DateTime.Parse(currentRow.GetProperty("ca_submit_date").ToString().Trim());
                    caApplicantName = currentRow.GetProperty("ca_applicant_name").ToString().Trim();
                    userCd = currentRow.GetProperty("user_cd").ToString().Trim();
                    companyId = currentRow.GetProperty("company_id").ToString().Trim();
                    updateDt = DateTime.Parse(currentRow.GetProperty("update_dt").ToString().Trim());
                    updateFlag = currentRow.GetProperty("update_flag").ToString().Trim();
                    transFlag = currentRow.GetProperty("trans_flag").ToString().Trim();
                    trfrFlag = currentRow.GetProperty("trfr_flag").ToString().Trim();
                    categoryId = currentRow.GetProperty("category_id").ToString().Trim();
                    creditType = currentRow.GetProperty("credit_type").ToString().Trim();

                    bool isUnique = DML.Select("cactaccountinboxhd", " where id = '" + id + "'", dbConnection, sqlTransaction);
                    if (!isUnique)
                    {
                        continue;
                    }

                   
                    //Jay 20221021 : add columns for ship to
                    sql = "INSERT INTO cactaccountinboxhd" +
                    "(" +
                       "id, company_name, trade_name, address, address2, zip, city, state, business_phone, fax, comp_email_address, " +
                       "website, facebook, instagram, buying_group1, buying_group2, buying_group3, buying_group4, buying_group5, " +
                       "buying_group6, buying_group7, how_we_met, request_catalog, subscribe_to_email, type_of_business, no_of_years, " +
                       "jbt_number, jbt_phone, jbt_credit_amount, jbt_membership_verified, bank_institution_name, bank_account_numer, " +
                       "bank_city, bank_state, bank_phone, bank_fax, bank_officer, resale_certificate_no, usa_patriot_act, ca_submit_date, " +
                       "ca_applicant_name, user_cd, company_id, update_dt, update_flag, trans_flag, trfr_flag, category_id, credit_type, comments" +
                        ", shipToaddress1,shipToaddress2,shipTozip,shipTocity,shipTostate, how_we_met_specify " +
                    ")" +
                   "VALUES" +
                   "(" +
                        "'" + id + "','" +companyName + "','" + tradeName + "','" + address1 + "','" + address2 + "','" + zip + "','" + city + "','" + state + "','" + business_phone + "','" + fax + "','" + comp_email_address +
                        "','" + website + "','" + facebook + "','" + instagram + "','" + buying_group1 + "','" + buying_group2 + "','" + buying_group3 + "','" + buying_group4 + "','" + buying_group5 +
                        "','" + buying_group6 + "','" + buying_group7 + "','" + howWeMet + "','" + requestCatalog + "','" + subscribeToEmail + "','" + typeOfBusiness + "','" + noOfYears+
                        "','" + jbtNumber + "','" + jbtPhone + "','" + jbtCreditAmount + "','" + jbtMembershipVerified + "','" + bankInstitutionName + "','" + bankAccountNumber +
                        "','" + bankCity + "','" + bankState + "','" + bankPhone + "','" + bankFax + "','" + bankOfficer + "','" + resaleCertificateNo + "','" + usaPatriotAct + "','" + caSubmitDate +
                        "','" + caApplicantName + "','" + userCd + "','" + companyId + "','" + updateDt + "','" + updateFlag + "','" + transFlag + "','" + trfrFlag + "','" + categoryId + "','" + creditType +
                   "','" + is_comments + "'," +
               " '" + ls_shipToaddress1 + "', '" + ls_shipToaddress2 + "', '" + ls_shipTozip + "', '" + ls_shipTocity + "', '" + ls_state + "', '" + howWeMetSpecify + "' ) ";

                    DML.Insert(sql, dbConnection, sqlTransaction);
                }

                foreach (var currentRow in inboxContacts)
                {

                    string serialNo, contactType, first_name, last_name, email, phone;

                    id = (int)long.Parse(currentRow.GetProperty("id").ToString().Trim());
                    serialNo = currentRow.GetProperty("serial_no").ToString().Trim();
                    contactType = currentRow.GetProperty("contact_type").ToString().Trim();
                    first_name = currentRow.GetProperty("first_name").ToString().Trim();
                    last_name = currentRow.GetProperty("last_name").ToString().Trim();
                    email = currentRow.GetProperty("email").ToString().Trim();
                    phone = currentRow.GetProperty("phone").ToString().Trim();
                    userCd = currentRow.GetProperty("user_cd").ToString().Trim();
                    companyId = currentRow.GetProperty("company_id").ToString().Trim();
                    updateDt = DateTime.Parse(currentRow.GetProperty("update_dt").ToString().Trim());
                    updateFlag = currentRow.GetProperty("update_flag").ToString().Trim();
                    transFlag = currentRow.GetProperty("trans_flag").ToString().Trim();

                    bool isUnique = DML.Select("cactaccountinboxcontacts", " where id = '" + id + "' and serial_no = '"+serialNo+"'", dbConnection, sqlTransaction);
                    if (!isUnique)
                    {
                        continue;
                    }

                    sql = "INSERT INTO cactaccountinboxcontacts" +
                   "(" +
                        "id, serial_no, contact_type, first_name, last_name, email, phone, user_cd, " +
                       "company_id, update_dt, update_flag, trans_flag " +
                    ")" +
                    "VALUES" +
                    "(" +
                        id + ",'" + serialNo + "','" + contactType + "','" + first_name + "','" + last_name+ "','" + email + "','" + phone + "','" + userCd +
                        "','" + companyId + "','" + updateDt + "','" + updateFlag + "','" + transFlag + "'" +
                    "); ";

                    DML.Insert(sql, dbConnection, sqlTransaction);

                }

                foreach (var currentRow in inboxTrades)
                {

                    string serialNo, email, phone, personToContact;

                    id = (int)long.Parse(currentRow.GetProperty("id").ToString().Trim());
                    serialNo = currentRow.GetProperty("serial_no").ToString().Trim();

                    companyName = currentRow.GetProperty("company_name").ToString().Trim();
                    personToContact = currentRow.GetProperty("person_to_contact").ToString().Trim();
                    email = currentRow.GetProperty("email").ToString().Trim();
                    phone = currentRow.GetProperty("phone").ToString().Trim();
                    userCd = currentRow.GetProperty("user_cd").ToString().Trim();
                    companyId = currentRow.GetProperty("company_id").ToString().Trim();
                    updateDt = DateTime.Parse(currentRow.GetProperty("update_dt").ToString().Trim());
                    updateFlag = currentRow.GetProperty("update_flag").ToString().Trim();
                    transFlag = currentRow.GetProperty("trans_flag").ToString().Trim();

                    bool isUnique = DML.Select("cactaccountinboxtraderef", " where id = '" + id + "' and serial_no = '" + serialNo + "'", dbConnection, sqlTransaction);
                    if (!isUnique)
                    {
                        //sqlTransaction.Dispose();
                        continue;
                    }

                    sql = "INSERT INTO cactaccountinboxtraderef" +
                        "(" +
                            "Id, serial_no, company_name, person_to_contact, email, phone, " +
                           "user_cd, company_id, update_dt, update_flag, trans_flag " +
                        ")" +
                        "VALUES" +
                        "(" +
                            id + ",'" + serialNo + "','" + companyName + "','" + personToContact + "','" + email + "','" + phone +
                            "','" + userCd + "','" + companyId + "','" + updateDt + "','" + updateFlag + "','" + transFlag + "'" +
                        "); ";

                    DML.Insert(sql, dbConnection, sqlTransaction);

                    //sqlTransaction.Commit();
                }

                // sql = "SET IDENTITY_INSERT dbo.cactaccountinboxhd OFF;";  //Jay 20221021
                //command = new SqlCommand(sql, dbConnection.conn, sqlTransaction);  //Jay 20221021 : comment
                //command.ExecuteNonQuery();  //Jay 20221021 : comment

                sqlTransaction.Commit();
            }

            catch(Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();
                throw new Exception(e.Message);
            }
            finally
            {
                dbConnection.conn.Close();
            }

            return "";

        }

    }
}
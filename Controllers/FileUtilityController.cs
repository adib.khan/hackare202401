﻿using Cact.BL.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;


namespace Cact.Controllers
{
    [Route("api/[controller]/{action}")]
	[ApiController]
	public class FileUtilityController : GenController
	{


		FileUtilityService fileUtilityService;

		


		[HttpGet]
		public ActionResult s(string file_name)
		{
			return Ok("jh");
		}

		
		[HttpPost]
		public IActionResult Upload()
		{
			try
			{
				string ls_fileName;
				fileUtilityService = new FileUtilityService(Request.Headers["companyName"]);
				ls_fileName = fileUtilityService.Upload(Request.Form.Files);
				
				return Ok(JsonConvert.SerializeObject(new
				{
					file_name = ls_fileName

				}));
				//return Ok(ls_fileName);
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}

		}

		[HttpGet]
		public ActionResult Download(string file_name)
		{

			try
			{
				fileUtilityService = new FileUtilityService(Request.Headers["companyName"]);
				
				return File(fileUtilityService.Download(file_name), "application/octet-stream", file_name);
				//return File(fileUtilityService.Download(file_name), "application/octet-stream", file_name);
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}




		}

		[HttpGet]
		public ActionResult GetFilePath(string file_name)
		{

			try
			{
				fileUtilityService = new FileUtilityService(Request.Headers["companyName"]);
				return Ok(fileUtilityService.GetFilePath(file_name));
				//return File(fileUtilityService.Download(file_name), "application/octet-stream", file_name);
			}
			catch (Exception e)
			{
				return BadRequest(e.Message);
			}




		}





	}
}
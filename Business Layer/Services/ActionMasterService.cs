﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Validations;
using GenService.BL.Services;
using GenService.Db;

namespace Cact.Bl.Services
{ 
    public class ActionMasterService : DataSavingService
    {
       public ActionMasterService() : base()
        {
            validation = new ActionMasterValidation(dbData);
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("cactmsaction", "dwc_master", true, false, "code = {code}");
        }

        public override void PreSaveChild()
        {
            //base.PreSaveChild();
        }

        public override void PostSaveChild()
        {
            //base.PostSaveChild();
        }
    }
}

﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using cact.Bl.Validation;
using Cact.BL.Posting;
using GenService.BL.Services;
using GenService.Db;
using System.Data;

namespace cact.Bl.Services
{
    public class CustomerMasterService : DataSavingService
    {
        public CustomerMasterService() : base()
        {
            validation = new CustomerMasterValidation(dbData);
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("faarmscustomer", "dwc_master", true, false, "id = {id}");
            dbData.updateProperties[1] = new UpdateProperty("cactmsprospectship", "dwc_detail", true, true, "id = {id}");
            dbData.updateProperties[2] = new UpdateProperty("cactmsaccountnotes", "dwc_detailprospectnotes", true, true, "account_id = {id} and company_id = {company_id}");
            dbData.updateProperties[3] = new UpdateProperty("faarmscustomerjbtranking", "dwc_detail3", true, true, "id = {id}");
            dbData.updateProperties[4] = new UpdateProperty("cactmscontact", "dwc_detailbuyerb", true, true, "id = {id}");
            dbData.updateProperties[5] = new UpdateProperty("cactmsaccountsummary", "dwc_summary", true, false, "account_id = {id}");
            dbData.updateProperties[6] = new UpdateProperty("cactaccountdocs", "dwc_detaildoc", true, true, "id = {id}");
            dbData.updateProperties[7] = new UpdateProperty("cactmstraderef", "dwc_detail_traderef", true, true, "id = {id}");

        }

        public override void PostSaveChild()
        {
            string customerId = dbData.requestDataset.Tables["faarmscustomer"].Rows[0].Field<string>("id");

            ProspectCustomerSync prospectCustomerSync = new ProspectCustomerSync();
            prospectCustomerSync.SyncToProspect(customerId, dbConnection, trans);
        }

    }
}

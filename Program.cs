using Cact.TelegramBot;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Telegram.Bot;

//11062020 Akshay/Divyanshi Created
namespace Cact
{
    public class Program
    {
        public static TelegramBotClient Client;
        public static InitTelegram initTelegram;
        public static void Main(string[] args)
        {
            initTelegram = new InitTelegram();
            initTelegram.initalizeTheBot();
            CreateHostBuilder(args).Build().Run();
           
        }

        //public static void initalizeTheBot()
        //{
        //    Client = new TelegramBotClient("6500087831:AAHMVZc4hB9oZvCmEsghLjsA2K-4J3Z4EnM");
        //    //Client.OnMessage += Client_OnMessage;
        //    //Client.StartReceiving();
        //    initTelegram.StartReceiver();
        //}

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

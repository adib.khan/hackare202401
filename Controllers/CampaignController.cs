﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Cact.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class CampaignController : DataSavingController
    {
        public CampaignController()
        {
            service = new CampaignService();
        }

        [EnableCors]
        [HttpPost]
        public IActionResult Save(JsonElement requestJson)
        {
            return Saving(requestJson);
        }

        [EnableCors]
        [HttpPost]
        public IActionResult AddRemoveCampaign(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                ValidateSession();
                CampaignService campaignService = (CampaignService)service;
                campaignService.AddRemoveCampaign(requestJson);
                return Ok();
            }
            catch
            {
                throw;
            }
        }

    }
}
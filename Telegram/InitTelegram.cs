﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Message = Telegram.Bot.Types.Message;
using System.Text;
using System.IO;
using GenService.Db;
using System.Data.Common;
using System.Diagnostics;
using File = System.IO.File;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using System.Globalization;
using DbConnection = GenService.Db.DbConnection;
using Newtonsoft.Json.Converters;
using System.Linq;
using System.Net.Http.Headers;
using System.Xml.Linq;
using static HuggingFace.RecommendedModelIds.Llama2;

namespace Cact.TelegramBot
{
    public class InitTelegram 
    {
        public static TelegramBotClient Client;
        public static string il_chat_id;
        public static DataTable taskdataTable;
        public static DataRow taskDataRow;
        public static Dictionary<long, string> userQuestions = new Dictionary<long, string>();
        public static DbConnection dbConnection;
        public static DataTable contactdataTable;
        public InitTelegram()
        {
            
        }

        public void initalizeTheBot()
        {
            Client = new TelegramBotClient("6500087831:AAHMVZc4hB9oZvCmEsghLjsA2K-4J3Z4EnM");
            dbConnection = new DbConnection("RCCRMLOCAL");
            dbConnection.conn.Open();
            StartReceiver();
        }

        //Initialization 
        public async Task StartReceiver() 
        { 
            var token = new CancellationTokenSource();
            var cancelToken = token.Token;
            var ReOpt = new ReceiverOptions { AllowedUpdates = { } };
            Message sentMessage = new Message();
            await Client.ReceiveAsync(OnMessage, ErrorMessage, ReOpt, cancelToken);
        }

        //Receiving 
        public async Task OnMessage(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            string ls_recvMsg = null;
            string ls_sql = "";
            if (update.Message is Message message)
            {

                Console.WriteLine(message);

                if (userQuestions.ContainsKey(message.Chat.Id))
                {
                    ls_recvMsg = (update.Message.Text).ToString();
                    

                    switch (userQuestions[message.Chat.Id])
                    {
                        case "Subject":
                            taskDataRow["subject"] = (update.Message.Text).ToString();
                            userQuestions.Remove(message.Chat.Id);
                            await botClient.SendTextMessageAsync(message.Chat.Id, "Whom do you want to assign this task (Name)?");
                            userQuestions[message.Chat.Id] = "AssignTo";
                            break;

                        case "AssignTo":
                            string ls_assignTo = (update.Message.Text).ToString();
                            ls_sql = "";

                            string[] assignto_words = ls_assignTo.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            ls_assignTo = "%" + string.Join("%", assignto_words) + "%";

                            userQuestions.Remove(message.Chat.Id);
                            ls_assignTo = Convert.ToString(DML.Select("id", "saoimssalesperson", $@" where name like '{ls_assignTo}'", dbConnection));
                            if (String.IsNullOrWhiteSpace(ls_assignTo))
                            {
                                await botClient.SendTextMessageAsync(message.Chat.Id, "Invalid Input. Please Enter a valid name:");
                                userQuestions[message.Chat.Id] = "AssignTo";
                                break;
                            }
                            taskDataRow["assigned_to"] = ls_assignTo;
                            await botClient.SendTextMessageAsync(message.Chat.Id, "Whom do you want to call(Customer Name)?");
                            userQuestions[message.Chat.Id] = "customer";
                            break;

                        case "customer":
                            string ls_prospectId = (update.Message.Text).ToString();
                            string ls_id, ls_salesperson_cd;
                            taskDataRow["prospect_id"] = ls_prospectId;
                            string[] prospect_words = ls_prospectId.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            ls_prospectId = "%" + string.Join("%", prospect_words) + "%";
                            ls_sql = $"select * from (select  cactmsaccount.id, cactmsaccount.salesperson_cd, CONCAT(cactmsaccount.address1,' ',cactmsaccount.address2) as address from cactmsaccount where company_name like '{ls_prospectId}'  ) as temp_table ";
                            
                            DataTable ldtbl_datatable = DML.Select(ls_sql,dbConnection);      

                            //ls_id = Convert.ToString(ldtbl_datatable.Rows[0]["id"]).Trim();
                            //ls_id = Convert.ToString(DML.Select("id", "cactmsaccount ", $" where company_name like '{ls_prospectId}'", dbConnection));

                            if (ldtbl_datatable.Rows.Count <= 0 )
                            {
                                await botClient.SendTextMessageAsync(message.Chat.Id, "Invalid Input. Please Enter a valid customer name:");
                                //userQuestions[message.Chat.Id] = "customer";
                                break;
                            }
                            else
                            {
                                ls_id = Convert.ToString(ldtbl_datatable.Rows[0]["id"]).Trim();
                                //ls_contact_id = Convert.ToString(ldtbl_datatable.Rows[0]["contact_id"]).Trim();
                                ls_salesperson_cd = Convert.ToString(ldtbl_datatable.Rows[0]["salesperson_cd"]).Trim();
                                taskDataRow["prospect_id"] = ls_id;
                                //taskDataRow["contact_id"] = ls_contact_id;
                                taskDataRow["salesperson_cd"] = ls_salesperson_cd;


                                ls_sql = $@" select * from cacttrtaskcontacts where trans_bk = ''";

                                contactdataTable = DML.Select(ls_sql, dbConnection);

                                DataRow ldr_contact = contactdataTable.NewRow();
                                ldr_contact["company_id"] = "DS02";
                                ldr_contact["serial_no"] = "101";
                                ldr_contact["trans_bk"] = taskDataRow["trans_bk"];
                                ldr_contact["trans_no"] = taskDataRow["trans_no"];
                                ldr_contact["trans_dt"] = taskDataRow["trans_dt"];
                                ldr_contact["prospect_id"] = taskDataRow["prospect_id"];
                                ldr_contact["contact_id"] = "101"; // Assuming contact_id is based on serial_no
                                ldr_contact["default_flag"] = "N";
                                ldr_contact["remarks"] = "";
                                ldr_contact["update_dt"] = DateTime.Today;

                                ls_sql = $"select * from (select cactmscontact.*, cactmssetupdtl.value as contact_type_desc from cactmscontact left join cactmssetupdtl on cactmssetupdtl.ID=cactmscontact.contact_type and setup_id = 'CONTACT_TYPE') as temp_table where id = '{ls_id}'";
                                ldtbl_datatable = DML.Select(ls_sql, dbConnection);
                                ldr_contact["contact_type"] = ldtbl_datatable.Rows[0]["contact_type"];
                                contactdataTable.Rows.Add(ldr_contact);

                                userQuestions.Remove(message.Chat.Id);
                                await botClient.SendTextMessageAsync(message.Chat.Id, "Please enter task due date(MM/dd/yyyy)");
                                userQuestions[message.Chat.Id] = "date";
                                break;
                            }
                            
                        case "date":
                            string dateString = (update.Message.Text).ToString(); ; // Replace with your date string

                            if (DateTime.TryParse(dateString, out DateTime result))
                            {
                                taskDataRow["start_date"] = dateString;
                                userQuestions.Remove(message.Chat.Id);
                                await botClient.SendTextMessageAsync(message.Chat.Id, "Please enter priority(High/Mid/Low):");
                                userQuestions[message.Chat.Id] = "priority";
                                break;
                            }
                            else
                            {                                
                                await botClient.SendTextMessageAsync(message.Chat.Id, "Invalid Input. Please Enter a valid date(MM/dd/yyyy):");
                                userQuestions[message.Chat.Id] = "date";
                                break;
                            }                            

                        case "priority":

                            if (update.Message.Text.ToString().ToUpper() == "HIGH" || update.Message.Text.ToString().ToUpper() == "H") taskDataRow["priority"] = 'H';
                            else if (update.Message.Text.ToString().ToUpper() == "MID" || update.Message.Text.ToString().ToUpper() == "M") taskDataRow["priority"] = 'M';
                            else if (update.Message.Text.ToString().ToUpper() == "LOW" || update.Message.Text.ToString().ToUpper() == "L") taskDataRow["priority"] = 'L';
                            else{ 
                                await botClient.SendTextMessageAsync(message.Chat.Id, "Invalid Input. Please Enter a valid priority:"); 
                                //userQuestions[message.Chat.Id] = "priority"; 
                                break;
                            }

                            userQuestions.Remove(message.Chat.Id);
                            await botClient.SendTextMessageAsync(message.Chat.Id, "Do you want to create this task?(Yes/No)");
                            userQuestions[message.Chat.Id] = "save";
                            break;

                        case "save":
                            if ((update.Message.Text).ToString().ToUpper() == "YES" || update.Message.Text.ToString().ToUpper() == "Y")
                            {
                                //save
                                try
                                {
                                    DataSet lds_taskdetail = new DataSet();
                                    taskdataTable.Rows.Add(taskDataRow);

                                    DataTable ldtbl_action = new DataTable();
                                    taskdataTable.TableName = "dwc_master";
                                    contactdataTable.TableName = "dwc_detail";
                                    ldtbl_action.TableName = "dwc_detail_actions";
                                    lds_taskdetail.Tables.Add(taskdataTable);
                                    lds_taskdetail.Tables.Add(contactdataTable);
                                    lds_taskdetail.Tables.Add(ldtbl_action);

                                    string ls_saoitrinvJson, ls_error, ls_session;

                                    ls_saoitrinvJson = JsonConvert.SerializeObject(lds_taskdetail);
                                    var data = new StringContent(ls_saoitrinvJson, Encoding.UTF8, "application/json");


                                    ls_session = Convert.ToString(DML.Select(" session_id", "gnsessions order by update_dt desc ", $@" ", dbConnection));

                                    HttpResponseMessage response = new HttpResponseMessage();
                                    HttpClient client = new HttpClient
                                    {
                                        BaseAddress = new Uri("http://192.168.201.163:5018")
                                    };
                                    client.DefaultRequestHeaders.Accept.Clear();
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    client.DefaultRequestHeaders.Add("companyName", "RCCRMLOCAL");
                                    client.DefaultRequestHeaders.Add("Session", ls_session);

                                    response = client.PostAsync("/api/Task/Save", data).Result; 

                                    if (response.IsSuccessStatusCode)
                                    {
                                        await botClient.SendTextMessageAsync(message.Chat.Id, "Saved Successfully.");
                                        await SendMenuAsync(message.Chat.Id);
                                    }
                                    else
                                    {
                                        ls_error = response.Content.ReadAsStringAsync().Result.ToUpper();
                                        await botClient.SendTextMessageAsync(message.Chat.Id, "Error while saving.");
                                        await SendMenuAsync(message.Chat.Id);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    System.IO.File.WriteAllText("d:\\t.txt", string.Join(", ", ex.Message));
                                }
                                    
                            }
                            else {
                                await botClient.SendTextMessageAsync(message.Chat.Id, "Changes have been Discarded.");
                                await SendMenuAsync(message.Chat.Id);
                            }
                            userQuestions.Remove(message.Chat.Id);
                            
                            
                            break;

                            //case "time":
                            //    taskDataRow["start_date"] = (update.Message.Text).ToString();
                            //    userQuestions.Remove(message.Chat.Id);
                            //    await botClient.SendTextMessageAsync(message.Chat.Id, "Please enter task due time(hh:mm AM/PM)");
                            //    userQuestions[message.Chat.Id] = "time";
                            //    break;

                    }

                    ////Customer
                    //await botClient.SendTextMessageAsync(chatId, "");

                    ////Date
                    //await botClient.SendTextMessageAsync(chatId, "");

                    ////Time
                    //await botClient.SendTextMessageAsync(chatId, "");

                    ////Priority 
                    //await botClient.SendTextMessageAsync(chatId, "");


                }
                else
                {
                    string ls_id;
                    ls_recvMsg = (update.Message.Text).ToString();
                    il_chat_id = message.Chat.Id.ToString();
                    if ((ls_recvMsg).ToUpper().Contains("HI") || (ls_recvMsg).ToUpper().Contains("HEY") || (ls_recvMsg).ToUpper().Contains("HELLO"))
                    {


                        ls_id = Convert.ToString( DML.Select("id", "sysmmstelegramuser ", $" where id = '{message.From.Username.Trim()}' ", dbConnection));
                        //if check if user is new 
                        if (string.IsNullOrWhiteSpace(ls_id))
                        {
                            await botClient.SendTextMessageAsync(message.Chat.Id, "Hi there, You seem new here. Please contact Diaspark to avail this service. ");
                        }
                        else
                        {
                            await botClient.SendTextMessageAsync(message.Chat.Id, "Hi RoyalChains, Good to hear from you :)");
                            await SendMenuAsync(message.Chat.Id);
                        }                         
                    }
                    else
                    {
                        await botClient.SendTextMessageAsync(message.Chat.Id, "Sorry, I didn't get it.");
                    }
                }
            }

            if (update.Type == UpdateType.CallbackQuery)
            {
                var callbackQuery = update.CallbackQuery;
                var chatId = callbackQuery.Message.Chat.Id;
                var data = callbackQuery.Data;

                //Console.WriteLine(sentMessage.MessageId);

                // You can perform different actions based on the callback data
                switch (data)
                {
                    case "call":
                        GetDataTableFromDatabaseForTask();
                        await botClient.SendTextMessageAsync(chatId, "Whats the subject of the call?");
                        userQuestions[chatId] = "Subject";
                        taskDataRow["task_type"] = "C";
                        break;


                    case "getReports":
                        await SendNewReportMenuAsync(botClient, data, chatId);
                        break;

                    case "support":
                        await botClient.SendTextMessageAsync(chatId, "Please visit below website https://www.diaspark.com/");
                        break;

                    case "meeting":
                        await botClient.SendTextMessageAsync(chatId, "This feature is under development");
                        break;

                    case "schedule_call":
                        await botClient.SendTextMessageAsync(chatId, "Please enter subject for call");                        
                        break;

                    case "openTask":
                        CallGetReportsApi(chatId,"openTask");
                        break;

                    case "closeTask":
                        CallGetReportsApi(chatId, "closeTask");
                        break;

                    case "allTasks":
                        CallGetReportsApi(chatId, "allTasks");
                        break;

                    case "hTasks":
                        CallGetReportsApi(chatId, "hTasks");
                        break;
                    case "mTasks":
                        CallGetReportsApi(chatId, "mTasks");
                        break;

                    case "lTasks":
                        CallGetReportsApi(chatId, "lTasks");
                        break;
                    case "back":
                        await SendMenuAsync(chatId);
                        break;
                    case "exit":
                        await Client.SendTextMessageAsync(chatId, "Bye. Have a good day!");
                        break;
                }
            }
        }

        private static async Task getCallDetails(ITelegramBotClient botClient, string data, long chatId)
        {
            //Subject
            await botClient.SendTextMessageAsync(chatId, "Whats the subject of the call?");
            userQuestions[chatId] = "Subject";

            ////AssignedTo
            //await botClient.SendTextMessageAsync(chatId, "Whom do you want to assign this task (Name)?");

            ////Customer
            //await botClient.SendTextMessageAsync(chatId, "Whom do you want to call(Customer Name)?");

            ////Date
            //await botClient.SendTextMessageAsync(chatId, "Please enter task due date(MM/dd/yyyy)");

            ////Time
            //await botClient.SendTextMessageAsync(chatId, "Please enter task due time(hh:mm AM/PM)");

            ////Priority 
            //await botClient.SendTextMessageAsync(chatId, "Please enter priority:");




        }


        private static async Task SendMenuAsync(long chatId)
        {
            var replyKeyboardMarkup = new InlineKeyboardMarkup(new[]
            {
            new[]
            {
                InlineKeyboardButton.WithCallbackData("Schedule a call","call"),
                InlineKeyboardButton.WithCallbackData("Schedule a meeting","meeting"),
            },
            new[]
            {
                InlineKeyboardButton.WithCallbackData("Get Task Reports", "getReports"),
                InlineKeyboardButton.WithCallbackData("Contact Us", "support"),
            },
            new[]
            {               
                InlineKeyboardButton.WithCallbackData("Exit", "exit"),
            }
        });

            await Client.SendTextMessageAsync(chatId, "How can I help you?", replyMarkup: replyKeyboardMarkup);
        }

        private static async Task SendExitMenuAsync(long chatId)
        {
            var replyKeyboardMarkup = new InlineKeyboardMarkup(new[]
            {
            new[]
            {
                InlineKeyboardButton.WithCallbackData("MainMenu","back"),
                InlineKeyboardButton.WithCallbackData("Back", "getReports"),
                InlineKeyboardButton.WithCallbackData("Exit","exit"),
            }
            
        });
            await Client.SendTextMessageAsync(chatId, "Anything Else?", replyMarkup: replyKeyboardMarkup);
        }


        private static async Task SendNewReportMenuAsync(ITelegramBotClient botClient, string data, long chatId)
        {
            var replyKeyboardMarkup = new InlineKeyboardMarkup(new[]
            {
                new[]
                {
                    InlineKeyboardButton.WithCallbackData("Open Tasks","openTask"),
                    InlineKeyboardButton.WithCallbackData("Closed Tasks","closeTask"),
                    InlineKeyboardButton.WithCallbackData("All Tasks","allTasks"),
                },
                new[]
                {
                    InlineKeyboardButton.WithCallbackData("High Priority Tasks","hTasks"),
                    InlineKeyboardButton.WithCallbackData("Mid Priority Tasks", "mTasks"),
                    InlineKeyboardButton.WithCallbackData("Low Priority Tasks", "lTasks"),
                },
                new[]
            {
                InlineKeyboardButton.WithCallbackData("Back", "back"),           
            }
            });

            await Client.SendTextMessageAsync(chatId, "Please select a report:", replyMarkup: replyKeyboardMarkup);
        }

        private async Task CallGetReportsApi(long chatId,string as_crit)
        {
            try
            {
                DataTable dataTable = await GetDataTableFromDatabase( chatId,as_crit);

                //FetchDataAsync("http://192.168.201.163:5018/api/cact/GetTaskReportList?");
                if (dataTable != null)
                {
                    byte[] excelFile = CreateExcelFile(dataTable);

                    // Save the Excel file to disk
                    string filePath = Path.Combine(Path.GetTempPath(), "task_reports.csv");

                    File.WriteAllBytes(filePath, excelFile);

                    // Send the Excel file
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Open))
                    {
                        await Client.SendDocumentAsync(chatId, new Telegram.Bot.Types.InputFiles.InputOnlineFile(fileStream, "task_reports.csv"));
                    }

                    File.Delete(filePath);
                    await SendExitMenuAsync(chatId);
                }
                else
                {
                    await Client.SendTextMessageAsync(chatId, "Failed to fetch task reports. Please try again later.");
                }
            }
            catch (Exception ex)
            {
                await Client.SendTextMessageAsync(chatId, $"Error: {ex.Message}");
            }
        }


        private async Task<DataTable> GetDataTableFromDatabase(long chatId, string as_crit)
            {
            string connectionString = "Data Source = 192.168.200.33; Initial Catalog = jewel_rchain_crm;User ID=sa;Password=diaspark@123";
            string query = "SELECT  [Task #] = a.trans_no, [Task Date] = FORMAT(a.trans_dt, 'MM/dd/yyyy'), [Type] = cactmssetupdtl.value , [Subject] = a.subject, [Status] = CASE WHEN a.status_flag = 'C' THEN 'Close' WHEN a.status_flag = 'O' THEN 'Open' END,[Priority] = CASE WHEN trim(a.priority) = 'H' THEN 'HIGH' WHEN trim(a.priority) = 'M' THEN 'Medium' WHEN trim(a.priority) = 'L' THEN 'Low' END, [Customer] = b.company_name, [Salesperson] = sp.name, [Assigned By] = temp.name, [Assigned To]= sysmmsuser.name , [Due Date] =  FORMAT(a.start_date, 'MM/dd/yyyy') ";
                query += " FROM cacttrtask a left join cactmssetupdtl on cactmssetupdtl.id = a.task_type AND cactmssetupdtl.trans_flag = 'A' AND Cactmssetupdtl.Setup_id = 'TASKTYPE' LEFT OUTER JOIN cactmsaccount b ON a.prospect_id = b.id LEFT OUTER JOIN saoimssalesperson sp  ON b.salesperson_cd = sp.id LEFT OUTER JOIN cactmscontact c ON a.prospect_id = c.id AND a.contact_id = c.serial_no left join sysmmsuser on a.assigned_to = g_user_cd AND sysmmsuser.trans_flag='A' left join sysmmsuser as temp on a.assigned_by = temp.g_user_cd AND sysmmsuser.trans_flag='A' ";
            string as_whereClause = "";

            await Client.SendTextMessageAsync(chatId, "Please wait until report get genereated");
            switch (as_crit)
            {
                case "openTask":
                    as_whereClause = " WHERE a.status_flag = 'O' ";                    
                    break;

                case "closeTask":
                    as_whereClause = " WHERE a.status_flag = 'C' ";
                    break;

                case "hTasks":
                    as_whereClause = " WHERE a.priority = 'H' ";
                    break;
                case "mTasks":
                    as_whereClause = " WHERE a.priority = 'M' ";
                    break;

                case "lTasks":
                    as_whereClause = " WHERE a.priority = 'L' ";
                    break;
                default:
                    as_whereClause = " ";
                    break;                
            }

            query += as_whereClause;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        DataTable dataTable = new DataTable();
                                             
                        adapter.FillSchema(dataTable, SchemaType.Source);
                        adapter.Fill(dataTable);
                        return dataTable;
                    }
                }
            }
        }

        public static async Task<string> FetchDataAsync(string apiUrl)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync(apiUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        return await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Failed to fetch data. Status code: {response.StatusCode}");
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error fetching data: {ex.Message}");
                return null;
            }
        }

        private byte[] CreateExcelFile(DataTable dataTable)
        {
            var csvContent = new StringBuilder();

            // Write column names
            foreach (DataColumn column in dataTable.Columns)
            {
                csvContent.Append(column.ColumnName);
                csvContent.Append(",");
            }
            csvContent.AppendLine();

            // Write data rows
            foreach (DataRow row in dataTable.Rows)
            {
                foreach (var item in row.ItemArray)
                {
                    csvContent.Append(item.ToString());
                    csvContent.Append(",");
                }
                csvContent.AppendLine();
            }

            // Convert CSV content to byte array
            return Encoding.UTF8.GetBytes(csvContent.ToString());
        }

        public async Task<DataTable> GetDataTableFromDatabaseForTask()
        {
            string connectionString = "Data Source = 192.168.200.33; Initial Catalog = jewel_rchain_crm;User ID=sa;Password=diaspark@123";
            string query = "SELECT TOP 1 * FROM cacttrtask";

            //using (SqlConnection connection = new SqlConnection(connectionString))
            //{
            //    await connection.OpenAsync();

            //    using (SqlCommand command = new SqlCommand(query, connection))
            //    {
            //        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
            //        {

            //adapter.FillSchema(taskdataTable, SchemaType.Source);
            //adapter.Fill(taskdataTable);
            try
            {
                taskdataTable = DML.Select($@"select * from cacttrtask 
                                                                where company_id = '{CompanyDbData.companyId}' 
                                                                and trans_bk = ''", dbConnection);

                System.IO.File.WriteAllText("d:\\t.txt", string.Join(", ", taskdataTable.Columns.OfType<DataColumn>().Select(t => t.ColumnName)));
            }
            catch(Exception ex)
            {
                System.IO.File.WriteAllText("d:\\t.txt", ex.ToString());
            }
                        taskDataRow = taskdataTable.NewRow();
                        taskDataRow["company_id"] = "DS02";
                        taskDataRow["priority"] = "L";
                        taskDataRow["trans_bk"] = "CMTS";
                        taskDataRow["trans_no"] = GenerateDocNo("CMTAS");
                        taskDataRow["trans_dt"] = DateTime.Today;
                        taskDataRow["prospect_id"] = "";
                        taskDataRow["contact_id"] = "";
                        taskDataRow["task_type"] = "";
                        taskDataRow["remarks"] = "";
                        taskDataRow["salesperson_cd"] = "";
                        taskDataRow["start_date"] = DateTime.Today;
                        taskDataRow["start_hour"] = DateTime.Now.Hour;
                        taskDataRow["start_minute"] = DateTime.Now.Minute;
                        taskDataRow["start_time_format"] = DateTime.Now.ToString("tt"); ;
                        taskDataRow["start_time_zone"] = "EST";
                        taskDataRow["duration_day"] = 0;
                        taskDataRow["duration_hour"] = 0;
                        taskDataRow["duration_minute"] = 0;
                        taskDataRow["end_date"] = DBNull.Value;
                        taskDataRow["end_hour"] = 0;
                        taskDataRow["end_minute"] = 0;
                        taskDataRow["end_time_format"] = "";
                        taskDataRow["close_date"] = DBNull.Value;
                        taskDataRow["close_hour"] = 0;
                        taskDataRow["close_minute"] = 0;
                        taskDataRow["close_time_format"] = "";
                        taskDataRow["close_time_zone"] = "EST";
                        taskDataRow["closing_remarks"] = "";
                        taskDataRow["user_cd"] = "1008";
                        taskDataRow["update_dt"] = "";
                        taskDataRow["update_flag"] = "Y";
                        taskDataRow["trans_flag"] = "A";
                        taskDataRow["subject"] = "";
                        taskDataRow["assigned_to"] = "";
                        taskDataRow["assigned_by"] = "1008";
                        taskDataRow["post_flag"] = "";
                        taskDataRow["status_flag"] = "O";
                        taskDataRow["status"] = "";
                        taskDataRow["activity_trans_no"] = GenerateDocNo("CMACT");

                        return taskdataTable;
            //        }
            //    }
            //}
        }

        private string GenerateDocNo(string as_doc_type)
        {

            string connectionString = "Data Source=192.168.200.33;Initial Catalog=jewel_rchain_crm;User ID=sa;Password=diaspark@123";
            string procedureName = "sperp_generate_document_no";
            string parameter1Value = as_doc_type;
            string parameter2Value = "DS02"; 
            string parameter3Value = "Y";

            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string commandText = "EXEC " + procedureName + " @param1, @param2, @param3";
                    using (SqlCommand command = new SqlCommand(commandText, connection))
                    {
                        command.Parameters.Add(new SqlParameter("@param1", SqlDbType.VarChar) { Value = parameter1Value });
                        command.Parameters.Add(new SqlParameter("@param2", SqlDbType.VarChar) { Value = parameter2Value });
                        command.Parameters.Add(new SqlParameter("@param3", SqlDbType.VarChar) { Value = parameter3Value });
                        command.CommandType = CommandType.Text;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                return reader[0].ToString();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            return "";

        }

        private void AddTableRowsToTask()
        {
            taskDataRow = taskdataTable.NewRow();


        }


        public async Task ErrorMessage(ITelegramBotClient botClient, Exception e, CancellationToken cancellationToken)
        {
            if (e is ApiRequestException apiRequestException)
            {
                await botClient.SendTextMessageAsync("", e.Message.ToString());
            }
        }

    }
}

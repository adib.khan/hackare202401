﻿using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text.Json;

namespace MagentoRestApi
{
    public class Magento
    {
        private RestClient Client { get; set; }
        private string Token { get; set; }

        public Magento(string magentoUrl, string userName, string passWord)
        {
            magentoUrl = "https://m2stage.royalchain.com";  //Sunil 210824
            userName = "diaspark";      //Sunil 210824
            passWord = "XesEFURHPeZqRq9Y4JxC";      //Sunil 210824

            Client = new RestClient(magentoUrl);
            Token = GetAdminToken(userName, passWord);
        }

        public string GetAdminToken(string userName, string passWord)
        {
            var request = CreateRequest("/rest/V1/integration/admin/token", Method.POST);

            //var request = CreateRequest("/rest/V1/integration/customer/token", Method.POST);
            //var user = new Credentials();
            //user.username = userName;
            //user.password = passWord;

            //HttpWebRequest.Credentials = new NetworkCredential("My Parse App Id", "My Parse REST API Key");


            string json = JsonConvert.SerializeObject("user", Formatting.Indented);

            request.AddParameter("application/json", json, ParameterType.RequestBody);

            var response = Client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Content;
            }
            else
            {
                return "";
            }
        }

        private RestRequest CreateRequest(string endPoint, Method method)
        {
            var request = new RestRequest(endPoint, method);
            request.RequestFormat = DataFormat.Json;
            return request;
        }

        public string CreateCategory(string categoryName)
        {
            var request = CreateRequest("/rest/V1/categories", Method.POST, Token);
            var cat = new ProductCategory();
            var category = new Category();
            category.Name = categoryName;
            cat.Category = category;

            string json = JsonConvert.SerializeObject(cat, Formatting.Indented);

            request.AddParameter("application/json", json, ParameterType.RequestBody);

            var response = Client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Content;
            }
            else
            {
                return ":( " + response.Content;
            }
        }
        public string CreateCategory(int id, int ParentId, string categoryName, bool IsActive, bool IncludeInMenu)
        {
            var request = CreateRequest("/rest/V1/categories", Method.POST, Token);
            var cat = new ProductCategory();
            var category = new Category();
            category.Id = id;
            category.ParentId = ParentId;
            category.Name = categoryName;
            category.IsActive = IsActive;
            category.IncludeInMenu = IncludeInMenu;
            cat.Category = category;

            string json = JsonConvert.SerializeObject(cat, Formatting.Indented);

            request.AddParameter("application/json", json, ParameterType.RequestBody);

            var response = Client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return response.Content;
            }
            else
            {
                return ":(" + response.Content;
            }
        }

        private RestRequest CreateRequest(string endPoint, Method method, string token)
        {
            var request = new RestRequest(endPoint, method);
            request.RequestFormat = DataFormat.Json;
            request.AddHeader("Authorization", "Bearer " + token);
            request.AddHeader("Accept", "application/json");
            return request;
        }
    }

    public class ProductCategory
    {

        [JsonProperty("category")]
        public Category Category { get; set; }
    }

    public class Category
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("parent_id")]
        public int ParentId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("is_active")]
        public bool IsActive { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("level")]
        public int Level { get; set; }

        [JsonProperty("children")]
        public string Children { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("available_sort_by")]
        public IList<string> AvailableSortBy { get; set; }

        [JsonProperty("include_in_menu")]
        public bool IncludeInMenu { get; set; }

    }

    //Sunil 210823 added start
    public class Customer
    {
        [JsonProperty("email")]
        public string email { get; set; }

        [JsonProperty("firstname")]
        public string firstname { get; set; }

        [JsonProperty("lastname")]
        public string lastname { get; set; }

        [JsonProperty("storeId")]
        public string storeId { get; set; }

        [JsonProperty("websiteId")]
        public string websiteId { get; set; }
    }
    //Sunil 210823 added End
}
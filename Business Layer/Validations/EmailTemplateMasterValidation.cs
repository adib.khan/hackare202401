﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;

namespace Cact.Bl.Validations
{
    public class EmailTemplateMasterValidation : GenValidation
    {
        public EmailTemplateMasterValidation(DbData dbData) : base(dbData){}

        public override int ValidateChild()
        {
            string code, subject;
            
            try
            {
                code = dbData.requestDataset.Tables["cactms_email_template"].Rows[0]["id"].ToString();
               
                if (string.IsNullOrEmpty(code))
                    throw new Exception("Email Template # can not be empty.");

                subject = dbData.requestDataset.Tables["cactms_email_template"].Rows[0]["subject"].ToString();

                if (string.IsNullOrEmpty(subject))
                    throw new Exception("Email subject can not be empty.");
            }
            catch (Exception e)
            {
                throw e;
            }
            return 0;
        }
    }
}

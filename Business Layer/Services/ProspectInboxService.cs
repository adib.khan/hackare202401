﻿/*********************************************************************************** 
    Copyright (c) Diaspark Inc. All Rights Reserved 
    Created Date : 2021/07/20
    Author : Sunil Agrawal
    Purpose : Transfer prospect from prospect inbox to prospect master.
    Modified Date :
    Modified By :
    Modification Purpose : 
***********************************************************************************/

using GenService.BL.Services;
using GenService.Db;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.Json;

namespace Cact.Bl.Services
{

    public class ProspectInboxService : DataSavingService
    {
        long inboxHDId;
        string newGenProsId, companyName, categoryId, isExistProspect, errorLogs = "", publicDomainAccountId, is_comments;
        string existingProspectId, busineesPhone, mainContactEmaildomain, isExistCustomer;
        DataTable inboxHd, inboxContactDtl, inboxTradeRefDtl;
        bool isPublicDomainUnique;
        string is_shipToaddress1, is_shipToaddress2, is_shipTozip, is_state, is_shipTocity;
        String is_shipToName, is_shipToConcact;
        public string ApproveProspect(JsonElement requestJson)
        {
            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();
            errorLogs = "";
            int count = 1;

            try
            {
                dbConnection.conn.Open();
                
                foreach (var currentRow in requestJson.EnumerateArray())
                {
                    sqlTransaction = dbConnection.conn.BeginTransaction();

                    if (count == 1)
                    {
                      // This Function will generate the prospectId for the first time and later will be incremented it by 1;
                       GenerateProspectId(dbConnection, sqlTransaction);
                        count++;

                    }

                    existingProspectId = currentRow.GetProperty("existing_prospect_id").ToString().Trim();
                    inboxHDId = long.Parse(currentRow.GetProperty("id").ToString().Trim());
                    categoryId = currentRow.GetProperty("category_id").ToString().Trim();

                    isExistProspect = currentRow.GetProperty("existing_prospect").ToString().Trim();
                    isExistCustomer = currentRow.GetProperty("existing_customer").ToString().Trim();

                    if (isExistCustomer == "Y") //If customer exist then stop
                    {
                        throw new Exception("Customer : " + existingProspectId + " exist based on phone or email domain, please open customer master and update directly from there.");
                    }

                    inboxHd = DML.Select("select * from cactaccountinboxhd where id ='" + inboxHDId + "'", dbConnection, sqlTransaction);
                    inboxContactDtl = DML.Select("select * from cactaccountinboxcontacts where id ='" + inboxHDId + "'", dbConnection, sqlTransaction);
                    inboxTradeRefDtl = DML.Select("select * from cactaccountinboxtraderef where id ='" + inboxHDId + "'", dbConnection, sqlTransaction);

                    companyName = inboxHd.Rows[0]["company_name"].ToString().Trim();
                    busineesPhone = inboxHd.Rows[0]["business_phone"].ToString().Trim();
                    is_comments = inboxHd.Rows[0]["comments"].ToString().Trim();
                    // Jay 20221104
                    is_shipToaddress1 = inboxHd.Rows[0]["shipToaddress1"].ToString().Trim();
                    is_shipToaddress2 = inboxHd.Rows[0]["shipToaddress2"].ToString().Trim();
                    is_shipTozip = inboxHd.Rows[0]["shipTozip"].ToString().Trim();
                    is_shipTocity = inboxHd.Rows[0]["shipTocity"].ToString().Trim();
                    is_state = inboxHd.Rows[0]["shipTostate"].ToString().Trim();

                    // Jay 20221104 : end
                    // is_comments = currentRow.GetProperty("comments"].ToString().Trim();// Jay 20220809
                    var response = ValidateCA(dbConnection, sqlTransaction);        //CA entry validation check

                    //Check credit application filled with public domain of e-mail and business phone # matches in existing system.
                    foreach (DataRow dataRow in inboxContactDtl.Rows)
                    {
                        if (dataRow["contact_type"].ToString().Trim() == "MC")
                        {
                            string mcEmailfrInbx = dataRow["email"].ToString().Trim();
                            string mcEmailDomainfrInbx = mcEmailfrInbx.Substring(mcEmailfrInbx.IndexOf("@") + 1);

                            isPublicDomainUnique = DML.Select("cactmssetupdtl", " WHERE setup_id = 'PUBLIC_DOMAIN' AND trans_flag = 'A' AND  UPPER(RTRIM(value)) = '" + mcEmailDomainfrInbx + "'", dbConnection, sqlTransaction);
                        }
                    }

                    if (string.IsNullOrEmpty(response))     //CA entry validation passed
                    {
                        if (isExistProspect == "Y")
                        {
                            if (! isPublicDomainUnique)     //If this is public domain
                            {
                                var resultDT = DML.Select("select id from cactmsaccount where phone ='" + busineesPhone + "'", dbConnection, sqlTransaction);
                                publicDomainAccountId = resultDT.Rows[0]["id"].ToString();

                                // Order of Execution Should not be changed (Setting Some class level Variable)
                                DML.Update(InboxHd(publicDomainAccountId), dbConnection, sqlTransaction);
                                DML.Update(InboxHdSummary(publicDomainAccountId), dbConnection, sqlTransaction);

                                DML.Delete("delete from cactmscontact where id = '" + publicDomainAccountId + "' ", dbConnection, sqlTransaction);
                                DML.Insert(InboxContactDtl(), dbConnection, sqlTransaction);

                                DML.Delete("delete from cactmstraderef where id = '" + publicDomainAccountId + "' ", dbConnection, sqlTransaction);
                                DML.Insert(InboxTradeRefDtl(), dbConnection, sqlTransaction);
                            }

                            else
                            {
                                // Order of Execution Should not be changed (Setting Some class level Variable)
                                DML.Update(InboxHd(existingProspectId), dbConnection, sqlTransaction);
                                DML.Update(InboxHdSummary(existingProspectId), dbConnection, sqlTransaction);

                                DML.Delete("delete from cactmscontact where id = '" + existingProspectId + "' ", dbConnection, sqlTransaction);
                                DML.Insert(InboxContactDtl(), dbConnection, sqlTransaction);

                                DML.Delete("delete from cactmstraderef where id = '" + existingProspectId + "' ", dbConnection, sqlTransaction);
                                DML.Insert(InboxTradeRefDtl(), dbConnection, sqlTransaction);
                            } 
                        }
                        else
                        {
                            DML.Insert(InboxHd(existingProspectId), dbConnection, sqlTransaction);
                            DML.Insert(InboxHdSummary(existingProspectId), dbConnection, sqlTransaction);

                            DML.Insert(InboxContactDtl(), dbConnection, sqlTransaction);
                            DML.Insert(InboxTradeRefDtl(), dbConnection, sqlTransaction);
                        }

                        DML.Insert(InboxNotesDtl(dbConnection, sqlTransaction), dbConnection, sqlTransaction); //Jay 20220908
                        DML.Insert(InboxShipToDtl(dbConnection, sqlTransaction), dbConnection, sqlTransaction); //Jay 20221104

                        DML.Update("UPDATE cactaccountinboxhd SET trfr_flag = 'P' WHERE id = '" + inboxHDId + "'", dbConnection, sqlTransaction);
                        errorLogs += "\n\n Prospect Approved Succesfully for company name : '" + companyName + "'";
                        
                        // Increment Prospect # by 1
                        newGenProsId = newGenProsId[0] + (int.Parse(newGenProsId.Substring(1)) + 1).ToString();
                    }
                    else
                    {
                        errorLogs += " \n\n Error while Approving prospect for  company name : '" + companyName + "'  \n" + response;
                        throw new Exception(errorLogs);
                    }
                    sqlTransaction.Commit();
                }
               return errorLogs;

            }

            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();
                throw new Exception(e.Message);
            }

            finally
            {
                dbConnection.conn.Close();
            }
        }

        public string InboxHd(string accountId)
        {
            string mainContactFName = "", mainContactPhone = "", mainContactEmail = "", accountsPayableFName = "", accountsPayablePhone = "", accountsPayableEmail = "", marketingManagerFName = "";
            string marketingManagerPhone = "", marketingManagerEmail = "", firstName = "", lastName = "", sql, mainContactLName = "";

            busineesPhone = inboxHd.Rows[0]["business_phone"].ToString().Trim();

            // Copying Contact dtl data in header
            foreach (DataRow dr in inboxContactDtl.Rows)
            {
                if (dr["contact_type"].ToString().Trim() == "MC")
                {
                    mainContactFName = dr["first_name"].ToString().Trim();
                    mainContactLName = dr["last_name"].ToString().Trim();
                    mainContactPhone = dr["phone"].ToString().Trim();
                    mainContactEmail = dr["email"].ToString().Trim();

                    firstName = mainContactFName;
                    lastName = mainContactLName;
                    is_shipToName = firstName + " " +  lastName; //Jay 20221104
                    is_shipToConcact = mainContactPhone; //Jay 20221104
                    mainContactEmaildomain = inboxHd.Rows[0]["comp_email_address"].ToString().Trim();
                    mainContactEmaildomain = mainContactEmaildomain.Substring(mainContactEmaildomain.IndexOf("@") + 1);
                }
                else if (dr["contact_type"].ToString().Trim() == "AP")
                {
                    accountsPayableFName = dr["first_name"].ToString().Trim();
                    accountsPayablePhone = dr["phone"].ToString().Trim();
                    accountsPayableEmail = dr["email"].ToString().Trim();
                }
                else if (dr["contact_type"].ToString().Trim() == "MM")
                {
                    marketingManagerFName = dr["first_name"].ToString().Trim();
                    marketingManagerPhone = dr["phone"].ToString().Trim();
                    marketingManagerEmail = dr["email"].ToString().Trim();
                }
            }

            if (isExistProspect == "Y")
            {
                //Jay 20230306 : Add inbox_flag, inbox_id
                sql = "UPDATE C " +
                        " SET " +
                        " C.company_name = A.company_name, C.category_id = ISNULL(A.category_id, C.category_id)," +
                        " C.business_name = A.trade_name, C.address1 = A.address, C.address2 = A.address2, C.zip = A.zip, " +
                        " C.contact1 = '" + mainContactFName + "', C.contact1_phone = '" + mainContactPhone + "', C.email = '" + mainContactEmail + "'," +
                        " C.contact2 = '" + accountsPayableFName + "', C.contact2_phone = '" + accountsPayablePhone + "', C.contact2_email = '" + accountsPayableEmail + "', " +
                        " C.contact3 = '" + marketingManagerFName + "', C.contact3_phone = '" + marketingManagerPhone + "', C.contact3_email = '" + marketingManagerEmail + "', " +
                        " C.first_name = '" + firstName + "', C.last_name = '" + lastName + "', C.city = A.city, " +
                        " C.state = A.state, C.fax = A.fax, " +
                        " C.business_email = A.comp_email_address, C.website = A.website, C.how_we_met = A.how_we_met, " +
                        " C.request_catalog = A.request_catalog, C.no_of_years = A.no_of_years, C.credit_type = A.credit_type, " +
                        " C.ein_no = A.jbt_number, C.jbt_credit_amount = A.jbt_credit_amount, C.bank_institution_name = A.bank_institution_name, " +
                        " C.bank_account_number = A.bank_account_numer, C.bank_city = A.bank_city, C.bank_State = A.bank_state, " +
                        " C.bank_phone = A.bank_phone, C.bank_fax = A.bank_fax, C.bank_officer = A.bank_officer, " +
                        " C.resale_certificate_no = A.resale_certificate_no, C.usa_patriot_act = A.usa_patriot_act, C.ca_submit_date = A.ca_submit_date," +
                        " C.ca_applicant_name = A.ca_applicant_name, C.company_id = '" + CompanyDbData.companyId + "', C.user_cd = '" + CompanyDbData.userCd + "'," +
                        " C.update_dt = GETDATE(), C.update_flag = 'Y', C.trans_flag = 'A', C.customer_flag = 'N', " +
                        " C.print_yn = 'N', C.transferred_to_tov = 'N', C.fax_yn = 'N', C.email_yn = 'N', C.so_partial_ship_flag = 'N', C.credit_approval_stage = 'CARECEIVED', C.current_stage = 'CREDIT_APP', C.phone = a.business_phone, c.inbox_flag = 'Y', c.inbox_id ='"+ inboxHDId+ "'" +
                        " " +
                        " FROM cactaccountinboxhd A" +
                        " LEFT JOIN cactaccountinboxcontacts B ON" +
                        " A.id = B.id" +
                        " JOIN cactmsaccount C ON" +
                        " upper(rtrim(a.business_phone)) = rtrim(c.phone)" +
                        " OR " +
                        " ( " +
                        " lower(rtrim(substring(b.email, charindex('@',b.email) + 1, len(b.email)))) = " +
                        " lower(rtrim(substring(c.email, charindex('@',c.email) + 1, len(c.email))))" +
                        " AND B.contact_type = 'MC'" +
                        " ) " +
                        " WHERE (A.business_phone = '" + busineesPhone + "' OR" +
                        " lower(rtrim(substring(b.email, charindex('@',b.email) + 1, len(b.email)))) = '" + mainContactEmaildomain + "')" +
                        " AND A.trfr_flag = 'A'" +
                        " AND A.id = '"+ inboxHDId+ "'" +
                        " AND C.id = '" + accountId + "'" +
                        " AND B.contact_type = 'MC' ";
            }
            else
            {
                //Jay 20230306 : Add inbox_flag, inbox_id
                //Jay 20230616 : add country = US Hardcoded as per Sambhrant Requirement
                sql = "INSERT INTO cactmsaccount " +
                    "( id, company_name, category_id, business_name, address1, address2, zip, " +
                    " contact1, contact1_phone, email, contact2, contact2_phone," +
                    " contact2_email,contact3, contact3_phone, contact3_email," +
                    " first_name, last_name," +
                    " city, state, phone, fax, business_email, " +
                    " website, how_we_met, request_catalog, no_of_years,credit_type, " +
                    " ein_no, jbt_credit_amount, bank_institution_name,bank_account_number,how_we_met_specify," +
                    " bank_city, bank_State, bank_phone, bank_fax, bank_officer," +
                    " resale_certificate_no, usa_patriot_act, ca_submit_date, ca_applicant_name," +
                    " company_id, user_cd ,update_dt, update_flag, trans_flag, customer_flag, " +
                    " print_yn, fax_yn, email_yn, so_partial_ship_flag , " +
                    " transferred_to_tov, created_fr, credit_approval_stage, current_stage, created_by, prospect_owner,  start_dt , inbox_flag, inbox_id,country " +
                    ")" +
                    "select " +
                    " '" + newGenProsId + "', a.company_name, '" + categoryId + "', a.trade_name, a.address, a.address2, a.zip," +
                    " '" + mainContactFName + "','" + mainContactPhone + "','" + mainContactEmail + "', '" + accountsPayableFName + "','" + accountsPayablePhone + "'," +
                    " '" + accountsPayableEmail + "','" + marketingManagerFName + "','" + marketingManagerPhone + "', '" + marketingManagerEmail + "'," +
                    " '" + firstName + "','" + lastName + "'," +
                    " a.city, a.state, a.business_phone, a.fax, a.comp_email_address," +
                    " a.website,a.how_we_met, a.request_catalog, a.no_of_years, a.credit_type," +
                    " a.jbt_number,a.jbt_credit_amount, a.bank_institution_name, a.bank_account_numer,how_we_met_specify," +
                    " a.bank_city, a.bank_state, a.bank_phone, a.bank_fax, a.bank_officer," +
                    " a.resale_certificate_no, a.usa_patriot_act, a.ca_submit_date, a.ca_applicant_name, " +
                    " a.company_id, '" + CompanyDbData.userCd + "' , '" + DateTime.Now + "' ,'Y','A','N', " +
                    " 'N','N','N','N', " +
                    " 'N', 'CA', 'CARECEIVED', 'CREDIT_APP', '" + CompanyDbData.userCd+"', '"+CompanyDbData.userCd+"', '"+DateTime.Today+"' , 'Y','"+inboxHDId+"' , 'US'" +
                    "from cactaccountinboxhd a " +
                    " where a.id = '" + inboxHDId + "'" +
                    "and  ISNULL(A.trfr_flag,'A') = 'A'" +
                    "and  A.trans_flag = 'A'";
            }

            return sql;
        }

        public string InboxHdSummary(string accountId)
        {
            string sql;

            if (isExistProspect == "Y")
            {

                sql = "UPDATE D  "+
                    "SET D.sm_facebook = A.facebook, D.sm_instagram = a.instagram, D.bg_sjo = ISNULL(A.buying_group1, 'N'), D.bg_ijo = ISNULL(A.buying_group2, 'N'), "+	
                    "D.bg_rjo = ISNULL(A.buying_group3, 'N'), D.bg_cbg = ISNULL(A.buying_group4, 'N'), "+
	                "D.bg_prime = ISNULL(A.buying_group5, 'N'), D.bg_ljg = ISNULL(A.buying_group6, 'N'), D.bg_big = ISNULL(A.buying_group7, 'N'), "+
	                "D.billing_via_rjo_sjo = NULL,D.user_cd = '', D.update_dt = getdate() "+
                    "FROM cactmsaccountsummary D "+
                    "JOIN cactaccountinboxhd A ON "+
                    "D.account_id = '"+ accountId + "'" +
                    " WHERE A.ID = '"+ inboxHDId + "'; ";
            }
            else
            {
                sql =   "INSERT INTO cactmsaccountsummary" +
                        "(" +
                        " account_id, ta_centurion_scottsdale, ta_select_tuscon, ta_select_dallas, ta_select_foxwoods, ta_select_dc," +
                        " ta_jck_tuscon, ta_rjo_shows, ta_ijo_shows, ta_jany_spring, ta_jany_summer, ta_centurion_arizona, " +
                        " ta_centurion_south_beach, ta_atlanta_spring, ta_atlanta_fall, ta_jck_las_vegas, ta_luxury_las_vegas, ta_couture, " +
                        " ta_montreal_expo, tra_ags, tra_agta, tra_ja, tra_jvc, tra_mjsa, " +
                        " tra_snag, tra_naja, tra_jbt, tra_wja, tra_aaa, tra_dca, " +
                        " tra_maja, bench_jeweler_onsite, pc_bridal, pc_branded, pc_fashion, " +
                        " pc_watches, pc_custom, pc_silver, user_cd, update_dt, update_flag, " +
                        " company_id, trans_flag, sm_twitter, LinkedIn, " +
                        " thinkspace, punchmark, gemfind_jewelcloud, avalon_solutions_jewel_exchange, clevergem," +
                        " price_list, web_data, social_media_images, web_banner," +
                        " printed_ads, bg_sjo, bg_ijo, bg_rjo, " +
                        " bg_cbg, bg_prime, bg_ljg, bg_big ,sm_facebook, sm_instagram, billing_via_rjo_sjo" +
                        ")" +
                        "SELECT " +
                        " '"+newGenProsId+"', 'N', 'N', 'N', 'N', 'N'," +
                        " 'N', 'N', 'N', 'N', 'N', 'N', " +
                        " 'N', 'N', 'N', 'N', 'N', 'N', " +
                        " 'N', 'N', 'N', 'N', 'N', 'N', " +
                        " 'N', 'N', 'N', 'N', 'N', 'N', " +
                        " 'N', 'N', 'N', 'N', 'N', " +
                        " 'N', 'N', 'N', '"+CompanyDbData.userCd+"', getdate(), 'Y', " +
                        " '"+CompanyDbData.companyId+"', 'A', '', '', " +
                        " 'N', 'N', 'N', 'N', 'N', " +
                        " 'N', 'N', 'N', 'N'," +
                        " 'N', CASE WHEN ISNULL(a.buying_group1,'N') <> 'N' THEN 'Y' ELSE 'N' END,   CASE WHEN ISNULL(a.buying_group2,'N') <> 'N' THEN 'Y' ELSE 'N' END,   CASE WHEN ISNULL(a.buying_group3,'N') <> 'N' THEN 'Y' ELSE 'N' END,  " +
                        " CASE WHEN ISNULL(a.buying_group4,'N') <> 'N' THEN 'Y' ELSE 'N' END,   CASE WHEN ISNULL(a.buying_group5,'N') <> 'N' THEN 'Y' ELSE 'N' END,   CASE WHEN ISNULL(a.buying_group6,'N') <> 'N' THEN 'Y' ELSE 'N' END,   CASE WHEN ISNULL(a.buying_group7,'N') <> 'N' THEN 'Y' ELSE 'N' END , a.facebook, a.instagram, NULL " +
                         " FROM cactaccountinboxhd a " +
                        " where a.id = '" + inboxHDId + "'" +
                        " and  ISNULL(A.trfr_flag,'A') = 'A' " +
                        " and  A.trans_flag = 'A' ";
            }
            return sql;
        }
        
        public string InboxShipToDtl(DbConnection dbConnection, SqlTransaction sqlTransaction) //Jay 20221104
        {
           
            string sql, contactDtlProspectId, ls_shipSerial = "";
            if (isExistProspect == "Y")
            {
                contactDtlProspectId = existingProspectId;
            }
            else
            {
                contactDtlProspectId = newGenProsId;
            }


            sql = "Select ISNULL(max(serial_no),100)  as max_Serial_no " +
                    "from cactmsprospectship where id = '" + contactDtlProspectId + "'";

            ls_shipSerial = (int.Parse(DML.Select(sql, dbConnection, sqlTransaction).Rows[0]["max_serial_no"].ToString()) + 1).ToString();
           
            sql = " Insert Into cactmsprospectship ( serial_no, id, name,contact1, address1,address2,city,state,zip, user_cd, update_dt, " +

            " update_flag, default_flag, company_id, trans_flag)" +
                    " Select '" + ls_shipSerial + "' ,'" + contactDtlProspectId + "', '" + is_shipToName + "', '"+ is_shipToConcact + "', '" + is_shipToaddress1 + "', '" + is_shipToaddress2 + "', '"+ is_shipTocity + "', '"+ is_state + "', '"+ is_shipTozip + "', '" + CompanyDbData.userCd + "', '" + DateTime.Now + "'," +
                    " 'Y', 'N', '" + CompanyDbData.companyId + "',  'A'  ";

            return sql;

        }

        public string InboxNotesDtl(DbConnection dbConnection, SqlTransaction sqlTransaction) //Jay 20220908 
        {
            string sql, contactDtlProspectId, ls_notesSerial = "";
            if (isExistProspect == "Y")
            {
                contactDtlProspectId = existingProspectId;
            }
            else
            {
                contactDtlProspectId = newGenProsId;
            }


            sql = "Select ISNULL(max(serial_no),100)  as max_Serial_no " +
                    "from cactmsaccountnotes where account_id = '" + contactDtlProspectId + "'";

            ls_notesSerial = (int.Parse(DML.Select(sql, dbConnection, sqlTransaction).Rows[0]["max_serial_no"].ToString() ) + 1 ).ToString();
           
            sql = " Insert Into cactmsaccountnotes ( serial_no, account_id, notes, user_cd, update_dt, " +
                    " update_flag, company_id, trans_flag, notes_type, notes_by )"  +
                    " Select '" + ls_notesSerial + "' ,'"  + contactDtlProspectId + "', '" + is_comments + "', '" + CompanyDbData.userCd + "', '" + DateTime.Now + "'," +
                    " 'Y', '" + CompanyDbData.companyId + "',  'A' , 'CS' , 'CUSTOMER' "; 

            return sql;

        }

        public string InboxContactDtl()
        {
            string sql, contactDtlProspectId;
            if (isExistProspect == "Y")
            {
                contactDtlProspectId = existingProspectId;
            }
            else
            {
                contactDtlProspectId = newGenProsId;
            }

            sql = "Insert into cactmscontact " +
                     "( " +
                     " id, serial_no, contact_type, first_name, " +
                     " last_name , email_id, phone1, user_cd , company_id, " +
                     " update_dt, update_flag, trans_flag, subscribed_to_emails " +
                     ") " +
                     " Select '" + contactDtlProspectId + "', a.serial_no, a.contact_type,  a.first_name, " +
                     " a.last_name, a.email, a.phone, '" + CompanyDbData.userCd + "', '" + CompanyDbData.companyId + "', " +
                     " '" + DateTime.Now + "', 'Y', 'A', " +
                     " Case When rtrim(contact_type) = 'MC' Then b.subscribe_to_email Else Null End " +
                     " from cactaccountinboxcontacts a " +
                     " Join cactaccountinboxhd b on" +
                     " a.id = b.id" +
                     " where" +
                     " a.id = '" + inboxHDId + "' and " +
                     " a.trans_flag = 'A'";


            return sql;

        }

        public string InboxTradeRefDtl()
        {
            string sql, tradeRefProspectId;

            if (isExistProspect == "Y")
            {
                tradeRefProspectId = existingProspectId;
            }
            else
            {
                tradeRefProspectId = newGenProsId;
            }

            sql = "INSERT INTO cactmstraderef" +
                    "(" +
                    " id, serial_no, company_name, person_to_contact, email, " +
                    " phone, user_cd, company_id, update_dt, update_flag, trans_flag" +
                    ")" +
                    "SELECT '" + tradeRefProspectId + "', a.serial_no, a.company_name, a.person_to_contact, a.email, " +
                    " a.phone, '" + CompanyDbData.userCd + "', '" + CompanyDbData.companyId + "', GETDATE(), 'Y', 'A' " +
                    " from cactaccountinboxtraderef a " +
                    " where id = '" + inboxHDId + "' " +
                    " and trans_flag = 'A'"; ;

            return sql;
        }


        public string ValidateCA(DbConnection dbConnection, SqlTransaction sqlTransaction)
        {
            string companyName, mainContactEmail = "", errorLog = "";
            Boolean  isExist = false;
            int pos;

            companyName = inboxHd.Rows[0]["company_name"].ToString().Trim();

            if (string.IsNullOrEmpty(companyName))
                errorLog += " \n * Company name is required ";

            foreach (DataRow dr in inboxContactDtl.Rows)
            {
                if (string.IsNullOrEmpty(dr["contact_type"].ToString()))
                {
                    errorLog += "\n Department can not be empty ";
                }

                if ((dr["contact_type"].ToString() == "MC") && (!string.IsNullOrEmpty(dr["email"].ToString())) && (!string.IsNullOrEmpty(dr["first_name"].ToString())))
                    isExist = true;

                if ((dr["contact_type"].ToString() == "MC") && (!string.IsNullOrEmpty(dr["email"].ToString())))
                    mainContactEmail = dr["email"].ToString();
            }

            if (isExist == false)
                errorLog += "\n * Main contact's name and email is required ";

            pos = mainContactEmail.IndexOf("@");

            if (pos <= 0)
            {
                errorLog += " \n * Main contact's email is not valid.";
            }
           
            return errorLog;
        }

        public void HoldProspect(JsonElement requestJson)
        {
            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();

            try
            {
                dbConnection.conn.Open();
                sqlTransaction = dbConnection.conn.BeginTransaction();

                var dtl = requestJson.EnumerateArray();
                foreach (var currentRow in dtl)
                {
                    inboxHDId = long.Parse(currentRow.GetProperty("id").ToString().Trim());
                    DML.Update("UPDATE cactaccountinboxhd SET trfr_flag = 'H' WHERE id = '" + inboxHDId + "'", dbConnection, sqlTransaction);
                }
                sqlTransaction.Commit();
            }

            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                throw e;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        public void ActiveProspect(JsonElement requestJson)
        {

            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();
            try
            {

                dbConnection.conn.Open();
                sqlTransaction = dbConnection.conn.BeginTransaction();

                var dtl = requestJson.EnumerateArray();

                foreach (var currentRow in dtl)
                {
                    inboxHDId = long.Parse(currentRow.GetProperty("id").ToString().Trim());
                    DML.Update("UPDATE cactaccountinboxhd SET trfr_flag = 'A' WHERE id = '" + inboxHDId + "'", dbConnection, sqlTransaction);
                }
                sqlTransaction.Commit();
            }

            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                throw e;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        //Sunil 211003 start
        public void DeleteProspect(JsonElement requestJson)
        {

            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();
            try
            {

                dbConnection.conn.Open();
                sqlTransaction = dbConnection.conn.BeginTransaction();

                var dtl = requestJson.EnumerateArray();
                foreach (var currentRow in dtl)
                {
                    inboxHDId = long.Parse(currentRow.GetProperty("id").ToString().Trim());
                    DML.Update("UPDATE cactaccountinboxhd SET trfr_flag = 'D' WHERE id = '" + inboxHDId + "'", dbConnection, sqlTransaction);
                }
                sqlTransaction.Commit();
            }

            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                throw e;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }
        //Sunil 211003 End

        public void GenerateProspectId(DbConnection dbConnection, SqlTransaction sqlTransaction)
        {
            string sql, fieldValue, genCustomerSerialNo, whereClause, prospectNo;
            int length;

            whereClause = " where  module_id = 'FAAR' " + " AND field_id = 'GENC'";
            fieldValue = (string)DML.Select("isnull(field_value,'N')", "gnini", whereClause, dbConnection, sqlTransaction);

            whereClause = " where  module_id = 'FAAR' " + " AND field_id = 'GENC_SERIAL'";
            genCustomerSerialNo = (string)DML.Select("isnull(field_value,'N')", "gnini", whereClause, dbConnection, sqlTransaction);

            length = fieldValue.Length;

            sql = "SELECT  Cast(MAX(CAST(SUBSTRING( id," + (length + 1) + ", LEN( id)) as int)) + 1 as varchar(10)) " +
                  " as  prospect_no  FROM cactmsaccount";
            whereClause = " WHERE  LEFT (id," + (length) + " ) = '" + fieldValue + "' AND " +
                           " ISNUMERIC(SUBSTRING( id, " + (length + 1) + ", LEN(id))) = 1 ";
            sql += whereClause;

            var result = DML.Select(sql, dbConnection,sqlTransaction);
            prospectNo = result.Rows[0]["prospect_no"].ToString();

            if (string.IsNullOrEmpty(prospectNo)){
                prospectNo = genCustomerSerialNo;
            }

            newGenProsId = (fieldValue + prospectNo).Trim();
        }

    }
}
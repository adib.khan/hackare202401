﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/22
Author : Sahil Gupta
Purpose : This class is reponsible for handling incoming request for SalesPerson Master
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/


using Cact.BL.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace Cact.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesPersonController : DataSavingController
    {
        public SalesPersonController()
        {
            service = new SalesPersonService();
        }

        [HttpPost]
        public IActionResult Save(JsonElement requestJson)
        {
            return Saving(requestJson);
        }
    }
}
﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;
using System.Data;

namespace Cact.Bl.Validation
{
    internal class SetupMasterValidation : GenValidation
    {
        public SetupMasterValidation(DbData dbData) : base(dbData){}

        public override int ValidateChild()
        {
            string setUpId, id;

            try
            {
                setUpId = dbData.requestDataset.Tables["cactmssetuphd"].Rows[0]["setup_id"].ToString();

                if (string.IsNullOrEmpty(setUpId))
                    throw new Exception("Setup # can not be empty in header.");

                if (dbData.requestDataset.Tables.Contains("cactmssetupdtl"))
                    if (dbData.requestDataset.Tables["cactmssetupdtl"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmssetupdtl"].Rows)
                        {
                            id = dr["id"].ToString();

                            if (string.IsNullOrEmpty(id))
                                throw new Exception("Id can not be empty in detail.");

                            dr["setup_id"] = setUpId;
                        }
            }

            catch (Exception e)
            {
                throw e;
            }

            return 0;
        }
    }
}
﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;
using System.Data;

namespace Cact.Bl.Validation
{
    internal class StageMasterValidation : GenValidation
    {
        public StageMasterValidation(DbData dbData) : base(dbData){}

        public override int ValidateChild()
        {
            string stageId, subStageId, parentWorkflow;

            try
            {
                stageId = dbData.requestDataset.Tables["cactmsstagehd"].Rows[0]["stage_id"].ToString();
                parentWorkflow = dbData.requestDataset.Tables["cactmsstagehd"].Rows[0]["parent_stage"].ToString();

                if (string.IsNullOrEmpty(stageId))
                    throw new Exception("Stage # can not be empty.");

                if (dbData.requestDataset.Tables.Contains("cactmsstagedtl"))
                    if (dbData.requestDataset.Tables["cactmsstagedtl"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmsstagedtl"].Rows)
                        {
                            subStageId = dr["substage_id"].ToString();

                            if (string.IsNullOrEmpty(subStageId))
                                throw new Exception("Sub stage # can not be empty.");

                            dr["stage_id"] = stageId;
                        }

                if (string.IsNullOrEmpty(parentWorkflow))
                    dbData.requestDataset.Tables["cactmsstagehd"].Rows[0]["parent_stage"] = stageId;
            }

            catch (Exception e)
            {
                throw e;
            }
            return 0;
        }
    }
}
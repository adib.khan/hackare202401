﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Gen.Controllers;
using Microsoft.AspNetCore.Mvc;
using Cact.Bl.Services;
using System.Text.Json;
using Microsoft.AspNetCore.Cors;

namespace Cact.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActionMasterController : DataSavingController
    {
        public ActionMasterController()
        {
            service = new ActionMasterService();
        }

        [EnableCors]
        [HttpPost]
        public IActionResult Save(JsonElement requestJson)
        {
            return Saving(requestJson);
        }
    }
}
﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.Db;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
namespace Cact.BL.Services
{
    public class FileUtilityService
	{
		DbConnection dbconnection;
		public FileUtilityService(string as_companyName) : base()
		{
			 dbconnection = new DbConnection(as_companyName);
		}

		public string Upload(dynamic Files)
		{

			IFormFile input_file = null;
			String ls_fileName = "", ls_SavePath, ls_temp = "";

			dbconnection.conn.Open();

			try
			{
				ls_SavePath = Convert.ToString(DML.Select("field_value", "gnini", " where field_id =  'FILE_UPLOAD_PATH' AND MODULE_ID = 'CACT'", dbconnection));
				ls_temp = ls_SavePath;
				if (string.IsNullOrEmpty(ls_SavePath))
				{
					throw new Exception("Path is not define in setting");
				}
				foreach (IFormFile files in Files)
				{
					input_file = files;
					if (input_file != null)
					{
						ls_fileName = input_file.FileName;
						ls_SavePath = Path.Combine(@ls_SavePath, ls_fileName);
						if (System.IO.File.Exists(ls_SavePath))
						{
							string filePathWithoutExt = Path.ChangeExtension(ls_fileName, null);
							ls_fileName = filePathWithoutExt + "_" + DateTime.Now.ToString("yyMMddhhmmss") + Path.GetExtension(ls_fileName);
							//ls_SavePath = Path.Combine(@"D:\temp\upload", ls_fileName);
							ls_SavePath = Path.Combine(@ls_temp, ls_fileName);
						}

						using (var stream = new FileStream(ls_SavePath, FileMode.Create))
							input_file.CopyTo(stream);
					}

				}
				return ls_fileName;
			}
			catch(Exception e)
            {
				throw e;
            }
			finally
            {
				dbconnection.conn.Close();
            }
		}
		//file://192.168.200.232/upload/701381.pdf
		public string GetFilePath(string as_fileName)
		{


			String ls_SavePath;

			dbconnection.conn.Open();

			try
			{
				ls_SavePath = Convert.ToString(DML.Select("field_value", "gnini", " where field_id =  'FILE_UPLOAD_PATH' AND MODULE_ID = 'CACT'", dbconnection));
				if (string.IsNullOrEmpty(ls_SavePath))
					throw new Exception("Path is not define in setting");

				ls_SavePath = Path.Combine(ls_SavePath, as_fileName);
				//ls_SavePath += "/" + as_fileName;
				if (!System.IO.File.Exists(ls_SavePath))
				{
					throw new Exception("File not found");
				}
				//file://192.168.200.232/upload/701381.pdf
				//ls_SavePath = "file://192.168.200.232/upload/701381.pdf" + sdf;
				ls_SavePath = "file://" + ls_SavePath;
				return ls_SavePath;
				//return (System.IO.File(fs, "application/octet-stream", as_fileName));

			}
			catch (Exception e)
			{
				throw e;
			}
			finally
			{
				dbconnection.conn.Close();
			}
		}

		public FileStream Download(string as_fileName)
		{

			
			String  ls_SavePath;

			dbconnection.conn.Open();

			try
			{
				ls_SavePath = Convert.ToString(DML.Select("field_value", "gnini", " where field_id =  'FILE_UPLOAD_PATH' AND MODULE_ID = 'CACT'", dbconnection));
				if (string.IsNullOrEmpty(ls_SavePath))
					throw new Exception("Path is not define in setting");

								ls_SavePath = Path.Combine(ls_SavePath, as_fileName);
				//ls_SavePath += "/" + as_fileName;
				if (!System.IO.File.Exists(ls_SavePath))
				{
					throw new Exception("File not found");
				}

				FileStream fs = new FileStream(ls_SavePath, FileMode.Open);

				return fs;
				//return (System.IO.File(fs, "application/octet-stream", as_fileName));

			}
			catch (Exception e)
			{
				throw e;
			}
			finally
			{
				dbconnection.conn.Close();
			}
		}
	}
}

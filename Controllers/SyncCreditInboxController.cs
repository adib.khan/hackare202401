﻿using Cact.Bl.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json;

namespace Cact.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class SyncCreditInboxController : DataSavingController
    {
        public SyncCreditInboxController()
        {
            service = new SyncCreditInboxService();
        }

        //[EnableCors]
        //[HttpPost]
        //public IActionResult Sync(JsonElement requestJson)
        //{
        //    try
        //    {
        //        SetCompany();

        //        SyncCreditInboxService syncCreditInboxService = (SyncCreditInboxService)service;
        //        var responseLog = syncCreditInboxService.Sync(requestJson);
        //        return Ok(responseLog);
        //    }
        //    catch (Exception e)
        //    {
        //        return BadRequest(e.Message);
        //    }
        //}

        [EnableCors]
        [HttpPost]
        public IActionResult Sync()
        {
            try
            {
                SetCompany();
                SyncCreditInboxService service = new SyncCreditInboxService();
                service.GetAwsData(Request.Headers["companyName"]);
                return Ok();
                //JsonElement requestJson = null;
                //service.GetAwsData(Request.Headers["companyName"]);
                //var responseLog = service.Sync(requestJson);
                //return Ok(responseLog);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        //Jay 20231027
        [EnableCors]
        [HttpPost]
        public IActionResult Syncca()
        {
            try
            {
                SetCompany();
                SyncCreditInboxService service = new SyncCreditInboxService();
                JsonElement requestJson;
                requestJson = service.GetAwsData(Request.Headers["companyName"]);
                var responseLog = service.Sync(requestJson);
                return Ok(responseLog);

                //return Ok();
                //JsonElement requestJson = null;
                //service.GetAwsData(Request.Headers["companyName"]);
                //var responseLog = service.Sync(requestJson);
                //return Ok(responseLog);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
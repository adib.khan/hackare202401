﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;

namespace Cact.Bl.Validations
{
    public class ActionMasterValidation : GenValidation
    {
        public ActionMasterValidation(DbData dbData) : base(dbData){}

        public override int ValidateChild()
        {
            string code;
            
            try
            {
                code = dbData.requestDataset.Tables["cactmsaction"].Rows[0]["code"].ToString();
               
                if (string.IsNullOrEmpty(code))
                    throw new Exception("Action code can not be empty.");
                //throw new Exception(e.Message);
            }
            catch (Exception e)
            {
                throw e;
            }
            return 0;
        }
    }
}

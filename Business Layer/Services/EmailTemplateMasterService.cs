﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Validations;
using GenService.BL.Services;
using GenService.Db;

namespace Cact.Bl.Services
{ 
    public class EmailTemplateMasterService : DataSavingService
    {
       public EmailTemplateMasterService() : base()
        {
            validation = new EmailTemplateMasterValidation(dbData);
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("cactms_email_template", "dwc_master", true, false, "id = {id}");
        }

        public override void PreSaveChild()
        {
            //base.PreSaveChild();
        }

        public override void PostSaveChild()
        {
            //base.PostSaveChild();
        }
    }
}

﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Cact.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class MagentoIntegrationController : DataSavingController
    {

        public MagentoIntegrationController()
        {
            service = new MagentoIntegrationService();
        }

        [EnableCors]
        [HttpGet("{id}")]
        public string GetCustomer(String id)
        {
                MagentoIntegrationService magentoIntegrationService = (MagentoIntegrationService)service;
                string responseLog = magentoIntegrationService.GetCustomer(id);
                return responseLog;
        }

        [HttpPost]
        public IActionResult GetAdminToken()
        {
            try
            {
                MagentoIntegrationService magentoIntegrationService = (MagentoIntegrationService)service;
                var responseLog = magentoIntegrationService.GetAdminToken();
                return Ok(responseLog);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public IActionResult CreateCustomer()
        {
            try
            {
                MagentoIntegrationService magentoIntegrationService = (MagentoIntegrationService)service;
                var responseLog = magentoIntegrationService.CreateCustomer();
                return Ok(responseLog);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
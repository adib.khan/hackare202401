﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/08/11
Author : Akshay Malviya
Purpose : Prospect Workflow Controller
***********************************************************************************/
using Gen.Controllers;
using Microsoft.AspNetCore.Mvc;
using Cact.BL.Services;
using System.Text.Json;
using System;
using Microsoft.AspNetCore.Cors;

namespace Cact.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProspectWorkflowController : DataSavingController
    {
        public ProspectWorkflowController() { }

        [EnableCors]
        [HttpPost]
        public IActionResult save(JsonElement requestJson) 
        {
            try
            {
                SetCompany();
                ValidateSession();
                ProspectWorkflowService prospectWorkflowService = new ProspectWorkflowService();
                prospectWorkflowService.updateStage(requestJson);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
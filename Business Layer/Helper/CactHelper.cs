﻿using GenService.Db;
using System;
using System.Data;

namespace Cact.BL.Helper
{
	public class CactHelper : CactGenHelper
	{
		public CactHelper(string as_dbName) : base(as_dbName)
		{

		}


		public DataTable GetUserSalesByCategory(int sales_year, string user_id)
		{
			is_sql = " Select ";
			if (sales_year == 0)
			{
				is_sql += " sum(isnull(a.ytd_sales,0)) as sales ";
			}
			else if (sales_year == 1)
			{
				is_sql += " sum(isnull(a.last_year_sales,0)) as sales ";
			}
			else if (sales_year == 2)
			{
				is_sql += " sum(isnull(a.sales_2years_ago,0)) as sales ";
			}
			else if (sales_year == 3)
			{
				is_sql += " sum(isnull(a.sales_3years_ago,0)) as sales ";
			}

			is_sql += " ,b.category, c.name,  count(b.category) as category_count ";
			is_sql += $@" from cactmsaccountsummary a 
						join faarmscustomer b 
						on a.account_id = b.id 
						join faarmscustomercategory c 
						on b.category = c.category
						 where ((isnull(b.category, '') <> '' )
						  and ('{user_id}' = case when '{user_id}' = '' then '' end or b.service_rep = '{user_id}')) 
						group by b.category , c.name";

			is_sql = is_sql.Replace("\r", "");
			is_sql = is_sql.Replace("\n", "");
			is_sql = is_sql.Replace("\t", "");

			return Execute();
		}

		public DataTable GetCustomerCount(string user_id)

		{

			is_sql = $@" select count(*) as count from  faarmscustomer 								
						WHERE   ('{user_id}' = case when '{user_id}' = '' then '' end or service_rep = '{user_id}')  ";


			return Execute();
		}

		public DataTable GetCustomerwithBO(string user_id)

		{

			is_sql = $@" select count(*) as count from  faarmscustomer									
						WHERE   ((back_order = 'Y') and ('{user_id}' = case when '{user_id}' = '' then '' end or service_rep = '{user_id}'))  ";


			return Execute();
		}

		public DataTable GetTotalTaskCount(string user_id)

		{

			is_sql = $@" select count(*) as count from  cacttrtask 								
						WHERE   ('{user_id}' = case when '{user_id}' = '' then '' end or assigned_to = '{user_id}')  ";


			return Execute();
		}

		public DataTable GetOpenTaskCount(string user_id)

		{

			is_sql = $@" select count(*) as count from  cacttrtask 								
						WHERE   ((status_flag = 'O') and ('{user_id}' = case when '{user_id}' = '' then '' end or assigned_to = '{user_id}'))  ";


			return Execute();
		}

		public DataTable GetClosedTaskCount(string user_id)

		{

			is_sql = $@" select count(*) as count from  cacttrtask 								
						WHERE   ((status_flag = 'C') and ('{user_id}' = case when '{user_id}' = '' then '' end or assigned_to = '{user_id}'))  ";


			return Execute();
		}

		//  public DataTable GetCompletedTask(string user_id)

		//  {

		//      is_sql = $@" select count(*) as count from  cacttrtask 								
		//WHERE   ((status_flag = 'C') and ('{user_id}' = case when '{user_id}' = '' then '' end or assigned_to = '{user_id}'))  ";


		//      return Execute();
		//  }

		public DataTable GetCustReqCatalog(string user_id)

		{

			is_sql = $@" select count(*) as count from  faarmscustomer 								
						WHERE   ((request_catalog = 'Y') and ('{user_id}' = case when '{user_id}' = '' then '' end or service_rep = '{user_id}'))  ";


			return Execute();
		}


		public DataTable GetAccountMgmtCount(string user_id)

		{

			is_sql = $@" select count(*) as count from  cactmsaccountsummary a 
					     join faarmscustomer b  on a.account_id = b.id 
						where ((a.price_list = 'Y' OR a.web_data = 'Y' OR a.social_media_images = 'Y' OR a.web_banner = 'Y' OR a.printed_ads = 'Y'  ) 
						and ('{user_id}' = case when '{user_id}' = '' then '' end or b.service_rep = '{user_id}'))";

			return Execute();
		}

		public DataTable GetTaskTypeCount(string user_id)

		{

			is_sql = $@" select count(*) as count, task_type from  cacttrtask 								
						where  ((status_flag= 'O') and 
						('{user_id}' = case when '{user_id}' = '' then '' end or assigned_to = '{user_id}')) 
						group by task_type ";


			return Execute();
		}

		public DataTable CheckPublicDomain(string From_Address)
		{
			string ls_domain = "", ls_account_id;

			Int64 li_count = 0;

			if (From_Address.IndexOf("@") < 0 || string.IsNullOrEmpty(From_Address))
			{
				throw new Exception("Invalid From address");
			}


			ls_domain = From_Address.Substring(From_Address.IndexOf("@") + 1);

			is_sql += $@" select count(*) as count
						from cactmssetupdtl 
						where setup_id = 'PUBLIC_DOMAIN' AND UPPER(value) ='{ls_domain}' ";


			DataTable dt = Execute();



			li_count = Convert.ToInt64(dt.Rows[0]["count"]);
			From_Address = From_Address.ToUpper();
			if (li_count > 0)
			{
				is_sql = $@" select TOP 1 ID FROM cactmscontact
						where  UPPER(email_id) = '{From_Address}' ";


			}
			else
			{
				is_sql = $@" SELECT TOP 1 ID FROM cactmscontact WHERE email_id LIKE '%{ls_domain}%' ";

			}

			OpenConnection();
			dt = Execute();

			return dt;

			//if (dt.Rows.Count > 0)
			//{
			//	ls_account_id = dt.Rows[0]["id"].ToString();
			//}
			//else
			//	ls_account_id = "";

			//return ls_account_id;



		}


		public DataTable GetCustomerProspectMergeList(string company_id, string company_name, string category_id, string tov_customer_id,

					string salesperson_cd, string contact_name, string business_phone, string customer_flag, string trans_flag, string current_stage,

					string credit_approval_stage, string send_to_tov, string prospect_source, string created_by, string prospect_owner, string remarks)





		{



			is_sql = $@" exec spcact_cust_prospect_mergelist '{company_id}', '{company_name}', '{category_id}', '{tov_customer_id}' ,'{salesperson_cd}', 

                            '{contact_name}' ,'{business_phone}', '{customer_flag}', '{trans_flag}', '{current_stage}', '{credit_approval_stage}', 

                            '{send_to_tov}', '{prospect_source}' ,'{created_by}', '{prospect_owner}', '{remarks}'";





			return Execute();

		}

		public DataTable GetModuleSettingList()
		{

			is_sql = $@" select field_name,field_id,field_value from gnini where module_id = 'CACT' ";

			return Execute();

		}

		public DataTable GetLookupSubStageList(string id)
		{

			is_sql = $@" SELECT substage_id as option_value, substage_name  as option_title  
					FROM cactmsstagedtl 
					where('{id}' = case when '{id}' = '' then '' end or stage_id = '{id}')
			
						 ORDER BY sequence_no";

			return Execute();

		}

		public DataTable GetShipViaList(string id, string name, string shippers_code)
		{

			is_sql = $@" SELECT shipvia_id, shipvia_name, company_id, user_cd, trans_flag, 
						 update_flag, update_dt, charge_shipping, 
						 shippers_code, shipper FROM saoimsshipvia
						 where ('{id}' = case when '{id}' = '' then '' end or  shipvia_id = '{id}')
							and ('{name}' = case when '{name}' = '' then '' end or  shipvia_name = '{name}')
							and ('{shippers_code}' = case when '{shippers_code}' = '' then '' end or  shippers_code = '{shippers_code}')
						";
			is_sql = is_sql.Replace("\r", "");
			is_sql = is_sql.Replace("\n", "");
			is_sql = is_sql.Replace("\t", "");

			return Execute();

		}

		public DataTable GetLookupCustomerCategory(string category)
		{

			is_sql = $@" SELECT category  as option_value, name as option_title  
						 FROM faarmscustomercategory 
						  WHERE trans_flag = 'A' 
						  And ('{category}' = case when '{category}' = '' then '' end or  category = '{category}')
						 ORDER BY category

						";


			return Execute();

		}

		public DataTable GetLookupSubStageAllList(string id)
		{

			is_sql = $@"SELECT B.substage_id as option_value, 
						A.stage_name+' :- '+B.substage_name as option_title 
						FROM cactmsstagehd A 
						LEFT JOIN cactmsstagedtl B 
						ON A.stage_id = B.stage_id
						 WHERE A.trans_flag = 'A' AND B.trans_flag = 'A' 
						 ORDER BY CASE A.stage_id WHEN 'PROSPECT_STAGE' THEN 1 WHEN 'CREDIT_APPROVAL' THEN 2 ELSE 3 END, B.sequence_no

					";
			is_sql = is_sql.Replace("\r", "");
			is_sql = is_sql.Replace("\n", "");
			is_sql = is_sql.Replace("\t", "");

			return Execute();

		}


		public DataTable GetLookupCountry(string id)
		{

			is_sql = $@" SELECT RTRIM(country_cd)  as  option_value, RTRIM(country_nm) as option_title  
						 FROM  utilmscountry 
						  where('{id}' = case when '{id}' = '' then '' end or  country_cd = '{id}')
						 ORDER BY country_nm ";

			return Execute();

		}

		public DataTable GetLookupCustomerCategory()
		{

			is_sql = $@" SELECT category  as option_value, name as option_title  
						 FROM faarmscustomercategory WHERE trans_flag = 'A'ORDER BY category";

			return Execute();

		}

		public DataTable GetLookupAction()
		{

			is_sql = $@" SELECT code as option_value, name as option_title 
						 FROM cactmsaction WHERE trans_flag = 'A'";

			return Execute();

		}

		public DataTable GetLookupEmail()
		{

			is_sql = $@" SELECT DISTINCT user_group as option_title,user_group as option_value 
						 from cactms_email_template where user_group is not null";

			return Execute();

		}

		public DataTable GetLookupGroup()
		{

			is_sql = $@" SELECT saoimsgroup.group_id as option_value, 
						saoimsgroup.group_name as option_title 
						FROM saoimsgroup ";

			return Execute();

		}

		public DataTable GetLookupSalesPerson()
		{

			//is_sql = $"Select ID as option_value ,  name as option_title  " +
			//		 $"From  ( SELECT id = g_user_cd, name " +
			//						 $"FROM sysmmsuser  " +
			//						 $"where trans_flag = 'A'" +
			//					 $") X " +
			//			   $"Order by id";

			//Jay 20231026 : salesperson lookup modification
			is_sql = $" select  ID as option_value ,  name as option_title  from saoimssalesperson Order by id ";
			return Execute();

		}

		public DataTable GetLookupSetup(string as_setUpId)
		{

			is_sql = $"SELECT Cactmssetupdtl.Id as option_value, " +
					  $"Cactmssetupdtl.Value as option_title " +
					  $"FROM  Cactmssetupdtl  " +
					  $" Where setup_id = '" + as_setUpId + "' " +
					  $"ORDER BY Cactmssetupdtl.sequence_no";

			return Execute();

		}

		public DataTable GetLookupUser()
		{

			is_sql = $"SELECT g_user_cd   as option_value , name as option_title " +
					 $"FROM sysmmsuser Where trans_flag = 'A'";

			return Execute();

		}

		public DataTable GetActivityList(string activity_type, string priority, string trans_no, string activity_dt, string prospect_id, string subject, string remarks, Int32 page_no, Int32 page_size, bool count)
		{

			if (count)
			{
				is_sql = $"Select Count(*) as Count ";
			}
			else
			{
				is_sql = $" SELECT cacttractivity.company_id, cacttractivity.trans_bk, cacttractivity.trans_no, " +
						$"cacttractivity.trans_dt, cacttractivity.prospect_id, cacttractivity.contact_id, " +
						$"cacttractivity.activity_type, cacttractivity.remarks, cacttractivity.activity_dt, " +
						$"cacttractivity.user_cd, cacttractivity.update_dt, cacttractivity.update_flag, " +
						$"cacttractivity.trans_flag, cacttractivity.post_flag, cacttractivity.subject, " +
						$"cacttractivity.salesperson_cd, cacttractivity.start_time, cacttractivity.task_id,  " +
						$"cacttractivity.time_zone, cacttractivity.time_format, cacttractivity.hour,  " +
						$"cacttractivity.minute, cacttractivity.ref_bk, cacttractivity.ref_no, cacttractivity.ref_dt, " +
						$"cacttractivity.priority, cactmsaccount.company_name, " +
						$"CASE WHEN rtrim(cacttractivity.priority) = 'H' THEN 'High' WHEN rtrim(cacttractivity.priority) = 'M' THEN 'Medium' WHEN rtrim(cacttractivity.priority) = 'L' THEN 'Low' END AS priority_name, " +
						$"sysmmsuser.name AS activity_by_name, cactmsaccount.address1, cactmsaccount.city, cactmsaccount.phone, cactmssetupdtl.value AS activity_type_title ";


			}
			is_sql += $"FROM cacttractivity LEFT OUTER JOIN " +
					$"cactmsaccount ON cacttractivity.prospect_id = cactmsaccount.id LEFT OUTER JOIN " +
					$"cactmssetupdtl ON cacttractivity.activity_type = cactmssetupdtl.id AND cactmssetupdtl.setup_id = 'TASKTYPE' LEFT OUTER JOIN " +
					$"sysmmsuser ON sysmmsuser.g_user_cd = cacttractivity.user_cd";

			if (!string.IsNullOrEmpty(activity_type))
			{
				is_whereClause = $"activity_type = '{activity_type}'";
			}


			//is_whereClause = $"('{activity_type}' = CASE WHEN activity_type IS NULL Then '{activity_type}' END OR activity_type = '{activity_type}' END)";

			if (!string.IsNullOrEmpty(priority))
			{
				if (!string.IsNullOrEmpty(is_whereClause))
				{
					is_whereClause += " AND " + $"priority = '{priority}'";
				}
				else is_whereClause = $"priority = '{priority}'";
			}
			if (!string.IsNullOrEmpty(trans_no))
			{
				if (!string.IsNullOrEmpty(is_whereClause))
				{
					is_whereClause += $" AND " + $"trans_no = '{trans_no}'";
				}
				else is_whereClause = $"trans_no = '{trans_no}'";
			}
			if (!string.IsNullOrEmpty(prospect_id))
			{
				if (!string.IsNullOrEmpty(is_whereClause))
				{
					is_whereClause += $" AND " + $"prospect_id = '{prospect_id}'";
				}
				else is_whereClause = $"prospect_id = '{prospect_id}'";
			}
			if (!string.IsNullOrEmpty(activity_dt))
			{
				if (!string.IsNullOrEmpty(is_whereClause))
				{
					is_whereClause += $" AND " + $"activity_dt = '{activity_dt}'";
				}
				else is_whereClause = $"activity_dt = '{activity_dt}'";
			}

			if (!string.IsNullOrEmpty(subject))
			{
				is_whereClause1 = $"subject like '{subject}'";
			}
			if (!string.IsNullOrEmpty(remarks))
			{
				if (!string.IsNullOrEmpty(is_whereClause1))
				{
					is_whereClause1 += $" AND " + $"remarks like '%{remarks}%'";
				}
				else is_whereClause1 = $"remarks like '%{remarks}%'";
			}
			if (!string.IsNullOrEmpty(is_whereClause1))
			{
				is_whereClause1 = "(" + is_whereClause1 + ")";
				if (!string.IsNullOrEmpty(is_whereClause))
				{
					is_whereClause += " AND " + is_whereClause1;
				}
				else
				{
					is_whereClause = is_whereClause1;
				}

			}

			if (!string.IsNullOrEmpty(is_whereClause))
			{
				is_sql += " WHERE " + is_whereClause;
			}
			//if (!string.IsNullOrEmpty(page_no.ToString()) && !string.IsNullOrEmpty(page_size.ToString()))
			if (page_no != 0 && page_size != 0)
			{

				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cacttractivity.activity_dt ) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}
			else if (!count)
			{
				is_sql += $" ORDER BY cacttractivity.activity_dt desc";
			}
			//throw new Exception(is_sql);
			return Execute();

		}

		public DataTable GetActivityContact(string as_transNo, string as_transBk, DateTime as_transDt, string as_companyId)
		{

			is_sql = $"SELECT cacttractivitycontacts.company_id, cacttractivitycontacts.trans_bk," +
					 $"cacttractivitycontacts.trans_no, cacttractivitycontacts.trans_dt,cacttractivitycontacts.serial_no," +
					 $"cacttractivitycontacts.prospect_id, cacttractivitycontacts.contact_id, cacttractivitycontacts.remarks, " +
					 $"cacttractivitycontacts.user_cd, cacttractivitycontacts.update_dt, cacttractivitycontacts.update_flag," +
					 $"cacttractivitycontacts.trans_flag,cacttractivitycontacts.default_flag, cacttractivitycontacts.contact_type," +
					 $"cactmssetupdtl.value as contact_type_desc,cactmscontact.first_name,cactmscontact.last_name, " +
					 $"cactmscontact.phone1, cactmscontact.phone2,cactmscontact.email_id, cactmscontact.title " +
					 $"FROM cacttractivitycontacts " +
					 $"JOIN cactmscontact " +
					 $"ON cacttractivitycontacts.contact_id = cactmscontact.serial_no " +
					 $"AND cacttractivitycontacts.prospect_id = cactmscontact.id " +
					 $"AND cactmscontact.trans_flag = 'A' " +
					 $"left join cactmssetupdtl " +
					 $"on cactmssetupdtl.ID=cactmscontact.contact_type " +
					 $"and setup_id = 'CONTACT_TYPE' " +
					 $"Where trans_no = '{as_transNo}' AND trans_bk = '{as_transBk}' " +
					 $"AND trans_dt = '{as_transDt}' AND cacttractivitycontacts.company_id = '{as_companyId}'";


			return Execute();

		}

		public DataTable GetWorkflowList(string stage_id, string stage_name, string parent_stage, string status, Int32 page_no, Int32 page_size, bool count)
		{



			if (count)
			{
				is_sql = $@"select count(*) as Count ";
			}
			else
			{
				is_sql = $@"SELECT cactmsstagehd.stage_id ,cactmsstagehd.stage_name , 
							cactmsstagehd.company_id ,cactmsstagehd.user_cd ,
							cactmsstagehd.trans_flag ,cactmsstagehd.update_flag,
							cactmsstagehd.update_dt ,cactmsstagehd.parent_stage ";
			}
			is_sql += $@"FROM cactmsstagehd
							 where ('{stage_id}' = case when '{stage_id}' = '' then '' end or  stage_id like '%{stage_id}%')
							and ('{stage_name}' = case when '{stage_name}' = '' then '' end or  stage_name like '%{stage_name}%')
							and ('{parent_stage}' = case when '{parent_stage}' = '' then '' end or  parent_stage like '%{parent_stage}%')
							and ('{status}' = case when '{status}' = '' then '' end or  trans_flag = '{status}')
						";
			if (page_no != 0 && page_size != 0)
			{

				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL ) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			//is_sql = is_sql.Replace("\r", "");
			//is_sql = is_sql.Replace("\n", "");
			//is_sql = is_sql.Replace("\t", "");



			return Execute();

		}

		public DataTable GetWorkflowDetail(string id)
		{

			is_sql = $"SELECT  cactmsstagedtl.stage_id , cactmsstagedtl.substage_id , cactmsstagedtl.substage_name ," +
					 $"cactmsstagedtl.company_id ,cactmsstagedtl.user_cd ,cactmsstagedtl.trans_flag ," +
					 $"cactmsstagedtl.update_flag , cactmsstagedtl.update_dt ,cactmsstagedtl.sequence_no " +
					 $"FROM cactmsstagedtl " +
					 $" where('{id}' = case when '{id}' = '' then '' end or  stage_id = '{id}')";

			return Execute();

		}

		public DataTable GetLookupEmailTemplate()
		{
			is_sql = $@"select LTRIM(RTRIM(ISNULL(id,''))) as option_title, LTRIM(RTRIM(ISNULL(id, ''))) as option_value 
					 from cactms_email_template 
					 WHERE trans_flag = 'A'";

			return Execute();
		}

		public DataTable GetLookupImportCustomerList()
		{
			is_sql = $@"SELECT DISTINCT cacttrcustomerlist.list_name as option_value,cacttrcustomerlist.list_name as option_title 
					 FROM cacttrcustomerlist 
					 WHERE cacttrcustomerlist.trans_flag = 'A'";

			return Execute();
		}

		public DataTable GetLookupProspectPattern(string id, string company_name, string phone, string first_name, string zip)
		{
			is_sql = $@"select * from (select  cactmsaccount.*, CONCAT(cactmsaccount.address1,' ',cactmsaccount.address2) as address 
					 from cactmsaccount) as temp_table 
					 where ('{id}' = case when '{id}' = '' then '' end or id like '%{id}%') 
					 or ('{company_name}' = case when '{company_name}' = '' then '' end or company_name like '%{company_name}%') 
					 or ('{phone}' = case when '{phone}' = '' then '' end or phone like '%{phone}%') 
					 or ('{first_name}' = case when '{first_name}' = '' then '' end or first_name like '%{first_name}%') 
					 or('{zip}' = case when '{zip}' = '' then '' end or  zip like '%{zip}%') ";

			return Execute();
		}

		public DataTable GetLookupProspectExact(string id)
		{
			is_sql = $@"select * from (select  cactmsaccount.*, CONCAT(cactmsaccount.address1,' ',cactmsaccount.address2) as address 
					 from cactmsaccount) as temp_table 
					 where (id = '{id}') ";

			return Execute();
		}

		public DataTable GetLookupProspectContact(string id)
		{
			is_sql = $@"select * from (select cactmscontact.*, cactmssetupdtl.value as contact_type_desc from cactmscontact left join 
					 cactmssetupdtl on cactmssetupdtl.ID=cactmscontact.contact_type and setup_id = 'CONTACT_TYPE') as temp_table
					 where (id = '{id}')";

			return Execute();
		}

		public DataTable GetLookupCustomerPattern(string id, string name, string phone1, string contact1, string zip)
		{
			is_sql = $@"select * from faarmscustomer 
					 where ('{id}' = case when '{id}' = '' then '' end or id like '%{id}%') 
					 or ('{name}' = case when '{name}' = '' then '' end or name like '%{name}%') 
					 or ('{phone1}' = case when '{phone1}' = '' then '' end or phone1 like '%{phone1}%') 
					 or ('{contact1}' = case when '{contact1}' = '' then '' end or contact1 like '%{contact1}%') 
					 or('{zip}' = case when '{zip}' = '' then '' end or zip like '%{zip}%') ";

			is_sql = is_sql.Replace("\r", "");
			is_sql = is_sql.Replace("\n", "");
			is_sql = is_sql.Replace("\t", "");
			return Execute();
		}

		public DataTable GetLookupCustomerExact(string id)
		{
			is_sql = $@"select * from faarmscustomer
					 where(id = '{id}')";

			return Execute();
		}

		public DataTable GetSetupDetail(string setup_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmssetupdtl.sequence_no, cactmssetupdtl.setup_id, cactmssetupdtl.id, cactmssetupdtl.value, cactmssetupdtl.user_cd, 
					 cactmssetupdtl.company_id,cactmssetupdtl.update_flag,cactmssetupdtl.update_dt, cactmssetupdtl.trans_flag FROM cactmssetupdtl 
					 where ('{setup_id}' = case when '{setup_id}' = '' then '' end or  cactmssetupdtl.setup_id = '{setup_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cactmssetupdtl.sequence_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetSetupList(string status, string setup_id, string name, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"select count(*) as Count ";
			}
			else
			{
				is_sql = $@"SELECT cactmssetuphd.setup_id, cactmssetuphd.name, cactmssetuphd.user_cd, cactmssetuphd.update_dt, 
							update_flag = cactmssetuphd.update_flag, cactmssetuphd.company_id, cactmssetuphd.trans_flag ";
			}
			is_sql += $@"FROM cactmssetuphd 
						 where ('{status}' = case when '{status}' = '' then '' end or  trans_flag = '{status}') 
						 and ('{setup_id}' LIKE case when '{setup_id}' = '' then '' end or  setup_id like '%{setup_id}%') 
						 and ('{name}' LIKE case when '{name}' = '' then '' end or  name like '%{name}%')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL ) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();

		}

		public DataTable GetMyOpenTask(int period, string as_assigned_to)
		{

			is_sql += $@"   select trans_no, 
							Subject,  Prospect_id  = isnull((select top 1 company_name from cactmsaccount where account_id = Prospect_id) , Prospect_id), 
							assigned_to = isnull((select top 1 name from sysmmsuser where g_user_cd = assigned_to) , assigned_to) , 
							start_date as due_dt, trans_dt, status_flag, 
							Priority 
							 
							from cacttrtask where status_flag = 'O'";
			
			is_sql += $@"  and ('{as_assigned_to}' LIKE case when '{as_assigned_to}' = '' then '' end or  assigned_to like '%{as_assigned_to}%')";

			if (period == 0)
			{
				is_sql += " and CAST(start_date AS DATE) = CAST(getdate() AS DATE)";
			}
			else if (period == 1)
			{
				is_sql += " and CAST(start_date AS DATE) < CAST(getdate() AS DATE)";
			}
			else if (period == 2)
			{
				is_sql += " and CAST(start_date AS DATE) > CAST(getdate() AS DATE)";
			}
			is_sql = is_sql.Replace("\r", "");
			is_sql = is_sql.Replace("\n", "");
			is_sql = is_sql.Replace("\t", "");

			return Execute();
		}

		public DataTable GetLeadSource(string from_dt, string to_dt)
		{

			if (string.IsNullOrEmpty(from_dt))
			{
				from_dt = "01/01/1900";

			}
			if (string.IsNullOrEmpty(to_dt))
			{
				to_dt = "01/01/2999";

			}
			is_sql += $@" select how_we_met as lead_source, count(id) as prospect_id 
						 from cactmsaccount 
                         where isnull(how_we_met, '') <> ''   
						 and  start_dt between  '{from_dt}' and '{to_dt}' 
						 group by how_we_met";

			return Execute();
		}

		public DataTable GetSalesPipeLine(string from_dt, string to_dt)
		{

			if (string.IsNullOrEmpty(from_dt))
			{
				from_dt = "01/01/1900";

			}
			if (string.IsNullOrEmpty(to_dt))
			{
				to_dt = "01/01/2999";

			}
			is_sql += $@" select count(account_id) as prospect_count,current_stage 
						from cactmsaccount 
                        where start_dt between  '{from_dt}' and '{to_dt}' group by current_stage ";

			return Execute();
		}

		public DataTable GetSalesByCategory(int sales_year)
		{
			is_sql = " Select ";
			if (sales_year == 0)
			{
				is_sql += " sum(isnull(a.ytd_sales,0)) as sales ";
			}
			else if (sales_year == 1)
			{
				is_sql += " sum(isnull(a.last_year_sales,0)) as sales ";
			}
			else if (sales_year == 2)
			{
				is_sql += " sum(isnull(a.sales_2years_ago,0)) as sales ";
			}
			else if (sales_year == 3)
			{
				is_sql += " sum(isnull(a.sales_3years_ago,0)) as sales ";
			}

			is_sql += " ,b.category, c.name,  count(b.category) as category_count ";
			is_sql += $@" from cactmsaccountsummary a 
						join faarmscustomer b 
						on a.account_id = b.id 
						join faarmscustomercategory c 
						on b.category = c.category
						 where isnull(b.category, '') <> '' 
						group by b.category , c.name";

			is_sql = is_sql.Replace("\r", "");
			is_sql = is_sql.Replace("\n", "");
			is_sql = is_sql.Replace("\t", "");

			return Execute();
		}

		public DataTable GetValidateSetup(string setup_id)
		{
			is_sql = $@"SELECT count(*) as matched_record 
						from cactmssetuphd 
						where ('{setup_id}' = case when '{setup_id}' = '' then '' end or setup_id = '{setup_id}')";

			return Execute();
		}

		public DataTable GetValidateSetupDetail(string id)
		{
			is_sql = $@"SELECT count(*) as matched_record 
						from cactmssetupdtl 
						where ('{id}' = case when '{id}' = '' then '' end or id = '{id}')";

			return Execute();
		}

		public DataTable GetValidateWorkflow(string stage_id)
		{
			is_sql = $@"SELECT count(*) as matched_record 
						from cactmsstagehd 
						where ('{stage_id}' = case when '{stage_id}' = '' then '' end or stage_id = '{stage_id}')";

			return Execute();
		}

		public DataTable GetValidateSubstage(string substage_id)
		{
			is_sql = $@"SELECT count(*) as matched_record 
						from cactmsstagedtl 
						where ('{substage_id}' = case when '{substage_id}' = '' then '' end or substage_id = '{substage_id}')";

			return Execute();
		}

		public DataTable GetValidateEmail(string id)
		{
			is_sql = $@"SELECT count(*) as matched_record 
						from cactms_email_template 
						where ('{id}' = case when '{id}' = '' then '' end or id = '{id}')";

			return Execute();
		}

		public DataTable GetValidateEmailSubject(string subject)
		{
			is_sql = $@"SELECT count(*) as matched_record 
						from cactms_email_template 
						where ('{subject}' = case when '{subject}' = '' then '' end or subject = '{subject}')";

			return Execute();
		}

		public DataTable GetValidateListName(string list_name)
		{
			is_sql = $@"SELECT count(*) as matched_record 
						from cacttrcustomerlist 
						where trans_flag= 'A' 
						and ('{list_name}' = case when '{list_name}' = '' then '' end or list_name = '{list_name}')";

			return Execute();
		}

		public DataTable GetTasklistMainList(string task_type, string task_date, string prospect_id, string priority, string task_no, string subject, string remarks, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"select count(*) as Count ";
			}
			else
			{
				is_sql = $@"SELECT cast(cacttrtask.start_date as date) as  start_date, sysmmsuser.name as assigned_to_name, cactmssetupdtl.value as tasktype_name, 
                            cacttrtask.company_id, cacttrtask.priority, cacttrtask.trans_bk, cacttrtask.trans_no, cacttrtask.trans_dt, cacttrtask.prospect_id, 
                            cacttrtask.contact_id, cacttrtask.task_type, cacttrtask.remarks, cacttrtask.salesperson_cd, cacttrtask.start_hour, 
                            cacttrtask.start_minute, cacttrtask.start_time_format, cacttrtask.start_time_zone, cacttrtask.duration_day, 
                            cacttrtask.duration_hour, cacttrtask.duration_minute, cacttrtask.end_date, cacttrtask.end_hour, cacttrtask.end_minute, 
                            cacttrtask.end_time_format, cacttrtask.close_date, cacttrtask.close_hour, cacttrtask.close_minute, cacttrtask.close_time_format, 
                            cacttrtask.close_time_zone, cacttrtask.closing_remarks, cacttrtask.user_cd, cacttrtask.update_dt, cacttrtask.update_flag, 
                            cacttrtask.trans_flag, cacttrtask.subject, cacttrtask.assigned_to, cacttrtask.assigned_by, cacttrtask.post_flag, cacttrtask.status_flag, 
                            CASE WHEN cacttrtask.status_flag = 'C' THEN 'Close' WHEN cacttrtask.status_flag = 'O' THEN 'Open' END As status_flag_name, 
                            CASE WHEN rtrim(cacttrtask.priority) = 'H' THEN 'High'  WHEN rtrim(cacttrtask.priority) = 'M' THEN 'Medium' 
                            WHEN rtrim(cacttrtask.priority) = 'L' THEN 'Low' END As priority_name, cactmsaccount.company_name as prospect_name, 
                            cactmsaccount.address1,cactmsaccount.address2, CONCAT(cactmsaccount.address1,' ',cactmsaccount.address2) as address, 
                            cactmsaccount.phone, cactmsaccount.city, cactmsaccount.sales_person, cactmsaccount.salesperson_cd ";
			}



			is_sql += $@"FROM  cacttrtask left join  cactmssetupdtl on cactmssetupdtl.id = cacttrtask.task_type AND cactmssetupdtl.trans_flag = 'A' 
                         AND Cactmssetupdtl.Setup_id = 'TASKTYPE' left join cactmsaccount on cactmsaccount.id =  cacttrtask.prospect_id left join sysmmsuser on 
                         cacttrtask.assigned_to = g_user_cd AND sysmmsuser.trans_flag = 'A'

 

                         where (('{task_type}' = case when '{task_type}' = '' then '' end or task_type = '{task_type}') 
                         and ('{task_date}' = case when '{task_date}' = '' then '' end or cast(trans_dt as date) = '{task_date}') 
                         and ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or prospect_id = '{prospect_id}')) 
                         and (('{priority}' LIKE case when '{priority}' = '' then '' end or priority like '%{priority}%') 
                         and ('{task_no}' LIKE case when '{task_no}' = '' then '' end or trans_no like '%{task_no}%') 
                         and ('{subject}' LIKE case when '{subject}' = '' then '' end or subject like '%{subject}%') 
                         and ('{remarks}' LIKE case when '{remarks}' = '' then '' end or remarks like '%{remarks}%')) ";



			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL ) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}



			return Execute();



		}
		public DataTable GetTasklistContactDetail(string company_id, string trans_bk, string trans_no, string trans_dt, string setup_id, string status, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT cacttrtaskcontacts.serial_no, cactmssetupdtl.value as contact_type_desc, cacttrtaskcontacts.company_id, 
						cacttrtaskcontacts.trans_bk, cacttrtaskcontacts.trans_no, cacttrtaskcontacts.trans_dt, cacttrtaskcontacts.prospect_id, 
						cacttrtaskcontacts.contact_id, cacttrtaskcontacts.remarks, cacttrtaskcontacts.user_cd, cacttrtaskcontacts.update_dt, 
						cacttrtaskcontacts.update_flag, cacttrtaskcontacts.trans_flag, cacttrtaskcontacts.default_flag, cacttrtaskcontacts.contact_type, 
						CONCAT(cactmscontact.first_name, cactmscontact.last_name) as full_name,cactmscontact.first_name, cactmscontact.last_name, 
						cactmscontact.phone1, cactmscontact.phone2, cactmscontact.email_id, cactmscontact.title 
						FROM cacttrtaskcontacts JOIN cactmscontact ON cacttrtaskcontacts.contact_id = cactmscontact.serial_no AND 
						cacttrtaskcontacts.prospect_id = cactmscontact.id AND cactmscontact.trans_flag = '{status}' 
						left join cactmssetupdtl on cactmssetupdtl.id = cacttrtaskcontacts.contact_type 

						where ('{company_id}' = case when '{company_id}' = '' then '' end or cacttrtaskcontacts.company_id = '{company_id}') 
						and ('{trans_bk}' = case when '{trans_bk}' = '' then '' end or cacttrtaskcontacts.trans_bk = '{trans_bk}') 
						and ('{trans_no}' = case when '{trans_no}' = '' then '' end or cacttrtaskcontacts.trans_no = '{trans_no}') 
						and (('{trans_dt}' = case when '{trans_dt}' = '' then '' end or cacttrtaskcontacts.trans_dt = '{trans_dt}') 
						and ('{setup_id}' = case when '{setup_id}' = '' then '' end or cactmssetupdtl.setup_id = '{setup_id}') 
						and ('{status}' = case when '{status}' = '' then '' end or cactmssetupdtl.trans_flag = '{status}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cacttrtaskcontacts.serial_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetTasklistActionDetail(string company_id, string trans_no, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT CASE WHEN cacttrtaskactions.action_type = 'C' THEN 'Call' WHEN cacttrtaskactions.action_type = 'E' 
						THEN 'Email' WHEN cacttrtaskactions.action_type = 'M' THEN 'Mail' WHEN cacttrtaskactions.action_type = 'I' 
						THEN 'Meeting' END as action_type_name,cactmsaction.name as action_full_name,cacttrtaskactions.company_id, 
						cacttrtaskactions.trans_bk, cacttrtaskactions.trans_no, cacttrtaskactions.trans_dt, cacttrtaskactions.serial_no, 
						cacttrtaskactions.prospect_id, cacttrtaskactions.sequence_no, cacttrtaskactions.action_name, cacttrtaskactions.detail, 
						cacttrtaskactions.planned_action_dt, cacttrtaskactions.completion_dt, cacttrtaskactions.complete_yn, cacttrtaskactions.action_type, 
						cacttrtaskactions.user_cd, cacttrtaskactions.update_dt, cacttrtaskactions.update_flag, cacttrtaskactions.trans_flag 
						FROM cacttrtaskactions left join cactmsaction on cactmsaction.code = cacttrtaskactions.action_name 

						where ('{company_id}' = case when '{company_id}' = '' then '' end or cacttrtaskactions.company_id = '{company_id}') 
						and ('{trans_no}' = case when '{trans_no}' = '' then '' end or cacttrtaskactions.trans_no = '{trans_no}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetActivityInfoDetail(string activity_no, string book, string company_id, string activity_date, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cacttractivity.company_id, cacttractivity.trans_bk, cacttractivity.trans_no, cactmsaccount.company_name as company_name, 
						cactmsaccount.city as city,rtrim(cactmscontact.email_id) as email_id,cactmscontact.phone1 as phone,contact_address = 
						rtrim(cactmsaccount.address1) + rtrim(cactmsaccount.address2), contact_name=rtrim(cactmscontact.first_name) + 
						rtrim(cactmscontact.last_name), cacttractivity.trans_dt, cacttractivity.prospect_id, cacttractivity.contact_id as 
						contact_id, rtrim(cactmsaccount.salesperson_cd) as salesperson_cd,saoimssalesperson.name as salesperson_name, 
						cacttractivity.activity_type, cacttractivity.remarks, cacttractivity.activity_dt, cacttractivity.user_cd, 
						cacttractivity.update_dt, cacttractivity.update_flag, cacttractivity.trans_flag, cacttractivity.post_flag, 
						cacttractivity.subject, cacttractivity.salesperson_cd, cacttractivity.time_zone, cacttractivity.hour as hour, 
						cacttractivity.minute as minute, cacttractivity.time_format as time_format, cacttractivity.ref_bk, cacttractivity.ref_no, 
						cacttractivity.ref_dt 
						FROM cacttractivity left join cactmsaccount on cactmsaccount.ID= cacttractivity.prospect_id left join saoimssalesperson on 
						cactmsaccount.salesperson_cd=saoimssalesperson.id left join cactmscontact on cactmscontact.ID = cacttractivity.prospect_id 
						AND cactmscontact.serial_no=cacttractivity.contact_id 

						where ('{activity_no}' = case when '{activity_no}' = '' then '' end or cacttractivity.trans_no = '{activity_no}') 
						and ('{book}' = case when '{book}' = '' then '' end or cacttractivity.trans_bk = '{book}')
						and ('{company_id}' = case when '{company_id}' = '' then '' end or cacttractivity.company_id = '{company_id}')
						and ('{activity_date}' = case when '{activity_date}' = '' then '' end or cacttractivity.trans_dt = '{activity_date}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetActivityInfoAddress(string id)
		{
			is_sql = $@"SELECT cactmsaccount.id, cactmsaccount.parent_account_id, cactmsaccount.salutation, cactmsaccount.category_id, cactmsaccount.company_name, 
						cactmsaccount.primary_contact_id, cactmsaccount.relationship_type, cactmsaccount.address_type, cactmsaccount.business_territory, 
						cactmsaccount.industry, cactmsaccount.ownership_type, cactmsaccount.preferred_contact_method, cactmsaccount.preferred_contact_time, 
						cactmsaccount.preferred_contact_day, cactmsaccount.first_name, cactmsaccount.last_name, cactmsaccount.address_name, cactmsaccount.address1, 
						cactmsaccount.address2, cactmsaccount.phone, cactmsaccount.fax, cactmsaccount.email, cactmsaccount.website, cactmsaccount.city, 
						cactmsaccount.state, cactmsaccount.zip, cactmsaccount.country, cactmsaccount.ship_via, cactmsaccount.term, cactmsaccount.annual_revenue, 
						cactmsaccount.credit_limit, cactmsaccount.no_of_employees, cactmsaccount.salesperson_cd, cactmsaccount.description, 
						cactmsaccount.credit_hold_flag, cactmsaccount.allow_email, cactmsaccount.allow_phone_Call, cactmsaccount.allow_fax, cactmsaccount.allow_mail, 
						cactmsaccount.send_marketing_material, cactmsaccount.company_id, cactmsaccount.user_cd, cactmsaccount.update_dt, cactmsaccount.update_flag, 
						cactmsaccount.trans_flag, cactmsaccount.email_addresses, cactmsaccount.start_dt, cactmsaccount.allow_send_catalog, cactmsaccount.group1, 
						cactmsaccount.group2, cactmsaccount.group3, cactmsaccount.group4, cactmsaccount.group5, cactmsaccount.customer_flag, cactmsaccount.whr_we_met, 
						cactmsaccount.catalog_qty, cactmsaccount.back_order, cactmsaccount.bank_account_no, cactmsaccount.business_name, 
						cactmsaccount.cert_expiry_date, cactmsaccount.coop_per, cactmsaccount.credit_rating, cactmsaccount.cy_paypattern,cactmsaccount.discount_per, 
						cactmsaccount.dunning_yn, cactmsaccount.ein_no, cactmsaccount.email_yn, cactmsaccount.fax_yn, cactmsaccount.gold_lock_on, cactmsaccount.gold_premium, 
						cactmsaccount.guarantee_name, cactmsaccount.inv_print_no, cactmsaccount.jbt_ranking, cactmsaccount.location, cactmsaccount.memo_limit, 
						cactmsaccount.memo_print_no, cactmsaccount.message_id, cactmsaccount.passport_no, cactmsaccount.paymentpriority, cactmsaccount.platinum_premium, 
						cactmsaccount.po_unique_since, cactmsaccount.postdated_checks, cactmsaccount.price_level, cactmsaccount.price_level_diam_cert, 
						cactmsaccount.price_level_diam_lot, cactmsaccount.print_yn, cactmsaccount.py_paypattern, cactmsaccount.py2_paypattern, 
						cactmsaccount.repair_terms, cactmsaccount.return_check, cactmsaccount.sales_person, cactmsaccount.sales_tax_code, cactmsaccount.salescomm_per, 
						cactmsaccount.service_rep, cactmsaccount.shelf_invn_cap, cactmsaccount.shipper_number, cactmsaccount.silver_premium, 
						cactmsaccount.so_item_partial_ship_flag, cactmsaccount.so_partial_ship_flag, cactmsaccount.style_suffix, cactmsaccount.tax_id, 
						cactmsaccount.ten99_yn, cactmsaccount.terms, cactmsaccount.memo_terms, cactmsaccount.territory, cactmsaccount.territory2, 
						cactmsaccount.territory3, cactmsaccount.time_zone, cactmsaccount.type1, cactmsaccount.type2, cactmsaccount.contact1, cactmsaccount.contact1_phone, 
						cactmsaccount.contact2, cactmsaccount.contact2_phone, cactmsaccount.contact2_email, cactmsaccount.contact3, cactmsaccount.contact3_phone, 
						cactmsaccount.contact3_email, cactmsaccount.contact4, cactmsaccount.contact4_phone, cactmsaccount.contact4_email, cactmsaccount.cell_no, 
						cactmsaccount.phone2, cactmsaccount.how_we_met, cactmsaccount.how_we_met_specify, cactmsaccount.current_stage, created_by, created_fr, 
						current_supplier, prospect_owner, business_email, web_account_yn, web_id, subscribed_to_emails, subscription_email, credit_approval_stage, 
						bank_institution_name, bank_city, bank_state, bank_phone, bank_fax, bank_officer, resale_certificate_no, usa_patriot_act, ca_submit_date, 
						ca_applicant_name, jbt_credit_amount, credit_type, no_of_years, request_catalog, bank_account_number, transferred_to_tov, tov_status, 
						tov_customer_id FROM cactmsaccount

						where ('{id}' = case when '{id}' = '' then '' end or cactmsaccount.id = '{id}') ";

			return Execute();
		}

		public DataTable GetActivityInfoContact(string activity_no, string book, string company_id, string activity_date, string setup_id, string status, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cacttractivitycontacts.company_id, cacttractivitycontacts.trans_bk,  cacttractivitycontacts.trans_no, cacttractivitycontacts.trans_dt, 
						cacttractivitycontacts.serial_no, cacttractivitycontacts.prospect_id, cacttractivitycontacts.contact_id, cacttractivitycontacts.remarks, 
						cacttractivitycontacts.user_cd, cacttractivitycontacts.update_dt, cacttractivitycontacts.update_flag, cacttractivitycontacts.trans_flag, 
						cactmscontact.first_name as first_name,cactmscontact.last_name as last_name,cactmscontact.phone1 as phone1, cactmscontact.phone2 as phone2, 
						cactmscontact.email_id as email_id, contact_type=cactmssetupdtl.value,cactmscontact.title 
						FROM cacttractivitycontacts INNER JOIN cactmscontact ON cacttractivitycontacts.prospect_id = cactmscontact.id AND 
						cacttractivitycontacts.contact_id = cactmscontact.serial_no AND cactmscontact.trans_flag = 'A' left join cactmssetupdtl on 
						cactmssetupdtl.ID=cacttractivitycontacts.contact_type 

						where ('{activity_no}' = case when '{activity_no}' = '' then '' end or cacttractivitycontacts.trans_no = '{activity_no}') 
						and ('{book}' = case when '{book}' = '' then '' end or cacttractivitycontacts.trans_bk = '{book}')
						and ('{company_id}' = case when '{company_id}' = '' then '' end or cacttractivitycontacts.company_id = '{company_id}')
						and ('{activity_date}' = case when '{activity_date}' = '' then '' end or cacttractivitycontacts.trans_dt = '{activity_date}')
						and ('{setup_id}' = case when '{setup_id}' = '' then '' end or cactmssetupdtl.setup_id = '{setup_id}')
						and ('{status}' = case when '{status}' = '' then '' end or cactmssetupdtl.trans_flag = '{status}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		//public DataTable GetActivityInfoNotes(string account_id, Int32 page_no, Int32 page_size)
		//{
		//	is_sql = $@"SELECT cactmsaccountnotes.serial_no,cactmsaccountnotes.account_id, user_nm = sysmmsuser.name,cactmsaccountnotes.notes as notes, 
		//				cactmsaccountnotes.user_cd as user_cd, cactmsaccountnotes.update_dt as update_dt , cactmsaccountnotes.update_flag, 
		//				cactmsaccountnotes.company_id, cactmsaccountnotes.trans_flag,  Cactmssetupdtl.Value as notes_type 
		//				FROM cactmsaccountnotes left join sysmmsuser on cactmsaccountnotes.user_cd = g_user_cd AND sysmmsuser.trans_flag='A' 
		//				left join Cactmssetupdtl on cactmsaccountnotes.notes_type = Cactmssetupdtl.id and Cactmssetupdtl.trans_flag = 'A' 
		//				and Cactmssetupdtl.Setup_id = 'NOTESTYPE' 

		//				where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountnotes.account_id = '{account_id}') ";

		//	if (page_no != 0 && page_size != 0)
		//	{
		//		page_no = (page_no - 1) * page_size;
		//		is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
		//	}

		//	return Execute();
		//}


		public DataTable GetActivityInfoNotes(string account_id, Int32 page_no, Int32 page_size)

		{

			is_sql = $@"SELECT cactmsaccountnotes.serial_no,cactmsaccountnotes.account_id, user_nm = sysmmsuser.name,cactmsaccountnotes.notes as notes, 

                        cactmsaccountnotes.user_cd as user_cd, cactmsaccountnotes.update_dt as update_dt , cactmsaccountnotes.update_flag, 

                        cactmsaccountnotes.company_id, cactmsaccountnotes.trans_flag,  Cactmssetupdtl.Value as notes_type 

                        FROM cactmsaccountnotes left join sysmmsuser on cactmsaccountnotes.notes_by = g_user_cd AND sysmmsuser.trans_flag='A' 

                        left join Cactmssetupdtl on cactmsaccountnotes.notes_type = Cactmssetupdtl.id and Cactmssetupdtl.trans_flag = 'A' 

                        and Cactmssetupdtl.Setup_id = 'NOTESTYPE'

 

                        where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountnotes.account_id = '{account_id}') ";



			if (page_no != 0 && page_size != 0)

			{

				page_no = (page_no - 1) * page_size;

				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";

			}



			return Execute();

		}
		public DataTable GetActivityInfoActivity(string prospect_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT cacttractivitycontacts.prospect_id, cacttractivity.trans_bk, cacttractivity.trans_no as trans_no, 
						Cactmssetupdtl.Value as activity_type, cacttractivity.remarks as remarks, cacttractivity.subject as subject, cacttractivity.activity_dt, 
						cacttractivity.user_cd as user_cd,cacttractivity.update_dt, cacttractivity.update_flag, cacttractivity.company_id, 
						assigned_by_name = sysmmsuser.name,cacttractivity.trans_flag, cacttractivity.trans_dt as trans_dt, cacttractivity.ref_bk as ref_bk, 
						cacttractivity.ref_no as ref_no,cacttractivity.ref_dt 
						FROM cacttractivity INNER JOIN cacttractivitycontacts ON cacttractivity.company_id = cacttractivitycontacts.company_id AND 
						cacttractivity.trans_bk = cacttractivitycontacts.trans_bk AND cacttractivity.trans_no = cacttractivitycontacts.trans_no AND 
						cacttractivity.trans_dt = cacttractivitycontacts.trans_dt left join Cactmssetupdtl on Cactmssetupdtl.Id=cacttractivity.activity_type and 
						Cactmssetupdtl.Setup_id = 'TASKTYPE' and Cactmssetupdtl.trans_flag = 'A' left join sysmmsuser on cacttractivity.user_cd = g_user_cd AND 
						sysmmsuser.trans_flag='A' 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or cacttractivitycontacts.prospect_id = '{prospect_id}') ";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cacttractivitycontacts.prospect_id) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetActivityInfoMeeting(string prospect_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT cacttrmeeting.prospect_id, cacttrmeeting.trans_bk, cacttrmeeting.trans_no as trans_no, cacttrmeeting.trans_dt as trans_dt, 
						cacttrmeeting.contact_id, cacttrmeeting.remarks as remarks, cacttrmeeting.stock_asset, cacttrmeeting.delivery_asset, cacttrmeeting.stock_memo, 
						cacttrmeeting.delivery_memo, cacttrmeeting.spo, cacttrmeeting.proposal, cacttrmeeting.trade_show, cacttrmeeting.no_orders, 
						cacttrmeeting.asset_order_amt, cacttrmeeting.ra_amt, cacttrmeeting.net_amt, cacttrmeeting.memo_order_amt, cacttrmeeting.not_ready_to_order, 
						cacttrmeeting.order_placed, cacttrmeeting.call_later_in_season, cacttrmeeting.set_appt_for_future, cacttrmeeting.owner_not_available, 
						cacttrmeeting.subject as subject,cacttrmeeting.meeting_task_id,cacttrmeeting.ref_bk as ref_bk,cacttrmeeting.ref_no as ref_no, 
						cacttrmeeting.ref_dt as ref_dt,cactmscontact.first_name,Cactmscontact.last_name 
						FROM cacttrmeeting JOIN cacttrmeetingcontacts ON cacttrmeeting.company_id = cacttrmeetingcontacts.company_id AND 
						cacttrmeeting.trans_bk = cacttrmeetingcontacts.trans_bk AND cacttrmeeting.trans_no = cacttrmeetingcontacts.trans_no AND 
						cacttrmeeting.trans_dt = cacttrmeetingcontacts.trans_dt LEFT JOIN cactmscontact ON cacttrmeeting.prospect_id = cactmscontact.id AND 
						cacttrmeeting.contact_id = cactmscontact.serial_no 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or cacttrmeetingcontacts.prospect_id = '{prospect_id}') ";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cacttrmeeting.prospect_id) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetTaskDetailContact(string company_id, string book, string contact_no, string contact_date, string setup_id, string status, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT cacttrtaskcontacts.serial_no, cactmssetupdtl.value as contact_type_desc, cacttrtaskcontacts.company_id, 
						cacttrtaskcontacts.trans_bk, cacttrtaskcontacts.trans_no, cacttrtaskcontacts.trans_dt, cacttrtaskcontacts.prospect_id, 
						cacttrtaskcontacts.contact_id, cacttrtaskcontacts.remarks, cacttrtaskcontacts.user_cd, cacttrtaskcontacts.update_dt, 
						cacttrtaskcontacts.update_flag, cacttrtaskcontacts.trans_flag, cacttrtaskcontacts.default_flag, cacttrtaskcontacts.contact_type, 
						CONCAT(cactmscontact.first_name, cactmscontact.last_name) as full_name,cactmscontact.first_name, cactmscontact.last_name, 
						cactmscontact.phone1, cactmscontact.phone2, cactmscontact.email_id, cactmscontact.title 
						FROM cacttrtaskcontacts JOIN cactmscontact ON cacttrtaskcontacts.contact_id = cactmscontact.serial_no AND 
						cacttrtaskcontacts.prospect_id = cactmscontact.id AND cactmscontact.trans_flag = 'A' left join cactmssetupdtl on 
						cactmssetupdtl.id = cacttrtaskcontacts.contact_type 

						where ('{company_id}' = case when '{company_id}' = '' then '' end or cacttrtaskcontacts.company_id = '{company_id}') 
						and ('{book}' = case when '{book}' = '' then '' end or cacttrtaskcontacts.trans_bk = '{book}')
						and ('{contact_no}' = case when '{contact_no}' = '' then '' end or cacttrtaskcontacts.trans_no = '{contact_no}')
						and ('{contact_date}' = case when '{contact_date}' = '' then '' end or cacttrtaskcontacts.trans_dt = '{contact_date}')
						and ('{setup_id}' = case when '{setup_id}' = '' then '' end or cactmssetupdtl.setup_id = '{setup_id}')
						and ('{status}' = case when '{status}' = '' then '' end or cactmssetupdtl.trans_flag = '{status}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cacttrtaskcontacts.serial_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetTaskDetailAction(string company_id, string action_no, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT CASE WHEN cacttrtaskactions.action_type = 'C' THEN 'Call' WHEN cacttrtaskactions.action_type = 'E'THEN 'Email' 
						WHEN cacttrtaskactions.action_type = 'M' THEN 'Mail' WHEN cacttrtaskactions.action_type = 'I' THEN 'Meeting' END as action_type_name, 
						cactmsaction.name as action_full_name, cacttrtaskactions.company_id, cacttrtaskactions.trans_bk, cacttrtaskactions.trans_no, 
						cacttrtaskactions.trans_dt, cacttrtaskactions.serial_no, cacttrtaskactions.prospect_id, cacttrtaskactions.sequence_no, 
						cacttrtaskactions.action_name, cacttrtaskactions.detail, cacttrtaskactions.planned_action_dt, cacttrtaskactions.completion_dt, 
						cacttrtaskactions.complete_yn, cacttrtaskactions.action_type, cacttrtaskactions.user_cd, cacttrtaskactions.update_dt, 
						cacttrtaskactions.update_flag, cacttrtaskactions.trans_flag 
						FROM cacttrtaskactions left join cactmsaction on cactmsaction.code = cacttrtaskactions.action_name 

						where ('{company_id}' = case when '{company_id}' = '' then '' end or cacttrtaskactions.company_id = '{company_id}') 
						and ('{action_no}' = case when '{action_no}' = '' then '' end or cacttrtaskactions.trans_no = '{action_no}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetTaskAddress(string id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmsaccount.id, cactmsaccount.parent_account_id, cactmsaccount.how_we_met_specify, cactmsaccount.salutation, 
						cactmsaccount.category_id, cactmsaccount.company_name, cactmsaccount.primary_contact_id, cactmsaccount.relationship_type, 
						cactmsaccount.address_type, cactmsaccount.business_territory, cactmsaccount.industry,cactmsaccount.ownership_type, 
						cactmsaccount.preferred_contact_method, cactmsaccount.preferred_contact_time, cactmsaccount.preferred_contact_day, cactmsaccount.first_name, 
						cactmsaccount.last_name, cactmsaccount.address_name, address1 = cactmsaccount.address1, cactmsaccount.address2, cactmsaccount.phone, 
						fax = cactmsaccount.fax, cactmsaccount.email, cactmsaccount.website, cactmsaccount.city, cactmsaccount.state, cactmsaccount.zip, 
						cactmsaccount.country, cactmsaccount.ship_via, cactmsaccount.term, cactmsaccount.annual_revenue, cactmsaccount.credit_limit, 
						cactmsaccount.no_of_employees, cactmsaccount.salesperson_cd, cactmsaccount.description, cactmsaccount.credit_hold_flag, 
						cactmsaccount.allow_email, cactmsaccount.allow_phone_Call, cactmsaccount.allow_fax, cactmsaccount.allow_mail, cactmsaccount.send_marketing_material, 
						cactmsaccount.company_id, cactmsaccount.user_cd, cactmsaccount.update_dt, cactmsaccount.update_flag, cactmsaccount.trans_flag, 
						cactmsaccount.email_addresses, cactmsaccount.start_dt, cactmsaccount.allow_send_catalog, cactmsaccount.group1, cactmsaccount.group2, 
						cactmsaccount.group3, cactmsaccount.group4, cactmsaccount.group5, cactmsaccount.customer_flag, cactmsaccount.whr_we_met, 
						cactmsaccount.catalog_qty, cactmsaccount.back_order, cactmsaccount.bank_account_no, cactmsaccount.business_name, 
						cactmsaccount.cert_expiry_date, cactmsaccount.coop_per, cactmsaccount.credit_rating, cactmsaccount.cy_paypattern, cactmsaccount.discount_per, 
						cactmsaccount.dunning_yn, cactmsaccount.ein_no, cactmsaccount.email_yn, cactmsaccount.fax_yn, cactmsaccount.gold_lock_on, 
						cactmsaccount.gold_premium, cactmsaccount.guarantee_name, cactmsaccount.inv_print_no, cactmsaccount.jbt_ranking, cactmsaccount.location, 
						cactmsaccount.memo_limit, cactmsaccount.memo_print_no, cactmsaccount.message_id, cactmsaccount.passport_no, cactmsaccount.paymentpriority, 
						cactmsaccount.platinum_premium, cactmsaccount.po_unique_since, cactmsaccount.postdated_checks, cactmsaccount.price_level, 
						cactmsaccount.price_level_diam_cert, cactmsaccount.price_level_diam_lot, cactmsaccount.print_yn, cactmsaccount.py_paypattern, 
						cactmsaccount.py2_paypattern, cactmsaccount.repair_terms, cactmsaccount.return_check, cactmsaccount.sales_person, cactmsaccount.sales_tax_code, 
						cactmsaccount.salescomm_per, cactmsaccount.service_rep, cactmsaccount.shelf_invn_cap, cactmsaccount.shipper_number, 
						cactmsaccount.silver_premium, cactmsaccount.so_item_partial_ship_flag, cactmsaccount.so_partial_ship_flag, cactmsaccount.style_suffix, 
						cactmsaccount.tax_id, cactmsaccount.ten99_yn, cactmsaccount.terms, cactmsaccount.memo_terms, cactmsaccount.territory, 
						cactmsaccount.territory2, cactmsaccount.territory3, cactmsaccount.time_zone, cactmsaccount.type1, cactmsaccount.type2, cactmsaccount.contact1, 
						cactmsaccount.contact1_phone, cactmsaccount.contact2, cactmsaccount.contact2_phone, cactmsaccount.contact2_email, cactmsaccount.contact3, 
						cactmsaccount.contact3_phone, cactmsaccount.contact3_email, cactmsaccount.contact4, cactmsaccount.contact4_phone, cactmsaccount.contact4_email, 
						cactmsaccount.cell_no, cactmsaccount.phone2, cactmsaccount.how_we_met, cactmsaccount.current_stage, created_by, created_fr, current_supplier, 
						prospect_owner, business_email, web_account_yn, web_id, subscribed_to_emails, subscription_email, credit_approval_stage, 
						bank_institution_name, bank_city, bank_state, bank_phone, bank_fax, bank_officer, resale_certificate_no, usa_patriot_act, ca_submit_date, 
						ca_applicant_name, jbt_credit_amount, credit_type, no_of_years, request_catalog, bank_account_number, transferred_to_tov, tov_status, tov_customer_id 

						FROM cactmsaccount 
						where ('{id}' = case when '{id}' = '' then '' end or id = '{id}') ";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetTaskContact(string company_id, string book, string contact_no, string contact_date, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cacttrtaskcontacts.company_id, cacttrtaskcontacts.trans_bk, cacttrtaskcontacts.trans_no, cacttrtaskcontacts.trans_dt, 
						cacttrtaskcontacts.serial_no, cacttrtaskcontacts.prospect_id, cacttrtaskcontacts.contact_id, cacttrtaskcontacts.remarks, 
						cacttrtaskcontacts.user_cd, cacttrtaskcontacts.update_dt, cacttrtaskcontacts.update_flag, cacttrtaskcontacts.trans_flag, 
						cactmscontact.first_name, cactmscontact.last_name,  phone1 = cactmscontact.phone1, phone2 = cactmscontact.phone2, cactmscontact.email_id, 
						contact_type = cacttrtaskcontacts.contact_type, cactmscontact.title 
						FROM cacttrtaskcontacts INNER JOIN cactmscontact ON cacttrtaskcontacts.prospect_id = cactmscontact.id 
						AND cacttrtaskcontacts.contact_id = cactmscontact.serial_no 

						where ('{company_id}' = case when '{company_id}' = '' then '' end or cacttrtaskcontacts.company_id = '{company_id}') 
						and ('{book}' = case when '{book}' = '' then '' end or cacttrtaskcontacts.trans_bk = '{book}') 
						and ('{contact_no}' = case when '{contact_no}' = '' then '' end or cacttrtaskcontacts.trans_no = '{contact_no}') 
						and ('{contact_date}' = case when '{contact_date}' = '' then '' end or cacttrtaskcontacts.trans_dt = '{contact_date}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		//public DataTable GetTaskNotes(string account_id, Int32 page_no, Int32 page_size)
		//{
		//	is_sql = $@"SELECT cactmsaccountnotes.serial_no, user_nm = sysmmsuser.name, cactmsaccountnotes.account_id, cactmsaccountnotes.notes, 
		//				cactmsaccountnotes.user_cd, cactmsaccountnotes.update_dt, cactmsaccountnotes.update_flag, cactmsaccountnotes.company_id, 
		//				cactmsaccountnotes.trans_flag, cactmsaccountnotes.notes_type, Cactmssetupdtl.Value as notes_type_name   FROM cactmsaccountnotes 
		//				left join sysmmsuser on cactmsaccountnotes.user_cd = g_user_cd AND sysmmsuser.trans_flag='A' 
		//				left join Cactmssetupdtl on cactmsaccountnotes.notes_type = Cactmssetupdtl.id and Cactmssetupdtl.trans_flag = 'A' and 
		//				Cactmssetupdtl.Setup_id = 'NOTESTYPE'

		//				where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountnotes.account_id = '{account_id}') ";

		//	if (page_no != 0 && page_size != 0)
		//	{
		//		page_no = (page_no - 1) * page_size;
		//		is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
		//	}

		//	return Execute();
		//}


		public DataTable GetTaskNotes(string account_id, Int32 page_no, Int32 page_size)

		{

			is_sql = $@"SELECT cactmsaccountnotes.serial_no, user_nm = sysmmsuser.name, cactmsaccountnotes.account_id, cactmsaccountnotes.notes, 

                        cactmsaccountnotes.user_cd, cactmsaccountnotes.update_dt, cactmsaccountnotes.update_flag, cactmsaccountnotes.company_id, 

                        cactmsaccountnotes.trans_flag, cactmsaccountnotes.notes_type, Cactmssetupdtl.Value as notes_type_name,notes_by   FROM cactmsaccountnotes 

                        left join sysmmsuser on cactmsaccountnotes.notes_by = g_user_cd AND sysmmsuser.trans_flag='A' 

                        left join Cactmssetupdtl on cactmsaccountnotes.notes_type = Cactmssetupdtl.id and Cactmssetupdtl.trans_flag = 'A' and 

                        Cactmssetupdtl.Setup_id = 'NOTESTYPE'

 

                        where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountnotes.account_id = '{account_id}') ";



			if (page_no != 0 && page_size != 0)

			{

				page_no = (page_no - 1) * page_size;

				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";

			}



			return Execute();

		}
		public DataTable GetTaskActivity(string prospect_id, string status, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT cacttractivitycontacts.prospect_id, sysmmsuser.name as activity_by_name, cacttractivity.trans_bk, cacttractivity.trans_no, 
						cacttractivity.activity_type, cacttractivity.remarks, cacttractivity.subject, cacttractivity.activity_dt, cacttractivity.user_cd, 
						cacttractivity.update_dt, cacttractivity.update_flag, cacttractivity.company_id, cacttractivity.trans_flag, cacttractivity.trans_dt, 
						cacttractivity.ref_bk, cacttractivity.ref_no, cacttractivity.ref_dt,cactmssetupdtl.value as activity_type_title 
						FROM cacttractivity INNER JOIN cacttractivitycontacts ON cacttractivity.company_id = cacttractivitycontacts.company_id AND 
						cacttractivity.trans_bk = cacttractivitycontacts.trans_bk AND cacttractivity.trans_no = cacttractivitycontacts.trans_no AND 
						cacttractivity.trans_dt = cacttractivitycontacts.trans_dt  inner join cactmssetupdtl ON ( cacttractivity.activity_type = cactmssetupdtl.id 
						and cactmssetupdtl.setup_id = 'TASKTYPE' ) left join sysmmsuser on cacttractivity.user_cd = g_user_cd 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or cacttractivitycontacts.prospect_id = '{prospect_id}') 
						and ('{status}' = case when '{status}' = '' then '' end or sysmmsuser.trans_flag = '{status}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cacttractivity.trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetUserMenuPermission(string user_id)
		{
			is_sql = $@" exec spcact_get_user_menu_permissions '{user_id}', 'CACT' ";


			return Execute();
		}

		public DataTable GenerateDocumentNo(string document_type, string company_id, string default_book)

		{

			is_sql = $@" exec sperp_generate_document_no '{document_type}', '{company_id}','{default_book}' ";


			return Execute();
		}

		public DataTable GetProspectMainList(string account_id, string salesperson_cd, string category_id, string customer_flag, string status, string current_stage, string created_fr, string created_by, string prospect_owner, string transferred_to_tov, string tov_status, string contact, string phone, string description, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"select count(*) as Count from cactmsaccount ";
			}
			else
			{
				is_sql = $@"select cactmsaccount.* ,faarmscustomercategory.name as category_desc,cactmssetupdtl.value as created_fr_desc ,sysmmsuser.name as 
							propsect_owner_desc, sysmmsuser_2.name as salesperson_desc ,RTRIM(cactmssetupdtl_2.value) as service_rep_desc 
							from cactmsaccount  left join faarmscustomercategory on cactmsaccount.category_id = faarmscustomercategory.category left join 
							cactmssetupdtl on setup_id = 'PROSPECT_SOURCE' AND cactmssetupdtl.trans_flag = 'A' and created_fr= cactmssetupdtl.id left join 
							cactmssetupdtl as cactmssetupdtl_2 on cactmssetupdtl_2.setup_id = 'SERVICE_REP' AND cactmssetupdtl_2.trans_flag = 'A' and 
							cactmssetupdtl_2.id = service_rep left join  sysmmsuser on prospect_owner = sysmmsuser.g_user_cd   left join  sysmmsuser as sysmmsuser_2 
							on salesperson_cd = sysmmsuser_2.g_user_cd ";
			}
			is_sql += $@"where (('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccount.id = '{account_id}') 
						 and ('{salesperson_cd}' = case when '{salesperson_cd}' = '' then '' end or salesperson_cd = '{salesperson_cd}') 
						 and ('{category_id}' = case when '{category_id}' = '' then '' end or category_id = '{category_id}') 
						 and ('{customer_flag}' = case when '{customer_flag}' = '' then '' end or customer_flag = '{customer_flag}') 
						 and ('{status}' = case when '{status}' = '' then '' end or cactmsaccount.trans_flag = '{status}') 
						 and ('{current_stage}' = case when '{current_stage}' = '' then '' end or current_stage = '{current_stage}') 
						 and ('{created_fr}' = case when '{created_fr}' = '' then '' end or created_fr = '{created_fr}') 
						 and ('{created_by}' = case when '{created_by}' = '' then '' end or created_by = '{created_by}') 
						 and ('{prospect_owner}' = case when '{prospect_owner}' = '' then '' end or prospect_owner = '{prospect_owner}') 
						 and ('{transferred_to_tov}' = case when '{transferred_to_tov}' = '' then '' end or transferred_to_tov = '{transferred_to_tov}') 
						 and ('{tov_status}' = case when '{tov_status}' = '' then '' end or tov_status = '{tov_status}')) 
						 
						 and (('{contact}' LIKE case when '{contact}' = '' then '' end or contact1 like '%{contact}%')
						 and ('{phone}' LIKE case when '{phone}' = '' then '' end or phone like '%{phone}%') 
						 and ('{description}' LIKE case when '{description}' = '' then '' end or description like '%{description}%'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();

		}

		public DataTable GetProspectContact(string contact_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmscontact.id, cactmscontact.serial_no, cactmscontact.first_name, cactmscontact.last_name, cactmscontact.address1, 
						cactmscontact.address2, cactmscontact.city, cactmscontact.state, cactmscontact.zip, cactmscontact.country, cactmscontact.phone1, 
						cactmscontact.phone2, cactmscontact.email_id, cactmscontact.user_cd, cactmscontact.company_id, cactmscontact.update_dt, 
						cactmscontact.update_flag, cactmscontact.trans_flag, cactmscontact.title, cactmscontact.contact_photo, cactmscontact.email_id2, 
						cactmscontact.web_id, cactmscontact.subscribed_to_emails, cactmscontact.subscription_email, cactmscontact.contact_type , 
						Cactmssetupdtl.value as contact_type_desc 
						from cactmscontact inner join Cactmssetupdtl ON (cactmscontact.contact_type = Cactmssetupdtl.id and Cactmssetupdtl.setup_id = 'CONTACT_TYPE') 

						where ('{contact_id}' = case when '{contact_id}' = '' then '' end or cactmscontact.id  = '{contact_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectShipTo(string ship_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmsprospectship.id, cactmsprospectship.name, cactmsprospectship.contact1, cactmsprospectship.address1, cactmsprospectship.address2, 
						cactmsprospectship.city, cactmsprospectship.state, cactmsprospectship.zip, cactmsprospectship.country, cactmsprospectship.phone1, 
						cactmsprospectship.fax1, cactmsprospectship.user_cd, cactmsprospectship.company_id, cactmsprospectship.update_dt, cactmsprospectship.update_flag, 
						cactmsprospectship.trans_flag, cactmsprospectship.serial_no, email_id, sales_person, default_flag, cactmsprospectship.dc_id 
						FROM cactmsprospectship 

						where ('{ship_id}' = case when '{ship_id}' = '' then '' end or cactmsprospectship.id = '{ship_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		//public DataTable GetProspectNotes(string account_id, Int32 page_no, Int32 page_size)
		//{
		//	is_sql = $@"SELECT serial_no = cactmsaccountnotes.serial_no, account_id = cactmsaccountnotes.account_id, notes = cactmsaccountnotes.notes, 
		//				user_cd = cactmsaccountnotes.user_cd, update_dt = cactmsaccountnotes.update_dt, cactmsaccountnotes.update_flag, cactmsaccountnotes.company_id, 
		//				trans_flag = cactmsaccountnotes.trans_flag, notes_type = cactmsaccountnotes.notes_type, Cactmssetupdtl.value as note_type_title, 
		//				sysmmsuser.name as notes_by 
		//				FROM cactmsaccountnotes left join Cactmssetupdtl ON (cactmsaccountnotes.notes_type = Cactmssetupdtl.Id and Cactmssetupdtl.setup_id = 
		//				'NOTESTYPE' and Cactmssetupdtl.trans_flag = 'A') left join sysmmsuser ON (cactmsaccountnotes.notes_by = sysmmsuser.id) 

		//				where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountnotes.account_id = '{account_id}')";

		//	if (page_no != 0 && page_size != 0)
		//	{
		//		page_no = (page_no - 1) * page_size;
		//		is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
		//	}

		//	return Execute();
		//}



		public DataTable GetProspectNotes(string account_id, Int32 page_no, Int32 page_size)

		{

			is_sql = $@"SELECT serial_no = cactmsaccountnotes.serial_no, account_id = cactmsaccountnotes.account_id, notes = cactmsaccountnotes.notes, 

                        user_cd = cactmsaccountnotes.user_cd, update_dt = cactmsaccountnotes.update_dt, cactmsaccountnotes.update_flag, cactmsaccountnotes.company_id, 

                        trans_flag = cactmsaccountnotes.trans_flag, notes_type = cactmsaccountnotes.notes_type, Cactmssetupdtl.value as note_type_title, notes_by,

                        sysmmsuser.name as notes_by_name 

                        FROM cactmsaccountnotes left join Cactmssetupdtl ON (cactmsaccountnotes.notes_type = Cactmssetupdtl.Id and Cactmssetupdtl.setup_id = 

                        'NOTESTYPE' and Cactmssetupdtl.trans_flag = 'A') left join sysmmsuser ON (cactmsaccountnotes.notes_by = g_user_cd)

 

                        where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountnotes.account_id = '{account_id}')";



			if (page_no != 0 && page_size != 0)

			{

				page_no = (page_no - 1) * page_size;

				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";

			}



			return Execute();

		}
		public DataTable GetProspectProfile(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmsaccountsummary.account_id, cactmsaccountsummary.ta_centurion_scottsdale, cactmsaccountsummary.ta_select_tuscon, 
						cactmsaccountsummary.ta_select_dallas, cactmsaccountsummary.ta_select_foxwoods, cactmsaccountsummary.ta_select_dc, 
						cactmsaccountsummary.ta_jck_tuscon, cactmsaccountsummary.ta_rjo_shows, cactmsaccountsummary.ta_ijo_shows, cactmsaccountsummary.ta_jany_spring, 
						cactmsaccountsummary.ta_jany_summer, cactmsaccountsummary.ta_centurion_arizona, cactmsaccountsummary.ta_centurion_south_beach, 
						cactmsaccountsummary.ta_atlanta_spring, cactmsaccountsummary.ta_atlanta_fall, cactmsaccountsummary.ta_jck_las_vegas, 
						cactmsaccountsummary.ta_luxury_las_vegas, cactmsaccountsummary.ta_couture, cactmsaccountsummary.ta_montreal_expo, cactmsaccountsummary.tra_ags, 
						cactmsaccountsummary.tra_agta, cactmsaccountsummary.tra_ja, cactmsaccountsummary.tra_jvc, cactmsaccountsummary.tra_mjsa, 
						cactmsaccountsummary.tra_snag, cactmsaccountsummary.tra_naja, cactmsaccountsummary.tra_jbt, cactmsaccountsummary.tra_wja, 
						cactmsaccountsummary.tra_aaa, cactmsaccountsummary.tra_dca, cactmsaccountsummary.tra_maja, cactmsaccountsummary.bench_jeweler_onsite, 
						cactmsaccountsummary.bg_sjo, cactmsaccountsummary.bg_ijo, cactmsaccountsummary.bg_rjo, cactmsaccountsummary.bg_cbg, cactmsaccountsummary.bg_ljg, 
						cactmsaccountsummary.bg_big, cactmsaccountsummary.bg_prime, cactmsaccountsummary.pc_bridal, cactmsaccountsummary.pc_branded, 
						cactmsaccountsummary.pc_fashion, cactmsaccountsummary.pc_watches, cactmsaccountsummary.pc_custom, cactmsaccountsummary.pc_silver, 
						cactmsaccountsummary.sm_facebook, cactmsaccountsummary.sm_instagram, cactmsaccountsummary.sm_twitter,cactmsaccountsummary.user_cd, 
						cactmsaccountsummary.update_dt, cactmsaccountsummary.update_flag, cactmsaccountsummary.company_id, cactmsaccountsummary.trans_flag, 
						cactmsaccountsummary.Linkedin, thinkspace, punchmark, gemfind_jewelcloud, avalon_solutions_jewel_exchange, clevergem, tov_customer_code, 
						last_year_sales, ytd_sales, sales_2years_ago, sales_3years_ago, ptd_sales, no_of_invoices, last_pay_dt, return_per, web_data, 
						social_media_images, web_banner, price_list, printed_ads, billing_via_rjo_sjo, bill_via_sjo, last_activity, customer_bal,on_order 
						FROM cactmsaccountsummary 

						where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountsummary.account_id = '{account_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectDocument(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT id, serial_no, company_id, user_cd, update_dt, update_flag,trans_flag, file_type, file_name, subject, remark, email_to, attachment 
						FROM cactaccountdocs 

						where ('{account_id}' = case when '{account_id}' = '' then '' end or id = '{account_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectTask(string prospect_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT company_id = a.company_id, trans_bk = a.trans_bk, trans_no = a.trans_no, trans_dt = a.trans_dt, task_date = a.trans_dt, 
						task_type = a.task_type, subject = a.subject,status = case a.status_flag when 'C' then 'CLOSE'when 'O' then 'OPEN' end, 
						account_id = a.prospect_id, company_name = b.company_name, salesperson_cd = a.salesperson_cd, salesperson_name = sp.name,description = 
						a.remarks, group1 = b.group1, group2 = b.group2, group3 = b.group3, group4 = b.group4, group5 = b.group5, assigned_by = sysmmsuser.name, 
						assigned_to =sysmmsuser2.name, due_date = a.start_date, contact_id = a.contact_id, c.first_name,c.last_name, cactmssetupdtl.value as task_type_desc 
						FROM cacttrtask a JOIN cacttrtaskcontacts D ON A.company_id = D.company_id AND A.trans_bk = D.trans_bk AND A.trans_no = D.trans_no AND 
						A.trans_dt = D.trans_dt JOIN cactmsaccount b  ON  D.prospect_id = b.id LEFT JOIN sysmmsuser sp  ON  b.salesperson_cd = sp.g_user_cd LEFT JOIN 
						sysmmsuser  ON  a.Assigned_by = sysmmsuser.g_user_cd LEFT JOIN sysmmsuser sysmmsuser2  ON  a.Assigned_to = sysmmsuser2.g_user_cd LEFT JOIN 
						cactmscontact c  ON  a.prospect_id = c.id AND a.contact_id = c.serial_no LEFT JOIN cactmssetupdtl ON cactmssetupdtl.id = a.task_type AND 
						cactmssetupdtl.setup_id = 'TASKTYPE' 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or Isnull(D.prospect_id, '') = '{prospect_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(a.trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectMeeting(string prospect_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT cacttrmeeting.prospect_id, cacttrmeeting.trans_bk, cacttrmeeting.trans_no, cacttrmeeting.trans_dt, cacttrmeeting.contact_id, 
						cacttrmeeting.remarks, cacttrmeeting.stock_asset, cacttrmeeting.delivery_asset, cacttrmeeting.stock_memo, cacttrmeeting.delivery_memo, 
						cacttrmeeting.spo, cacttrmeeting.proposal, cacttrmeeting.trade_show, cacttrmeeting.no_orders, cacttrmeeting.asset_order_amt, 
						cacttrmeeting.ra_amt, cacttrmeeting.net_amt, cacttrmeeting.memo_order_amt, cacttrmeeting.not_ready_to_order, cacttrmeeting.order_placed, 
						cacttrmeeting.call_later_in_season, cacttrmeeting.set_appt_for_future, cacttrmeeting.owner_not_available, cacttrmeeting.subject, 
						cacttrmeeting.meeting_task_id, cacttrmeeting.ref_bk, cacttrmeeting.ref_no, cacttrmeeting.ref_dt, cactmscontact.first_name, Cactmscontact.last_name 
						FROM cacttrmeeting JOIN cacttrmeetingcontacts ON cacttrmeeting.company_id = cacttrmeetingcontacts.company_id AND 
						cacttrmeeting.trans_bk = cacttrmeetingcontacts.trans_bk AND cacttrmeeting.trans_no = cacttrmeetingcontacts.trans_no AND 
						cacttrmeeting.trans_dt = cacttrmeetingcontacts.trans_dt LEFT JOIN cactmscontact ON cacttrmeeting.prospect_id = cactmscontact.id AND 
						cacttrmeeting.contact_id = cactmscontact.serial_no 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or cacttrmeetingcontacts.prospect_id = '{prospect_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cacttrmeeting.trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectCampaign(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cacttrcampaignhd.campaign_type, cactmssetupdtl.value as campaign_type_desc, cacttrcampaignhd.company_id, cacttrcampaignhd.trans_dt, 
						cacttrcampaignhd.trans_no, cacttrcampaignhd.trans_flag, cacttrcampaignhd.user_cd, cacttrcampaignhd.update_dt, cacttrcampaignhd.update_flag, 
						cacttrcampaignhd.campaign_id, case cacttrcampaignhd.campaign_status when 'P' then 'Planned' when 'I' then 'In-progress' when 'C' 
						then 'Complete' end as campaign_status, cacttrcampaignhd.remarks, cacttrcampaignhd.salesperson, cacttrcampaignhd.start_date, cacttrcampaignhd.end_date, 
						parent_campaign_no, parent_campaign_dt, sysmmsuser.name as salesperson_desc 
						FROM cacttrcampaigndtl LEFT JOIN cacttrcampaignhd ON cacttrcampaigndtl.company_id = cacttrcampaignhd.company_id and 
						cacttrcampaigndtl.trans_bk = cacttrcampaignhd.trans_bk and cacttrcampaigndtl.trans_no = cacttrcampaignhd.trans_no and 
						cacttrcampaigndtl.trans_dt = cacttrcampaignhd.trans_dt LEFT JOIN cactmssetupdtl ON cacttrcampaignhd.campaign_type = cactmssetupdtl.id and 
						cactmssetupdtl.setup_id = 'CAMPAIGN' left join sysmmsuser on salesperson = sysmmsuser.g_user_cd 

						where ('{account_id}' = case when '{account_id}' = '' then '' end or cacttrcampaigndtl.account_id = '{account_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectCreditApplication(string trade_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmstraderef.id, cactmstraderef.serial_no, cactmstraderef.person_to_contact, cactmstraderef.company_name, cactmstraderef.email, 
						cactmstraderef.user_cd, cactmstraderef.company_id, cactmstraderef.update_dt, cactmstraderef.update_flag, cactmstraderef.trans_flag, 
						cactmstraderef.phone FROM cactmstraderef 

						where ('{trade_id}' = case when '{trade_id}' = '' then '' end or id = '{trade_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInboxHeader()
		{
			is_sql = $@"SELECT * FROM cactaccountinboxhd WHERE trfr_flag <> 'P'";

			return Execute();
		}

		public DataTable GetProspectInboxContact()
		{
			is_sql = $@"SELECT A.* FROM cactaccountinboxcontacts A INNER JOIN cactaccountinboxhd B ON  A.ID = B.ID WHERE trfr_flag <> 'P'";

			return Execute();
		}

		public DataTable GetProspectInboxTrade()
		{
			is_sql = $@"SELECT A.* FROM cactaccountinboxtraderef A INNER JOIN cactaccountinboxhd B ON  A.ID = B.ID WHERE trfr_flag <> 'P'";

			return Execute();
		}

		public DataTable GetProspectInboxCreditApplication()
		{
			is_sql = $@"EXEC spcact_prospect_inbox_new 'A'";

			return Execute();
		}

		public DataTable GetProspectInboxProspectOnHold()
		{
			is_sql = $@"EXEC spcact_prospect_inbox_new 'H'";

			return Execute();
		}

		public DataTable GetProspectInfoMainList(string start_date_from, string start_date_to, string category_id_from, string category_id_to, string state_from, string state_to, string company_name_from, string company_name_to, string id_from, string id_to, string salesperson_cd_from, string salesperson_cd_to, string city_from, string city_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string customer_flag, string created_fr, string current_stage, string credit_approval_stage, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT b.customer_flag,prospect_owner_desc = sysmmsuser.name,source = d.value,start_date = b.start_dt, category= b.category_id, state = b.state, 
						company_name = b.company_name, customer_id= b.parent_account_id, salesperson_id = b.salesperson_cd, type = c.type2, group_cd = c.type1, 
						case b.customer_flag when 'Y' then 'Yes' else 'No' end as customer_flag1, group1 = b.group1, group2 = b.group2, group3 = b.group3, group4 = b.group4, 
						group5 = b.group5, account_id = b.ID, city = b.city, zip = b.zip, email =b.email, phone_call = b.allow_phone_Call, fax = b.fax, Mail = b.allow_mail, 
						marketing_material= b.send_marketing_material, send_catelog=b.allow_send_catalog, b.description, co.first_name, co.last_name, salesperson_name = sp.name, 
						phone = b.phone, status = b.trans_flag, B.current_stage, credit_approval_stage = B.credit_approval_stage, select_flag = 'N', prospect_owner, 
						b.tov_customer_id, b.created_fr, b.country, co.web_id, case bg_big when 'Y' then ',BIG' else '' end+ case bg_cbg when 'Y' then ',CBG' else '' end+ 
						case bg_ijo when 'Y' then ',IJO' else '' end+ case bg_ljg when 'Y' then ',LJG' else '' end+ case bg_prime when 'Y' then ',PRIME' else '' end+ case bg_rjo 
						when 'Y' then ',RJO' else '' end+ case bg_sjo when 'Y' then ',SJO' else '' end as buying_group, case price_list when 'Y' then ',Price List' else '' end+ 
						case web_data when 'Y' then ',Web Data' else '' end+ case social_media_images when 'Y' then ',Social media Images' else '' end+ case web_banner 
						when 'Y' then ',Web Banner' else '' end+ case printed_ads when 'Y' then ',Printed Ads' else '' end as account_management, csum.ytd_sales, 
						csum.last_year_sales, csum.sales_2years_ago, csum.sales_3years_ago, faarmscustomercategory.name as category_desc, case ISNULL(status,'Active') 
						when 'A' then 'Active' when 'H' then 'Hold' when 'D' then 'Delete' end as status_desc,b.* 
						From cactmsaccount b left join faarmscustomer c on b.parent_account_id = c.id LEFT JOIN cactmsaccountsummary csum ON b.id = csum.account_id 
						LEFT OUTER JOIN cactmscontact co ON b.ID = co.id AND co.contact_type = 'MC' LEFT OUTER JOIN saoimssalesperson sp ON b.salesperson_cd = sp.id 
						left join cactmssetupdtl d on b.created_fr = d.id and ISNULL(d.setup_id,'') = 'PROSPECT_SOURCE'  AND ISNULL(d.trans_flag,'') = 'A' 
						left join faarmscustomercategory on b.category_id =  faarmscustomercategory.category left join sysmmsuser on sysmmsuser.g_user_cd = b.prospect_owner 

						where(('{start_date_from}' = case when '{start_date_from}' = '' then '' end or b.start_dt BETWEEN '{start_date_from}' AND '{start_date_to}') 
						and ('{category_id_from}' = case when '{category_id_from}' = '' then '' end or b.category_id BETWEEN '{category_id_from}' AND '{category_id_to}') 
						and ('{state_from}' = case when '{state_from}' = '' then '' end or b.state BETWEEN '{state_from}' AND '{state_to}') 
						and ('{company_name_from}' = case when '{company_name_from}' = '' then '' end or b.company_name BETWEEN '{company_name_from}' AND '{company_name_to}') 
						and ('{id_from}' = case when '{id_from}' = '' then '' end or b.id BETWEEN '{id_from}' AND '{id_to}') 
						and ('{salesperson_cd_from}' = case when '{salesperson_cd_from}' = '' then '' end or b.salesperson_cd BETWEEN '{salesperson_cd_from}' AND '{salesperson_cd_to}') 
						and ('{city_from}' = case when '{city_from}' = '' then '' end or b.city BETWEEN '{city_from}' AND '{city_to}') 
						and ('{zip_from}' = case when '{zip_from}' = '' then '' end or b.zip BETWEEN '{zip_from}' AND '{zip_to}') 
						and ('{group1_from}' = case when '{group1_from}' = '' then '' end or b.group1 BETWEEN '{group1_from}' AND '{group1_to}') 
						and ('{group2_from}' = case when '{group2_from}' = '' then '' end or b.group2 BETWEEN '{group2_from}' AND '{group2_to}') 
						and ('{group3_from}' = case when '{group3_from}' = '' then '' end or b.group3 BETWEEN '{group3_from}' AND '{group3_to}') 
						and ('{group4_from}' = case when '{group4_from}' = '' then '' end or b.group4 BETWEEN '{group4_from}' AND '{group4_to}') 
						and ('{group5_from}' = case when '{group5_from}' = '' then '' end or b.group5 BETWEEN '{group5_from}' AND '{group5_to}') 
						and ('{customer_flag}' = case when '{customer_flag}' = '' then '' end or b.customer_flag = '{customer_flag}')
						and ('{created_fr}' = case when '{created_fr}' = '' then '' end or b.created_fr = '{created_fr}')
						
						)";

			if (string.IsNullOrEmpty(current_stage))
            {
				if (!string.IsNullOrEmpty(credit_approval_stage))
				{
					is_sql += @$" and  ( 
										('{credit_approval_stage}' = case when '{credit_approval_stage}' = '' then '' end or b.credit_approval_stage = '{credit_approval_stage}') 
								 )";



				}
			}
			
			else
            {
				if (current_stage == "credit_app")
				{
					is_sql += $"  and ( b.current_stage = 'credit_app' or b.current_stage ='OPPORTUNITY') ";
				}
				else
				{
					is_sql += @$" and  ( 
										('{current_stage}' = case when '{current_stage}' = '' then '' end or b.current_stage = '{current_stage}') 
								 )";
				}
			}
			

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(b.id) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			is_sql = is_sql.Replace("\r", "");
			is_sql = is_sql.Replace("\n", "");
			is_sql = is_sql.Replace("\t", "");
			return Execute();
		}

		public DataTable GetProspectInfoStageCount()
		{
			is_sql = $@"exec spcact_stagewise_count";

			return Execute();
		}

		public DataTable GetProspectInfoContact(string contact_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmscontact.id, cactmscontact.serial_no, cactmscontact.first_name, cactmscontact.last_name, cactmscontact.address1, 
						cactmscontact.address2, cactmscontact.city, cactmscontact.state, cactmscontact.zip, cactmscontact.country, cactmscontact.phone1, 
						cactmscontact.phone2, cactmscontact.email_id, cactmscontact.user_cd, cactmscontact.company_id, cactmscontact.update_dt, 
						cactmscontact.update_flag, cactmscontact.trans_flag, cactmscontact.title, cactmscontact.contact_photo, cactmscontact.email_id2, 
						cactmscontact.web_id, cactmscontact.subscribed_to_emails, cactmscontact.subscription_email, cactmscontact.contact_type, 
						Cactmssetupdtl.value as contact_type_desc 
						from cactmscontact inner join Cactmssetupdtl ON ( cactmscontact.contact_type = Cactmssetupdtl.id and Cactmssetupdtl.setup_id = 'CONTACT_TYPE' ) 

						where ('{contact_id}' = case when '{contact_id}' = '' then '' end or cactmscontact.id = '{contact_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoShipTo(string ship_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmsprospectship.id, cactmsprospectship.name, cactmsprospectship.contact1, cactmsprospectship.address1, cactmsprospectship.address2, 
						cactmsprospectship.city, cactmsprospectship.state, cactmsprospectship.zip, cactmsprospectship.country, cactmsprospectship.phone1, cactmsprospectship.fax1, 
						cactmsprospectship.user_cd, cactmsprospectship.company_id, cactmsprospectship.update_dt, cactmsprospectship.update_flag, cactmsprospectship.trans_flag, 
						cactmsprospectship.serial_no,email_id, sales_person,default_flag, cactmsprospectship.dc_id FROM cactmsprospectship 

						where ('{ship_id}' = case when '{ship_id}' = '' then '' end or cactmsprospectship.id = '{ship_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoNotes(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT serial_no = cactmsaccountnotes.serial_no, account_id = cactmsaccountnotes.account_id, notes = cactmsaccountnotes.notes, 
						user_cd = cactmsaccountnotes.user_cd, update_dt = cactmsaccountnotes.update_dt, cactmsaccountnotes.update_flag, cactmsaccountnotes.company_id, 
						trans_flag = cactmsaccountnotes.trans_flag, notes_type = cactmsaccountnotes.notes_type, Cactmssetupdtl.value as note_type_title, 
						sysmmsuser.name as user_name 
						FROM cactmsaccountnotes left join Cactmssetupdtl ON (cactmsaccountnotes.notes_type = Cactmssetupdtl.Id and Cactmssetupdtl.setup_id = 
						'NOTESTYPE' and Cactmssetupdtl.trans_flag = 'A' ) left join sysmmsuser ON (cactmsaccountnotes.user_cd = sysmmsuser.g_user_cd )

						where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountnotes.account_id = '{account_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoProfile(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmsaccountsummary.account_id, cactmsaccountsummary.ta_centurion_scottsdale, cactmsaccountsummary.ta_select_tuscon, 
						cactmsaccountsummary.ta_select_dallas, cactmsaccountsummary.ta_select_foxwoods, cactmsaccountsummary.ta_select_dc, 
						cactmsaccountsummary.ta_jck_tuscon, cactmsaccountsummary.ta_rjo_shows, cactmsaccountsummary.ta_ijo_shows, cactmsaccountsummary.ta_jany_spring, 
						cactmsaccountsummary.ta_jany_summer, cactmsaccountsummary.ta_centurion_arizona, cactmsaccountsummary.ta_centurion_south_beach, 
						cactmsaccountsummary.ta_atlanta_spring ,cactmsaccountsummary.ta_atlanta_fall , cactmsaccountsummary.ta_jck_las_vegas, cactmsaccountsummary.ta_luxury_las_vegas, 
						cactmsaccountsummary.ta_couture, cactmsaccountsummary.ta_montreal_expo, cactmsaccountsummary.tra_ags, cactmsaccountsummary.tra_agta, 
						cactmsaccountsummary.tra_ja, cactmsaccountsummary.tra_jvc, cactmsaccountsummary.tra_mjsa, cactmsaccountsummary.tra_snag, cactmsaccountsummary.tra_naja, 
						cactmsaccountsummary.tra_jbt, cactmsaccountsummary.tra_wja, cactmsaccountsummary.tra_aaa, cactmsaccountsummary.tra_dca, cactmsaccountsummary.tra_maja, 
						cactmsaccountsummary.bench_jeweler_onsite, cactmsaccountsummary.bg_sjo, cactmsaccountsummary.bg_ijo, cactmsaccountsummary.bg_rjo, 
						cactmsaccountsummary.bg_cbg, cactmsaccountsummary.bg_ljg, cactmsaccountsummary.bg_big, cactmsaccountsummary.bg_prime, cactmsaccountsummary.pc_bridal, 
						cactmsaccountsummary.pc_branded , cactmsaccountsummary.pc_fashion ,cactmsaccountsummary.pc_watches , cactmsaccountsummary.pc_custom , 
						cactmsaccountsummary.pc_silver , cactmsaccountsummary.sm_facebook,cactmsaccountsummary.sm_instagram, cactmsaccountsummary.sm_twitter, 
						cactmsaccountsummary.user_cd , cactmsaccountsummary.update_dt ,cactmsaccountsummary.update_flag , cactmsaccountsummary.company_id ,
						cactmsaccountsummary.trans_flag, cactmsaccountsummary.Linkedin,thinkspace, punchmark, gemfind_jewelcloud, avalon_solutions_jewel_exchange, 
						clevergem,tov_customer_code, last_year_sales, ytd_sales, sales_2years_ago, sales_3years_ago, ptd_sales, no_of_invoices, last_pay_dt, 
						return_per, web_data, social_media_images, web_banner, price_list, printed_ads, billing_via_rjo_sjo, bill_via_sjo, last_activity, customer_bal,on_order 
						FROM cactmsaccountsummary 

						where ('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountsummary.account_id = '{account_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoDocument(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT id, serial_no, company_id, user_cd, update_dt, update_flag,trans_flag, file_type, file_name, subject, remark, email_to, attachment 
						FROM cactaccountdocs 

						where ('{account_id}' = case when '{account_id}' = '' then '' end or id = '{account_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoTask(string prospect_id, string status_flag, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT assigned_by_desc = sysmmsuser.name,assigned_to_desc = sysmmsuser_2.name,company_id = a.company_id, trans_bk = a.trans_bk, 
						trans_no = a.trans_no, trans_dt = a.trans_dt, task_date = a.trans_dt, task_type = a.task_type, subject = a.subject, status = a.status_flag, 
						account_id = a.prospect_id, company_name = b.company_name, salesperson_cd = a.salesperson_cd, salesperson_name = sp.name, 
						description = a.remarks, group1 = b.group1, group2 = b.group2, group3 = b.group3, group4 = b.group4, group5 = b.group5, 
						assigned_by =a.Assigned_by, assigned_to = a.Assigned_to, due_date = a.start_date, contact_id = a.contact_id, c.first_name, c.last_name, 
						cactmssetupdtl.value as task_type_desc 
						FROM cacttrtask a JOIN cacttrtaskcontacts D ON A.company_id = D.company_id AND A.trans_bk = D.trans_bk AND A.trans_no = D.trans_no 
						AND A.trans_dt = D.trans_dt JOIN cactmsaccount b ON D.prospect_id = b.id LEFT JOIN saoimssalesperson sp ON b.salesperson_cd = sp.id 
						LEFT JOIN cactmscontact c ON a.prospect_id = c.id AND a.contact_id = c.serial_no LEFT JOIN cactmssetupdtl  ON cactmssetupdtl.id = a.task_type 
						AND cactmssetupdtl.setup_id = 'TASKTYPE' left join sysmmsuser on sysmmsuser.g_user_cd = a.assigned_by left join sysmmsuser as 
						sysmmsuser_2 on sysmmsuser_2.g_user_cd = a.assigned_to 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or Isnull(D.prospect_id, '') = '{prospect_id}')
						and ('{status_flag}' = case when '{status_flag}' = '' then '' end or Upper(a.status_flag) = '{status_flag}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(a.trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoMeeting(string prospect_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT cacttrmeeting.prospect_id, cacttrmeeting.trans_bk, cacttrmeeting.trans_no, cacttrmeeting.trans_dt, 
						cacttrmeeting.contact_id, cacttrmeeting.remarks, cacttrmeeting.stock_asset, cacttrmeeting.delivery_asset, cacttrmeeting.stock_memo, 
						cacttrmeeting.delivery_memo, cacttrmeeting.spo, cacttrmeeting.proposal, cacttrmeeting.trade_show, cacttrmeeting.no_orders, 
						cacttrmeeting.asset_order_amt, cacttrmeeting.ra_amt, cacttrmeeting.net_amt, cacttrmeeting.memo_order_amt, cacttrmeeting.not_ready_to_order, 
						cacttrmeeting.order_placed, cacttrmeeting.call_later_in_season, cacttrmeeting.set_appt_for_future, cacttrmeeting.owner_not_available, 
						cacttrmeeting.subject, cacttrmeeting.meeting_task_id, cacttrmeeting.ref_bk, cacttrmeeting.ref_no, cacttrmeeting.ref_dt, 
						cactmscontact.first_name, Cactmscontact.last_name
						FROM cacttrmeeting JOIN cacttrmeetingcontacts ON cacttrmeeting.company_id = cacttrmeetingcontacts.company_id AND 
						cacttrmeeting.trans_bk = cacttrmeetingcontacts.trans_bk AND cacttrmeeting.trans_no = cacttrmeetingcontacts.trans_no AND 
						cacttrmeeting.trans_dt = cacttrmeetingcontacts.trans_dt LEFT JOIN cactmscontact ON cacttrmeeting.prospect_id = cactmscontact.id AND 
						cacttrmeeting.contact_id = cactmscontact.serial_no 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or cacttrmeetingcontacts.prospect_id = '{prospect_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(cacttrmeeting.trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoCampaign(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT sysmmsuser.name as salesperson_desc,cacttrcampaignhd.campaign_type, cactmssetupdtl.value as campaign_type_desc, cacttrcampaignhd.company_id, 
						cacttrcampaignhd.trans_dt, cacttrcampaignhd.trans_no, cacttrcampaignhd.trans_flag, cacttrcampaignhd.user_cd, cacttrcampaignhd.update_dt, 
						cacttrcampaignhd.update_flag, cacttrcampaignhd.campaign_id, case  cacttrcampaignhd.campaign_status when 'P' then 'Planned' when 'I' then 
						'In-progress' when 'C' then 'Complete' end as campaign_status, cacttrcampaignhd.remarks, cacttrcampaignhd.salesperson, cacttrcampaignhd.start_date, 
						cacttrcampaignhd.end_date,parent_campaign_no, parent_campaign_dt 
						FROM cacttrcampaigndtl LEFT JOIN cacttrcampaignhd ON cacttrcampaigndtl.company_id = cacttrcampaignhd.company_id and cacttrcampaigndtl.trans_bk = 
						cacttrcampaignhd.trans_bk and cacttrcampaigndtl.trans_no = cacttrcampaignhd.trans_no and cacttrcampaigndtl.trans_dt =cacttrcampaignhd.trans_dt 
						LEFT JOIN cactmssetupdtl ON cacttrcampaignhd.campaign_type =cactmssetupdtl.id and cactmssetupdtl.setup_id = 'CAMPAIGN' left join  
						sysmmsuser  on salesperson = sysmmsuser.g_user_cd 

						where ('{account_id}' = case when '{account_id}' = '' then '' end or cacttrcampaigndtl.account_id = '{account_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoCreditApplication(string trade_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmstraderef.id, cactmstraderef.serial_no, cactmstraderef.person_to_contact, cactmstraderef.company_name, cactmstraderef.email, 
						cactmstraderef.user_cd, cactmstraderef.company_id, cactmstraderef.update_dt, cactmstraderef.update_flag, cactmstraderef.trans_flag, cactmstraderef.phone 
						FROM cactmstraderef

						where ('{trade_id}' = case when '{trade_id}' = '' then '' end or id = '{trade_id}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInfoActivity(string prospect_id, string status)
		{
			is_sql = $@"SELECT DISTINCT activityby_desc = sysmmsuser.name,Cactmssetupdtl.Value,cacttractivitycontacts.prospect_id, cacttractivity.trans_bk, 
						cacttractivity.trans_no, cacttractivity.activity_type, cacttractivity.remarks, cacttractivity.subject, cacttractivity.activity_dt, 
						cacttractivity.user_cd, cacttractivity.update_dt, cacttractivity.update_flag, cacttractivity.company_id, cacttractivity.trans_flag, 
						cacttractivity.trans_dt, cacttractivity.ref_bk, cacttractivity.ref_no, cacttractivity.ref_dt 
						FROM cacttractivity INNER JOIN cacttractivitycontacts ON cacttractivity.company_id = cacttractivitycontacts.company_id AND 
						cacttractivity.trans_bk = cacttractivitycontacts.trans_bk AND cacttractivity.trans_no = cacttractivitycontacts.trans_no AND 
						cacttractivity.trans_dt = cacttractivitycontacts.trans_dt left join Cactmssetupdtl on cacttractivity.activity_type = Cactmssetupdtl.id 
						left join sysmmsuser on sysmmsuser.g_user_cd = cacttractivity.user_cd 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or cacttractivitycontacts.prospect_id = '{prospect_id}') 
						and ('{status}' = case when '{status}' = '' then '' end or Cactmssetupdtl.trans_flag = '{status}') 
						and Cactmssetupdtl.Setup_id = 'TASKTYPE'";

			return Execute();
		}

		public DataTable GetProspectInfoCloseTask(string prospect_id, string status_flag, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT assigned_by_desc = sysmmsuser.name,assigned_to_desc = sysmmsuser_2.name,company_id = a.company_id, trans_bk = 
						a.trans_bk, trans_no = a.trans_no, trans_dt = a.trans_dt, task_date = a.trans_dt, task_type = a.task_type, subject = a.subject, 
						status = a.status_flag, account_id = a.prospect_id, company_name = b.company_name, salesperson_cd = a.salesperson_cd, 
						salesperson_name = sp.name, description = a.remarks, group1 = b.group1, group2 = b.group2, group3 = b.group3, group4 = b.group4, 
						group5 = b.group5, assigned_by =a.Assigned_by, assigned_to = a.Assigned_to, due_date = a.start_date, contact_id = a.contact_id, 
						c.first_name, c.last_name, cactmssetupdtl.value as task_type_desc 
						FROM cacttrtask a JOIN cacttrtaskcontacts D ON A.company_id = D.company_id AND A.trans_bk = D.trans_bk AND A.trans_no = D.trans_no 
						AND A.trans_dt = D.trans_dt JOIN cactmsaccount b ON D.prospect_id = b.id LEFT JOIN saoimssalesperson sp ON b.salesperson_cd = sp.id 
						LEFT JOIN cactmscontact c ON a.prospect_id = c.id AND a.contact_id = c.serial_no LEFT JOIN cactmssetupdtl  ON cactmssetupdtl.id = 
						a.task_type AND cactmssetupdtl.setup_id = 'TASKTYPE' left join sysmmsuser on sysmmsuser.g_user_cd = a.assigned_by left join 
						sysmmsuser as sysmmsuser_2 on sysmmsuser_2.g_user_cd = a.assigned_to 

						where ('{prospect_id}' = case when '{prospect_id}' = '' then '' end or Isnull(D.prospect_id, '') = '{prospect_id}') 
						and ('{status_flag}' = case when '{status_flag}' = '' then '' end or Upper(a.status_flag) = '{status_flag}')";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(a.trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetEmailList(string status, string email_id, string subject, string message, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"select count(*) as Count ";
			}
			else
			{
				is_sql = $@"SELECT id, subject, message, user_group = RTRIM(user_group), user_cd, company_id, update_dt, update_flag, trans_flag ";
			}
			is_sql += $@"FROM cactms_email_template 

						 where (('{status}' = case when '{status}' = '' then '' end or trans_flag = '{status}')) 
						 and (('{email_id}' LIKE case when '{email_id}' = '' then '' end or id like '%{email_id}%') 
						 and ('{subject}' LIKE case when '{subject}' = '' then '' end or subject like '%{subject}%') 
						 and ('{message}' LIKE case when '{message}' = '' then '' end or message like '%{message}%'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL ) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetEmailTemplateList(string email_id, string subject, string message, string user_group, string status, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"select count(*) as Count ";
			}
			else
			{
				is_sql = $@"SELECT id, subject, message, user_group = RTRIM(user_group), user_cd, company_id, update_dt, update_flag, trans_flag ";
			}
			is_sql += $@"FROM cactms_email_template 

						 where (('{user_group}' = case when '{user_group}' = '' then '' end or user_group = '{user_group}') 
						 and ('{status}' = case when '{status}' = '' then '' end or trans_flag = '{status}')) 
						 and (('{email_id}' LIKE case when '{email_id}' = '' then '' end or id like '%{email_id}%') 
						 and ('{subject}' LIKE case when '{subject}' = '' then '' end or subject like '%{subject}%') 
						 and ('{message}' LIKE case when '{message}' = '' then '' end or message like '%{message}%'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL ) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();

		}

		public DataTable GetCustomerList(string customer_id, string customer_name, string customer_category, string phone, string credit_approval_flag, string stop_shipment_days, string stop_shimpment_amt, string tov_customer_id, string status, string credit_status, string stop_ship, string cc_fidelity, string patriot_act, string collection, string blacklisted_flag, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"select count() as Count from faarmscustomer ";
			}
			else
			{
				is_sql = $@"SELECT faarmscustomer.*,faarmscustomercategory.name as category_desc ,sysmmsuser.name as sales_person_desc,ytd_sales,last_year_sales,
							cactmssetupdtl.value as created_fr_desc ,sysmmsuser_2.name as prospect_owner_desc,RTRIM(cactmssetupdtl_2.value) as service_rep_desc 
							from faarmscustomer left join cactmsaccount on cactmsaccount.id = faarmscustomer.id left join faarmscustomercategory on faarmscustomer.category = 
							faarmscustomercategory.category left join sysmmsuser on faarmscustomer.sales_person = sysmmsuser.g_user_cd left join cactmsaccountsummary on 
							cactmsaccountsummary.account_id = faarmscustomer.id left join cactmssetupdtl on setup_id = 'PROSPECT_SOURCE' AND cactmssetupdtl.trans_flag = 'A' and 
							created_fr= cactmssetupdtl.id left join sysmmsuser as sysmmsuser_2 on prospect_owner = sysmmsuser_2.g_user_cd left join cactmssetupdtl 
							as cactmssetupdtl_2 on cactmssetupdtl_2.setup_id = 'SERVICE_REP' AND cactmssetupdtl_2.trans_flag = 'A'  and cactmssetupdtl_2.id = faarmscustomer.service_rep ";
			}
			is_sql += $@"where (('{customer_id}' = case when '{customer_id}' = '' then '' end or faarmscustomer.id = '{customer_id}') 
						 and ('{customer_name}' = case when '{customer_name}' = '' then '' end or faarmscustomer.name = '{customer_name}') 
						 and ('{customer_category}' = case when '{customer_category}' = '' then '' end or faarmscustomer.category = '{customer_category}') 
						 and ('{phone}' = case when '{phone}' = '' then '' end or phone1 = '{phone}')
						 and ('{credit_approval_flag}' = case when '{credit_approval_flag}' = '' then '' end or credit_approval_flag = '{credit_approval_flag}') 
						 and ('{stop_shipment_days}' = case when '{stop_shipment_days}' = '' then '' end or Stop_shipment_days = '{stop_shipment_days}') 
						 and ('{stop_shimpment_amt}' = case when '{stop_shimpment_amt}' = '' then '' end or Stop_shimpment_amt = '{stop_shimpment_amt}') 
						 and ('{tov_customer_id}' = case when '{tov_customer_id}' = '' then '' end or faarmscustomer.tov_customer_id = '{tov_customer_id}') 
						 and ('{status}' = case when '{status}' = '' then '' end or faarmscustomer.trans_flag = '{status}') 
						 and ('{credit_status}' = case when '{credit_status}' = '' then '' end or faarmscustomer.credit_status = '{credit_status}')

and ('{stop_ship}' = case when '{stop_ship}' = '' then '' end or stop_ship = '{stop_ship}') 
						 and ('{cc_fidelity}' = case when '{cc_fidelity}' = '' then '' end or cc_fidelity = '{cc_fidelity}') 
						 and ('{patriot_act}' = case when '{patriot_act}' = '' then '' end or patriot_act = '{patriot_act}') 
						 and ('{collection}' = case when '{collection}' = '' then '' end or collection = '{collection}') 
						 and ('{blacklisted_flag}' = case when '{blacklisted_flag}' = '' then '' end or blacklisted_flag = '{blacklisted_flag}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL ) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();

		}

		public DataTable GetCustomerContact(string contact_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmscontact.id, cactmscontact.serial_no, cactmscontact.first_name, cactmscontact.last_name, cactmscontact.address1, cactmscontact.address2, 
						cactmscontact.city, cactmscontact.state, cactmscontact.zip, cactmscontact.country, cactmscontact.phone1, cactmscontact.phone2, cactmscontact.email_id, 
						cactmscontact.user_cd, cactmscontact.company_id, cactmscontact.update_dt, cactmscontact.update_flag, cactmscontact.trans_flag, cactmscontact.title, 
						cactmscontact.contact_photo, cactmscontact.email_id2, cactmscontact.web_id, cactmscontact.subscribed_to_emails, cactmscontact.subscription_email, 
						cactmscontact.contact_type , Cactmssetupdtl.value as contact_type_desc 
						FROM cactmscontact inner join Cactmssetupdtl ON ( cactmscontact.contact_type = Cactmssetupdtl.id and Cactmssetupdtl.setup_id = 'CONTACT_TYPE') 

						where (('{contact_id}' = case when '{contact_id}' = '' then '' end or cactmscontact.id = '{contact_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerShip(string ship_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmsprospectship.id, cactmsprospectship.name, cactmsprospectship.contact1, cactmsprospectship.address1, cactmsprospectship.address2, 
						cactmsprospectship.city, cactmsprospectship.state, cactmsprospectship.zip, cactmsprospectship.country, cactmsprospectship.phone1, cactmsprospectship.fax1, 
						cactmsprospectship.user_cd, cactmsprospectship.company_id, cactmsprospectship.update_dt, cactmsprospectship.update_flag, cactmsprospectship.trans_flag, 
						cactmsprospectship.serial_no, email_id, sales_person, default_flag, cactmsprospectship.dc_id FROM cactmsprospectship  

						where (('{ship_id}' = case when '{ship_id}' = '' then '' end or cactmsprospectship.id = '{ship_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerNotes(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT serial_no = cactmsaccountnotes.serial_no, account_id = cactmsaccountnotes.account_id, notes = cactmsaccountnotes.notes, user_cd = 
						cactmsaccountnotes.user_cd, update_dt = cactmsaccountnotes.update_dt, cactmsaccountnotes.update_flag, cactmsaccountnotes.company_id, 
						trans_flag = cactmsaccountnotes.trans_flag, notes_type = cactmsaccountnotes.notes_type , Cactmssetupdtl.value as note_type_title, 
						sysmmsuser.name as notes_by 
						FROM cactmsaccountnotes left join Cactmssetupdtl ON ( cactmsaccountnotes.notes_type = Cactmssetupdtl.Id and Cactmssetupdtl.setup_id = 'NOTESTYPE' and 
						Cactmssetupdtl.trans_flag = 'A' ) left join sysmmsuser ON ( cactmsaccountnotes.notes_by = sysmmsuser.id ) 

						where (('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountnotes.account_id = '{account_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerProfile(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmsaccountsummary.account_id , cactmsaccountsummary.ta_centurion_scottsdale ,cactmsaccountsummary.ta_select_tuscon , 
						cactmsaccountsummary.ta_select_dallas, cactmsaccountsummary.ta_select_foxwoods, cactmsaccountsummary.ta_select_dc ,cactmsaccountsummary.ta_jck_tuscon , 
						cactmsaccountsummary.ta_rjo_shows, cactmsaccountsummary.ta_ijo_shows , cactmsaccountsummary.ta_jany_spring ,cactmsaccountsummary.ta_jany_summer , 
						cactmsaccountsummary.ta_centurion_arizona , cactmsaccountsummary.ta_centurion_south_beach , cactmsaccountsummary.ta_atlanta_spring , 
						cactmsaccountsummary.ta_atlanta_fall , cactmsaccountsummary.ta_jck_las_vegas , cactmsaccountsummary.ta_luxury_las_vegas , cactmsaccountsummary.ta_couture , 
						cactmsaccountsummary.ta_montreal_expo , cactmsaccountsummary.tra_ags ,cactmsaccountsummary.tra_agta , cactmsaccountsummary.tra_ja , 
						cactmsaccountsummary.tra_jvc , cactmsaccountsummary.tra_mjsa ,cactmsaccountsummary.tra_snag , cactmsaccountsummary.tra_naja , cactmsaccountsummary.tra_jbt , 
						cactmsaccountsummary.tra_wja ,cactmsaccountsummary.tra_aaa , cactmsaccountsummary.tra_dca ,cactmsaccountsummary.tra_maja , 
						cactmsaccountsummary.bench_jeweler_onsite ,cactmsaccountsummary.bg_sjo , cactmsaccountsummary.bg_ijo ,cactmsaccountsummary.bg_rjo , 
						cactmsaccountsummary.bg_cbg , cactmsaccountsummary.bg_ljg , cactmsaccountsummary.bg_big ,cactmsaccountsummary.bg_prime , cactmsaccountsummary.pc_bridal , 
						cactmsaccountsummary.pc_branded , cactmsaccountsummary.pc_fashion ,cactmsaccountsummary.pc_watches , cactmsaccountsummary.pc_custom , 
						cactmsaccountsummary.pc_silver , cactmsaccountsummary.sm_facebook, cactmsaccountsummary.sm_instagram, cactmsaccountsummary.sm_twitter, 
						cactmsaccountsummary.user_cd , cactmsaccountsummary.update_dt ,cactmsaccountsummary.update_flag , cactmsaccountsummary.company_id , 
						cactmsaccountsummary.trans_flag, cactmsaccountsummary.Linkedin,thinkspace, punchmark, gemfind_jewelcloud, avalon_solutions_jewel_exchange, 
						clevergem,tov_customer_code, last_year_sales, ytd_sales, sales_2years_ago, sales_3years_ago, ptd_sales, no_of_invoices, last_pay_dt, return_per, 
						web_data, social_media_images, web_banner, price_list, printed_ads, billing_via_rjo_sjo,bill_via_sjo,last_activity, customer_bal,on_order 
						FROM cactmsaccountsummary 

						where (('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountsummary.account_id = '{account_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerSales(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmsaccountsummary.account_id, cactmsaccountsummary.ta_centurion_scottsdale, cactmsaccountsummary.ta_select_tuscon, cactmsaccountsummary.ta_select_dallas, 
						cactmsaccountsummary.ta_select_foxwoods, cactmsaccountsummary.ta_select_dc, cactmsaccountsummary.ta_jck_tuscon, cactmsaccountsummary.ta_rjo_shows, 
						cactmsaccountsummary.ta_ijo_shows, cactmsaccountsummary.ta_jany_spring, cactmsaccountsummary.ta_jany_summer, cactmsaccountsummary.ta_centurion_arizona, 
						cactmsaccountsummary.ta_centurion_south_beach, cactmsaccountsummary.ta_atlanta_spring, cactmsaccountsummary.ta_atlanta_fall, cactmsaccountsummary.ta_jck_las_vegas, 
						cactmsaccountsummary.ta_luxury_las_vegas, cactmsaccountsummary.ta_couture, cactmsaccountsummary.ta_montreal_expo, cactmsaccountsummary.tra_ags, 
						cactmsaccountsummary.tra_agta, cactmsaccountsummary.tra_ja, cactmsaccountsummary.tra_jvc, cactmsaccountsummary.tra_mjsa, cactmsaccountsummary.tra_snag, 
						cactmsaccountsummary.tra_naja, cactmsaccountsummary.tra_jbt, cactmsaccountsummary.tra_wja, cactmsaccountsummary.tra_aaa, cactmsaccountsummary.tra_dca, 
						cactmsaccountsummary.tra_maja, cactmsaccountsummary.bench_jeweler_onsite, cactmsaccountsummary.bg_sjo, cactmsaccountsummary.bg_ijo, 
						cactmsaccountsummary.bg_rjo, cactmsaccountsummary.bg_cbg, cactmsaccountsummary.bg_ljg, cactmsaccountsummary.bg_big ,cactmsaccountsummary.bg_prime, 
						cactmsaccountsummary.pc_bridal, cactmsaccountsummary.pc_branded, cactmsaccountsummary.pc_fashion, cactmsaccountsummary.pc_watches, 
						cactmsaccountsummary.pc_custom, cactmsaccountsummary.pc_silver, cactmsaccountsummary.sm_facebook, cactmsaccountsummary.sm_instagram, 
						cactmsaccountsummary.sm_twitter,cactmsaccountsummary.user_cd, cactmsaccountsummary.update_dt, cactmsaccountsummary.update_flag, cactmsaccountsummary.company_id, 
						cactmsaccountsummary.trans_flag, cactmsaccountsummary.Linkedin, thinkspace, punchmark, gemfind_jewelcloud, avalon_solutions_jewel_exchange, 
						clevergem,tov_customer_code, last_year_sales, ytd_sales, sales_2years_ago, sales_3years_ago, ptd_sales, no_of_invoices, last_pay_dt, return_per, 
						web_data, social_media_images, web_banner, price_list, printed_ads, billing_via_rjo_sjo,bill_via_sjo,last_activity, customer_bal,on_order 
						FROM cactmsaccountsummary 

						where (('{account_id}' = case when '{account_id}' = '' then '' end or cactmsaccountsummary.account_id = '{account_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerDocument(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT id, serial_no, company_id, user_cd, update_dt, update_flag,trans_flag, file_type, file_name, subject, remark, email_to, attachment 
						FROM cactaccountdocs 

						where (('{account_id}' = case when '{account_id}' = '' then '' end or id = '{account_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerTask(string prospect_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT DISTINCT company_id = a.company_id, trans_bk = a.trans_bk, trans_no = a.trans_no, trans_dt = a.trans_dt, task_date = a.trans_dt,task_type = a.task_type, 
						subject = a.subject,status = case a.status_flag when 'C' then 'CLOSE'when 'O' then 'OPEN' end, account_id = a.prospect_id, company_name = b.company_name, 
						salesperson_cd = a.salesperson_cd, salesperson_name = sp.name,description = a.remarks,group1 = b.group1, group2 = b.group2, group3 = b.group3, group4 = 
						b.group4, group5 = b.group5,assigned_by = sysmmsuser.name, assigned_to = sysmmsuser2.name, due_date = a.start_date, contact_id = a.contact_id, 
						c.first_name,c.last_name, cactmssetupdtl.value as task_type_desc 
						FROM cacttrtask a JOIN cacttrtaskcontacts D ON A.company_id = D.company_id AND A.trans_bk = D.trans_bk AND A.trans_no = D.trans_no AND A.trans_dt = 
						D.trans_dt JOIN cactmsaccount b ON D.prospect_id = b.id LEFT JOIN sysmmsuser sp ON b.salesperson_cd = sp.g_user_cd LEFT JOIN sysmmsuser ON a.Assigned_by = 
						sysmmsuser.g_user_cd LEFT JOIN sysmmsuser sysmmsuser2 ON a.Assigned_to = sysmmsuser2.g_user_cd LEFT JOIN cactmscontact c ON a.prospect_id = c.id AND 
						a.contact_id = c.serial_no LEFT JOIN cactmssetupdtl ON cactmssetupdtl.id = a.task_type AND cactmssetupdtl.setup_id = 'TASKTYPE' 

						where (('{prospect_id}' = case when '{prospect_id}' = '' then '' end or Isnull(D.prospect_id, '') = '{prospect_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(a.trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerCampaign(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cacttrcampaignhd.campaign_type, cactmssetupdtl.value as campaign_type_desc, cacttrcampaignhd.company_id, cacttrcampaignhd.trans_dt, 
						cacttrcampaignhd.trans_no, cacttrcampaignhd.trans_flag, cacttrcampaignhd.user_cd, cacttrcampaignhd.update_dt, cacttrcampaignhd.update_flag, 
						cacttrcampaignhd.campaign_id, case cacttrcampaignhd.campaign_status when 'P' then 'Planned' when 'I' then 'In-progress' when 'C' then 'Complete' 
						end as campaign_status, cacttrcampaignhd.remarks, cacttrcampaignhd.salesperson, cacttrcampaignhd.start_date, cacttrcampaignhd.end_date, 
						parent_campaign_no, parent_campaign_dt,sysmmsuser.name as salesperson_desc 
						FROM cacttrcampaigndtl LEFT JOIN cacttrcampaignhd ON cacttrcampaigndtl.company_id= cacttrcampaignhd.company_id and cacttrcampaigndtl.trans_bk = 
						cacttrcampaignhd.trans_bk and cacttrcampaigndtl.trans_no = cacttrcampaignhd.trans_no and cacttrcampaigndtl.trans_dt =cacttrcampaignhd.trans_dt 
						LEFT JOIN cactmssetupdtl ON cacttrcampaignhd.campaign_type = cactmssetupdtl.id and cactmssetupdtl.setup_id = 'CAMPAIGN' left join sysmmsuser on 
						salesperson = sysmmsuser.g_user_cd 

						where (('{account_id}' = case when '{account_id}' = '' then '' end or cacttrcampaigndtl.account_id = '{account_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerCredit(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT cactmstraderef.id, cactmstraderef.serial_no, cactmstraderef.person_to_contact, cactmstraderef.company_name, cactmstraderef.email, 
						cactmstraderef.user_cd, cactmstraderef.company_id, cactmstraderef.update_dt, cactmstraderef.update_flag, cactmstraderef.trans_flag , cactmstraderef.phone 
						FROM cactmstraderef 

						where (('{account_id}' = case when '{account_id}' = '' then '' end or Id = '{account_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCustomerJbt(string account_id, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT faarmscustomerjbtranking.id, faarmscustomerjbtranking.serial_no, faarmscustomerjbtranking.ranking_date, faarmscustomerjbtranking.jbt_ranking, 
						faarmscustomerjbtranking.remark, faarmscustomerjbtranking.company_id, faarmscustomerjbtranking.update_dt, faarmscustomerjbtranking.update_flag, 
						faarmscustomerjbtranking.trans_flag, faarmscustomerjbtranking.user_cd 
						FROM faarmscustomerjbtranking 

						where (('{account_id}' = case when '{account_id}' = '' then '' end or faarmscustomerjbtranking.id = '{account_id}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetExistingCustomerList(Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"SELECT  COUNT('') FROM cacttrcustomerlist ";
			}
			else
			{
				is_sql = $@"SELECT DISTINCT cacttrcustomerlist.list_name, created_by =  sysmmsuser.name, trans_dt 
							FROM cacttrcustomerlist left join sysmmsuser on sysmmsuser.g_user_cd = cacttrcustomerlist.user_cd 
							WHERE cacttrcustomerlist.trans_flag = 'A' ";
			}

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(list_name) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetExistingCustomerDetail(string status, string list_name, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT account_id = c.id,category_name = d.name,a.trans_bk,b.ship_via, a.trans_no, a.trans_dt, a.serial_no, a.list_name, a.user_cd, b.credit_limit, 
						b.sales_person, a.customer_email, id = a.customer_id, a.customer_name, a.update_dt, a.company_id, a.trans_flag, a.update_flag,b.category, name = b.name, 
						contact1 =b.contact1, contact2=b.contact2, address1= b.address1, address2=b.address2, city=b.city, state= b.state,zip =b.zip, country=b.country, 
						phone1=b.phone1, phone2=b.phone2, fax1=b.fax1, email = b.email 
						FROM cacttrcustomerlist a left join faarmscustomer b on b.id = a.customer_id left join cactmsaccount c on c.parent_account_id = a.customer_id 
						left join faarmscustomercategory d on b.category = d.category 

						where (('{status}' = case when '{status}' = '' then '' end or a.trans_flag = '{status}')
						and ('{list_name}' = case when '{list_name}' = '' then '' end or a.list_name = '{list_name}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCampaignList(string campaign_type, string actual_cost, string method, string purpose, string campaign_status, string frequency, string salesperson, string budget, string start_date, string end_date, string campaign_date, string trans_no, string update_dt, string user_cd, string parent_campaign_no, string parent_campaign_dt, string campaign_id, string remarks, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"SELECT COUNT('') from cacttrcampaignhd ";
			}
			else
			{
				is_sql = $@"SELECT method_desc = cactmssetupdtl.value, sysmmsuser.name as salesperson_name, cacttrcampaignhd.campaign_type, cacttrcampaignhd.company_id, 
							cacttrcampaignhd.trans_bk, cacttrcampaignhd.trans_no, cacttrcampaignhd.trans_dt, cacttrcampaignhd.trans_flag, cacttrcampaignhd.user_cd, 
							cacttrcampaignhd.update_dt, cacttrcampaignhd.update_flag, cacttrcampaignhd.campaign_id, CASE WHEN cacttrcampaignhd.campaign_status = 'I' 
							THEN 'In-progress' WHEN cacttrcampaignhd.campaign_status = 'P' THEN 'Planned' WHEN cacttrcampaignhd.campaign_status = 'C' THEN 'Complete' 
							END As campaign_status_title, campaign_status, cacttrcampaignhd.remarks, cacttrcampaignhd.start_date, cacttrcampaignhd.end_date, 
							cacttrcampaignhd.salesperson, cacttrcampaignhd.post_flag, parent_campaign_no, parent_campaign_dt, frequency = cacttrcampaignhd.frequency, 
							CASE WHEN cacttrcampaignhd.frequency = 'O' THEN 'One-time'  WHEN cacttrcampaignhd.frequency = 'D' THEN 'Daily' WHEN cacttrcampaignhd.frequency = 'W' 
							THEN 'Weekly' WHEN cacttrcampaignhd.frequency = 'M' THEN 'Monthly' WHEN cacttrcampaignhd.frequency = 'A' THEN 'Annual' end as frequency_desc, budget, 
							actual_cost, RTRIM(method) AS method, RTRIM(purpose) AS purpose 
							FROM cacttrcampaignhd LEFT Join sysmmsuser on cacttrcampaignhd.salesperson = g_user_cd left join cactmssetupdtl on cactmssetupdtl.id = 
							cacttrcampaignhd.method and cactmssetupdtl.setup_id = 'CAMPAIGN_METHOD' AND cactmssetupdtl.trans_flag = 'A' ";
			}

			is_sql += $@"where (('{campaign_type}' = case when '{campaign_type}' = '' then '' end or campaign_type = '{campaign_type}') 
						 and ('{actual_cost}' = case when '{actual_cost}' = '' then '' end or actual_cost = '{actual_cost}') 
						 and ('{method}' = case when '{method}' = '' then '' end or method = '{method}') 
						 and ('{purpose}' = case when '{purpose}' = '' then '' end or purpose = '{purpose}')
						 and ('{campaign_status}' = case when '{campaign_status}' = '' then '' end or campaign_status = '{campaign_status}') 
						 and ('{frequency}' = case when '{frequency}' = '' then '' end or frequency = '{frequency}') 
						 and ('{salesperson}' = case when '{salesperson}' = '' then '' end or salesperson = '{salesperson}') 
						 and ('{budget}' = case when '{budget}' = '' then '' end or budget = '{budget}') 
						 and ('{start_date}' = case when '{start_date}' = '' then '' end or cast(start_date as date) = '{start_date}') 
						 and ('{end_date}' = case when '{end_date}' = '' then '' end or cast(end_date as date) = '{end_date}') 
						 and ('{campaign_date}' = case when '{campaign_date}' = '' then '' end or cast(trans_dt as date) = '{campaign_date}') 
						 and ('{trans_no}' = case when '{trans_no}' = '' then '' end or trans_no = '{trans_no}') 
						 and ('{update_dt}' = case when '{update_dt}' = '' then '' end or cast( cacttrcampaignhd.update_dt as date) = '{update_dt}') 
						 and ('{user_cd}' = case when '{user_cd}' = '' then '' end or cacttrcampaignhd.user_cd = '{user_cd}') 
						 and ('{parent_campaign_no}' = case when '{parent_campaign_no}' = '' then '' end or parent_campaign_no = '{parent_campaign_no}') 
						 and ('{parent_campaign_dt}' = case when '{parent_campaign_dt}' = '' then '' end or cast(parent_campaign_dt as date) = '{parent_campaign_dt}'))

						 and (('{campaign_id}' LIKE case when '{campaign_id}' = '' then '' end or campaign_id like '%{campaign_id}%')
						 and ('{remarks}' LIKE case when '{remarks}' = '' then '' end or remarks like '%{remarks}%'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();

		}

		public DataTable GetCampaignCustomerList(string campaign_no, string book, string company_id, string campaign_date, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT salesperson_name = sp.name, faarmscustomercategory.name as category_name, cacttrcampaigndtl.category, cacttrcampaigndtl.id, cacttrcampaigndtl.company_id, 
						cacttrcampaigndtl.trans_bk, cacttrcampaigndtl.trans_no, cacttrcampaigndtl.trans_dt, cacttrcampaigndtl.serial_no, cacttrcampaigndtl.user_cd, 
						cacttrcampaigndtl.update_dt, cacttrcampaigndtl.update_flag, cacttrcampaigndtl.trans_flag, cacttrcampaigndtl.name, cacttrcampaigndtl.contact1, 
						cacttrcampaigndtl.contact2, cacttrcampaigndtl.address1, cacttrcampaigndtl.address2, cacttrcampaigndtl.city, cacttrcampaigndtl.state, 
						cacttrcampaigndtl.zip, cacttrcampaigndtl.country, cacttrcampaigndtl.phone1, cacttrcampaigndtl.phone2, cacttrcampaigndtl.fax1, cacttrcampaigndtl.email, 
						cacttrcampaigndtl.credit_limit, cacttrcampaigndtl.sales_person, cacttrcampaigndtl.ship_via, cacttrcampaigndtl.account_id, tracking_no = shiptrshipments.tracking_no, 
						cacttrcampaigndtl.ship_id, no_of_catalog = faarmscustomer.udf6, cactmsaccount.web_id, cactmsaccount.subscription_email 
						FROM cacttrcampaigndtl  LEFT JOIN faarmscustomer ON cacttrcampaigndtl.id = faarmscustomer.id LEFT JOIN shiptrshipments ON  cacttrcampaigndtl.trans_bk = 
						shiptrshipments.trans_bk AND Ltrim(Rtrim(cacttrcampaigndtl.trans_no)) + '-' + Ltrim(Rtrim(cacttrcampaigndtl.serial_no)) = shiptrshipments.trans_no 
						LEFT JOIN cactmsaccount ON cactmsaccount.id = cacttrcampaigndtl.account_id left join  faarmscustomercategory  on  faarmscustomercategory.category = 
						cacttrcampaigndtl.category LEFT JOIN saoimssalesperson sp ON cacttrcampaigndtl.sales_person = sp.id 

						where (('{campaign_no}' = case when '{campaign_no}' = '' then '' end or cacttrcampaigndtl.trans_no = '{campaign_no}') 
						and ('{book}' = case when '{book}' = '' then '' end or cacttrcampaigndtl.trans_bk = '{book}') 
						and ('{company_id}' = case when '{company_id}' = '' then '' end or cacttrcampaigndtl.company_id = '{company_id}') 
						and ('{campaign_date}' = case when '{campaign_date}' = '' then '' end or cacttrcampaigndtl.trans_dt = '{campaign_date}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCampaignImportCustomer(string status, string list_name, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT account_id = c.id,category_name = d.name,a.trans_bk,b.ship_via, a.trans_no, a.trans_dt, a.serial_no, a.list_name, a.user_cd,b.credit_limit, 
						b.sales_person, a.customer_email, id = a.customer_id, a.customer_name, a.update_dt, a.company_id, a.trans_flag, a.update_flag, b.category, name = b.name, 
						contact1 =b.contact1, contact2=b.contact2, address1= b.address1, address2=b.address2, city=b.city, state= b.state, zip =b.zip, country=b.country, 
						phone1=b.phone1, phone2=b.phone2, fax1=b.fax1, email = b.email 
						FROM cacttrcustomerlist a left join faarmscustomer b on b.id = a.customer_id left join cactmsaccount c on c.parent_account_id = a.customer_id 
						left join faarmscustomercategory d on b.category = d.category 

						where (('{status}' = case when '{status}' = '' then '' end or a.trans_flag = '{status}') 
						and ('{list_name}' = case when '{list_name}' = '' then '' end or a.list_name = '{list_name}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetCampaignDocument(string campaign_no, string book, string company_id, string campaign_date, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT trans_bk, trans_no, trans_dt, serial_no, company_id, user_cd, update_dt, update_flag, trans_flag, file_type, file_name, subject, remark, 
						email_to, attachment FROM cactcampaigndocs 

						where (('{campaign_no}' = case when '{campaign_no}' = '' then '' end or cactcampaigndocs.trans_no = '{campaign_no}') 
						and ('{book}' = case when '{book}' = '' then '' end or cactcampaigndocs.trans_bk = '{book}') 
						and ('{company_id}' = case when '{company_id}' = '' then '' end or cactcampaigndocs.company_id = '{company_id}') 
						and ('{campaign_date}' = case when '{campaign_date}' = '' then '' end or cactcampaigndocs.trans_dt = '{campaign_date}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetPrepareCustomerList(string category_from, string category_to, string id_from, string id_to, string name_from, string name_to, string state_from, string state_to, string city_from, string city_to, string group1_from, string group1_to, string group2_from, string group2_to, string status, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT distinct 'N' as select_flag,id = a.id,category = a.category, name = a.name, contact1 = a.contact1, contact2 = a.contact2, address1 =a.address1, 
						address2 = a.address2, city = a.city,state = a.state, zip = a.zip, country = a.country, phone1 = a.phone1, phone2 = a.phone2, fax1 = a.fax1, 
						email = a.email, credit_limit = a.credit_limit, salesperson_name = sp.name, sales_person = a.sales_person, ship_via = a.ship_via, trans_flag = a.trans_flag, 
						account_id = b.id, c.name as category_name, 'Y' as update_flag, tracking_no = d.tracking_no, web_id = b.web_id, subscription_email = b.subscription_email 
						FROM faarmscustomer a left join cactmsaccount b on a.id = b.parent_account_id left join faarmscustomercategory c on (c.category = a.category) left join	
						shiptrshipments d on d.account_id = a.id LEFT JOIN saoimssalesperson sp ON a.sales_person = sp.id 

						where (('{category_from}' = case when '{category_from}' = '' then '' end or a.category BETWEEN '{category_from}' AND '{category_to}') 
						and ('{id_from}' = case when '{id_from}' = '' then '' end or a.id BETWEEN '{id_from}' AND '{id_to}') 
						and ('{name_from}' = case when '{name_from}' = '' then '' end or a.name BETWEEN '{name_from}' AND '{name_to}') 
						and ('{state_from}' = case when '{state_from}' = '' then '' end or ISNULL(A.STATE,'') BETWEEN '{state_from}' AND '{state_to}')
						and ('{city_from}' = case when '{city_from}' = '' then '' end or ISNULL(A.CITY,'') BETWEEN '{city_from}' AND '{city_to}') 
						and ('{group1_from}' = case when '{group1_from}' = '' then '' end or B.group1 BETWEEN '{group1_from}' AND '{group1_to}') 
						and ('{group2_from}' = case when '{group2_from}' = '' then '' end or B.group2 BETWEEN '{group2_from}' AND '{group2_to}'))
						AND (('{status}' = case when '{status}' = '' then '' end or A.TRANS_FLAG = '{status}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(id) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}
			else
            {
				is_sql += $" ORDER BY b.id ";
			}
			return Execute();
		}

		public DataTable GetGenerateChildCampaign(string campaign_no)
		{
			is_sql = $@"exec [spcact_generate_child_campaign_no] '{campaign_no}' ";

			return Execute();
		}

		public DataTable GetActivityInfoList(string activity_no_from, string activity_no_to, string activity_date_from, string activity_date_to, string activity_type_from, string activity_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string activity_by_from, string activity_by_to, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"SELECT distinct COUNT(distinct concat( a.trans_no,a.company_id, sysmmsuser.name, a.trans_bk,  a.trans_dt,a.trans_dt, Cactmssetupdtl.value, a.subject, 
							D.prospect_id, b.company_name, a.salesperson_cd, sp.name, a.remarks, b.group1, b.group2, b.group3, b.group4, b.group5, b.address1+b.address2, b.phone, 
							b.city, b.state, b.zip, b.parent_account_id, b.customer_flag,a.user_cd, a.contact_id, task_id, a.ref_bk, a.ref_no, a.ref_dt)) ";
			}
			else
			{
				is_sql = $@"SELECT distinct trans_no = a.trans_no, company_id = a.company_id,assigned_by_name = sysmmsuser.name, trans_bk = a.trans_bk, trans_dt = a.trans_dt, 
							task_date = a.trans_dt, task_type = Cactmssetupdtl.value, subject = a.subject, account_id = D.prospect_id, company_name = b.company_name, 
							salesperson_cd = a.salesperson_cd, salesperson_name = sp.name, description = a.remarks, group1 = b.group1, group2 = b.group2, group3 = b.group3, 
							group4 = b.group4, group5 = b.group5, address = b.address1+b.address2, phone = b.phone, city = b.city, state = b.state, zip = b.zip, 
							customer_id = b.parent_account_id, CASE WHEN b.customer_flag = 'Y' THEN 'Yes' WHEN b.customer_flag = 'N' THEN 'No' END As customer_flag, 
							assigned_by = a.user_cd, contact_id = a.contact_id, task_id, a.ref_bk, a.ref_no, a.ref_dt ";
			}

			is_sql += $@"FROM cacttractivity a  LEFT JOIN cacttractivitycontacts D ON A.company_id = D.company_id AND A.trans_bk = D.trans_bk AND A.trans_no = D.trans_no AND 
						A.trans_dt = D.trans_dt LEFT OUTER JOIN cactmsaccount b  ON a.prospect_id = b.id LEFT OUTER JOIN saoimssalesperson sp ON b.salesperson_cd = sp.id 
						LEFT OUTER JOIN cactmscontact c ON D.prospect_id = c.id AND D.contact_id = c.serial_no left join Cactmssetupdtl on Cactmssetupdtl.Id = a.activity_type 
						and Cactmssetupdtl.trans_flag = 'A' and Cactmssetupdtl.Setup_id = 'TASKTYPE'  left join sysmmsuser on sysmmsuser.g_user_cd = a.user_cd 

						where (('{activity_no_from}' = case when '{activity_no_from}' = '' then '' end or a.trans_no BETWEEN '{activity_no_from}' AND '{activity_no_to}') 
						and ('{activity_date_from}' = case when '{activity_date_from}' = '' then '' end or a.trans_dt BETWEEN '{activity_date_from}' AND '{activity_date_to}') 
						and ('{activity_type_from}' = case when '{activity_type_from}' = '' then '' end or a.activity_type BETWEEN '{activity_type_from}' AND '{activity_type_to}') 
						and ('{prospect_id_from}' = case when '{prospect_id_from}' = '' then '' end or D.prospect_id BETWEEN '{prospect_id_from}' AND '{prospect_id_to}') 
						and ('{company_name_from}' = case when '{company_name_from}' = '' then '' end or b.company_name BETWEEN '{company_name_from}' AND '{company_name_to}') 
						and ('{salesperson_cd_from}' = case when '{salesperson_cd_from}' = '' then '' end or a.salesperson_cd BETWEEN '{salesperson_cd_from}' AND '{salesperson_cd_to}') 
						and ('{city_from}' = case when '{city_from}' = '' then '' end or b.city BETWEEN '{city_from}' AND '{city_to}') 
						and ('{state_from}' = case when '{state_from}' = '' then '' end or b.state BETWEEN '{state_from}' AND '{state_to}') 
						and ('{zip_from}' = case when '{zip_from}' = '' then '' end or b.zip BETWEEN '{zip_from}' AND '{zip_to}') 
						and ('{group1_from}' = case when '{group1_from}' = '' then '' end or b.group1 BETWEEN '{group1_from}' AND '{group1_to}') 
						and ('{group2_from}' = case when '{group2_from}' = '' then '' end or b.group2 BETWEEN '{group2_from}' AND '{group2_to}') 
						and ('{group3_from}' = case when '{group3_from}' = '' then '' end or b.group3 BETWEEN '{group3_from}' AND '{group3_to}') 
						and ('{group4_from}' = case when '{group4_from}' = '' then '' end or b.group4 BETWEEN '{group4_from}' AND '{group4_to}') 
						and ('{group5_from}' = case when '{group5_from}' = '' then '' end or b.group5 BETWEEN '{group5_from}' AND '{group5_to}') 
						and ('{activity_by_from}' = case when '{activity_by_from}' = '' then '' end or a.user_cd BETWEEN '{activity_by_from}' AND '{activity_by_to}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(a.trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetTaskList(string task_no_from, string task_no_to, string task_date_from, string task_date_to, string start_date_from, string start_date_to, string task_type_from, string task_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string assigned_by_from, string assigned_by_to, string assigned_to_from, string assigned_to_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string status_flag, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"SELECT DISTINCT COUNT ( DISTINCT CONCAT(a.company_id,a.status_flag,cactmssetupdtl.value, a.trans_bk, a.trans_no, a.trans_dt, a.trans_dt, a.task_type, 
							a.subject, a.status_flag, a.prospect_id, b.company_name, a.salesperson_cd, sp.name,  a.remarks, b.group1, b.group2, b.group3, b.group4, b.group5, 
							b.city, b.state, b.zip, b.parent_account_id, b.customer_flag, a.Assigned_by, a.assigned_to, a.start_date, a.start_hour, a.start_minute, 
							a.start_time_format, a.start_time_zone, a.duration_day, a.duration_hour, a.duration_minute, a.priority , a.remarks, b.phone, b.address1 + b.address2, 
							sysmmsuser.name, sysmmsuser_2.name, a.contact_id)) ";
			}
			else
			{
				is_sql = $@"SELECT DISTINCT company_id = a.company_id, status_flag, CASE WHEN a.status_flag = 'C' THEN 'Close' WHEN a.status_flag = 'O' THEN 'Open' END as status_name, 
							cactmssetupdtl.value as tasktype_name, trans_bk = a.trans_bk, trans_no = a.trans_no, trans_dt = a.trans_dt, task_date = a.trans_dt, 
							task_type = a.task_type, subject = a.subject, status = a.status_flag, account_id = a.prospect_id, company_name = b.company_name, 
							salesperson_cd = a.salesperson_cd, salesperson_name = sp.name, description = a.remarks, group1 = b.group1, group2 = b.group2, group3 = b.group3, 
							group4 = b.group4, group5 = b.group5, city = b.city, state = b.state, zip = b.zip, customer_id = b.parent_account_id, customer_flag = b.customer_flag, 
							CASE WHEN b.customer_flag = 'Y' THEN 'Yes' WHEN b.customer_flag = 'N' THEN 'No' END As customer_flag_name, assigned_by =a.Assigned_by, 
							assigned_to = a.assigned_to, due_date = a.start_date, due_hour = a.start_hour, due_min = a.start_minute, due_time_format = a.start_time_format, 
							due_time_zone = a.start_time_zone, duration_day = a.duration_day, duration_hour = a.duration_hour, duration_minute= a.duration_minute, a.priority, 
							CASE WHEN rtrim(a.priority) = 'H' THEN 'High' WHEN rtrim(a.priority) = 'M' THEN 'Medium' WHEN rtrim(a.priority) = 'L' THEN 'Low' END As priority_name, 
							remarks = a.remarks, phone =b.phone,address = b.address1 + b.address2, assigned_by_name = sysmmsuser.name, assigned_to_name = sysmmsuser_2.name, 
							contact_id = a.contact_id ";
			}

			is_sql += $@"FROM cacttrtask a LEFT JOIN cacttrtaskcontacts D ON A.company_id = D.company_id AND A.trans_bk = D.trans_bk AND A.trans_no = D.trans_no AND 
						 A.trans_dt = D.trans_dt LEFT JOIN cactmsaccount b ON a.prospect_id = b.id LEFT JOIN saoimssalesperson sp ON b.salesperson_cd = sp.id LEFT JOIN 
						 cactmscontact c ON D.prospect_id = c.id AND D.contact_id = c.serial_no left join cactmssetupdtl on cactmssetupdtl.id = a.task_type AND 
						 cactmssetupdtl.setup_id = 'TASKTYPE' AND Cactmssetupdtl.trans_flag = 'A' left join sysmmsuser on sysmmsuser.g_user_cd = a.assigned_by AND 
						 sysmmsuser.trans_flag='A' left join sysmmsuser as sysmmsuser_2 on sysmmsuser_2.g_user_cd = a.assigned_to AND sysmmsuser_2.trans_flag='A' 

						where (('{status_flag}' = case when '{status_flag}' = '' then '' end or a.status_flag = '{status_flag}') 
						and ('{task_no_from}' = case when '{task_no_from}' = '' then '' end or a.trans_no BETWEEN '{task_no_from}' AND '{task_no_to}') 
						and ('{task_date_from}' = case when '{task_date_from}' = '' then '' end or a.trans_dt BETWEEN '{task_date_from}' AND '{task_date_to}') 
						and ('{start_date_from}' = case when '{start_date_from}' = '' then '' end or a.start_date BETWEEN '{start_date_from}' AND '{start_date_to}') 
						and ('{task_type_from}' = case when '{task_type_from}' = '' then '' end or task_type BETWEEN '{task_type_from}' AND '{task_type_to}') 
						and ('{prospect_id_from}' = case when '{prospect_id_from}' = '' then '' end or a.prospect_id BETWEEN '{prospect_id_from}' AND '{prospect_id_to}') 
						and ('{company_name_from}' = case when '{company_name_from}' = '' then '' end or b.company_name BETWEEN '{company_name_from}' AND '{company_name_to}') 
						and ('{salesperson_cd_from}' = case when '{salesperson_cd_from}' = '' then '' end or ISNULL(a.salesperson_cd,'') BETWEEN '{salesperson_cd_from}' AND '{salesperson_cd_to}') 
						and ('{assigned_by_from}' = case when '{assigned_by_from}' = '' then '' end or ISNULL(assigned_by,'') BETWEEN '{assigned_by_from}' AND '{assigned_by_to}') 
						and ('{assigned_to_from}' = case when '{assigned_to_from}' = '' then '' end or ISNULL(assigned_to,'') BETWEEN '{assigned_to_from}' AND '{assigned_to_to}') 
						and ('{city_from}' = case when '{city_from}' = '' then '' end or ISNULL(b.city,'') BETWEEN '{city_from}' AND '{city_to}') 
						and ('{state_from}' = case when '{state_from}' = '' then '' end or ISNULL(b.state,'') BETWEEN '{state_from}' AND '{state_to}') 
						and ('{zip_from}' = case when '{zip_from}' = '' then '' end or ISNULL(b.zip,'') BETWEEN '{zip_from}' AND '{zip_to}') 
						and ('{group1_from}' = case when '{group1_from}' = '' then '' end or ISNULL(b.group1,'') BETWEEN '{group1_from}' AND '{group1_to}') 
						and ('{group2_from}' = case when '{group2_from}' = '' then '' end or ISNULL(b.group2,'') BETWEEN '{group2_from}' AND '{group2_to}') 
						and ('{group3_from}' = case when '{group3_from}' = '' then '' end or ISNULL(b.group3,'') BETWEEN '{group3_from}' AND '{group3_to}') 
						and ('{group4_from}' = case when '{group4_from}' = '' then '' end or ISNULL(b.group4,'') BETWEEN '{group4_from}' AND '{group4_to}') 
						and ('{group5_from}' = case when '{group5_from}' = '' then '' end or ISNULL(b.group5,'') BETWEEN '{group5_from}' AND '{group5_to}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(trans_no) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}




		public DataTable GetCopyFromSales(string category_from, string category_to, string id_from, string id_to, string name_from, string name_to, string last_year_sales_from, string last_year_sales_to, string ytd_sales_from, string ytd_sales_to, string sales_2years_ago_from, string sales_2years_ago_to, string sales_3years_ago_from, string sales_3years_ago_to, string last_pay_dt_from, string last_pay_dt_to, Int32 page_no, Int32 page_size)
		{
			is_sql = $@"SELECT account_id = a.account_id, address1 = b.address1, address2=b.address2, name=b.name, category_name = c.name, contact1 = b.contact1, 
						contact2 = b.contact2, city = b.city, state = b.state,zip = b.zip, country = b.country, phone1 = b.phone1, phone2 = b.phone2 , credit_limit= b.credit_limit, 
						fax1 = b.fax1,salesperson_name= sp.name, salesperson_name = b.sales_person,sales_person = b.sales_person,ship_via = b.ship_via,email = b.email,id = b.id, 
						trans_flag = b.trans_flag,category = b.category, name = b.name,last_year_sales = a.last_year_sales,ytd_sales = a.ytd_sales,sales_2years_ago = 
						a.sales_2years_ago,sales_3years_ago = a.sales_3years_ago, a.ptd_sales, a.no_of_invoices,last_pay_dt = a.last_pay_dt,a.return_per,select_flag = 'N', 
						last_pay_dt = a.last_pay_dt,tracking_no = d.tracking_no,web_id = e.web_id, subscription_email = e.subscription_email 
						FROM cactmsaccountsummary  a Inner Join faarmscustomer b on    a.account_id = b.id left join faarmscustomercategory c on (c.category = b.category)  left join 
						shiptrshipments d on d.account_id = b.id left join cactmsaccount e on    b.id = e.parent_account_id LEFT JOIN saoimssalesperson sp ON b.sales_person = sp.id 

						where (('{category_from}' = case when '{category_from}' = '' then '' end or b.category BETWEEN '{category_from}' AND '{category_to}') 
						and ('{id_from}' = case when '{id_from}' = '' then '' end or b.id BETWEEN '{id_from}' AND '{id_to}') 
						and ('{name_from}' = case when '{name_from}' = '' then '' end or b.name BETWEEN '{name_from}' AND '{name_to}') 
						and ('{last_year_sales_from}' = case when '{last_year_sales_from}' = '' then '' end or ISNULL(a.last_year_sales,0.00) BETWEEN '{last_year_sales_from}' AND '{last_year_sales_to}') 
						and ('{ytd_sales_from}' = case when '{ytd_sales_from}' = '' then '' end or ISNULL(a.ytd_sales,0.00) BETWEEN '{ytd_sales_from}' AND '{ytd_sales_to}') 
						and ('{sales_2years_ago_from}' = case when '{sales_2years_ago_from}' = '' then '' end or ISNULL(a.sales_2years_ago,0.00) BETWEEN '{sales_2years_ago_from}' AND '{sales_2years_ago_to}') 
						and ('{sales_3years_ago_from}' = case when '{sales_3years_ago_from}' = '' then '' end or ISNULL(a.sales_3years_ago,0.00) BETWEEN '{sales_3years_ago_from}' AND '{sales_3years_ago_to}') 
						and ('{last_pay_dt_from}' = case when '{last_pay_dt_from}' = '' then '' end or ISNULL(a.last_pay_dt,'01/01/1950') BETWEEN '{last_pay_dt_from}' AND '{last_pay_dt_to}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(b.id) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}
			else
            {
				is_sql += $" ORDER BY b.id ";
			}
			return Execute();
		}

		public DataTable GetContactInfoList(string category_id_from, string category_id_to, string id_from, string id_to, string salesperson_cd_from, string salesperson_cd_to, string city_from, string city_to, string start_date_from, string start_date_to, string state_from, string state_to, string zip_from, string zip_to, string phone_from, string phone_to, string status, string customer_flag, string subscribed_to_emails, string contact_type, string company_id, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"Select count(*) as Count ";
			}
			else
			{
				is_sql = $@"SELECT account_id = A.id, category = A.category_id, A.start_dt, cactmssetupdtl.value as contact_type_desc, salesperson = C.name, customer_name = 
							A.company_name, buyer_id = B.serial_no, contact_type = B.contact_type, name = RTRIM( ISNULL( B.first_name,'')) +' ' + 
							RTRIM(ISNULL(B.last_name,'')), address = RTRIM(ISNULL(B.address1,'')) +' ' + RTRIM(ISNULL(B.address2,'')) +' '+ RTRIM(ISNULL(B.city,'')) +' '+ 
							RTRIM(ISNULL(B.state,'')) +' '+ RTRIM(ISNULL(B.zip,'')) +' '+ RTRIM(ISNULL(B.country,'')), phone = ISNULL(B.phone1, B.phone2), 
							email_id = CASE When ISNULL(B.email_id,'') = '' Then ISNULL(B.email_id2,'') Else ISNULL(B.email_id,'') End, title = B.title, 
							subscribed_to_emails = ISNULL(B.subscribed_to_emails,'N') ";
			}

			is_sql += $@"FROM cactmsaccount A INNER JOIN cactmscontact B on A.id = B.id AND A.company_id = B.company_id LEFT JOIN saoimssalesperson C ON A.salesperson_cd = C.id 
						 left join cactmssetupdtl on cactmssetupdtl.id = B.contact_type  AND cactmssetupdtl.setup_id = 'CONTACT_TYPE' AND cactmssetupdtl.trans_flag = 'A' 

						where (('{category_id_from}' = case when '{category_id_from}' = '' then '' end or A.category_id BETWEEN '{category_id_from}' AND '{category_id_to}') 
						and ('{id_from}' = case when '{id_from}' = '' then '' end or A.id BETWEEN '{id_from}' AND '{id_to}') 
						and ('{salesperson_cd_from}' = case when '{salesperson_cd_from}' = '' then '' end or ISNULL(A.salesperson_cd,'') BETWEEN '{salesperson_cd_from}' AND '{salesperson_cd_to}') 
						and ('{city_from}' = case when '{city_from}' = '' then '' end or ISNULL(B.city,'') BETWEEN '{city_from}' AND '{city_to}') 
						and ('{start_date_from}' = case when '{start_date_from}' = '' then '' end or cast(A.start_dt as date) BETWEEN '{start_date_from}' AND '{start_date_to}') 
						and ('{state_from}' = case when '{state_from}' = '' then '' end or ISNULL(B.state,'') BETWEEN '{state_from}' AND '{state_to}') 
						and ('{zip_from}' = case when '{zip_from}' = '' then '' end or B.zip BETWEEN '{zip_from}' AND '{zip_to}') 
						and ('{phone_from}' = case when '{phone_from}' = '' then '' end or B.PHONE1 BETWEEN '{phone_from}' AND '{phone_to}') 
						and ('{status}' = case when '{status}' = '' then '' end or B.trans_flag = '{status}') 
						and ('{customer_flag}' = case when '{customer_flag}' = '' then '' end or A.customer_flag = '{customer_flag}') 
						and ('{subscribed_to_emails}' = case when '{subscribed_to_emails}' = '' then '' end or B.subscribed_to_emails = '{subscribed_to_emails}') 
						and ('{contact_type}' = case when '{contact_type}' = '' then '' end or B.contact_type = '{contact_type}')) 

						and (('{company_id}' = case when '{company_id}' = '' then '' end or A.COMPANY_ID = '{company_id}')) ";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetTaskReportList(string trans_no_from, string trans_no_to, string trans_dt_from, string trans_dt_to, string start_date_from, string start_date_to, string task_type_from, string task_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string assigned_by_from, string assigned_by_to, string assigned_to_from, string assigned_to_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string status_flag, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"Select count(*) as Count ";
			}
			else
			{
				is_sql = $@"SELECT company_id = a.company_id, trans_bk = a.trans_bk, trans_no = a.trans_no, trans_dt = a.trans_dt, task_date = a.trans_dt, task_type = a.task_type, 
							cactmssetupdtl.value as tasktype_name, subject = a.subject, status = a.status_flag, CASE WHEN a.status_flag = 'C' THEN 'Close' WHEN a.status_flag = 'O' 
							THEN 'Open' END As status_flag_name, account_id = a.prospect_id, company_name = b.company_name, salesperson_cd = a.salesperson_cd, salesperson_name = 
							sp.name, description = a.remarks, group1 = b.group1, group2 = b.group2, group3 = b.group3, group4 = b.group4, group5 = b.group5, city = b.city, 
							state = b.state, zip = b.zip, customer_id = b.parent_account_id, customer_flag = b.customer_flag, assigned_by = a.Assigned_by, assigned_to = 
							a.Assigned_to, due_date = a.start_date, contact_id = a.contact_id, c.first_name, c.last_name, phone = c.phone1, salesperson_contact = 
							RTRIM(isnull(c.first_name,'')) +' '+ RTRIM(isnull(c.last_name,'')), sysmmsuser.name as assigned_to_name, temp.name as assigned_by_name ";
			}

			is_sql += $@"FROM cacttrtask a left join cactmssetupdtl on cactmssetupdtl.id = a.task_type AND cactmssetupdtl.trans_flag = 'A' AND Cactmssetupdtl.Setup_id = 'TASKTYPE' 
						 LEFT OUTER JOIN cactmsaccount b ON a.prospect_id = b.id LEFT OUTER JOIN saoimssalesperson sp  ON b.salesperson_cd = sp.id LEFT OUTER JOIN cactmscontact c 
						 ON a.prospect_id = c.id AND a.contact_id = c.serial_no left join sysmmsuser on a.assigned_to = g_user_cd AND sysmmsuser.trans_flag='A' left join 
						 sysmmsuser as temp on a.assigned_by = temp.g_user_cd AND sysmmsuser.trans_flag='A' 

						where(('{trans_no_from}' = case when '{trans_no_from}' = '' then '' end or a.trans_no BETWEEN '{trans_no_from}' AND '{trans_no_to}') 
						and ('{trans_dt_from}' = case when '{trans_dt_from}' = '' then '' end or a.trans_dt BETWEEN '{trans_dt_from}' AND '{trans_dt_to}') 
						and ('{start_date_from}' = case when '{start_date_from}' = '' then '' end or cast(a.start_date as date) BETWEEN '{start_date_from}' AND '{start_date_to}') 
						and ('{task_type_from}' = case when '{task_type_from}' = '' then '' end or a.task_type BETWEEN '{task_type_from}' AND '{task_type_to}') 
						and ('{prospect_id_from}' = case when '{prospect_id_from}' = '' then '' end or a.prospect_id BETWEEN '{prospect_id_from}' AND '{prospect_id_to}') 
						and ('{company_name_from}' = case when '{company_name_from}' = '' then '' end or b.company_name BETWEEN '{company_name_from}' AND '{company_name_to}') 
						and ('{salesperson_cd_from}' = case when '{salesperson_cd_from}' = '' then '' end or ISNULL(a.salesperson_cd,'') BETWEEN '{salesperson_cd_from}' AND '{salesperson_cd_to}') 
						and ('{assigned_by_from}' = case when '{assigned_by_from}' = '' then '' end or ISNULL(assigned_by,'') BETWEEN '{assigned_by_from}' AND '{assigned_by_to}') 
						and ('{assigned_to_from}' = case when '{assigned_to_from}' = '' then '' end or ISNULL(assigned_to,'') BETWEEN '{assigned_to_from}' AND '{assigned_to_to}') 
						and ('{city_from}' = case when '{city_from}' = '' then '' end or ISNULL(b.city,'') BETWEEN '{city_from}' AND '{city_to}') 
						and ('{state_from}' = case when '{state_from}' = '' then '' end or ISNULL(b.state,'') BETWEEN '{state_from}' AND '{state_to}') 
						and ('{zip_from}' = case when '{zip_from}' = '' then '' end or ISNULL(b.zip,'') BETWEEN '{zip_from}' AND '{zip_to}') 
						and ('{group1_from}' = case when '{group1_from}' = '' then '' end or ISNULL(b.group1,'') BETWEEN '{group1_from}' AND '{group1_to}') 
						and ('{group2_from}' = case when '{group2_from}' = '' then '' end or ISNULL(b.group2,'') BETWEEN '{group2_from}' AND '{group2_to}') 
						and ('{group3_from}' = case when '{group3_from}' = '' then '' end or ISNULL(b.group3,'') BETWEEN '{group3_from}' AND '{group3_to}') 
						and ('{group4_from}' = case when '{group4_from}' = '' then '' end or ISNULL(b.group4,'') BETWEEN '{group4_from}' AND '{group4_to}') 
						and ('{group5_from}' = case when '{group5_from}' = '' then '' end or ISNULL(b.group5,'') BETWEEN '{group5_from}' AND '{group5_to}')) 
						and (('{status_flag}' = case when '{status_flag}' = '' then '' end or a.status_flag = '{status_flag}'))";

			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(SELECT NULL) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetNotesRegisterList(string date_from, string date_to, string account_id_from, string account_id_to, string sales_person_from, string sales_person_to, string user_from, string user_to, string customer_flag, string notes_type, string company_id)
		{
			is_sql = $@"SELECT account_id = A.id, A.company_name, serial_no = b.serial_no, B.update_dt, B.notes_type,notes_type_desc = D.Value, B.notes, B.user_cd,user_desc=E.name, A.salesperson_cd, 
						C.name, A.customer_flag 
						FROM cactmsaccount A inner join cactmsaccountnotes B ON A.id = B.account_id left join saoimssalesperson C ON A.salesperson_cd = C.id left join 
						Cactmssetupdtl D on B.notes_type = D.id and D.trans_flag = 'A' and D.Setup_id = 'NOTESTYPE' left join sysmmsuser E on g_user_cd= B.user_cd 

						where (('{date_from}' = case when '{date_from}' = '' then '' end or cast(B.update_dt as date) BETWEEN cast('{date_from}' as date)  AND cast('{date_to}' as date)) 
						and ('{account_id_from}' = case when '{account_id_from}' = '' then '' end or ISNULL(A.id, '') BETWEEN '{account_id_from}' AND '{account_id_to}') 
						and ('{sales_person_from}' = case when '{sales_person_from}' = '' then '' end or ISNULL(A.salesperson_cd, '') BETWEEN '{sales_person_from}' AND '{sales_person_to}') 
						and ('{user_from}' = case when '{user_from}' = '' then '' end or ISNULL(B.user_cd, '') BETWEEN '{user_from}' AND '{user_to}') 
						and ('{customer_flag}' = case when '{customer_flag}' = '' then '' end or A.customer_flag = '{customer_flag}' or '{customer_flag}' = 'A') 
						and ('{notes_type}' = case when '{notes_type}' = '' then '' end or B.notes_type = '{notes_type}' or '{notes_type}' = 'X') 
						and ('{company_id}' = case when '{company_id}' = '' then '' end or ISNULL(A.company_id,'') = '{company_id}'))";

			return Execute();
		}


		public DataTable GetOpenOrder(string company_id,
									  string trans_date_from , string trans_date_to,string ship_date_from, string ship_date_to, 
									  string po_date_from,string po_date_to, string ref_date_from , string ref_date_to,
									  string period_from , string period_to, string account_id_from , string account_id_to, 
									  string item_id_from, string item_id_to, string category_from ,string category_to, 
									  string salesperson_from , string salesperson_to, string type_from, string type_to, 
									  string item_type_from, string item_type_to, string location_from, string location_to, 
									  string order_no_from , string order_no_to, string packet_no_from ,string packet_no_to, 
									  string po_no_from , string po_no_to, string customer_category_from , string customer_category_to,
									  string billto_from , string billto_to, string order_for_from, string order_for_to,
									  string group_from , string group_to, string group1_from , string group1_to,
									  string group2_from, string group2_to, string group3_from, string group3_to,
									  string group4_from, string group4_to, string group5_from, string group5_to
									 )

		{
			is_sql = $@"   select saoitrinvhd.trans_bk,    
							saoitrinvhd.trans_no,   
							saoitrinvhd.trans_dt,   
							saoitrinvhd.account_id,   
							saoitrinvhd.trans_period,   
							saoitrinvhd.ship_dt,  
							saoitrinvhd.sales_person,   
							saoitrinvhd.po_no,   
							saoitrinvhd.po_dt,  
							saoitrinvdtl.serial_no,   
							saoitrinvdtl.item_id,   
							saoitrinvdtl.size,   
							saoitrinvdtl.trans_qty,   
							saoitrinvdtl.item_price,   
							saoitrinvdtl.item_category,   
							saoitrinvdtl.item_description,   
							saoitrinvdtl.clear_qty,   
							saoitrinvdtl.packet_no,   
							faarmscustomer.name,   
							faarmscustomer.contact1,   
							faarmscustomer.phone1,   
							faarmscustomer.fax1,   
							saoitrinvdtl.trans_qty_pc,    
							saoitrinvdtl.trans_qty_wt,   
							0  as cancel_qty,
							saoitrinvhd.ref_dt as cancel_dt,
							saoitrinvdtl.sku_no as sku_no
							from saoitrinvdtl inner join saoitrinvhd on 
							(
								saoitrinvdtl.company_id = saoitrinvhd.company_id and  
								saoitrinvdtl.trans_bk = saoitrinvhd.trans_bk and  
								saoitrinvdtl.trans_no = saoitrinvhd.trans_no and  
         					saoitrinvdtl.trans_dt = saoitrinvhd.trans_dt
							)
							inner join faarmscustomer on 
							(
								saoitrinvhd.account_id = faarmscustomer.id
							)	 
						join saoisytrinv On
								saoisytrinv.id = saoitrinvhd.trans_for_flag and
								saoisytrinv.ms_type = saoitrinvhd.trans_type
							LEFT JOIN saoimsitem ON
								saoitrinvdtl.item_id = saoimsitem.id 
								AND saoitrinvdtl.item_category = saoimsitem.category 
								AND saoitrinvdtl.item_flag = saoimsitem.item_type	

						   where (saoitrinvhd.trans_type = 'O') and
								 (saoitrinvhd.trans_flag = 'A') and
								 (saoitrinvdtl.trans_qty - saoitrinvdtl.clear_qty <> 0)
						and ('{company_id}' = case when '{company_id}' = '' then '' end or saoitrinvhd.company_id = '{company_id}')
						and ('{category_from}' = case when '{category_from}' = '' then '' end or saoitrinvdtl.item_category BETWEEN '{category_from}' AND '{category_to}') 
						and ('{trans_date_from}' = case when '{trans_date_from}' = '' then '' end or saoitrinvhd.trans_dt BETWEEN '{trans_date_from}' AND '{trans_date_to}') 
						and ('{ship_date_from}' = case when '{ship_date_from}' = '' then '' end or saoitrinvhd.ship_dt BETWEEN '{ship_date_from}' AND '{ship_date_to}') 
						and ('{po_date_from}' = case when '{po_date_from}' = '' then '' end or saoitrinvhd.po_dt BETWEEN '{po_date_from}' AND '{po_date_to}') 
						and ('{ref_date_from}' = case when '{ref_date_from}' = '' then '' end or saoitrinvhd.ref_dt BETWEEN '{ref_date_from}' AND '{ref_date_to}') 
						and ('{period_from}' = case when '{period_from}' = '' then '' end or saoitrinvhd.trans_period BETWEEN '{period_from}' AND '{period_to}') 
						and ('{account_id_from}' = case when '{account_id_from}' = '' then '' end or saoitrinvhd.account_id BETWEEN '{period_from}' AND '{period_to}') 
						and ('{item_id_from}' = case when '{item_id_from}' = '' then '' end or saoitrinvdtl.item_id BETWEEN '{item_id_from}' AND '{item_id_to}') 
						and ('{order_for_from}' = case when '{order_for_from}' = '' then '' end or saoitrinvhd.trans_for_flag BETWEEN '{order_for_from}' AND '{order_for_to}') 
						and ('{salesperson_from}' = case when '{salesperson_from}' = '' then '' end or saoitrinvhd.sales_person BETWEEN '{salesperson_from}' AND '{salesperson_to}') 
						and ('{category_from}' = case when '{category_from}' = '' then '' end or saoitrinvdtl.item_category BETWEEN '{category_from}' AND '{category_to}') 
						and ('{order_no_from}' = case when '{order_no_from}' = '' then '' end or saoitrinvhd.trans_no BETWEEN '{order_no_from}' AND '{order_no_to}') 
						and ('{item_type_from}' = case when '{item_type_from}' = '' then '' end or saoitrinvdtl.item_flag BETWEEN '{item_type_from}' AND '{item_type_to}') 
						and ('{packet_no_from}' = case when '{packet_no_from}' = '' then '' end or  IsNull(saoitrinvdtl.packet_no, 'NA') BETWEEN '{packet_no_from}' AND '{packet_no_to}') 
						and ('{po_no_from}' = case when '{po_no_from}' = '' then '' end or isnull(saoitrinvdtl.po_no,'') BETWEEN '{po_no_from}' AND '{po_no_to}') 
						and ('{customer_category_from}' = case when '{customer_category_from}' = '' then '' end or faarmscustomer.category BETWEEN '{customer_category_from}' AND '{customer_category_to}') 
						and ('{billto_from}' = case when '{billto_from}' = '' then '' end or faarmscustomer.billto_id BETWEEN '{billto_from}' AND '{billto_to}') 
						and ('{location_from}' = case when '{location_from}' = '' then '' end or IsNull(saoitrinvdtl.location, 'NA') BETWEEN '{location_from}' AND '{location_to}') 
						and ('{group_from}' = case when '{group_from}' = '' then '' end or saoisytrinv.group_id BETWEEN '{group_from}' AND '{group_to}') 
						and ('{group1_from}' = case when '{group1_from}' = '' then '' end or IsNull( saoimsitem.group_cd1, '') BETWEEN '{group1_from}' AND '{group1_to}') 
						and ('{group2_from}' = case when '{group2_from}' = '' then '' end or IsNull( saoimsitem.group_cd2, '')  BETWEEN '{group2_from}' AND '{group2_to}') 
						and ('{group3_from}' = case when '{group3_from}' = '' then '' end or IsNull( saoimsitem.group_cd3, '')  BETWEEN '{group3_from}' AND '{group3_to}') 
						and ('{group4_from}' = case when '{group4_from}' = '' then '' end or IsNull( saoimsitem.group_cd4, '')  BETWEEN '{group4_from}' AND '{group4_to}') 
						and ('{group5_from}' = case when '{group5_from}' = '' then '' end or IsNull( saoimsitem.group_cd5, '')  BETWEEN '{group5_from}' AND '{group5_to}') 



						";

		//	throw new Exception( is_sql);
			//is_sql = is_sql.Replace("\r", "");
			//is_sql = is_sql.Replace("\n", "");
			//is_sql = is_sql.Replace("\t", "");
			return Execute();
		}
		public DataTable GetCampaignForProspect(string id, Int32 page_no, Int32 page_size, bool count)
		{
			if (count)
			{
				is_sql = $@"Select count(*) as Count ";
			}
			else
			{
				is_sql = $@" SELECT trans_bk = a.trans_bk, trans_no = a.trans_no, trans_dt = a.trans_dt, campaign_id = a.campaign_id, campaign_type = a.campaign_type, 
							case  a.campaign_status when 'P' then 'Planned' when 'I' then 'In-progress' when 'C' then 'Complete' end as campaign_status
							,start_date = a.start_date, end_date = a.end_date, parent_campaign_no = a.parent_campaign_no, parent_campaign_dt = a.parent_campaign_dt, select_flag = CASE WHEN Z.account_id is null THEN 'N' ELSE 'Y' END, prospect_id = Z.account_id FROM cacttrcampaignhd a LEFT JOIN ( Select b.trans_bk,b.trans_no,b.trans_dt, max(b.account_id) as account_id 
							from cacttrcampaigndtl b
							where ('{id}' = case when '{id}' = '' then '' end or b.account_id = '{id}')
						
							group by b.trans_bk, b.trans_no, b.trans_dt ) z on a.trans_bk = z.trans_bk
							and a.trans_no = z.trans_no and z.trans_dt = z.trans_dt   ";

			}

			
			if (page_no != 0 && page_size != 0)
			{
				page_no = (page_no - 1) * page_size;
				is_sql += $" ORDER BY(Z.account_id) OFFSET {page_no} ROWS FETCH NEXT {page_size} ROWS ONLY";
			}

			return Execute();
		}

		public DataTable GetProspectInboxContacttList(string id)
		{
			is_sql = $@" select cactaccountinboxcontacts.*,Cactmssetupdtl.value as contact_type_desc 
						from cactaccountinboxcontacts 
						inner join  Cactmssetupdtl 
						on cactaccountinboxcontacts.contact_type = Cactmssetupdtl.id 
						and setup_id = 'CONTACT_TYPE' 
						and  ('{id}' = case when '{id}' = '' then '' end or cactaccountinboxcontacts.id = '{id}')
						";

			

			return Execute();
		}

		public DataTable GetProspectInboxTradeReferenceList(string id)
		{
			is_sql = $@" SELECT id, serial_no, company_name, person_to_contact, email, phone, 
						user_cd, company_id, update_dt, update_flag, trans_flag
						FROM cactaccountinboxtraderef 
						 where ('{id}' = case when '{id}' = '' then '' end or id = '{id}')";



			return Execute();
		}

		public DataTable GenerateProspectNo()

		{
			
			is_sql = $@" exec spcact_generate_prospect_no  ";


			return Execute();
		}

		public DataTable GetCustomerDetail(string id)

		{
			string[] splitValue;
			string in_Clause = "";


			if (!string.IsNullOrEmpty(id))
			{
				splitValue = id.Split(',');

				foreach (string item in splitValue)
				{
					if (!string.IsNullOrEmpty(in_Clause))
						in_Clause += " , '" + item + "'";
					else

						in_Clause += "  '" + item + "'";
				}
			}
			is_sql = $@" SELECT distinct 'N' as select_flag,id = a.id,category = a.category,name = a.name,contact1 = a.contact1,contact2 = a.contact2,address1 =a.address1,address2 = a.address2,city = a.city,state = a.state,zip = a.zip,country = a.country,phone1 = a.phone1,phone2 = a.phone2,
							fax1 = a.fax1,email = a.email,credit_limit = a.credit_limit,salesperson_name = sp.name,sales_person = a.sales_person,ship_via = a.ship_via,trans_flag = a.trans_flag,account_id = b.id,c.name as category_name,'Y' as update_flag,tracking_no = d.tracking_no,web_id = b.web_id,subscription_email = b.subscription_email
							 FROM faarmscustomer a 
							left join cactmsaccount b 
							on a.id = b.parent_account_id left join faarmscustomercategory c 
							on (c.category = a.category) left join shiptrshipments d 
							on d.account_id = a.id LEFT JOIN saoimssalesperson sp 
							ON a.sales_person = sp.id ";

			if (!string.IsNullOrEmpty(in_Clause))
            {
				is_sql += $@" where a.id in ( {in_Clause}) ";
            }
			
			is_sql = is_sql.Replace("\r", "");
			is_sql = is_sql.Replace("\n", "");
			is_sql = is_sql.Replace("\t", "");
			return Execute();
		}

		public DataTable GetEmailTemplateDetail(string id)

		{

			is_sql = $@" select LTRIM(RTRIM(ISNULL(id,''))) as id,LTRIM(RTRIM(ISNULL(subject,''))) as subject,
						LTRIM(RTRIM(ISNULL(message,''))) as message 
						from cactms_email_template 
						WHERE trans_flag = 'A' and  ('{id}' = case when '{id}' = '' then '' end or id = '{id}')  ";


			return Execute();
		}
	}
}
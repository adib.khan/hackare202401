﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Auth.BL.Services;
using Cact.Bl.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json;

namespace Cact.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class ProspectMasterController : DataSavingController
    {
        public ProspectMasterController()
        {
            service = new ProspectMasterService();
        }

        [EnableCors]
        [HttpPost]
        public IActionResult Save(JsonElement requestJson)
        {
            return Saving(requestJson);
        }

        [EnableCors]
        [HttpPost]
        public IActionResult CreateCustomer(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                ValidateSession();
                ProspectMasterService prospectService = (ProspectMasterService)service;
                prospectService.CreateCustomer(requestJson);
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //[HttpPost]
        //public IActionResult CustomerExists(JsonElement requestJson)
        //{
        //    bool IsExists;
        //    try
        //    {
        //        ProspectMasterService prospectService = (ProspectMasterService)service;
        //        IsExists = prospectService.CustomerExists(requestJson);

        //        return Ok(IsExists.ToString());

        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        [EnableCors]
        [HttpPost]
        public IActionResult AddContact(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                ValidateSession();
                ProspectMasterService prospectService = (ProspectMasterService)service;
                prospectService.AddContact(requestJson);
                return Ok();
            }

            catch(Exception e)
            {

                return BadRequest(e.Message);
                //throw new Exception(ExceptionDetail.Info(e));
            }
        }

        [EnableCors]
        [HttpPost]
        public IActionResult UpdateContact(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                ValidateSession();
                ProspectMasterService prospectService = (ProspectMasterService)service;
                prospectService.UpdateContact(requestJson);
                return Ok();
            }
            catch
            {
                throw;
            }
        }

    }
}
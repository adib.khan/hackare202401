﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : Make prospect Active / Hold / Or transfer to CRM
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json;

namespace cact.Controllers
{
    [Route("api/[controller]/{action}")]
    [ApiController]
    public class ProspectInboxController : DataSavingController
    {
        public ProspectInboxController()
        {
            service = new ProspectInboxService();
        }

        [EnableCors]
        [HttpPost]
        public IActionResult ApproveProspect(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                //ValidateSession();
                ProspectInboxService prospectInboxService = (ProspectInboxService)service;
                var responseLog = prospectInboxService.ApproveProspect(requestJson);
                return Ok(responseLog);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [EnableCors]

        [HttpPost]
        public IActionResult HoldProspect(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                ValidateSession();
                ProspectInboxService prospectInboxService = (ProspectInboxService)service;
                prospectInboxService.HoldProspect(requestJson);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [EnableCors]
        [HttpPost]
        public IActionResult ActiveProspect(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                ValidateSession();
                ProspectInboxService prospectInboxService = (ProspectInboxService)service;
                prospectInboxService.ActiveProspect(requestJson);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [EnableCors]
        //Sunil 211003  Start
        [HttpPost]
        public IActionResult DeleteProspect(JsonElement requestJson)
        {
            try
            {
                SetCompany();
                ValidateSession();
                ProspectInboxService prospectInboxService = (ProspectInboxService)service;
                prospectInboxService.DeleteProspect(requestJson);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        //Sunil 211003 End

    }
}

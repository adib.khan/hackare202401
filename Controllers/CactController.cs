﻿
using Cact.BL.Helper;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

namespace Cact.Controllers
{
	[Route("api/[controller]/{action}")]
	[ApiController]
	public class CactController : GenController
	{


		public CactHelper cactHelper;
		public CactController()
		{

		}

		public void Init()
		{
			SetCompany();
			ValidateSession();
			cactHelper = new CactHelper(Request.Headers["companyName"]);

		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetUserSalesByCategory(int sales_year, string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetUserSalesByCategory(sales_year, user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerCount(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerCount(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerwithBO(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerwithBO(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTotalTaskCount(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTotalTaskCount(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetOpenTaskCount(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetOpenTaskCount(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetClosedTaskCount(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetClosedTaskCount(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustReqCatalog(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustReqCatalog(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}
		[EnableCors]
		[HttpGet]
		public IActionResult GetAccountMgmtCount(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetAccountMgmtCount(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}
		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskTypeCount(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskTypeCount(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}
		//
		
		[EnableCors]
		[HttpGet]
		public IActionResult CheckPublicDomain(string From_Address)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.CheckPublicDomain(From_Address)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]

		[HttpGet]

		public IActionResult GetCustomerProspectMergeList(string account_id, string company_name, string category_id, string tov_customer_id,

					string salesperson_cd, string contact, string phone, string customer_flag, string status, string current_stage,

					string credit_approval_stage, string transferred_to_tov, string created_fr, string created_by, string prospect_owner, string description)

		{

			try

			{

				Init();

				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerProspectMergeList(account_id, company_name, category_id, tov_customer_id,

					 salesperson_cd, contact, phone, customer_flag, status, current_stage,

					 credit_approval_stage, transferred_to_tov, created_fr, created_by, prospect_owner, description)));



			}

			catch (Exception e)

			{

				throw new Exception(e.Message);

			}

		}
		[EnableCors]
		[HttpGet]
		public IActionResult GetModuleSettingList()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetModuleSettingList()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupSubStageList(string Stage_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupSubStageList(Stage_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupSubStageAllList(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupSubStageAllList(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupCustomerCategory(string category)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupCustomerCategory(category)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupCountry(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupCountry(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupAction()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupAction()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupEmail()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupEmail()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupGroup()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupGroup()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupSetup(string Setup_Id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupSetup(Setup_Id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupSalesPerson()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupSalesPerson()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupUser()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupUser()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityList(string activity_type, string priority, string trans_no, string activity_dt, string prospect_id, string subject, string remarks, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityList(activity_type, priority, trans_no, activity_dt, prospect_id, subject, remarks, page_no, page_size, false)));
			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityListCount(string activity_type, string priority, string trans_no, string activity_dt, string prospect_id, string subject, string remarks, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityList(activity_type, priority, trans_no, activity_dt, prospect_id, subject, remarks, page_no, page_size, true)));
			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityContact(string trans_no, string trans_bk, DateTime trans_dt, string company_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityContact(trans_no, trans_bk, trans_dt, company_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetWorkflowList(string stage_id, string stage_name, string parent_stage, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetWorkflowList(stage_id, stage_name, parent_stage, status, page_no, page_size, false)));
			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetWorkflowListCount(string stage_id, string stage_name, string parent_stage, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetWorkflowList(stage_id, stage_name, parent_stage, status, page_no, page_size, true)));
			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetWorkflowDetail(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetWorkflowDetail(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetShipViaList(string id, string name, string shippers_code)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetShipViaList(id, name, shippers_code)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupEmailTemplate()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupEmailTemplate()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupImportCustomerList()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupImportCustomerList()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupProspectPattern(string id, string company_name, string phone, string first_name, string zip)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupProspectPattern(id, company_name, phone, first_name, zip)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupProspectExact(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupProspectExact(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupProspectContact(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupProspectContact(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupCustomerPattern(string id, string name, string phone1, string contact1, string zip)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupCustomerPattern(id, name, phone1, contact1, zip)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLookupCustomerExact(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLookupCustomerExact(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetSetupDetail(string setup_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetSetupDetail(setup_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetSetupList(string status, string setup_id, string name, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetSetupList(status, setup_id, name, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetMyOpenTask(int period,string assigned_to)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetMyOpenTask(period, assigned_to)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetLeadSource(string from_dt, string to_dt)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetLeadSource(from_dt, to_dt)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetSalesPipeLine(string from_dt, string to_dt)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetSalesPipeLine(from_dt, to_dt)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetSalesByCategory(int sales_year)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetSalesByCategory(sales_year)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetValidateSetup(string setup_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetValidateSetup(setup_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetValidateSetupDetail(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetValidateSetupDetail(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetValidateWorkflow(string stage_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetValidateWorkflow(stage_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetValidateSubstage(string substage_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetValidateSubstage(substage_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetValidateEmail(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetValidateEmail(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetValidateEmailSubject(string subject)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetValidateEmailSubject(subject)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetValidateListName(string list_name)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetValidateListName(list_name)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTasklistMainList(string task_type, string task_date, string prospect_id, string priority, string task_no, string subject, string remarks, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTasklistMainList(task_type, task_date, prospect_id, priority, task_no, subject, remarks, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTasklistContactDetail(string company_id, string trans_bk, string trans_no, string trans_dt, string setup_id, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTasklistContactDetail(company_id, trans_bk, trans_no, trans_dt, setup_id, status, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTasklistActionDetail(string company_id, string trans_no, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTasklistActionDetail(company_id, trans_no, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityInfoDetail(string activity_no, string book, string company_id, string activity_date, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityInfoDetail(activity_no, book, company_id, activity_date, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityInfoAddress(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityInfoAddress(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityInfoContact(string activity_no, string book, string company_id, string activity_date, string setup_id, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityInfoContact(activity_no, book, company_id, activity_date, setup_id, status, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityInfoNotes(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityInfoNotes(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityInfoActivity(string prospect_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityInfoActivity(prospect_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityInfoMeeting(string prospect_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityInfoMeeting(prospect_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskDetailContact(string company_id, string book, string contact_no, string contact_date, string setup_id, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskDetailContact(company_id, book, contact_no, contact_date, setup_id, status, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskDetailAction(string company_id, string action_no, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskDetailAction(company_id, action_no, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskAddress(string id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskAddress(id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskContact(string company_id, string book, string contact_no, string contact_date, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskContact(company_id, book, contact_no, contact_date, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskNotes(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskNotes(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskActivity(string prospect_id, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskActivity(prospect_id, status, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetUserMenuPermission(string user_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetUserMenuPermission(user_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GenerateDocumentNo(string document_type, string company_id, string default_book)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GenerateDocumentNo(document_type, company_id, default_book)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectMainList(string account_id, string salesperson_cd, string category_id, string customer_flag, string status, string current_stage, string created_fr, string created_by, string prospect_owner, string transferred_to_tov, string tov_status, string contact, string phone, string description, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectMainList(account_id, salesperson_cd, category_id, customer_flag, status, current_stage, created_fr, created_by, prospect_owner, transferred_to_tov, tov_status, contact, phone, description, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectContact(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectContact(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectShipTo(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectShipTo(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectNotes(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectNotes(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectProfile(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectProfile(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectDocument(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectDocument(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectTask(string prospect_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectTask(prospect_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectMeeting(string prospect_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectMeeting(prospect_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectCampaign(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectCampaign(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectCreditApplication(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectCreditApplication(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInboxHeader()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInboxHeader()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInboxContact()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInboxContact()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInboxTrade()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInboxTrade()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInboxCreditApplication()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInboxCreditApplication()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInboxProspectOnHold()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInboxProspectOnHold()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoMainList(string start_date_from, string start_date_to, string category_id_from, string category_id_to, string state_from, string state_to, string company_name_from, string company_name_to, string id_from, string id_to, string salesperson_cd_from, string salesperson_cd_to, string city_from, string city_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string customer_flag, string created_fr, string current_stage, string credit_approval_stage, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoMainList(start_date_from, start_date_to, category_id_from, category_id_to, state_from, state_to, company_name_from, company_name_to, id_from, id_to, salesperson_cd_from, salesperson_cd_to, city_from, city_to, zip_from, zip_to, group1_from, group1_to, group2_from, group2_to, group3_from, group3_to, group4_from, group4_to, group5_from, group5_to, customer_flag, created_fr, current_stage, credit_approval_stage, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoStageCount()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoStageCount()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoContact(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoContact(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoShipTo(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoShipTo(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoNotes(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoNotes(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoProfile(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoProfile(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoDocument(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoDocument(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoTask(string prospect_id, string status_flag, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoTask(prospect_id, status_flag, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoMeeting(string prospect_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoMeeting(prospect_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoCampaign(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoCampaign(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoCreditApplication(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoCreditApplication(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoActivity(string prospect_id, string status)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoActivity(prospect_id, status)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInfoCloseTask(string prospect_id, string status_flag, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInfoCloseTask(prospect_id, status_flag, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetEmailList(string status, string account_id, string subject, string message, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetEmailList(status, account_id, subject, message, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetEmailTemplateList(string account_id, string subject, string message, string user_group, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetEmailTemplateList(account_id, subject, message, user_group, status, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerList(string customer_id, string customer_name, string customer_category, string phone, string credit_approval_flag, string stop_shipment_days, string stop_shimpment_amt, string tov_customer_id, string status, string credit_status, string stop_ship, string cc_fidelity, string patriot_act, string collection, string blacklisted_flag, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerList(customer_id, customer_name, customer_category, phone, credit_approval_flag, stop_shipment_days, stop_shimpment_amt, tov_customer_id, status, credit_status, stop_ship, cc_fidelity, patriot_act, collection, blacklisted_flag, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerContact(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerContact(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerShip(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerShip(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerNotes(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerNotes(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerProfile(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerProfile(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerSales(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerSales(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerDocument(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerDocument(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerTask(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerTask(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerCampaign(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerCampaign(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerCredit(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerCredit(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerJbt(string account_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerJbt(account_id, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetExistingCustomerList(Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetExistingCustomerList(page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetExistingCustomerDetail(string status, string list_name, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetExistingCustomerDetail(status, list_name, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetCampaignList(string campaign_type, string actual_cost, string method, string purpose, string campaign_status, string frequency, string salesperson, string budget, string start_date, string end_date, string campaign_date, string trans_no, string update_dt, string user_cd, string parent_campaign_no, string parent_campaign_dt, string campaign_id, string remarks, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCampaignList(campaign_type, actual_cost, method, purpose, campaign_status, frequency, salesperson, budget, start_date, end_date, campaign_date, trans_no, update_dt, user_cd, parent_campaign_no, parent_campaign_dt, campaign_id, remarks, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetCampaignCustomerList(string campaign_no, string book, string company_id, string campaign_date, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCampaignCustomerList(campaign_no, book, company_id, campaign_date, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCampaignImportCustomer(string status, string list_name, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCampaignImportCustomer(status, list_name, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCampaignDocument(string campaign_no, string book, string company_id, string campaign_date, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCampaignDocument(campaign_no, book, company_id, campaign_date, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetPrepareCustomerList(string category_from, string category_to, string id_from, string id_to, string name_from, string name_to, string state_from, string state_to, string city_from, string city_to, string group1_from, string group1_to, string group2_from, string group2_to, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetPrepareCustomerList(category_from, category_to, id_from, id_to, name_from, name_to, state_from, state_to, city_from, city_to, group1_from, group1_to, group2_from, group2_to, status, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetGenerateChildCampaign(string campaign_no)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetGenerateChildCampaign(campaign_no)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityInfoList(string activity_no_from, string activity_no_to, string activity_date_from, string activity_date_to, string activity_type_from, string activity_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string activity_by_from, string activity_by_to, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityInfoList(activity_no_from, activity_no_to, activity_date_from, activity_date_to, activity_type_from, activity_type_to, prospect_id_from, prospect_id_to, company_name_from, company_name_to, salesperson_cd_from, salesperson_cd_to, city_from, city_to, state_from, state_to, zip_from, zip_to, group1_from, group1_to, group2_from, group2_to, group3_from, group3_to, group4_from, group4_to, group5_from, group5_to, activity_by_from, activity_by_to, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskList(string task_no_from, string task_no_to, string task_date_from, string task_date_to, string start_date_from, string start_date_to, string task_type_from, string task_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string assigned_by_from, string assigned_by_to, string assigned_to_from, string assigned_to_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string status_flag, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskList(task_no_from, task_no_to, task_date_from, task_date_to, start_date_from, start_date_to, task_type_from, task_type_to, prospect_id_from, prospect_id_to, company_name_from, company_name_to, salesperson_cd_from, salesperson_cd_to, assigned_by_from, assigned_by_to, assigned_to_from, assigned_to_to, city_from, city_to, state_from, state_to, zip_from, zip_to, group1_from, group1_to, group2_from, group2_to, group3_from, group3_to, group4_from, group4_to, group5_from, group5_to, status_flag, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}



		[EnableCors]
		[HttpGet]
		public IActionResult GetCopyFromSales(string category_from, string category_to, string id_from, string id_to, string name_from, string name_to, string last_year_sales_from, string last_year_sales_to, string ytd_sales_from, string ytd_sales_to, string sales_2years_ago_from, string sales_2years_ago_to, string sales_3years_ago_from, string sales_3years_ago_to, string last_pay_dt_from, string last_pay_dt_to, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCopyFromSales(category_from, category_to, id_from, id_to, name_from, name_to, last_year_sales_from, last_year_sales_to, ytd_sales_from, ytd_sales_to, sales_2years_ago_from, sales_2years_ago_to, sales_3years_ago_from, sales_3years_ago_to, last_pay_dt_from, last_pay_dt_to, page_no, page_size)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetContactInfoList(string category_id_from, string category_id_to, string id_from, string id_to, string salesperson_cd_from, string salesperson_cd_to, string city_from, string city_to, string start_date_from, string start_date_to, string state_from, string state_to, string zip_from, string zip_to, string phone_from, string phone_to, string status, string customer_flag, string subscribed_to_emails, string contact_type, string company_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetContactInfoList(category_id_from, category_id_to, id_from, id_to, salesperson_cd_from, salesperson_cd_to, city_from, city_to, start_date_from, start_date_to, state_from, state_to, zip_from, zip_to, phone_from, phone_to, status, customer_flag, subscribed_to_emails, contact_type, company_id, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskReportList(string trans_no_from, string trans_no_to, string trans_dt_from, string trans_dt_to, string start_date_from, string start_date_to, string task_type_from, string task_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string assigned_by_from, string assigned_by_to, string assigned_to_from, string assigned_to_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string status_flag, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskReportList(trans_no_from, trans_no_to, trans_dt_from, trans_dt_to, start_date_from, start_date_to, task_type_from, task_type_to, prospect_id_from, prospect_id_to, company_name_from, company_name_to, salesperson_cd_from, salesperson_cd_to, assigned_by_from, assigned_by_to, assigned_to_from, assigned_to_to, city_from, city_to, state_from, state_to, zip_from, zip_to, group1_from, group1_to, group2_from, group2_to, group3_from, group3_to, group4_from, group4_to, group5_from, group5_to, status_flag, page_no, page_size, false)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetNotesRegisterList(string date_from, string date_to, string account_id_from, string account_id_to, string sales_person_from, string sales_person_to, string user_from, string user_to, string customer_flag, string notes_type, string company_id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetNotesRegisterList(date_from, date_to, account_id_from, account_id_to, sales_person_from, sales_person_to, user_from, user_to, customer_flag, notes_type, company_id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}


		[EnableCors]
		[HttpGet]
		public IActionResult GetSetupListCount(string status, string setup_id, string name, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetSetupList(status, setup_id, name, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectMainListCount(string account_id, string salesperson_cd, string category_id, string customer_flag, string status, string current_stage, string created_fr, string created_by, string prospect_owner, string transferred_to_tov, string tov_status, string contact, string phone, string description, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectMainList(account_id, salesperson_cd, category_id, customer_flag, status, current_stage, created_fr, created_by, prospect_owner, transferred_to_tov, tov_status, contact, phone, description, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetEmailListCount(string status, string account_id, string subject, string message, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetEmailList(status, account_id, subject, message, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetEmailTemplateListCount(string account_id, string subject, string message, string user_group, string status, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetEmailTemplateList(account_id, subject, message, user_group, status, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerListCount(string customer_id, string customer_name, string customer_category, string phone, string credit_approval_flag, string stop_shipment_days, string stop_shimpment_amt, string tov_customer_id, string status, string credit_status, string stop_ship, string cc_fidelity, string patriot_act, string collection, string blacklisted_flag, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerList(customer_id, customer_name, customer_category, phone, credit_approval_flag, stop_shipment_days, stop_shimpment_amt, tov_customer_id, status, credit_status, stop_ship, cc_fidelity, patriot_act, collection, blacklisted_flag, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetExistingCustomerListCount(Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetExistingCustomerList(page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCampaignListCount(string campaign_type, string actual_cost, string method, string purpose, string campaign_status, string frequency, string salesperson, string budget, string start_date, string end_date, string campaign_date, string trans_no, string update_dt, string user_cd, string parent_campaign_no, string parent_campaign_dt, string campaign_id, string remarks, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCampaignList(campaign_type, actual_cost, method, purpose, campaign_status, frequency, salesperson, budget, start_date, end_date, campaign_date, trans_no, update_dt, user_cd, parent_campaign_no, parent_campaign_dt, campaign_id, remarks, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetActivityInfoListCount(string activity_no_from, string activity_no_to, string activity_date_from, string activity_date_to, string activity_type_from, string activity_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string activity_by_from, string activity_by_to, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetActivityInfoList(activity_no_from, activity_no_to, activity_date_from, activity_date_to, activity_type_from, activity_type_to, prospect_id_from, prospect_id_to, company_name_from, company_name_to, salesperson_cd_from, salesperson_cd_to, city_from, city_to, state_from, state_to, zip_from, zip_to, group1_from, group1_to, group2_from, group2_to, group3_from, group3_to, group4_from, group4_to, group5_from, group5_to, activity_by_from, activity_by_to, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskListCount(string task_no_from, string task_no_to, string task_date_from, string task_date_to, string start_date_from, string start_date_to, string task_type_from, string task_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string assigned_by_from, string assigned_by_to, string assigned_to_from, string assigned_to_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string status_flag, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskList(task_no_from, task_no_to, task_date_from, task_date_to, start_date_from, start_date_to, task_type_from, task_type_to, prospect_id_from, prospect_id_to, company_name_from, company_name_to, salesperson_cd_from, salesperson_cd_to, assigned_by_from, assigned_by_to, assigned_to_from, assigned_to_to, city_from, city_to, state_from, state_to, zip_from, zip_to, group1_from, group1_to, group2_from, group2_to, group3_from, group3_to, group4_from, group4_to, group5_from, group5_to, status_flag, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTaskReportListCount(string trans_no_from, string trans_no_to, string trans_dt_from, string trans_dt_to, string start_date_from, string start_date_to, string task_type_from, string task_type_to, string prospect_id_from, string prospect_id_to, string company_name_from, string company_name_to, string salesperson_cd_from, string salesperson_cd_to, string assigned_by_from, string assigned_by_to, string assigned_to_from, string assigned_to_to, string city_from, string city_to, string state_from, string state_to, string zip_from, string zip_to, string group1_from, string group1_to, string group2_from, string group2_to, string group3_from, string group3_to, string group4_from, string group4_to, string group5_from, string group5_to, string status_flag, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTaskReportList(trans_no_from, trans_no_to, trans_dt_from, trans_dt_to, start_date_from, start_date_to, task_type_from, task_type_to, prospect_id_from, prospect_id_to, company_name_from, company_name_to, salesperson_cd_from, salesperson_cd_to, assigned_by_from, assigned_by_to, assigned_to_from, assigned_to_to, city_from, city_to, state_from, state_to, zip_from, zip_to, group1_from, group1_to, group2_from, group2_to, group3_from, group3_to, group4_from, group4_to, group5_from, group5_to, status_flag, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetContactInfoListCount(string category_id_from, string category_id_to, string id_from, string id_to, string salesperson_cd_from, string salesperson_cd_to, string city_from, string city_to, string start_date_from, string start_date_to, string state_from, string state_to, string zip_from, string zip_to, string phone_from, string phone_to, string status, string customer_flag, string subscribed_to_emails, string contact_type, string company_id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetContactInfoList(category_id_from, category_id_to, id_from, id_to, salesperson_cd_from, salesperson_cd_to, city_from, city_to, start_date_from, start_date_to, state_from, state_to, zip_from, zip_to, phone_from, phone_to, status, customer_flag, subscribed_to_emails, contact_type, company_id, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetTasklistMainListCount(string task_type, string task_date, string prospect_id, string priority, string task_no, string subject, string remarks, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetTasklistMainList(task_type, task_date, prospect_id, priority, task_no, subject, remarks, page_no, page_size, true)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetOpenOrder(string company_id,
									  string trans_date_from, string trans_date_to, string ship_date_from, string ship_date_to,
									  string po_date_from, string po_date_to, string ref_date_from, string ref_date_to,
									  string period_from, string period_to, string account_id_from, string account_id_to,
									  string item_id_from, string item_id_to, string category_from, string category_to,
									  string salesperson_from, string salesperson_to, string type_from, string type_to,
									  string item_type_from, string item_type_to, string location_from, string location_to,
									  string order_no_from, string order_no_to, string packet_no_from, string packet_no_to,
									  string po_no_from, string po_no_to, string customer_category_from, string customer_category_to,
									  string billto_from, string billto_to, string order_for_from, string order_for_to,
									  string group_from, string group_to, string group1_from, string group1_to,
									  string group2_from, string group2_to, string group3_from, string group3_to,
									  string group4_from, string group4_to, string group5_from, string group5_to
									 )
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetOpenOrder(company_id,
													   trans_date_from, trans_date_to, ship_date_from, ship_date_to,
													   po_date_from, po_date_to, ref_date_from, ref_date_to,
													   period_from, period_to, account_id_from, account_id_to,
													   item_id_from, item_id_to, category_from, category_to,
													   salesperson_from, salesperson_to, type_from, type_to,
													   item_type_from, item_type_to, location_from, location_to,
													   order_no_from, order_no_to, packet_no_from, packet_no_to,
													   po_no_from, po_no_to, customer_category_from, customer_category_to,
													   billto_from, billto_to, order_for_from, order_for_to,
													   group_from, group_to, group1_from, group1_to,
													   group2_from, group2_to, group3_from, group3_to,
													   group4_from, group4_to, group5_from, group5_to)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCampaignForProspectCount(string id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCampaignForProspect(id, page_no, page_size, true)));


			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCampaignForProspect(string id, Int32 page_no, Int32 page_size)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCampaignForProspect(id, page_no, page_size, false)));
					

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInboxContacttList(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInboxContacttList(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetProspectInboxTradeReferenceList(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetProspectInboxTradeReferenceList(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GenerateProspectNo()
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GenerateProspectNo()));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetCustomerDetail(string id)
		{
			try
			{
				Init();
				
				return Ok(JsonConvert.SerializeObject(cactHelper.GetCustomerDetail(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}

		[EnableCors]
		[HttpGet]
		public IActionResult GetEmailTemplateDetail(string id)
		{
			try
			{
				Init();
				return Ok(JsonConvert.SerializeObject(cactHelper.GetEmailTemplateDetail(id)));

			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
		}
	}
}
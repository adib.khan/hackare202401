﻿/*********************************************************************************** 
    Copyright (c) Diaspark Inc. All Rights Reserved 
    Created Date : 2021/07/20
    Author : Sunil Agrawal
    Purpose : 
    Modified Date :
    Modified By :
    Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;
using System.Data;
using System.Text.RegularExpressions;

namespace Cact.Bl.Validation
{
    public class ProspectMasterValidation : GenValidation
    {
        public ProspectMasterValidation(DbData dbData) : base(dbData) { }

        public override int ValidateChild()
        {
            string accountId, salesPersonId, category, companyName, companyId, businessEmail, businessPhone;
            string emailDomain, mainContactEmail = "", personToContact, tradeRefCompanyName, lsSQL, sql, transferredToTov, billByRjoSjo;
            int count = 1, pos;
            decimal oldSeqNo = 0, newSeqNo = 0;
            bool isExist = false, isUnique, isDomainUnique;
            Match match;
            string prospectCurrStageNew, prospectCurrStageOld = "", creditAppStageNew, creditAppStageOld, notesby;
            DataTable selectResult;

            try
            {
                accountId = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["id"].ToString();
                companyId = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["company_id"].ToString();
                salesPersonId = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["salesperson_cd"].ToString();
                category = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["category_id"].ToString();
                companyName = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["company_name"].ToString();
                prospectCurrStageNew = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["current_stage"].ToString();
                creditAppStageNew = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["credit_approval_stage"].ToString();

                if (! dbData.requestDataset.Tables.Contains("cactmsaccountsummary"))
                {
                    throw new Exception("Prospect's profile: Bill by field can not be empty.");
                }
                else
                {
                    billByRjoSjo = dbData.requestDataset.Tables["cactmsaccountsummary"].Rows[0]["billing_via_rjo_sjo"].ToString();
                    if(string.IsNullOrEmpty(billByRjoSjo))
                        throw new Exception("Prospect's profile: Bill by field could not be empty.");
                }

                if (prospectCurrStageNew == "CONVERTED")
                    throw new Exception("Prospect can not be transferred to CONVERETED stage directly by user action.");

                sql = "SELECT transferred_to_tov FROM cactmsaccount WHERE id='" + accountId + "'";

                selectResult = DML.Select(sql, dbConnection);
                if (selectResult.Rows.Count > 0)
                {
                    transferredToTov = selectResult.Rows[0]["transferred_to_tov"].ToString().Trim();

                    //Jay 20231027 : Comment - no need to this validation 
                    //if (transferredToTov == "Y")
                    //    throw new Exception("Account # " + accountId + " is already transferred to TOV so no meaning to change the stage...");
                }

                lsSQL = "SELECT sequence_no FROM cactmsstagedtl WHERE stage_id = 'PROSPECT_STAGE' AND substage_id = '" + prospectCurrStageNew + "'";
                selectResult = DML.Select(lsSQL, dbConnection);

                if (selectResult.Rows.Count > 0)
                    newSeqNo = (decimal)selectResult.Rows[0]["sequence_no"];

                lsSQL = "SELECT current_stage FROM cactmsaccount where id = '" + accountId + "'";
                selectResult = DML.Select(lsSQL, dbConnection);

                if (selectResult.Rows.Count > 0)
                {
                    prospectCurrStageOld = selectResult.Rows[0]["current_stage"].ToString().Trim();

                    if (prospectCurrStageOld == "CONVERTED")
                        throw new Exception("Prospect's current stage is already CONVERTED so no meaning to transfer further.");

                    //Jay 20231115:comment
                    //if (prospectCurrStageOld == "OPPORTUNITY")
                    //    throw new Exception("Prospect's current stage is already on OPPORTUNITY stage so no meaning to transfer further.");
                    
                    lsSQL = "SELECT sequence_no FROM cactmsstagedtl WHERE stage_id = 'PROSPECT_STAGE' AND substage_id = '" + prospectCurrStageOld + "'";
                    selectResult = DML.Select(lsSQL, dbConnection);
                    oldSeqNo = (decimal)selectResult.Rows[0]["sequence_no"];

                    if (newSeqNo - oldSeqNo == 0)
                    {
                        //do nothing.   
                    }
                    //Jay 20231109 : Comment
                    //else if (oldSeqNo > newSeqNo)
                    //    throw new Exception("Prospect's current stage can not bet set to older stage");
                    //else if (newSeqNo - oldSeqNo != 10)
                    //    throw new Exception("Prospect's current stage can be changed to just next stage only.");
                }

                //Reset var
                newSeqNo = 0;
                oldSeqNo = 0;

                lsSQL = "SELECT sequence_no FROM cactmsstagedtl WHERE stage_id = 'CREDIT_APPROVAL' AND substage_id = '" + creditAppStageNew + "'";
                selectResult = DML.Select(lsSQL, dbConnection);

                if (selectResult.Rows.Count > 0)
                    newSeqNo = (decimal)selectResult.Rows[0]["sequence_no"];

                lsSQL = "SELECT credit_approval_stage FROM cactmsaccount where id = '" + accountId + "'";
                selectResult = DML.Select(lsSQL, dbConnection);

                if (selectResult.Rows.Count > 0)
                {
                    creditAppStageOld = selectResult.Rows[0]["credit_approval_stage"].ToString().Trim();

                    lsSQL = "SELECT sequence_no FROM cactmsstagedtl WHERE stage_id = 'CREDIT_APPROVAL' AND substage_id = '" + creditAppStageOld + "'";
                    selectResult = DML.Select(lsSQL, dbConnection);
                    if (selectResult.Rows.Count > 0)
                        oldSeqNo = (decimal)selectResult.Rows[0]["sequence_no"];

                    if (newSeqNo - oldSeqNo == 0)
                    {
                        //do nothing.   
                    }
                    //Jay 20231109 : Comment
                    //else if (oldSeqNo > newSeqNo)
                    //    throw new Exception("Prospect's credit approval stage can not bet set to older stage");
                    //else if (newSeqNo - oldSeqNo != 10)
                    //    throw new Exception("Prospect's credit approval stage can be changed to just next stage only.");
                }
                 
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,30})+)$");

                businessEmail = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["business_email"].ToString();

                if (!string.IsNullOrEmpty(businessEmail))
                {
                    match = regex.Match(businessEmail);
                    if (!match.Success)
                    {
                        throw new Exception("Business email is not valid format.");
                    }
                }
                businessPhone = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["phone"].ToString();

                if (string.IsNullOrEmpty(accountId))
                    throw new Exception("Prospect # is required.");

                isUnique = DML.Select("faarmscustomer", " where id = '" + accountId + "'", dbConnection);
                if (!isUnique)
                    throw new Exception("Same account # already exist with another customer.");

                if (string.IsNullOrEmpty(category))
                    dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["category"] = "NA";

                if (string.IsNullOrEmpty(companyName))
                    throw new Exception("Company name is required.");

                if (!string.IsNullOrEmpty(businessPhone))
                {
                    isUnique = DML.Select("cactmsaccount", " where phone = '" + businessPhone + "' AND id <> '" + accountId + "'", dbConnection);
                    if (!isUnique)
                        throw new Exception("Business phone # already used in another prospect's business phone.");
                }

                isUnique = DML.Select("cactmsaccount", " where company_name = '" + companyName + "' AND id <> '" + accountId + "'", dbConnection);
                //if (!isUnique)    //Sunil 211126: As Rohan instructed
                //    throw new Exception("Company name already used in another prospect's company name."); //Sunil 211126: As Rohan instructed

                //Validate Main Contact
                if (!dbData.requestDataset.Tables.Contains("cactmscontact"))
                    throw new Exception("Main contact with name and email # is required.");

                foreach (DataRow dr in dbData.requestDataset.Tables["cactmscontact"].Rows)
                {
                    if (string.IsNullOrEmpty(dr["contact_type"].ToString()))
                    {
                        throw new Exception("Department can not be empty");
                    }

                    if ((dr["contact_type"].ToString() == "MC") && (!string.IsNullOrEmpty(dr["email_id"].ToString())) && (!string.IsNullOrEmpty(dr["first_name"].ToString())))
                        isExist = true;

                    if ((dr["contact_type"].ToString() == "MC") && (!string.IsNullOrEmpty(dr["email_id"].ToString())))
                        mainContactEmail = dr["email_id"].ToString();

                    string contactEmail = dr["email_id"].ToString();

                    if (!string.IsNullOrEmpty(contactEmail))
                    {
                        match = regex.Match(contactEmail);
                        if (!match.Success)
                        {
                            throw new Exception("Contact email is not valid format.");
                        }
                    }
                }

                if (isExist == false)
                    throw new Exception("Main contact's name and email is required.");

                pos = mainContactEmail.IndexOf("@");

                if (pos > 0)
                {
                    emailDomain = mainContactEmail.Substring(pos + 1);

                    isUnique = DML.Select("cactmscontact", " WHERE RTRIM(SUBSTRING(email_id, CHARINDEX('@',email_id) + 1, LEN(email_id))) = '" + emailDomain + "' AND id <> '" + accountId + "' AND contact_type = 'MC'", dbConnection);

                    //Bypass validation for blacklisted domains.
                    isDomainUnique = DML.Select("cactmssetupdtl", " WHERE setup_id = 'PUBLIC_DOMAIN' AND trans_flag = 'A' AND  UPPER(RTRIM(value)) = '" + emailDomain + "'", dbConnection);

                    if (isDomainUnique && !isUnique)
                    {
                        throw new Exception("Main contact's email domain already used in another prospect's main contact email.");

                    }
                }
                else
                {
                    throw new Exception("Main contact's email is not valid.");
                }

                if (dbData.requestDataset.Tables.Contains("cactmscontact"))
                    if (dbData.requestDataset.Tables["cactmscontact"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmscontact"].Rows)
                        {
                            dr["Id"] = accountId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

                if (dbData.requestDataset.Tables.Contains("cactmsprospectship"))
                    if (dbData.requestDataset.Tables["cactmsprospectship"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmsprospectship"].Rows)
                        {
                            dr["Id"] = accountId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

                notesby = dbData.requestDataset.Tables["cactmsaccount"].Rows[0]["user_cd"].ToString(); //Jay 20220729


                if (dbData.requestDataset.Tables.Contains("cactmsaccountnotes"))
                    if (dbData.requestDataset.Tables["cactmsaccountnotes"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmsaccountnotes"].Rows)
                        {
                            if (string.IsNullOrEmpty(dr["notes_by"].ToString())) //Jay 20220729
                                dr["notes_by"] = notesby;
                            
                            dr["account_id"] = accountId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

                if (dbData.requestDataset.Tables.Contains("cactmsaccountsummary"))
                    if (dbData.requestDataset.Tables["cactmsaccountsummary"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmsaccountsummary"].Rows)
                        {
                            dr["account_id"] = accountId;
                        }

                if (dbData.requestDataset.Tables.Contains("cactaccountdocs"))
                    if (dbData.requestDataset.Tables["cactaccountdocs"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactaccountdocs"].Rows)
                        {
                            dr["id"] = accountId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;
                if (dbData.requestDataset.Tables.Contains("cactmstraderef"))
                    if (dbData.requestDataset.Tables["cactmstraderef"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmstraderef"].Rows)
                        {
                            personToContact = dr["person_to_contact"].ToString().Trim();
                            tradeRefCompanyName = dr["company_name"].ToString().Trim();

                            if (string.IsNullOrEmpty(personToContact))
                                throw new Exception("Person To Contact can not be empty in Trade References detail");

                            if (string.IsNullOrEmpty(tradeRefCompanyName))
                                throw new Exception("Company Name can not be empty in Trade References detail");


                            dr["id"] = accountId;
                            dr["company_id"] = CompanyDbData.companyId.Trim();
                            dr["user_cd"] = CompanyDbData.userCd.Trim();
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

            }
            catch (Exception e)
            {
                throw e;
            }
            return 0;
        }
    }
}
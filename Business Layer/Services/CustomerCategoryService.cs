﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Services;
using GenService.Db;
using Cact.Bl.Validation;

namespace Cact.Bl.Services
{
    public class CustomerCategoryService : DataSavingService
    {
        public CustomerCategoryService() : base()
        {
            validation = new CustomerCategoryValidation(dbData);
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("faarmscustomercategory", "dwc_master", true, false, "category = {category}");
        }

        public override void PreSaveChild()
        {
            //base.PreSaveChild();
        }

        public override void PostSaveChild()
        {
            //base.PostSaveChild();
        }
    }
}

﻿using GenService.Db;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Cact.BL.Posting
{
    public class ProspectCustomerSync
    {
        string prospectId = "";

        public void SyncToProspect(string customerId, DbConnection dbConnection, SqlTransaction sqlTransaction)
        {
            bool isUnique;

            try
            {
                isUnique = DML.Select("cactmsaccount", " where id = '" + customerId + "'", dbConnection, sqlTransaction);

                /*If prospect already exist then update data else create new prospect.*/
                if (isUnique)
                    DML.Insert(CreateProspect(customerId), dbConnection, sqlTransaction);
                else
                    DML.Update(UpdateProspect(customerId), dbConnection, sqlTransaction);
            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public void CreateCustomer(string accountId, DbConnection dbConnection, SqlTransaction sqlTransaction)
        {
            prospectId = accountId;

            try
            {
                ValidateProspect(prospectId, dbConnection, sqlTransaction);

                DML.Insert(InsertCustomer(), dbConnection, sqlTransaction);
                DML.Update(MakeProspectViewOnly(prospectId), dbConnection, sqlTransaction);

            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void ValidateProspect(string prospectId, DbConnection dbConnection, SqlTransaction sqlTransaction)
        {
            bool isUnique;

            try
            {
                isUnique = DML.Select("cactmsaccount", " where id = '" + prospectId + "'", dbConnection, sqlTransaction);

                if (isUnique)
                    throw new Exception("Prospect does not exist.");

                isUnique = DML.Select("faarmscustomer", " where id = '" + prospectId + "'", dbConnection, sqlTransaction);

                if (!isUnique)
                    throw new Exception("Customer already exist.");
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        public string MakeProspectViewOnly(string prospectId)
        {
            string sql;
            sql = " UPDATE cactmsaccount " +
                    " SET update_flag = 'V', " +
                    " parent_account_id = id, " +
                    "customer_flag = 'Y', " +
                    "update_dt = getdate() " +
                    " where id = '" + prospectId + "'";

            return sql;
        }

        public string UpdateProspect(string customerId)
        {
            string sql;

            try
            {
                sql = "UPDATE B" +
                        " Set id = A.ID, category_id = A.category,   company_name = A.name, salesperson_cd = A.sales_person, " +
                        "contact1 = A.contact1, contact1_phone = A.contact1_phone, email = A.email, " +
                        "contact2 = A.contact2, contact2_phone = A.contact2_phone, contact2_email = A.contact2_email," +
                        "contact3 = A.contact3, contact3_phone = A.contact3_phone, contact3_email = A.contact3_email, " +
                        "phone = a.phone1, phone2 = a.phone2, business_email = a.business_email, fax = a.fax1," +
                        "cell_no = a.cell_no, address1 = A.address1, address2 = A.address2, city = A.city, state = A.state," +
                        "zip = A.zip, country = A.country, website = A.web, start_dt = A.valid_dt, how_we_met = A.how_we_met," +
                        "terms = A.terms, memo_terms = A.memo_terms, repair_terms = A.repair_terms," +
                        "sales_tax_code = A.sales_tax_code, discount_per = A.discount_per, credit_limit = A.credit_limit, " +
                        "memo_limit = A.memo_limit, price_level = A.price_level, price_level_diam_cert = A.price_level_diam_cert," +
                        "price_level_diam_lot = A.price_level_diam_lot, credit_rating = A.credit_rating, sales_person = A.sales_person, " +
                        "salescomm_per = A.salescomm_per, type1 = A.type1, type2 = A.type2, territory = A.territory," +
                        "territory2 = A.territory2, territory3 = A.territory3, ein_no = A.ein_no,  jbt_ranking = A.jbt_ranking," +
                        "tax_id = A.tax_id, passport_no = A.passport_no," +
                        "guarantee_name = A.guarantee_name,  business_name = A.business_name, service_rep = A.service_rep," +
                        "ship_via = A.ship_via, paymentpriority = A.paymentpriority, return_check = A.return_check, " +
                        "shipper_number = A.shipper_number, location = A.location, coop_per = A.coop_per," +
                        "postdated_checks = A.postdated_checks, po_unique_since = A.po_unique_since,  style_suffix = A.style_suffix," +
                        "shelf_invn_cap = A.shelf_invn_cap, message_id = A.message_id, inv_print_no = A.inv_print_no," +
                        "memo_print_no = A.memo_print_no,  back_order = A.back_order, gold_lock_on = A.gold_lock_on," +
                        "gold_premium = A.gold_premium, silver_premium = A.silver_premium, platinum_premium = A.platinum_premium," +
                        "ten99_yn = A.ten99_yn, print_yn = A.print_yn, fax_yn = A.fax_yn, email_yn = A.email_yn, " +
                        "so_partial_ship_flag = A.so_partial_ship_flag, so_item_partial_ship_flag = A.so_item_partial_ship_flag," +
                        "dunning_yn = A.dunning_yn, web_account_yn = A.web_account_yn, web_id = A.web_id," +
                        "subscribed_to_emails = A.subscribed_to_emails, subscription_email = A.subscription_email," +
                        " first_name  =  SUBSTRING(a.contact1, 0, CHARINDEX(' ',a.contact1)), last_name = SUBSTRING(a.contact1,CHARINDEX(' ',a.contact1),LEN(a.contact1)), " +
                        " bank_institution_name = A.bank_institution_name, bank_city = A.bank_city,bank_state = A.bank_state," +
                        " bank_officer = A.bank_officer, resale_certificate_no = A.resale_certificate_no, ca_submit_date = A.ca_submit_date, " +
                        " ca_applicant_name = A.ca_applicant_name, jbt_credit_amount = A.jbt_credit_amount, credit_type = A.credit_type , " +
                        " no_of_years = A.no_of_years, request_catalog = A.request_catalog, bank_account_number = A.bank_account_number," +
                        " bank_phone = A.bank_phone,bank_fax = A.bank_Fax, usa_patriot_act = A.patriot_act, tov_status = 'A', tov_customer_id = A.tov_customer_id " +
                        " From faarmscustomer a" +
                        " INNER JOIN cactmsaccount b ON" +
                        " a.id = b.id" +
                        " WHERE a.id = '" + customerId + "';";
            }

            catch (Exception e)
            {
                throw e;

            }

            return sql;
        }

        public string CreateProspect(string customerId)
        {
            string sql;

            try
            {   
                sql = "Insert into cactmsaccount" +
                        "(" +
                         "id, parent_account_id, category_id, company_name, business_email,address1, address2," +
                        "city, state, zip, country, first_name, last_name," +
                        " phone, fax, email, salesperson_cd, ship_via," +
                        "term, credit_limit, website, start_dt, description, allow_email," +
                        "allow_fax, allow_mail, allow_phone_Call, send_marketing_material, allow_send_catalog, salutation," +
                        "primary_contact_id, relationship_type, address_type, business_territory, industry, ownership_type," +
                        "preferred_contact_method, preferred_contact_time, preferred_contact_day, annual_revenue, no_of_employees, credit_hold_flag," +
                        "email_addresses, group1, group2, group3, group4, group5," +
                        "trans_flag, company_id, update_dt, update_flag, user_cd," +
                        "customer_flag, dunning_yn, email_yn, fax_yn, print_yn," +
                        "so_item_partial_ship_flag, so_partial_ship_flag, ten99_yn," +
                        "contact1,contact1_phone,contact2,contact2_phone,contact2_email," +
                        "contact3,contact3_phone,contact3_email,how_we_met,cell_no,terms," +
                        "memo_terms,discount_per,memo_limit,price_level,credit_rating," +
                        "sales_person, salescomm_per,type1,type2, territory,territory2," +
                        "territory3,ein_no,jbt_ranking,tax_id,guarantee_name,business_name," +
                        " service_rep,return_check,coop_per,style_suffix,shelf_invn_cap,back_order," +
                        " gold_lock_on,web_account_yn, current_stage, created_fr, created_by, prospect_owner," +
                        " bank_institution_name, bank_city, bank_state, bank_phone, bank_fax, bank_officer, " +
                        " resale_certificate_no, ca_submit_date, ca_applicant_name, jbt_credit_amount, no_of_years, " +
                        " request_catalog, bank_account_number, credit_type, usa_patriot_act, tov_status, tov_customer_id " +
                     ")" +

                    "Select id, id, category, name, business_email, address1, address2, " +
                    " city, state, zip, country, LEFT(SUBSTRING(contact1, 0, CHARINDEX(' ',contact1)),50), LEFT(SUBSTRING(contact1,CHARINDEX(' ',contact1),LEN(contact1)),50)," +
                    " phone1, fax1, email, sales_person, ship_via," +
                    "terms, credit_limit, web, valid_dt as start_dt, notes as description, 'N' allow_email, " +
                    "'N' allow_fax, 'N' allow_mail, 'N' allow_phone_Call, 'N' send_marketing_material, 'N' allow_send_catalog, 'Mr' salutation, " +
                    "null primary_contact_id, 'CUST' relationship_type, 'S' address_type, 'NA' business_territory, 'ACCT' industry, 'PUBL' ownership_type, " +
                    "'MAIL'preferred_contact_method, 'E' preferred_contact_time, 'MON' preferred_contact_day, 0 annual_revenue, 0 no_of_employees, 'N' credit_hold_flag, " +
                    "'' email_addresses, 'NA' group1, 'NA' group2, 'NA' group3, 'NA' group4, 'NA' group5, " +
                    "trans_flag, company_id, update_dt, 'V', user_cd," +
                    "'Y',dunning_yn,email_yn,fax_yn,print_yn," +
                    "so_item_partial_ship_flag,so_partial_ship_flag,ten99_yn," +
                    "contact1,  contact1_phone, contact2,   contact2_phone, contact2_email," +
                    "contact3,contact3_phone,contact3_email,how_we_met,cell_no,terms," +
                    "memo_terms, discount_per, memo_limit, price_level, credit_rating," +
                    "sales_person, salescomm_per, type1, type2, territory, territory2," +
                    "territory3, ein_no, jbt_ranking, tax_id, guarantee_name, business_name, " +
                    " service_rep, return_check, coop_per, style_suffix, shelf_invn_cap, back_order," +
                    " gold_lock_on, web_account_yn, 'NEW','DIRT','"+ CompanyDbData.userCd + "','"+CompanyDbData.userCd+"' ," +
                    " bank_institution_name, bank_city, bank_state, bank_phone, bank_fax, bank_officer, " +
                    " resale_certificate_no, ca_submit_date, ca_applicant_name, jbt_credit_amount, no_of_years, " +
                    " request_catalog, bank_account_number, credit_type, patriot_act, 'A', tov_customer_id " +
                    " FROM faarmscustomer a" +
                    " WHERE id NOT IN(Select id from cactmsaccount)" +
                    " and id = '" + customerId + "';";


            }
            catch (Exception e)
            {
                throw e;

            }

            return sql;

        }

        public string InsertCustomer()
        {
            string sql;
            try
            {   
                sql = "INSERT INTO faarmscustomer  " +
                            "(" +
                            "id, category, name, business_name, business_email, status,	tax_id, " +
                            "discount_per,	discount_days,	terms, gl_cd1,	gl_cd2, " +
                            "gl_cd3,	gl_cd4,	user_cd, update_dt, update_flag, " +
                            "contact1,contact2,	address1 ,  address2 ,	city , " +
                            "state ,  zip ,  country ,  phone1 ,  phone2 ,  " +
                            "fax1 ,  email ,  company_id ,  trans_flag , inv_type,  " +
                            "notes ,	contact3,	contact4,  territory,	credit_limit, " +
                            "price_level ,sales_person ,	message_id,	inv_print_no,	ship_via , " +
                            "web,	email_yn ,  fax_yn ,  print_yn , message_yn , " +
                            "message1,	type1 ,  type2 , 	paymentpriority ,	ten99_yn , " +
                            "ein_no,	valid_dt ,  udf1 ,  udf2 ,  udf3 ,  " +
                            "udf4 ,	udf5 ,  udf6 ,  dunning_yn ,  salescomm_per," +
                            "billto_id ,	base_goldprice,	goldfactor1,	goldfactor2,	memo_terms," +
                            "stop_ship , message2,	message3,	message4,	message5, " +
                            "message6,	message7,	old_category,	old_id,	jbt_ranking, " +
                            "territory2,	territory3,	credit_approval_flag ,	blacklisted_flag, " +
                            "bank_account_no,	wfca_flag,	passport_no,	reference1_name,	reference2_name, " +
                            "reference3_name,	guarantee_name,	reference1_phone,	reference2_phone,	reference3_phone, " +
                            "supporting1,	supporting2,	shipper_number,	contact1_phone ,	contact2_phone , " +
                            "contact3_phone ,  contact4_phone ,	inactive,	collection,	return_check, " +
                            "cell_no,	location ,	coop_per,	postdated_checks,	style_suffix,	 " +
                            "bank_name,	bank_address1,	bank_address2, " +
                            "bank_contact_person,	so_partial_ship_flag,	so_item_partial_ship_flag,	customer_group,	impairment_percent, " +
                            "memo_print_no,	due_days,b2b_flag, cc_fidelity,  " +
                            "price_level_diam_lot, price_level_diam_cert, sales_tax_code, po_unique_since, " +
                            "memo_limit, back_order, gold_lock_on, service_rep, gold_premium, silver_premium, " +
                            "platinum_premium, credit_rating, repair_terms, shelf_invn_cap, cert_expiry_date, " +
                            "cy_paypattern, py_paypattern, py2_paypattern, time_zone, contact2_email, contact3_email, contact4_email, web_account_yn, how_we_met," +
                            " bank_institution_name, bank_city, bank_state, bank_phone, bank_fax, bank_officer, " +
                            " resale_certificate_no, ca_submit_date, ca_applicant_name, jbt_credit_amount, no_of_years, " +
                            " request_catalog, bank_account_number, credit_type, patriot_act, tov_status, tov_customer_id " +

                            ")" +
                            " SELECT  id, category_id,	company_name, business_name ,business_email ,'A', tax_id, " +
                            "discount_per,  NULL,terms,	'NA',	'NA', " +
                            "'NA',	'NA',	USER_cD, getdate(), 'Y', " +
                            "contact1,	contact2, address1,	address2,	city, " +
                            "state,	zip,	country,	phone,	contact2_phone, " +
                            "fax,	email,	company_id,	trans_flag,	NULL, " +
                            "description,	contact3,	contact4,	territory,	credit_limit,  " +
                            "price_level,	salesperson_cd,	message_id,	inv_print_no,	ship_via, " +
                            "website,	email_yn, fax_yn,	print_yn,	'N', " +
                            "NULL,	type1,	type2,	paymentpriority, ten99_yn, " +
                            "ein_no,	GETDATE(), NULL,	NULL,	NULL, " +
                            "NULL ,	NULL ,  NULL ,  isnull(dunning_yn,'N') ,  salescomm_per, " +
                            "id,	NULL,	NULL,	NULL,	memo_terms, " +
                            "'N',	NULL,	NULL,	NULL ,	NULL, " +
                            "NULL,	NULL,	NULL,	NULL, jbt_ranking, " +
                            "territory2,	territory3,	'Y',	'N', " +
                            " bank_account_no,	'Y',	passport_no,	NULL,	NULL, " +
                            " NULL,	guarantee_name,	NULL,	NULL,	NULL, " +
                            " NULL,	NULL,	shipper_number,	contact1_phone ,	contact2_phone , " +
                            " contact3_phone ,  contact4_phone ,	'N',	'N',	return_check, " +
                            " cell_no,	location ,	coop_per,	NULL,	style_suffix,	 " +
                            " NULL,	NULL,	NULL, " +
                            " NULL,	so_partial_ship_flag,	so_item_partial_ship_flag,	NULL,	NULL, " +
                            " memo_print_no,NULL,'N', 'N', " +
                            " price_level_diam_lot, price_level_diam_cert, sales_tax_code, po_unique_since, " +
                            " memo_limit, back_order, gold_lock_on, service_rep, gold_premium, silver_premium, " +
                            " platinum_premium, credit_rating, repair_terms, shelf_invn_cap, cert_expiry_date, " +
                            " cy_paypattern, py_paypattern, py2_paypattern, time_zone, contact2_email, contact3_email, contact4_email, web_account_yn, how_we_met," +
                            " bank_institution_name, bank_city, bank_state, bank_phone, bank_fax, bank_officer, " +
                            " resale_certificate_no, ca_submit_date, ca_applicant_name, jbt_credit_amount, no_of_years, " +
                            " request_catalog, bank_account_number, credit_type, usa_patriot_act, 'A', tov_customer_id " +
                            " FROM cactmsaccount " +
                            "where id = '" + prospectId + "'";
            }
            catch (Exception e)
            {
                throw e;

            }
            return sql;
        }
    }
}

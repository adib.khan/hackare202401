﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using cact.Bl.Services;
using Gen.Controllers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Text.Json;

namespace cact.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerMasterController : DataSavingController
    {
        public CustomerMasterController()
        {
            service = new CustomerMasterService();
        }

        [EnableCors]
        [HttpPost]
        public IActionResult Save(JsonElement requestJson)
        {
            //throw new Exception("Customer can be saved from TOV only.");

            return Saving(requestJson);
        }
    }
}

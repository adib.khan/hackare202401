﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Services;
using GenService.Db;
using Cact.Bl.Validation;

namespace Cact.Bl.Services
{
    public class SetupMasterService : DataSavingService
    {
        public SetupMasterService() : base()
        {
            validation = new SetupMasterValidation(dbData);
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("cactmssetuphd", "dwc_master", true, false, "setup_id = {setup_id}");
            dbData.updateProperties[1] = new UpdateProperty("cactmssetupdtl", "dwc_detail", true, true, "setup_id = {setup_id}");
        }

        public override void PreSaveChild()
        {
            //base.PreSaveChild();
        }

        public override void PostSaveChild()
        {
            //base.PostSaveChild();
        }
    }
}

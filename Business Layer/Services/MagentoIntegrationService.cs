﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Services;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;

namespace Cact.Bl.Services
{
    public class MagentoIntegrationService : DataSavingService
    {
        
        public string GetCustomer(string id)
        {
            string url = "https://m2stage.royalchain.com/rest/V1/customers/18";

            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            IRestResponse response = client.Execute(request);

            return response.Content.ToString();
        }

        public string GetAdminToken()
        {
            var client = new RestClient("https://m2stage.royalchain.com/rest/V1/integration/admin/token");
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer b4efbe4671926b79908b64c235afca06fa030311");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Cookie", "PHPSESSID=q6sulj3gkal54psf0abm174dn1");

            var body = "{\"username\":\"diaspark\",\"password\":\"XesEFURHPeZqRq9Y4JxC\"}";

            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);

            return response.Content;
        }

        public string CreateCustomer()
        {
            //string token = GetAdminToken();

            string url = "https://m2stage.royalchain.com/rest/default/V1/customers";
            string email = "aatestsunagddrddaa99284@gmail.com";
            string fName = "Rahul";
            string lName = "Sharma";
            string password = "Customer123";

            var client = new RestClient(url);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", "Bearer 435ialixklvs6ye4g4i10c0ok8zyjzvq");
            request.AddHeader("Content-Type", "application/json");

            var body = "{\"customer\":{\"email\":\""+email+"\",\"firstname\":\""+fName+"\",\"lastname\":\""+lName+"\"},\"password\":\""+password+"\"}";

            request.AddParameter("application/json", body, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            dynamic data = JObject.Parse(response.Content);
            string id = data.id;

            return id;
        }
    }
}

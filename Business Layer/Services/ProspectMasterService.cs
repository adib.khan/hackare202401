﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Validation;
using Cact.BL.Posting;
using GenService.BL.Services;
using GenService.Db;
using System;
using System.Data.SqlClient;
using System.Text.Json;

namespace Cact.Bl.Services
{
    public class ProspectMasterService : DataSavingService
    {
        public ProspectMasterService() : base()
        {
            validation = new ProspectMasterValidation(dbData);
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("cactmsaccount", "dwc_master", true, false, "id = {id}");
            dbData.updateProperties[1] = new UpdateProperty("cactmscontact", "dwc_detailcontacta", true, true, "id = {id}");
            dbData.updateProperties[2] = new UpdateProperty("cactmsprospectship", "dwc_detailshipa", true, true, "id = {id}");
            dbData.updateProperties[3] = new UpdateProperty("cactmsaccountnotes", "dwc_detail_notes", true, true, "account_id = {id}");
            dbData.updateProperties[4] = new UpdateProperty("cactmsaccountsummary", "dwc_summary", true, false, "account_id = {id}");
            dbData.updateProperties[5] = new UpdateProperty("cactaccountdocs", "dwc_detaildoc", true, false, "id = {id}");
            dbData.updateProperties[6] = new UpdateProperty("cactmstraderef", "dwc_detail_traderef", true, true, "id = {id}");
        }

        public void CreateCustomer(JsonElement requestJson)
        {
            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();

            try
            {
                dbConnection.conn.Open();
                sqlTransaction = dbConnection.conn.BeginTransaction();
                new ProspectCustomerSync().CreateCustomer(requestJson.GetProperty("id").ToString(), dbConnection, sqlTransaction);
            }
            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                throw e;
            }            
            finally
            {
                dbConnection.conn.Close();
            }

            sqlTransaction.Commit();
        }

        public void AddContact(JsonElement requestJson)
        {
            try
            {
                string prospectId, serialNo, firstName, lastName, address1, address2, city, state;
                string zip, country, phone1, phone2, emailId, userCd, companyId, updateDt, updateFlag;
                string transFlag, title, contactType, contactPhoto, lsSQL;
                bool IsUnique;

                prospectId = requestJson.GetProperty("id").ToString();
                serialNo = requestJson.GetProperty("serial_no").ToString();
                firstName = requestJson.GetProperty("first_name").ToString();
                lastName = requestJson.GetProperty("last_name").ToString();
                address1 = requestJson.GetProperty("address1").ToString();
                address2 = requestJson.GetProperty("address2").ToString();
                city = requestJson.GetProperty("city").ToString();
                state = requestJson.GetProperty("state").ToString();
                zip = requestJson.GetProperty("zip").ToString();
                country = requestJson.GetProperty("country").ToString();
                phone1 = requestJson.GetProperty("phone1").ToString();
                phone2 = requestJson.GetProperty("phone2").ToString();
                emailId = requestJson.GetProperty("email_id").ToString();
                userCd = requestJson.GetProperty("user_cd").ToString();
                companyId = requestJson.GetProperty("company_id").ToString();
                updateDt = requestJson.GetProperty("update_dt").ToString();
                updateFlag = requestJson.GetProperty("update_flag").ToString();
                transFlag = requestJson.GetProperty("trans_flag").ToString();
                title = requestJson.GetProperty("title").ToString();
                contactType = requestJson.GetProperty("contact_type").ToString().Trim();
                contactPhoto = requestJson.GetProperty("contact_photo").ToString();

                DbConnection dbConnection;
                dbConnection = new DbConnection();
                dbConnection.conn.Open();

                if (contactType != "NA")
                {
                    IsUnique = DML.Select("cactmscontact", " where id = '" + prospectId + "' AND contact_type = '" + contactType + "'", dbConnection);

                    if (IsUnique == false)
                        throw new Exception("This contact type is already in use.");
                }

                lsSQL = "INSERT INTO cactmscontact" +
                    "(" +
                        "id, serial_no, first_name, last_name, address1," +
                        "address2, city, state, zip, country," +
                        "phone1, phone2, email_id, user_cd," +
                        "company_id, update_dt, update_flag, trans_flag, title," +
                        "contact_type, contact_photo" +
                    ")" +
                    "VALUES" +
                    "(" +
                        "'" + prospectId + "','" + serialNo + "','" + firstName + "','" + lastName + "','" + address1 + "','" +
                        address2 + "','" + city + "','" + state + "','" + zip + "','" + country + "','" +
                        phone1 + "','" + phone2 + "','" + emailId + "','" + userCd + "','" +
                        companyId + "','" + updateDt + "','" + updateFlag + "','" + transFlag + "','" + title + "','" +
                        contactType + "','" + contactPhoto +
                    "'); ";

                DML.Insert(lsSQL, dbConnection);

            }

            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void UpdateContact(JsonElement requestJson)
        {
            try
            {
                string prospectId, serialNo, firstName, lastName, address1, address2, city, state, transFlag, title;
                string zip, country, phone1, phone2, emailId, contactType, contactPhoto, lsSQL;
                DateTime updateDt;
                bool IsUnique;

                prospectId = requestJson.GetProperty("id").ToString().Trim();
                serialNo = requestJson.GetProperty("serial_no").ToString().Trim();
                firstName = requestJson.GetProperty("first_name").ToString().Trim();
                lastName = requestJson.GetProperty("last_name").ToString().Trim();
                address1 = requestJson.GetProperty("address1").ToString().Trim();
                address2 = requestJson.GetProperty("address2").ToString().Trim();
                city = requestJson.GetProperty("city").ToString().Trim();
                state = requestJson.GetProperty("state").ToString().Trim();
                zip = requestJson.GetProperty("zip").ToString().Trim();
                country = requestJson.GetProperty("country").ToString().Trim();
                phone1 = requestJson.GetProperty("phone1").ToString().Trim();
                phone2 = requestJson.GetProperty("phone2").ToString().Trim();
                emailId = requestJson.GetProperty("email_id").ToString().Trim();
                updateDt = Convert.ToDateTime(requestJson.GetProperty("update_dt").ToString().Trim());
                transFlag = requestJson.GetProperty("trans_flag").ToString().Trim();
                title = requestJson.GetProperty("title").ToString().Trim();
                contactType = requestJson.GetProperty("contact_type").ToString().Trim();
                contactPhoto = requestJson.GetProperty("contact_photo").ToString().Trim();

                DbConnection dbConnection;
                dbConnection = new DbConnection();
                dbConnection.conn.Open();

                IsUnique = DML.Select("cactmscontact", " where id = '" + prospectId + "' AND serial_no = ' " + serialNo + "'", dbConnection);

                if (IsUnique == false)
                    throw new Exception("This contact does not exist.");

                lsSQL = "UPDATE cactmscontact" +
                        " SET first_name = '" + firstName + "'," +
                        " last_name = '" + lastName + "'," +
                        " address1 = '" + address1 + "'," +
                        " address2 = '" + address2 + "'," +
                        " city = '" + city + "'," +
                        " state = '" + state + "'," +
                        " zip = '" + zip + "'," +
                        " country = '" + country + "'," +
                        " phone1 = '" + phone1 + "'," +
                        " phone2 = '" + phone2 + "'," +
                        " email_id = '" + emailId + "'," +
                        " update_dt = '" + updateDt + "'," +
                        " trans_flag = '" + transFlag + "'," +
                        " title = '" + title + "'," +
                        " contact_type = '" + contactType + "'," +
                        " contact_photo = '" + contactPhoto + "'" +
                        " WHERE   id = '" + prospectId + "' AND" +
                        " serial_no = '" + serialNo + "';";

                DML.Update(lsSQL, dbConnection);

            }

            catch (Exception e)
            {
                throw e;
            }
        }

        public override void PreSaveChild()
        {
            //base.PreSaveChild();
        }

        public override void PostSaveChild()
        {
            //ProspectTransferToTOV prospectTransferToTOV = new ProspectTransferToTOV();
            //prospectTransferToTOV.transferToTOV(dbConnection, dbData, trans);

        }
    }
}
﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/22
Author : Sahil Gupta
Purpose : This Class is responsible for handling services related to SalesPerson Master
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/



using Cact.BL.Validation;
using GenService.BL.Services;
using GenService.Db;

namespace Cact.BL.Services
{
    public class SalesPersonService : DataSavingService
    {
        public SalesPersonService() : base()
        {
            validation = new SalesPersonValidation(dbData);
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("saoimssalesperson", "dwc_master", true, false, "id = {id}");
            dbData.updateProperties[1] = new UpdateProperty("saoimssalespersondtl", "dwc_detail_cust", true, true, "id = {id}");
            dbData.updateProperties[2] = new UpdateProperty("sapemsinventory", "dwc_detail_inventory", true, true, "salesperson_id = {id}");
        }


    }



}

﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Validation;
using GenService.BL.Services;
using GenService.Db;
using System;
using System.Data.SqlClient;
using System.Text.Json;

namespace Cact.Bl.Services
{
    public class TaskService : DataSavingService
    {
        public TaskService() : base()
        {
            validation = new TaskValidation(dbData);
            dbData.bookType = "CMTAS";

        }

        public override void SetUpdateProperty()
        {

            dbData.updateProperties[0] = new UpdateProperty("cacttrtask", "dwc_master", true, false, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
            dbData.updateProperties[1] = new UpdateProperty("cacttrtaskcontacts", "dwc_detail", true, true, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
            dbData.updateProperties[2] = new UpdateProperty("cacttrtaskactions", "dwc_detail_actions", true, true, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");

        }
        public void CloseTask(JsonElement requestJson)
        {

            SqlTransaction sqlTransaction = null;
            DbConnection dbConnection = new DbConnection();

            try
            {
                string closeTimeZone, closeTimeFormat;
                string remarks, lsSql, status, companyId, transBk, transNo;
                int closeHour, closeMinute;
                DateTime completionDate, transDt;

                remarks = requestJson.GetProperty("remarks").ToString().Trim();
                status = requestJson.GetProperty("status").ToString().Trim();
                transBk = requestJson.GetProperty("trans_bk").ToString().Trim();
                transNo = requestJson.GetProperty("trans_no").ToString().Trim();
                companyId = requestJson.GetProperty("company_id").ToString().Trim();

                closeTimeZone = requestJson.GetProperty("close_time_zone").ToString().Trim();
                closeTimeFormat = requestJson.GetProperty("close_time_format").ToString().Trim();
                closeHour = int.Parse(requestJson.GetProperty("close_hour").ToString().Trim());
                closeMinute = int.Parse(requestJson.GetProperty("close_minute").ToString().Trim());
                completionDate = Convert.ToDateTime(requestJson.GetProperty("completion_date").ToString());
                transDt = Convert.ToDateTime(requestJson.GetProperty("trans_dt").ToString());

                dbConnection.conn.Open();
                sqlTransaction = dbConnection.conn.BeginTransaction();

                lsSql = "UPDATE cacttrtask" +
                        " Set status_flag = 'C'," +
                        "close_date = '" + completionDate + "'," +
                        "close_time_zone = '" + closeTimeZone + "'," +
                        "close_time_format = '" + closeTimeFormat + "'," +
                        "close_hour = " + closeHour + "," +
                        "close_minute = " + closeMinute + "," +
                        "closing_remarks = '" + remarks + "'," +
                        "update_dt = '" + DateTime.Now + "'," +
                        "update_flag = 'V'" +
                        "WHERE company_id =  '" + companyId + "'" +
                        "AND trans_bk = '" + transBk + "'" +
                        "AND trans_no ='" + transNo + "'" +
                        "AND trans_dt ='" + transDt + "'";

                DML.Update(lsSql, dbConnection, sqlTransaction);

                //If Status is completed then make activity
                if (status.Equals("CO"))
                {
                    CreateActivity(requestJson, dbConnection, sqlTransaction);

                }
                else if (status.Equals("CA"))  //Cancelled
                {
                    lsSql = "UPDATE cacttrtask" +
                         " Set trans_flag = 'D'," +
                         "update_dt = '" + DateTime.Now + "'" +
                         "WHERE company_id =  '" + companyId + "'" +
                         "AND trans_bk = '" + transBk + "'" +
                         "AND trans_no ='" + transNo + "'" +
                         "AND trans_dt ='" + transDt + "'";

                    DML.Update(lsSql, dbConnection, sqlTransaction);
                }

                sqlTransaction.Commit();

            }

            catch (Exception e)
            {
                if (sqlTransaction != null)
                    sqlTransaction.Rollback();

                throw e;
            }
            finally
            {
                dbConnection.conn.Close();
            }
        }

        public void CreateActivity(JsonElement requestJson, DbConnection dbConnection, SqlTransaction sqlTransaction)
        {
            try
            {
                string prospectId, contactId, taskType, salespPersonCd, closeTimeZone, closeTimeFormat;
                string remarks, lsSql, companyId, transBk, transNo, newActivityTransNo, taskSubject, gUserCd;
                int closeHour, closeMinute;
                DateTime transDt;

                newActivityTransNo = requestJson.GetProperty("activity_trans_no").ToString().Trim();
                prospectId = requestJson.GetProperty("prospect_id").ToString().Trim();
                contactId = requestJson.GetProperty("contact_id").ToString().Trim();
                taskType = requestJson.GetProperty("task_type").ToString().Trim();
                salespPersonCd = requestJson.GetProperty("salesperson_cd").ToString().Trim();
                remarks = requestJson.GetProperty("remarks").ToString().Trim();
                taskSubject = requestJson.GetProperty("task_subject").ToString().Trim();
                gUserCd = requestJson.GetProperty("user_cd").ToString().Trim();

                closeTimeZone = requestJson.GetProperty("close_time_zone").ToString().Trim();
                closeTimeFormat = requestJson.GetProperty("close_time_format").ToString().Trim();
                closeHour = int.Parse(requestJson.GetProperty("close_hour").ToString().Trim());
                closeMinute = int.Parse(requestJson.GetProperty("close_minute").ToString().Trim());

                transBk = requestJson.GetProperty("trans_bk").ToString().Trim();
                transNo = requestJson.GetProperty("trans_no").ToString().Trim();
                transDt = Convert.ToDateTime(requestJson.GetProperty("trans_dt").ToString());
                companyId = requestJson.GetProperty("company_id").ToString().Trim();


                lsSql = "INSERT INTO cacttractivity" +
                   "(" +
                       "company_id, trans_bk, trans_no, trans_dt, prospect_id, " +
                       "contact_id, activity_type, remarks, activity_dt, user_cd," +
                       "update_dt, update_flag, trans_flag, post_flag, subject," +
                       "salesperson_cd, task_id, hour, minute, time_format, time_zone," +
                       "ref_bk, ref_no, ref_dt" +
                   ")" +
                   "VALUES" +
                   "(" +
                       "'" + companyId + "','CMAT','" + newActivityTransNo + "','" + DateTime.Today + "','" + prospectId + "','" +
                       contactId + "','" + taskType + "','" + remarks + "','" + DateTime.Today + "','" + gUserCd + "','" +
                       DateTime.Now + "','V','A','Y','" + taskSubject + "','"
                       + salespPersonCd + "','" + transNo + "'," + closeHour + "," + closeMinute + ",'" + closeTimeFormat + "','" +
                       closeTimeZone + "','" + transBk + "','" + transNo + "','" + transDt +
                    "'); ";

                DML.Insert(lsSql, dbConnection, sqlTransaction);

                lsSql = "Insert into cacttractivitycontacts" +
                        "(" +
                         "company_id, trans_bk, trans_no, trans_dt, serial_no, " +
                        "prospect_id, contact_id, remarks, user_cd, update_dt," +
                        "update_flag, trans_flag, default_flag, contact_type" +
                        ")" +
                        "Select '" + companyId + "', 'CMAT', '" + newActivityTransNo + "', '" + DateTime.Today + "',serial_no ," +
                        "prospect_id , contact_id, '', '" + gUserCd + "', '" + DateTime.Now + "'," +
                        "'Y', 'A', default_flag, contact_type" +
                        " FROM cacttrtaskcontacts " +
                        " WHERE  company_id = '" + companyId + "'  AND  " +
                        " trans_bk = '" + transBk + "'  AND  " +
                        " trans_no = '" + transNo + "'  AND  " +
                        " trans_dt = '" + transDt + "';";

                DML.Insert(lsSql, dbConnection, sqlTransaction);

                lsSql = "UPDATE gnmsbook" +
                        " SET book_lno = '" + newActivityTransNo + "'," +
                        "  lno_date = '" + DateTime.Today + "'" +
                        " WHERE docu_typ = 'CMACT'" +
                        " And book_cd = 'CMAT' ";

                DML.Update(lsSql, dbConnection, sqlTransaction);

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }

}
﻿///*********************************************************************************** 
//    Copyright (c) Diaspark Inc. All Rights Reserved 
//    Created Date : 2021/07/20
//    Author : Sunil Agrawal
//    Purpose : 
//    Modified Date :
//    Modified By :
//    Modification Purpose : 
//***********************************************************************************/

//using GenService.BL.Validation;
//using GenService.Db;
//using System;
//using System.Data;
//using System.Data.SqlClient;

//namespace Cact.Bl.Validation
//{
//    public class PrepareCustomerListValidation : GenValidation
//    {
//        public PrepareCustomerListValidation(DbData dbData) : base(dbData) { }

//        public override int ValidateChild()
//        {
//            int count = 1;

//            SqlTransaction sqlTransaction = null;
//            DbConnection dbConnection = new DbConnection();

//            try
//            {
//                string transNo = dbData.requestDataset.Tables["cacttrcustomerlist"].Rows[0]["trans_no"].ToString();

//                dbConnection.conn.Open();
//                sqlTransaction = dbConnection.conn.BeginTransaction();

//                string lsSql = "UPDATE gnmsbook" +
//                " SET book_lno = '" + transNo + "'," +
//                "  lno_date = '" + DateTime.Today + "'" +
//                " WHERE docu_typ = 'LC01'" +
//                " And book_cd = 'LC01' ";

//                foreach (DataRow dr in dbData.requestDataset.Tables["cacttrcustomerlist"].Rows)
//                {
//                    string listName = dr["list_name"].ToString();

//                    if (string.IsNullOrEmpty(listName))
//                    {
//                        throw new Exception("List name can not be empty.");
//                    }

//                    if (string.IsNullOrEmpty(dr["customer_id"].ToString()))
//                    {
//                        throw new Exception("Customer # can not be empty");
//                    }

//                    dr["serial_no"] = count;
//                    count += 1;
//                }

//                DML.Update(lsSql, dbConnection, sqlTransaction);
//                sqlTransaction.Commit();

//            }
//            catch (Exception e)
//            {
//                if (sqlTransaction != null)
//                    sqlTransaction.Rollback();

//                throw e;
//            }
//            finally
//            {
//                dbConnection.conn.Close();
//            }

//            return 0;
//        }
//    }
//}
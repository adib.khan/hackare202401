﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;
using System.Data;

namespace cact.Bl.Validation
{
    public class CustomerMasterValidation : GenValidation
    {
        public CustomerMasterValidation(DbData dbData) : base(dbData) { }

        public override int ValidateChild()
        {
            string customerId, customerName, billToId, styleSuffix, sqlQuery, personToContact, tradeRefCompanyName;
            string businessPhone, emailDomain, mainContactEmail = "", companyId;
            int? countRecord;
            DataTable selectResult;

            int count = 1, pos; ;
            bool isUnique, isExist = false, isDomainUnique;

            try
            {
                customerId = dbData.requestDataset.Tables[0].Rows[0]["id"].ToString();
                companyId = dbData.requestDataset.Tables["faarmscustomer"].Rows[0]["company_id"].ToString();
                customerName = dbData.requestDataset.Tables[0].Rows[0]["name"].ToString();
                billToId = dbData.requestDataset.Tables[0].Rows[0]["billto_id"].ToString();

                businessPhone = dbData.requestDataset.Tables[0].Rows[0]["phone1"].ToString();

                if (string.IsNullOrEmpty(customerId))
                    throw new Exception("Customer # is required.");

                //isUnique = DML.Select("cactmsaccount", " where id = '" + customerId + "'", dbConnection);
                //if (!isUnique)
                //    throw new Exception("Same account # already exist with another prospect.");

                if (string.IsNullOrEmpty(customerName))
                    throw new Exception("Company name is required.");

                isUnique = DML.Select("faarmscustomer", " where name = '" + customerName + "' AND id <> '" + customerId + "'", dbConnection);
                //if (!isUnique)    //Sunil 211126: As Rohan instructed
                //    throw new Exception("Company name already used in another customer's company name."); //Sunil 211126: As Rohan instructed

                isUnique = DML.Select("cactmsaccount", " where company_name = '" + customerName + "' AND id <> '" + customerId + "'", dbConnection);
                //if (!isUnique)    //Sunil 211126: As Rohan instructed
                //    throw new Exception("Company name already used in another Prospect's company name."); //Sunil 211126: As Rohan instructed

                if (!string.IsNullOrEmpty(businessPhone))
                {
                    isUnique = DML.Select("faarmscustomer", " where phone1 = '" + businessPhone + "' AND id <> '" + customerId + "'", dbConnection);
                    if (!isUnique)
                        throw new Exception("Business phone # already used in another customer's business phone.");

                    isUnique = DML.Select("cactmsaccount", " where phone = '" + businessPhone + "' AND id <> '" + customerId + "'", dbConnection);
                    if (!isUnique)
                        throw new Exception("Business phone # already used in another Prospect's business phone.");
                }

                //Validate Main Contact.
                if (!dbData.requestDataset.Tables.Contains("cactmscontact"))
                    throw new Exception("Main contact with name and email # is required.");

                foreach (DataRow dr in dbData.requestDataset.Tables["cactmscontact"].Rows)
                {
                    if (string.IsNullOrEmpty(dr["contact_type"].ToString()))
                    {
                        throw new Exception("Department can not be empty");
                    }

                    if ((dr["contact_type"].ToString() == "MC") && (!string.IsNullOrEmpty(dr["email_id"].ToString())) && (!string.IsNullOrEmpty(dr["first_name"].ToString())))
                        isExist = true;

                    if ((dr["contact_type"].ToString() == "MC") && (!string.IsNullOrEmpty(dr["email_id"].ToString())))
                        mainContactEmail = dr["email_id"].ToString();
                }

                if (isExist == false)
                    throw new Exception("Main contact's name and email is required.");

                pos = mainContactEmail.IndexOf("@");

                if (pos > 0)
                {
                    emailDomain = mainContactEmail.Substring(pos + 1);

                    isUnique = DML.Select("cactmscontact", " WHERE RTRIM(SUBSTRING(email_id, CHARINDEX('@',email_id) + 1, LEN(email_id))) = '" + emailDomain + "' AND id <> '" + customerId + "' AND contact_type = 'MC'", dbConnection);

                    //Bypass validation for blacklisted domains.
                    isDomainUnique = DML.Select("cactmssetupdtl", " WHERE setup_id = 'PUBLIC_DOMAIN' AND trans_flag = 'A' AND  UPPER(RTRIM(value)) = '" + emailDomain + "'", dbConnection);

                    if (isDomainUnique && !isUnique)
                    {
                        throw new Exception("Main contact's email domain already used in another account's main contact email.");
                    }
                }
                else
                {
                    throw new Exception("Main contact's email is not valid.");
                }

                if (dbData.requestDataset.Tables.Contains("cactmscontact"))
                    if (dbData.requestDataset.Tables["cactmscontact"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmscontact"].Rows)
                        {
                            dr["Id"] = customerId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;


                styleSuffix = dbData.requestDataset.Tables[0].Rows[0]["style_suffix"].ToString();
                if (!string.IsNullOrEmpty(styleSuffix))
                {

                    sqlQuery = "select count(*) as count from faarmscustomer " +
                             "where style_suffix = '" + styleSuffix + "' and id <> '" + customerId + "'";
                    selectResult = DML.Select(sqlQuery, dbConnection);
                    countRecord = int.Parse(selectResult.Rows[0]["count"].ToString());

                    if (countRecord > 0)
                    {
                        throw new Exception("Duplicate Suffix. Please Enter another Suffix!!'");
                    }
                }

                if (string.IsNullOrEmpty(billToId))
                    dbData.requestDataset.Tables[0].Rows[0]["billto_id"] = customerId;


                if (dbData.requestDataset.Tables.Contains("cactmsprospectship"))
                    if (dbData.requestDataset.Tables["cactmsprospectship"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmsprospectship"].Rows)
                        {
                            dr["Id"] = customerId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

                if (dbData.requestDataset.Tables.Contains("cactmsaccountnotes"))
                    if (dbData.requestDataset.Tables["cactmsaccountnotes"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmsaccountnotes"].Rows)
                        {
                            dr["account_id"] = customerId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

                if (dbData.requestDataset.Tables.Contains("cactmscustomernotes"))
                    if (dbData.requestDataset.Tables["cactmscustomernotes"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmscustomernotes"].Rows)
                        {
                            dr["account_id"] = customerId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

                if (dbData.requestDataset.Tables["faarmscustomerjbtranking"] != null)
                    foreach (DataRow dr in dbData.requestDataset.Tables["faarmscustomerjbtranking"].Rows)
                    {

                        dr["id"] = customerId;
                        dr["serial_no"] = (100 + count).ToString();
                        count++;
                    }
                count = 1;

                if (dbData.requestDataset.Tables.Contains("cactaccountdocs"))
                    if (dbData.requestDataset.Tables["cactaccountdocs"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactaccountdocs"].Rows)
                        {
                            dr["id"] = customerId;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

                if (dbData.requestDataset.Tables.Contains("cactmsaccountsummary"))
                    if (dbData.requestDataset.Tables["cactmsaccountsummary"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmsaccountsummary"].Rows)
                        {
                            dr["account_id"] = customerId;
                        }

                if (dbData.requestDataset.Tables.Contains("cactmstraderef"))
                    if (dbData.requestDataset.Tables["cactmstraderef"].Rows.Count > 0)
                        foreach (DataRow dr in dbData.requestDataset.Tables["cactmstraderef"].Rows)
                        {
                            personToContact = dr["person_to_contact"].ToString().Trim();
                            tradeRefCompanyName = dr["company_name"].ToString().Trim();

                            if (string.IsNullOrEmpty(personToContact))
                                throw new Exception("Person To Contact can not be empty in Trade References detail");

                            if (string.IsNullOrEmpty(tradeRefCompanyName))
                                throw new Exception("Company Name can not be empty in Trade References detail");


                            dr["id"] = customerId;
                            dr["company_id"] = CompanyDbData.companyId.Trim();
                            dr["user_cd"] = CompanyDbData.userCd.Trim();
                            dr["serial_no"] = (100 + count).ToString();
                            count++;
                        }

                count = 1;

            }
            catch (Exception e)
            {
                throw e;
            }
            return 0;


        }

    }
}

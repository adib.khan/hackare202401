﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using Cact.Bl.Validation;
using GenService.BL.Services;
using GenService.Db;

namespace Cact.Bl.Services
{
    public class ActivityService : DataSavingService
    {
        public ActivityService() : base()
        {
            validation = new ActivityValidation(dbData);
            dbData.bookType = "CMACT";
        }

        public override void SetUpdateProperty()
        {
            dbData.updateProperties[0] = new UpdateProperty("cacttractivity", "dwc_master", true, false, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
            dbData.updateProperties[1] = new UpdateProperty("cacttractivitycontacts", "dwc_detail", true, true, "company_id = {company_id} and trans_bk = {trans_bk} and trans_no = {trans_no} and trans_dt = {trans_dt} ");
        }
        
    }
}

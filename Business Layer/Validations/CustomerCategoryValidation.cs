﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;

namespace Cact.Bl.Validation
{
    internal class CustomerCategoryValidation : GenValidation
    {
        public CustomerCategoryValidation(DbData dbData) : base(dbData){}

        public override int ValidateChild()
        {
            string category, companyId;

            try
            {
                category = dbData.requestDataset.Tables["faarmscustomercategory"].Rows[0]["category"].ToString();
                companyId = dbData.requestDataset.Tables["faarmscustomercategory"].Rows[0]["company_id"].ToString();

                if (string.IsNullOrEmpty(category))
                    throw new Exception("category # can not be empty.");

                if (string.IsNullOrEmpty(companyId))
                    dbData.requestDataset.Tables["faarmscustomercategory"].Rows[0]["company_id"] = "DS02";
            }

            catch (Exception e)
            {
                throw e;
            }

            return 0;
        }
    }
}
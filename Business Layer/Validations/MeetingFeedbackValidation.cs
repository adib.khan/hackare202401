﻿/*********************************************************************************** 
Copyright (c) Diaspark Inc. All Rights Reserved 
Created Date : 2021/07/20
Author : Sunil Agrawal
Purpose : 
Modified Date :
Modified By :
Modification Purpose : 
***********************************************************************************/

using GenService.BL.Validation;
using GenService.Db;
using System;
using System.Data;

namespace Cact.Bl.Validation
{
    public class MeetingFeedbackValidation : GenValidation
    {
        public MeetingFeedbackValidation(DbData dbData) : base(dbData) { }

        //  Overide this method for Specific Service Validation
        public override int ValidateChild()
        {
            string prospectId, transBk, transNo, userCd, subject, companyId, dtlContactId, dtlProspectId;

            int max_serial_no;
            DateTime transDate;

            try
            {
                prospectId = dbData.requestDataset.Tables["cacttrmeeting"].Rows[0]["prospect_id"].ToString();
                transNo = dbData.requestDataset.Tables["cacttrmeeting"].Rows[0]["trans_no"].ToString();
                transBk = dbData.requestDataset.Tables["cacttrmeeting"].Rows[0]["trans_bk"].ToString();
                transDate = DateTime.Parse((string)dbData.requestDataset.Tables["cacttrmeeting"].Rows[0]["trans_dt"]);
                companyId = dbData.requestDataset.Tables["cacttrmeeting"].Rows[0]["company_id"].ToString();
                userCd = dbData.requestDataset.Tables["cacttrmeeting"].Rows[0]["user_cd"].ToString();
                subject = dbData.requestDataset.Tables["cacttrmeeting"].Rows[0]["subject"].ToString();
                //dueDate = DateTime.Parse((string)dbData.requestDataset.Tables["cacttrmeeting"].Rows[0]["start_date"]);

                if (string.IsNullOrEmpty(subject))
                    throw new Exception("Meeting subject can not be empty.");

                if (string.IsNullOrEmpty(prospectId))
                    throw new Exception("Company # can not be empty.");

                max_serial_no = 100;
                if (dbData.requestDataset.Tables.Contains("cacttrmeetingcontacts"))
                {
                    if (dbData.requestDataset.Tables["cacttrmeetingcontacts"].Rows.Count > 0)

                    {
                        foreach (DataRow dr in dbData.requestDataset.Tables["cacttrmeetingcontacts"].Rows)
                        {
                            dtlContactId = dr["contact_id"].ToString();
                            dtlProspectId = dr["prospect_id"].ToString();

                            if ((string.IsNullOrEmpty(dtlProspectId)) && (string.IsNullOrEmpty(dtlContactId)))
                                throw new Exception("Detail can not be empty.");

                            string WhereClause = " Where id = '" + dtlProspectId + "' and serial_no = '" + dtlContactId + "'";
                            bool isUnique = DML.Select("cactmscontact", WhereClause, dbConnection);

                            if (isUnique)
                            {
                                throw new Exception("Contact info is incorrect, please choose correct detail.");
                            }

                            max_serial_no++;
                            dr["trans_bk"] = transBk;
                            dr["trans_no"] = transNo;
                            dr["trans_dt"] = transDate;
                            dr["company_id"] = companyId;
                            dr["serial_no"] = max_serial_no;
                        }
                    }
                }
                else
                {
                    throw new Exception("Detail Contact # can not be empty.");
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            return 0;
        }
    }
}